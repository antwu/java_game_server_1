/**
 * Account.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.domain;


public class Account{

	private AccountBean bean;

	public Account()
	{
		
	}

	/** Gets  */
	public int getId()
	{
		return bean.getId();
	}

	/** Gets  */
	public String getAccount()
	{
		return bean.getAccount();
	}

	/** Gets  */
	public int getSex()
	{
		return bean.getSex();
	}

	/** Sets  */
	void setId(int id)
	{
		bean.setId(id);
	}

	/** Sets  */
	void setAccount(String Account)
	{
		bean.setAccount(Account);
	}

	/** Sets  */
	void setSex(int sex)
	{
		bean.setSex(sex);
	}

}