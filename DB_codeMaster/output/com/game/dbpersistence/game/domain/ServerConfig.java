/**
 * ServerConfig.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.domain;


public class ServerConfig{

	private ServerConfigBean bean;

	public ServerConfig()
	{
		
	}

	/** Gets  */
	public int getUID()
	{
		return bean.getUID();
	}

	/** Gets  */
	public int getServerType()
	{
		return bean.getServerType();
	}

	/** Gets  */
	public String getServerName()
	{
		return bean.getServerName();
	}

	/** Gets  */
	public String getIPForServer()
	{
		return bean.getIPForServer();
	}

	/** Gets  */
	public int getPortForServer()
	{
		return bean.getPortForServer();
	}

	/** Gets  */
	public String getIPForClient()
	{
		return bean.getIPForClient();
	}

	/** Gets  */
	public int getPortForClient()
	{
		return bean.getPortForClient();
	}

	/** Gets  */
	public String getIPForGMT()
	{
		return bean.getIPForGMT();
	}

	/** Gets  */
	public int getPortForGMT()
	{
		return bean.getPortForGMT();
	}

	/** Sets  */
	void setUID(int UID)
	{
		bean.setUID(UID);
	}

	/** Sets  */
	void setServerType(int ServerType)
	{
		bean.setServerType(ServerType);
	}

	/** Sets  */
	void setServerName(String ServerName)
	{
		bean.setServerName(ServerName);
	}

	/** Sets  */
	void setIPForServer(String IPForServer)
	{
		bean.setIPForServer(IPForServer);
	}

	/** Sets  */
	void setPortForServer(int PortForServer)
	{
		bean.setPortForServer(PortForServer);
	}

	/** Sets  */
	void setIPForClient(String IPForClient)
	{
		bean.setIPForClient(IPForClient);
	}

	/** Sets  */
	void setPortForClient(int PortForClient)
	{
		bean.setPortForClient(PortForClient);
	}

	/** Sets  */
	void setIPForGMT(String IPForGMT)
	{
		bean.setIPForGMT(IPForGMT);
	}

	/** Sets  */
	void setPortForGMT(int PortForGMT)
	{
		bean.setPortForGMT(PortForGMT);
	}

}