/**
 * ServerConfigBean.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.bean;

import com.game.dbpersistence.BeanFather;
import com.game.core.dbwrite.server.DBService;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for ServerConfigBean
 *
 * @author CodeMaster v1.0
 */
public class ServerConfigBean extends BeanFather implements Cloneable
{
/**
 *  */
	private int UID;

/**
 *  */
	private int ServerType;

/**
 *  */
	private String ServerName;

/**
 *  */
	private String IPForServer;

/**
 *  */
	private int PortForServer;

/**
 *  */
	private String IPForClient;

/**
 *  */
	private int PortForClient;

/**
 *  */
	private String IPForGMT;

/**
 *  */
	private int PortForGMT;

	private long beanNum;

	public ServerConfigBean()
	{
		init();
		beanNum = super.getBeanId();
	}

	/** Gets  */
	public int getUID()
	{
		return this.UID;
	}

	/** Gets  */
	public int getServerType()
	{
		return this.ServerType;
	}

	/** Gets  */
	public String getServerName()
	{
		return this.ServerName;
	}

	/** Gets  */
	public String getIPForServer()
	{
		return this.IPForServer;
	}

	/** Gets  */
	public int getPortForServer()
	{
		return this.PortForServer;
	}

	/** Gets  */
	public String getIPForClient()
	{
		return this.IPForClient;
	}

	/** Gets  */
	public int getPortForClient()
	{
		return this.PortForClient;
	}

	/** Gets  */
	public String getIPForGMT()
	{
		return this.IPForGMT;
	}

	/** Gets  */
	public int getPortForGMT()
	{
		return this.PortForGMT;
	}

	public long getBeanNum()
	{
		return this.beanNum;
	}

	/** Initializes the values */
	public void init()
	{
		this.UID = 0;
		this.ServerType = 0;
		this.ServerName = "";
		this.IPForServer = "";
		this.PortForServer = 0;
		this.IPForClient = "";
		this.PortForClient = 0;
		this.IPForGMT = "";
		this.PortForGMT = 0;
	}

	/** Sets  */
	public void setUID(int UID)
	{
		this.UID = UID;
	}

	/** Sets  */
	public void setServerType(int ServerType)
	{
		this.ServerType = ServerType;
	}

	/** Sets  */
	public void setServerName(String ServerName)
	{
		this.ServerName = ServerName;
	}

	/** Sets  */
	public void setIPForServer(String IPForServer)
	{
		this.IPForServer = IPForServer;
	}

	/** Sets  */
	public void setPortForServer(int PortForServer)
	{
		this.PortForServer = PortForServer;
	}

	/** Sets  */
	public void setIPForClient(String IPForClient)
	{
		this.IPForClient = IPForClient;
	}

	/** Sets  */
	public void setPortForClient(int PortForClient)
	{
		this.PortForClient = PortForClient;
	}

	/** Sets  */
	public void setIPForGMT(String IPForGMT)
	{
		this.IPForGMT = IPForGMT;
	}

	/** Sets  */
	public void setPortForGMT(int PortForGMT)
	{
		this.PortForGMT = PortForGMT;
	}

	/** Sets put init into cacheMemory or db **/ 
	public void update()
	{
		DBService.getInstance().update(beanNum, this.getClass(), this);
	}
	/** Sets insert this bean into cacheMemory or db **/ 
	public void insert()
	{
		DBService.getInstance().insert(beanNum, this.getClass(), this);
	}
	/** depth clone **/ 
	public Object clone()
	{
		try{
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) { 
			e.printStackTrace();
			return null;
		}
	}
	/** Returns the String representation */
	public String toString()
	{
		return "(ServerConfigBean) " 
			+ "UID='" + UID + "', "
			+ "ServerType='" + ServerType + "', "
			+ "ServerName='" + ServerName + "', "
			+ "IPForServer='" + IPForServer + "', "
			+ "PortForServer='" + PortForServer + "', "
			+ "IPForClient='" + IPForClient + "', "
			+ "PortForClient='" + PortForClient + "', "
			+ "IPForGMT='" + IPForGMT + "', "
			+ "PortForGMT='" + PortForGMT + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine()
	{
		return 
			"\""+UID+"\","+ 
			"\""+ServerType+"\","+ 
			"\""+ServerName+"\","+ 
			"\""+IPForServer+"\","+ 
			"\""+PortForServer+"\","+ 
			"\""+IPForClient+"\","+ 
			"\""+PortForClient+"\","+ 
			"\""+IPForGMT+"\","+ 
			"\""+PortForGMT+"\"";
	}

}