/**
 * AccountBean.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.bean;

import com.game.dbpersistence.BeanFather;
import com.game.core.dbwrite.server.DBService;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for AccountBean
 *
 * @author CodeMaster v1.0
 */
public class AccountBean extends BeanFather implements Cloneable
{
/**
 *  */
	private int id;

/**
 *  */
	private String Account;

/**
 *  */
	private int sex;

	private long beanNum;

	public AccountBean()
	{
		init();
		beanNum = super.getBeanId();
	}

	/** Gets  */
	public int getId()
	{
		return this.id;
	}

	/** Gets  */
	public String getAccount()
	{
		return this.Account;
	}

	/** Gets  */
	public int getSex()
	{
		return this.sex;
	}

	public long getBeanNum()
	{
		return this.beanNum;
	}

	/** Initializes the values */
	public void init()
	{
		this.id = 0;
		this.Account = "";
		this.sex = 0;
	}

	/** Sets  */
	public void setId(int id)
	{
		this.id = id;
	}

	/** Sets  */
	public void setAccount(String Account)
	{
		this.Account = Account;
	}

	/** Sets  */
	public void setSex(int sex)
	{
		this.sex = sex;
	}

	/** Sets put init into cacheMemory or db **/ 
	public void update()
	{
		DBService.getInstance().update(beanNum, this.getClass(), this);
	}
	/** Sets insert this bean into cacheMemory or db **/ 
	public void insert()
	{
		DBService.getInstance().insert(beanNum, this.getClass(), this);
	}
	/** depth clone **/ 
	public Object clone()
	{
		try{
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) { 
			e.printStackTrace();
			return null;
		}
	}
	/** Returns the String representation */
	public String toString()
	{
		return "(AccountBean) " 
			+ "id='" + id + "', "
			+ "Account='" + Account + "', "
			+ "sex='" + sex + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine()
	{
		return 
			"\""+id+"\","+ 
			"\""+Account+"\","+ 
			"\""+sex+"\"";
	}

}