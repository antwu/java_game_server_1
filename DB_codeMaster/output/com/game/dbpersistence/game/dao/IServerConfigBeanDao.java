/**
 * IServerConfigBeanDao.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.dao;

import java.util.List;
import java.util.Map;
import com.game.core.dbcs.daoproxy.IDaoExecutor;
import com.game.core.dbcs.executor.DAOInfo;
import com.game.dbpersistence.game.bean.ServerConfigBean;
/**
 * dao Interface for IServerConfigBeanDao
 *
 * @author CodeMaster v1.0
 */
public interface IServerConfigBeanDao extends IDaoExecutor
{
	@DAOInfo(Params = "UID")
	public ServerConfigBean selectServerConfigBean(int UID);

	@DAOInfo(Params = "ServerName")
	public ServerConfigBean selectServerConfigBeanByidxServerName(String ServerName);

	@DAOInfo(Params = "")
	public List<ServerConfigBean> selectServerConfigBeanList();

	@DAOInfo(Params = "")
	public Integer insertServerConfigBean(ServerConfigBean ServerConfigBean);

	@DAOInfo(Params = "")
	public Integer updateServerConfigBean(ServerConfigBean ServerConfigBean);

	@DAOInfo(Params = "")
	public Integer deleteServerConfigBean(ServerConfigBean ServerConfigBean);

}