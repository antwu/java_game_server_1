/**
 * IAccountBeanDao.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.dao;

import java.util.List;
import java.util.Map;
import com.game.core.dbcs.daoproxy.IDaoExecutor;
import com.game.core.dbcs.executor.DAOInfo;
import com.game.dbpersistence.game.bean.AccountBean;
/**
 * dao Interface for IAccountBeanDao
 *
 * @author CodeMaster v1.0
 */
public interface IAccountBeanDao extends IDaoExecutor
{
	@DAOInfo(Params = "Account")
	public AccountBean selectAccountBean(String Account);

	@DAOInfo(Params = "Account")
	public List<AccountBean> selectAccountBeanListByaccount(String Account);

	@DAOInfo(Params = "")
	public List<AccountBean> selectAccountBeanList();

	@DAOInfo(Params = "")
	public Integer insertAccountBean(AccountBean AccountBean);

	@DAOInfo(Params = "")
	public Integer updateAccountBean(AccountBean AccountBean);

	@DAOInfo(Params = "")
	public Integer deleteAccountBean(AccountBean AccountBean);

}