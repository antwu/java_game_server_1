package codemaster.core;

public class Constant
{
	public static final int C_AUTO_INCREMENT = 7;
	public static final int C_DATA_TYPE = 3;
	public static final int C_DEFAULT = 6;
	public static final int C_NAME = 0;
	public static final int C_NON_PERSISTENT = 9;
	public static final int C_NULLABLE = 5;
	public static final int C_UNIQUE_KEY = 8;
	public static final String YES = "Y";
	public static final int C_TABLE = 10;
	public static final int C_PRIMARY_KEY = 4;
	public static final int C_JAVA_TYPE = 2;
	public static final int C_INDEX = 11;
	public static final int C_INDEX_ISUNIQUE = 12;
}
