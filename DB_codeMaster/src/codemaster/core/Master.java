package codemaster.core;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import codemaster.dao.DaoImplBuilder;
import codemaster.domain.DomainObjectBuilder;
import codemaster.persistence.SqlBuilder;
import codemaster.persistence.SqlMapBuilder;

import common.util.FileUtility;

import csv.CsvFileBuilder;

public class Master
{
	public static void main(String[] args)
	{
		Master master = new Master();
		master.build();
	}

	private final String currentDir = System.getProperty("user.dir");

	private DomainObjectBuilder domainObjectBuilder = new DomainObjectBuilder();// mm平台̨domain
	private SqlMapBuilder sqlMapBuilder = new SqlMapBuilder();
	private DaoImplBuilder daoImpBuilder = new DaoImplBuilder();

	private String header;

	public static Properties props;

	private SqlBuilder sqlBuilder = new SqlBuilder();

	public Master()
	{
		try
		{
			props = FileUtility.loadProps(currentDir + "/codemaster.properties");
			header = FileUtility.read(new File(currentDir + "/resource/FileHeader.java"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void build()
	{
		try
		{
			CsvFileBuilder.build(props.getProperty("mysql.url"), "input");
		}
		catch (InstantiationException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buildDomainObject();// mm平台
		buildSql();
		buildSqlMap();
		buildDaoImp();
		// buildDaoInterface();

	}

	public void buildDomainObject()
	{
		File[] dtoSourceFiles = (new File(currentDir + props.getProperty("dir.input"))).listFiles();
		File dtoOutputDir = new File(currentDir + props.getProperty("dir.domain"));
		for (int i = 0; i < dtoSourceFiles.length; i++)
		{
			if (dtoSourceFiles[i].isFile() && !dtoSourceFiles[i].isHidden())
			{
				System.out.println("Generating domain object class from " + dtoSourceFiles[i]);
				domainObjectBuilder.buildBean(header, dtoSourceFiles[i], dtoOutputDir, props.getProperty("package.bean"));

				domainObjectBuilder.buildDomain(header, dtoSourceFiles[i], dtoOutputDir, props.getProperty("package.domain"));
				System.out.println("Generated domain object class from " + dtoSourceFiles[i] + " successfully");

			}
		}
	}

	public void buildSql()
	{
		File[] sqlSourceFiles = (new File(currentDir + props.getProperty("dir.input"))).listFiles();
		File sqlOutputDir = new File(currentDir + props.getProperty("dir.sql"));
		for (int i = 0; i < sqlSourceFiles.length; i++)
		{
			if (sqlSourceFiles[i].isFile() && !sqlSourceFiles[i].isHidden())
			{
				System.out.println("Generating sql script from " + sqlSourceFiles[i]);
				sqlBuilder.build(sqlSourceFiles[i], sqlOutputDir, props.getProperty("db.type"));
				System.out.println("Generated sql script from " + sqlSourceFiles[i] + " successfully");
			}
		}
	}

	public void clean()
	{
		File file;
		File files[];
		ArrayList<File> fileList = new ArrayList<File>();
		fileList.add(new File(props.getProperty("dir.clean.input")));

		while (!fileList.isEmpty())
		{
			// Processes the first file in the list
			file = (File) fileList.remove(0);

			// If the file is a file and it is NOT hidden, clean it up
			if (file.isFile() && !file.isHidden())
			{
				System.out.println("Cleaning source file " + file);
				Styler.clean(file);
				System.out.println("Cleaned source file " + file + " successfully");
			}
			// if current file is a directory and it is NOT hidden, add the
			// files in this directory to the fileList
			else if (file.isDirectory() && !file.isHidden())
			{
				files = file.listFiles();
				for (int i = 0; i < files.length; i++)
				{
					fileList.add(files[i]);
				}
			}
		}
	}

	public void buildSqlMap()
	{
		File[] sqlSourceFiles = (new File(currentDir + props.getProperty("dir.input"))).listFiles();
		File sqlOutputDir = new File(currentDir + props.getProperty("dir.dao"));
		String domainPackage = props.getProperty("package.domain");
		for (int i = 0; i < sqlSourceFiles.length; i++)
		{
			if (sqlSourceFiles[i].isFile() && !sqlSourceFiles[i].isHidden())
			{
				System.out.println("Generating sql map xml from " + sqlSourceFiles[i]);
				sqlMapBuilder.build(sqlSourceFiles[i], sqlOutputDir, "", props.getProperty("package.daoimpl"), props.getProperty("package.bean"));
				System.out.println("Generated sql map xml from " + sqlSourceFiles[i] + " successfully");
			}
		}
	}

	public void buildDaoImp()
	{
		File[] dtoSourceFiles = (new File(currentDir + props.getProperty("dir.input"))).listFiles();
		File dtoOutputDir = new File(currentDir + props.getProperty("dir.dao"));
		for (int i = 0; i < dtoSourceFiles.length; i++)
		{
			if (dtoSourceFiles[i].isFile() && !dtoSourceFiles[i].isHidden())
			{
				System.out.println("Generating dao imp object class from " + dtoSourceFiles[i]);
				daoImpBuilder.build(header, dtoSourceFiles[i], dtoOutputDir, props.getProperty("package.daoimpl"));
				System.out.println("Generated dao imp object class from " + dtoSourceFiles[i] + " successfully");
			}
		}
	}

}
