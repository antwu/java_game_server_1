package codemaster.core;

import java.io.File;

public class Helper
{

	public static final String getPackageDir(File outputDir, String packageName)
	{
		return outputDir + File.separator
				+ packageName.replace('.', File.separatorChar);
	}

	public static final String getField(String attribute)
	{
		String field = "";
		String currentChar;
		for (int i = 0; i < attribute.length(); i++)
		{
			currentChar = attribute.substring(i, i + 1);
			if (currentChar.toUpperCase().equals(currentChar))
			{
				field = field + "_" + currentChar.toLowerCase();
			}
			else
			{
				field = field + currentChar;
			}
		}
		return attribute;
	}
}
