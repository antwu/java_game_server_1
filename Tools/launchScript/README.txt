起服脚本的目录结构： 最上层：／home/project4/dev
    dist : 用于存放服务器内部jar
    lib  ：用于存放系统引用的第三方jar
    log  ：存放log的目录

    server:　存放起服脚本
	    manager --> ManagerServer.py 
	    auth    --> AuthServer.py
	    gameserver --> GameServer.py
        battleserver --> 开启战斗服务器
        work_bin    --> 可用于配置
    ** 目录下的resources文件时起服时需要的配置文件，里面存储起服所用的数据信息，可以修改
