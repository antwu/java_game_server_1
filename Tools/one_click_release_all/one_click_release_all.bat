cd ../..
echo -------------Message.jar---------------------------
cd ./Message
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/Message.jar (
	msg compile error
	goto end
)



echo -------------CorgiServerCore.jar---------------------------
cd ..
cd ./CorgiServerCore
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/CorgiServerCore.jar (
	msg compile error
	goto end
)

echo -------------ServerCommon.jar---------------------------
cd ..
cd ./ServerCommon
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/ServerCommon.jar (
	msg compile error
	goto end
)

echo -------------DBPersistence.jar---------------------------
cd ..
cd ./DBPersistence
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/DBPersistence.jar (
	msg compile error
	goto end
)

echo -------------GameServer.jar---------------------------
cd ..
cd ./GameServer
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/GameServer.jar (
	msg compile error
	goto end
)

echo -------------BattleServer.jar---------------------------
cd ..
cd ./BattleServer
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/BattleServer.jar (
	msg compile error
	goto end
)

echo -----------GatewayServer.jar--------------------------
cd ..
cd ./GatewayServer
call ant -buildfile ./build_no_api.xml default
if not exist ../jar/GatewayServer.jar (
        msg compile error
        goto end
)

echo -----Success !!!-----------
:end
