drop table if EXISTS mission;
CREATE TABLE `mission` (
  `uid` varchar(255) NOT NULL DEFAULT '',
  `ownerId` varchar(255) NOT NULL DEFAULT '',
  `missionId` int(32) NOT NULL DEFAULT 0,
  `achievement` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;