drop table if EXISTS cloth;
CREATE TABLE `cloth` (
  `uid` varchar(255) NOT NULL DEFAULT '',
  `ownerId` varchar(255) NOT NULL DEFAULT '',
  `clothId` int(32) NOT NULL DEFAULT 0,
  `unlocked` int(32) NOT NULL DEFAULT 0,
  `skinColour` int(32) NOT NULL DEFAULT 0,
  `hairColour` int(32) NOT NULL DEFAULT 0,
  `mainbodyColour` int(32) NOT NULL DEFAULT 0,
  `decorateColour` int(32) NOT NULL DEFAULT 0,
  `skinColours` varchar(255) NOT NULL DEFAULT '',
  `hairColours` varchar(255) NOT NULL DEFAULT '',
  `mainbodyColours` varchar(255) NOT NULL DEFAULT '',
  `decorateColours` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `weapon_set` 
CHANGE COLUMN `cloth` `clothId` VARCHAR(255) NULL DEFAULT '0' ,
CHANGE COLUMN `buncher` `buncherId` VARCHAR(255) NULL DEFAULT '0' ;