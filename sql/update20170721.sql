drop table if EXISTS account;
CREATE TABLE `account` (
  `uid` varchar(255) NOT NULL DEFAULT '',
  `acc` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `appearance` varchar(255) NOT NULL DEFAULT '',
  `sex` int(32) NOT NULL DEFAULT '0',
  `level` int(32) NOT NULL DEFAULT '1',
  `weaponSet` int(32) NOT NULL DEFAULT '0',
  `createTime` bigint(32) NOT NULL DEFAULT '0',
  `lastLoginTime` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `account` (`acc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




drop table if EXISTS weapon;
CREATE TABLE `weapon` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `weaponId` int(32) NOT NULL,
  `forged` varchar(255) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if EXISTS weapon_set;
CREATE TABLE `weapon_set` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `setId` int(32) NOT NULL,
  `setName` varchar(255) NOT NULL DEFAULT '',
  `weapon1` varchar(255) DEFAULT '0',
  `weapon2` varchar(255) DEFAULT '0',
  `weapon3` varchar(255) DEFAULT '0',
  `weapon4` varchar(255) DEFAULT '0',
  `cloth` varchar(255) DEFAULT '0',
  `buncher` varchar(255) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`,`setId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


