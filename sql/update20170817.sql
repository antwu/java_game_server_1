drop table if EXISTS `weapon`;
CREATE TABLE `weapon` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `weaponId` int(32) NOT NULL,
  `name` varchar(255) DEFAULT '0',
  `weaponType` int(32) DEFAULT NULL,
  `weaponLevel` int(32) DEFAULT NULL,
  `mainSkillId` int(32) DEFAULT NULL,
  `talents` varchar(255) DEFAULT NULL,
  `combines` varchar(255) DEFAULT NULL,
  `mainNodes` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
