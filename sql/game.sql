/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : game

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2017-06-19 15:11:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(32) NOT NULL,
  `Account` varchar(255) NOT NULL DEFAULT '',
  `sex` int(32) NOT NULL,
  PRIMARY KEY (`Account`),
  KEY `account` (`Account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('0', '111_aaa', '0');
INSERT INTO `account` VALUES ('0', '111_aaac', '0');
INSERT INTO `account` VALUES ('0', '11_aaa', '0');
INSERT INTO `account` VALUES ('0', '11_bbb', '0');
INSERT INTO `account` VALUES ('0', '21_aa2', '0');
INSERT INTO `account` VALUES ('0', '21_aaa', '0');
INSERT INTO `account` VALUES ('0', '22_aaa', '0');
INSERT INTO `account` VALUES ('0', '22_bbb', '0');
INSERT INTO `account` VALUES ('0', '33_ab', '0');
INSERT INTO `account` VALUES ('0', '33_cd', '0');
INSERT INTO `account` VALUES ('0', 'aaa', '0');
INSERT INTO `account` VALUES ('0', 'aaac_111', '0');

-- ----------------------------
-- Table structure for `server_config`
-- ----------------------------
DROP TABLE IF EXISTS `server_config`;
CREATE TABLE `server_config` (
  `UID` int(11) NOT NULL,
  `ServerType` int(11) NOT NULL DEFAULT '0',
  `ServerName` varchar(32) NOT NULL,
  `IPForServer` varchar(20) NOT NULL,
  `PortForServer` int(11) NOT NULL DEFAULT '0',
  `IPForClient` varchar(20) NOT NULL,
  `PortForClient` int(11) NOT NULL DEFAULT '0',
  `IPForGMT` varchar(20) NOT NULL,
  `PortForGMT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UID`),
  UNIQUE KEY `idxServerName` (`ServerName`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='前面1000个ID作为保留';

-- ----------------------------
-- Records of server_config
-- ----------------------------
INSERT INTO `server_config` VALUES ('1000', '1', 'game_server', '127.0.0.1', '13600', '127.0.0.1', '3600', '127.0.0.1', '23600');
INSERT INTO `server_config` VALUES ('2000', '2', 'battle_server_2000', '127.0.0.1', '13700', '127.0.0.1', '3700', '127.0.0.1', '23700');
INSERT INTO `server_config` VALUES ('3000', '3', 'gateway_server_3000', '127.0.0.1', '13800', '127.0.0.1', '3800', '127.0.0.1', '23800');
