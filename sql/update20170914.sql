ALTER TABLE `game`.`account` 

ADD COLUMN `experience` BIGINT(64) NULL AFTER `lastLoginTime`,

ADD COLUMN `goldCoin` BIGINT(64) NULL AFTER `experience`,

ADD COLUMN `diamonds` BIGINT(64) NULL AFTER `goldCoin`,

ADD COLUMN `giftCert` BIGINT(64) NULL AFTER `diamonds`,

ADD COLUMN `feats` BIGINT(64) NULL AFTER `giftCert`;
ALTER TABLE `game`.`account` 
DROP COLUMN `coin`;