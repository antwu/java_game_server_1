CREATE DATABASE `game`;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `uid` varchar(255) NOT NULL DEFAULT '',
  `acc` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `appearance` varchar(255) NOT NULL DEFAULT '',
  `sex` int(32) NOT NULL DEFAULT '0',
  `level` int(32) NOT NULL DEFAULT '1',
  `weaponSet` int(32) NOT NULL DEFAULT '0',
  `createTime` bigint(32) NOT NULL DEFAULT '0',
  `lastLoginTime` bigint(32) NOT NULL DEFAULT '0',
  `experience` bigint(64) DEFAULT NULL,
  `goldCoin` bigint(64) DEFAULT NULL,
  `diamonds` bigint(64) DEFAULT NULL,
  `giftCert` bigint(64) DEFAULT NULL,
  `feats` bigint(64) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `account` (`acc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `fashion`;
CREATE TABLE `fashion` (
  `uid` varchar(255) NOT NULL DEFAULT '',
  `ownerId` varchar(255) NOT NULL DEFAULT '',
  `fashionId` int(32) NOT NULL DEFAULT '0',
  `unlocked` int(32) NOT NULL DEFAULT '0',
  `skinColour` int(32) NOT NULL DEFAULT '0',
  `hairColour` int(32) NOT NULL DEFAULT '0',
  `mainbodyColour` int(32) NOT NULL DEFAULT '0',
  `decorateColour` int(32) NOT NULL DEFAULT '0',
  `skinColours` varchar(255) NOT NULL DEFAULT '',
  `hairColours` varchar(255) NOT NULL DEFAULT '',
  `mainbodyColours` varchar(255) NOT NULL DEFAULT '',
  `decorateColours` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `itemId` int(32) NOT NULL,
  `itemType` int(32) DEFAULT '0',
  `count` int(32) DEFAULT '0',
  `vanishTime` int(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `making`;
CREATE TABLE `making` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `makingId` int(32) NOT NULL,
  `status` int(32) DEFAULT NULL,
  `startTime` bigint(64) DEFAULT NULL,
  `speedTime` bigint(64) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mission`;
CREATE TABLE `mission` (
  `uid` varchar(255) NOT NULL DEFAULT '',
  `ownerId` varchar(255) NOT NULL DEFAULT '',
  `missionId` int(32) NOT NULL DEFAULT '0',
  `achievement` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `weapon`;
CREATE TABLE `weapon` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `weaponId` int(32) NOT NULL,
  `name` varchar(255) DEFAULT '0',
  `weaponType` int(32) DEFAULT NULL,
  `weaponLevel` int(32) DEFAULT NULL,
  `mainSkillId` int(32) DEFAULT NULL,
  `talents` varchar(255) DEFAULT NULL,
  `combines` varchar(255) DEFAULT NULL,
  `mainNodes` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `weapon_set`;
CREATE TABLE `weapon_set` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `setId` int(32) NOT NULL,
  `setName` varchar(255) NOT NULL DEFAULT '',
  `weapon1` varchar(255) DEFAULT '0',
  `weapon2` varchar(255) DEFAULT '0',
  `weapon3` varchar(255) DEFAULT '0',
  `weapon4` varchar(255) DEFAULT '0',
  `fashionId` int(32) DEFAULT '0',
  `buncherId` int(32) DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`,`setId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
