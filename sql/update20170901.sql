ALTER TABLE `game`.`weapon_set` CHANGE COLUMN `clothId` `fashionId` VARCHAR(255) NULL DEFAULT '0' ;
ALTER TABLE `game`.`cloth` CHANGE COLUMN `clothId` `fashionId` INT(32) NOT NULL DEFAULT '0' ;
ALTER TABLE `game`.`cloth` RENAME TO `game`.`fashion`;
drop table if EXISTS `making`;
CREATE TABLE `making` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `makingId` int(32) NOT NULL,
  `status` int(32) DEFAULT NULL,
  `startTime` int(32) DEFAULT NULL,
  `speedTime` int(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
