CREATE TABLE `item` (
  `uid` varchar(255) NOT NULL,
  `ownerId` varchar(255) NOT NULL,
  `itemId` int(32) NOT NULL,
  `itemType` int(32) DEFAULT 0,
  `count` int(32) DEFAULT 0,
  `vanishTime` int(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `index1` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;