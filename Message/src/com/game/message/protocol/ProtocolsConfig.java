//This file is generated by program. Do not edit it manually.

package com.game.message.protocol;
import com.game.message.generaor.ProtocolConfigAnnotation;

public class ProtocolsConfig extends Protocols
{
	//Client and gateWay Protocols for Clients 0x0001xxxx, xxxx start from 0x0000
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.CWAccountLoginREQ.class, needLogin = false)
	final public static int P_CW_ACCOUNT_LOGIN_REQ = 0x10001;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.WCAccountLoginRES.class, needLogin = false)
	final public static int P_WC_ACCOUNT_LOGIN_RES = 0x10002;
		final public static int LOGIN_SUCCESS = 0x110002;
		final public static int LOGIN_FAILED = 0x1010002;
		final public static int LOGIN_FAILED_FORBID = 0x1110002;
		final public static int LOGIN_FAILED_INVALID_PARAM = 0x1210002;
		final public static int LOGIN_FAILED_REFRESHTOKEN_INVALID = 0x1310002;
		final public static int LOGIN_FAILED_PLATFORM_NOTALLOW = 0x1410002;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.heart.HeartProtoBuf.CWHeartBeatREQ.class, needLogin = false)
	final public static int P_CW_HEART_BEAT_REQ = 0x10003;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.heart.HeartProtoBuf.WCHeartBeatRES.class, needLogin = false)
	final public static int P_WC_HEART_BEAT_RES = 0x10004;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.WCAccountLogoutRES.class, needLogin = false)
	final public static int P_WC_ACCOUNT_LOGOUT_RES = 0x10005;
		final public static int LOGOUT_KICKOFF = 0x1010005;
		final public static int LOGOUT_FORBID = 0x1110005;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.CWRoomCreateREQ.class, needLogin = true)
	final public static int P_CW_ROOM_CREATE_REQ = 0x10006;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.WCRoomCreateRES.class, needLogin = false)
	final public static int P_WC_ROOM_CREATE_RES = 0x10007;
		final public static int ROOM_CREATE_SUCCESS = 0x110007;
		final public static int ROOM_CREATE_FAILED_INNER_ERROR = 0x1010007;
		final public static int ROOM_CREATE_FAILED_ALREADY_IN_ROOM = 0x1110007;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.CWRoomEnterREQ.class, needLogin = true)
	final public static int P_CW_ROOM_ENTER_REQ = 0x10008;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.WCAccountModifySYN.class, needLogin = false)
	final public static int P_WC_ACCOUNT_MODIFY_SYN = 0x10009;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.WCContinueBattleSYN.class, needLogin = false)
	final public static int P_WC_CONTINUE_BATTLE_SYN = 0x1000a;
		final public static int CONTINUE_BATTLE_SYN_SUCCESS = 0x11000a;
		final public static int CONTINUE_BATTLE_SYN_FAILD = 0x101000a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWSearchPathREQ.class, needLogin = false)
	final public static int P_CW_SEARCH_PATH = 0x1000b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCSearchPathRES.class, needLogin = false)
	final public static int P_WC_SEARCH_PATH = 0x1000c;
		final public static int SEARCH_SUCCESS = 0x101000c;
		final public static int NOT_YOUR_TURN = 0x111000c;
		final public static int ACTION_POWER_NOT_ENOUGH = 0x121000c;
		final public static int CAN_NOT_ATTACK = 0x131000c;
		final public static int CAN_NOT_MOVE = 0x141000c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWStartBattleREQ.class, needLogin = false)
	final public static int P_CW_START_BATTLE = 0x1000d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCStartBattleRES.class, needLogin = false)
	final public static int P_WC_START_BATTLE = 0x1000e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWEnterBattleREQ.class, needLogin = false)
	final public static int P_CW_ENTER_BATTLE = 0x1000f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCEnterBattleRES.class, needLogin = false)
	final public static int P_WC_ENTER_BATTLE = 0x10010;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWReadyBattleREQ.class, needLogin = false)
	final public static int P_CW_READY_BATTLE = 0x10011;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCReadyBattleRES.class, needLogin = false)
	final public static int P_WC_READY_BATTLE = 0x10012;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWCancleOperateREQ.class, needLogin = false)
	final public static int P_CW_CANCLE_OPERATE_REQ = 0x10013;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCCancleOperateRES.class, needLogin = false)
	final public static int P_WC_CANCLE_OPERATE_RES = 0x10014;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWChangeWeaponREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_WEAPON_REQ = 0x10015;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCChangeWeaponRES.class, needLogin = false)
	final public static int P_WC_CHANGE_WEAPON_RES = 0x10016;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.CWUseShieldREQ.class, needLogin = false)
	final public static int P_CW_USE_SHIELD_REQ = 0x10017;
		final public static int USE_SHIELD_SUCCESS = 0x1010017;
		final public static int POWER_NOT_ENOUGH = 0x1110017;
		final public static int CAN_NOT_USE_SHIELD = 0x1210017;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WCUseShieldRES.class, needLogin = false)
	final public static int P_WC_USE_SHIELD_RES = 0x10018;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeWeaponSetREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_WEAPON_SET_REQ = 0x10019;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeWeaponSetRES.class, needLogin = false)
	final public static int P_WC_CHANGE_WEAPON_SET_RES = 0x1001a;
		final public static int CHANGE_WEAPON_SET_SUCCESS = 0x101001a;
		final public static int WEAPON_NOT_EXIST = 0x111001a;
		final public static int NOT_HIS_WEAPON = 0x121001a;
		final public static int INVALID_WEAPON_SET = 0x131001a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeCurrentWeaponSetREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_CURRENT_WEAPON_SET_REQ = 0x1001b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeCurrentWeaponSetRES.class, needLogin = false)
	final public static int P_WC_CHANGE_CURRENT_WEAPON_SET_RES = 0x1001c;
		final public static int CHANGE_CURRENT_WEAPON_SET_SUCCESS = 0x101001c;
		final public static int INVALID_CHANGE_WEAPON_SET = 0x111001c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeWeaponSetNameREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_WEAPON_SET_NAME_REQ = 0x1001d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeWeaponSetNameRES.class, needLogin = false)
	final public static int P_WC_CHANGE_WEAPON_SET_NAME_RES = 0x1001e;
		final public static int CHANGE_WEAPON_SET_NAME_SUCCESS = 0x101001e;
		final public static int INVALID_WEAPON_SET_NAME = 0x111001e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeFashionSetREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_FASHION_SET_REQ = 0x1001f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeFashionSetRES.class, needLogin = false)
	final public static int P_WC_CHANGE_FASHION_SET_RES = 0x10020;
		final public static int CHANGE_FASHION_SET_SUCCESS = 0x1010020;
		final public static int CHANGE_FASHION_SET_FAIL = 0x1110020;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeFashionUnlockREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_FASHION_UNLOCK_REQ = 0x10021;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeFashionUnlockRES.class, needLogin = false)
	final public static int P_WC_CHANGE_FASHION_UNLOCK_RES = 0x10022;
		final public static int CHANGE_FASHION_UNLOCK_SUCCESS = 0x1010022;
		final public static int CHANGE_FASHION_UNLOCK_FAIL = 0x1110022;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeColourSetREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_COLOUR_SET_REQ = 0x10023;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeColourSetRES.class, needLogin = false)
	final public static int P_WC_CHANGE_COLOUR_SET_RES = 0x10024;
		final public static int CHANGE_COLOUR_SET_SUCCESS = 0x1010024;
		final public static int CHANGE_COLOUR_SET_FAIL = 0x1110024;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeColourUnlockREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_COLOUR_UNLOCK_REQ = 0x10025;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeColourUnlockRES.class, needLogin = false)
	final public static int P_WC_CHANGE_COLOUR_UNLOCK_RES = 0x10026;
		final public static int CHANGE_COLOUR_UNLOCK_SUCCESS = 0x1010026;
		final public static int CHANGE_COLOUR_UNLOCK_FAIL = 0x1110026;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.player.PlayerProtoBuf.CWChangePlayerInfoREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_PLAYER_INFO_REQ = 0x10027;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.player.PlayerProtoBuf.WCChangePlayerInfoRES.class, needLogin = false)
	final public static int P_WC_CHANGE_PLAYER_INFO_RES = 0x10028;
		final public static int CHANGE_PLAYER_INFO_SUCCESS = 0x1010028;
		final public static int INVALID_PLAYER_INFO_PARAMS = 0x1110028;
		final public static int INVALID_NAME_CHAR = 0x1210028;
		final public static int INVALID_NAME_LENGTH = 0x1310028;
		final public static int NAME_TAKEN = 0x1410028;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWGetTeamListREQ.class, needLogin = false)
	final public static int P_CW_GET_TEAM_LIST_REQ = 0x10029;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCGetTeamListRES.class, needLogin = false)
	final public static int P_WC_GET_TEAM_LIST_RES = 0x1002a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWGetTeamInfoREQ.class, needLogin = false)
	final public static int P_CW_GET_TEAM_INFO_REQ = 0x1002b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCGetTeamInfoRES.class, needLogin = false)
	final public static int P_WC_GET_TEAM_INFO_RES = 0x1002c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWJoinTeamREQ.class, needLogin = false)
	final public static int P_CW_JOIN_TEAM_REQ = 0x1002d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCJoinTeamRES.class, needLogin = false)
	final public static int P_WC_JOIN_TEAM_RES = 0x1002e;
		final public static int JOIN_TEAM_SUCCESS = 0x101002e;
		final public static int JOIN_TEAM_FAIL = 0x111002e;
		final public static int TEAM_IN_BATTLE = 0x121002e;
		final public static int TEAM_FULL = 0x131002e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWCreateTeamREQ.class, needLogin = false)
	final public static int P_CW_CREATE_TEAM_REQ = 0x1002f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCCreateTeamRES.class, needLogin = false)
	final public static int P_WC_CREATE_TEAM_RES = 0x10030;
		final public static int CREATE_TEAM_SUCCESS = 0x1010030;
		final public static int CREATE_TEAM_FAIL = 0x1110030;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWKickTeamMemberREQ.class, needLogin = false)
	final public static int P_CW_KICK_TEAM_MEMBER_REQ = 0x10031;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCKickTeamMemberRES.class, needLogin = false)
	final public static int P_WC_KICK_TEAM_MEMBER_RES = 0x10032;
		final public static int KICK_TEAM_MEMBER_SUCCESS = 0x1010032;
		final public static int KICK_TEAM_MEMBER_FAIL = 0x1110032;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWChangeTeamPlayerStatusREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_TEAM_PLAYER_STATUS_REQ = 0x10033;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCChangeTeamPlayerStatusRES.class, needLogin = false)
	final public static int P_WC_CHANGE_TEAM_PLAYER_STATUS_RES = 0x10034;
		final public static int CHANGE_TEAM_PLAYER_STATUS_SUCCESS = 0x1010034;
		final public static int CHANGE_TEAM_PLAYER_STATUS_FAIL = 0x1110034;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWStartTeamBattleREQ.class, needLogin = false)
	final public static int P_CW_START_TEAM_BATTLE_REQ = 0x10035;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCStartTeamBattleRES.class, needLogin = false)
	final public static int P_WC_START_TEAM_BATTLE_RES = 0x10036;
		final public static int START_TEAM_BATTLE_SUCCESS = 0x1010036;
		final public static int START_TEAM_BATTLE_FAIL = 0x1110036;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWLeaveTeamREQ.class, needLogin = false)
	final public static int P_CW_LEAVE_TEAM_REQ = 0x10037;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCLeaveTeamRES.class, needLogin = false)
	final public static int P_WC_LEAVE_TEAM_RES = 0x10038;
		final public static int LEAVE_TEAM_SUCCESS = 0x1010038;
		final public static int LEAVE_TEAM_FAIL = 0x1110038;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWModifyTeamREQ.class, needLogin = false)
	final public static int P_CW_MODIFY_TEAM_REQ = 0x10039;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCModifyTeamRES.class, needLogin = false)
	final public static int P_WC_MODIFY_TEAM_RES = 0x1003a;
		final public static int MODIFY_TEAM_SUCCESS = 0x101003a;
		final public static int MODIFY_TEAM_FAIL = 0x111003a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWQuickJoinTeamREQ.class, needLogin = false)
	final public static int P_CW_QUICK_JOIN_TEAM_REQ = 0x1003b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCQuickJoinTeamRES.class, needLogin = false)
	final public static int P_WC_QUICK_JOIN_TEAM_RES = 0x1003c;
		final public static int QUICK_JOIN_TEAM_SUCCESS = 0x101003c;
		final public static int QUICK_JOIN_TEAM_FAIL = 0x111003c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.CWStartSingleBattleREQ.class, needLogin = false)
	final public static int P_CW_START_SINGLE_BATTLE_REQ = 0x1003d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCStartSingleBattleRES.class, needLogin = false)
	final public static int P_WC_START_SINGLE_BATTLE_RES = 0x1003e;
		final public static int START_SINGLE_BATTLE_SUCCESS = 0x101003e;
		final public static int START_SINGLE_BATTLE_FAIL = 0x111003e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WCFinishBattleRES.class, needLogin = false)
	final public static int P_WC_FINISH_BATTLE_RES = 0x1003f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeMainNodeUnlockREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_MAIN_NODE_UNLOCK_REQ = 0x10040;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeMainNodeUnlockRES.class, needLogin = false)
	final public static int P_WC_CHANGE_MAIN_NODE_UNLOCK_RES = 0x10041;
		final public static int CHANGE_MAIN_NODE_UNLOCK_SUCCESS = 0x1010041;
		final public static int CHANGE_MAIN_NODE_UNLOCK_FAIL = 0x1110041;
		final public static int CHANGE_MAIN_NODE_HAS_UNLOCKED = 0x1210041;
		final public static int UNLOCK_MAIN_NODE_NOT_EXIST = 0x1310041;
		final public static int UNLOCK_MAIN_NODE_WEAPON_NOT_EXIST = 0x1410041;
		final public static int UNLOCK_MAIN_NODE_MONEY_NOT_ENOUGH = 0x1510041;
		final public static int UNLOCK_MAIN_NODE_CONDITION_NOT_CONTENT = 0x1610041;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.CWChangeItemNodeUnlockREQ.class, needLogin = false)
	final public static int P_CW_CHANGE_ITEM_NODE_UNLOCK_REQ = 0x10042;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCChangeItemNodeUnlockRES.class, needLogin = false)
	final public static int P_WC_CHANGE_ITEM_NODE_UNLOCK_RES = 0x10043;
		final public static int CHANGE_ITEM_NODE_UNLOCK_SUCCESS = 0x1010043;
		final public static int CHANGE_ITEM_NODE_UNLOCK_FAIL = 0x1110043;
		final public static int CHANGE_ITEM_NODE_HAS_UNLOCKED = 0x1210043;
		final public static int UNLOCK_ITEM_NODE_NOT_EXIST = 0x1310043;
		final public static int UNLOCK_ITEM_NODE_WEAPON_NOT_EXIST = 0x1410043;
		final public static int UNLOCK_ITEM_NODE_MONEY_NOT_ENOUGH = 0x1510043;
		final public static int UNLOCK_ITEM_NODE_CONDITION_NOT_CONTENT = 0x1610043;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.CWMakeEquipmentREQ.class, needLogin = false)
	final public static int P_CW_MAKE_EQUIPMENT_REQ = 0x10044;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.WCMakeEquipmentRES.class, needLogin = false)
	final public static int P_WC_MAKE_EQUIPMENT_RES = 0x10045;
		final public static int MAKE_EQUIPMENT_OPERATE_SUCCESS = 0x1010045;
		final public static int MAKE_EQUIPMENT_OPERATE_FAILED = 0x1110045;
		final public static int MAKE_EQUIPMENT_OPERATE_ALREADY = 0x1210045;
		final public static int MAKE_EQUIPMENT_ALREADY_EXIST = 0x1310045;
		final public static int MAKE_EQUIPMENT_OPERATE_NOEXIST = 0x1410045;
		final public static int MAKE_EQUIPMENT_OPERATE_UNENOUGH = 0x1510045;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.CWSpeedEquipmentREQ.class, needLogin = false)
	final public static int P_CW_SPEED_EQUIPMENT_REQ = 0x10046;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.WCSpeedEquipmentRES.class, needLogin = false)
	final public static int P_WC_SPEED_EQUIPMENT_RES = 0x10047;
		final public static int SPEED_EQUIPMENT_OPERATE_SUCCESS = 0x1010047;
		final public static int SPEED_EQUIPMENT_OPERATE_FAILED = 0x1110047;
		final public static int SPEED_EQUIPMENT_OPERATE_ALREADY = 0x1210047;
		final public static int SPEED_EQUIPMENT_OPERATE_NOEXIST = 0x1310047;
		final public static int SPEED_EQUIPMENT_OPERATE_UNENOUGH = 0x1410047;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.item.ItemProtoBuf.WCSynItemInfoRES.class, needLogin = false)
	final public static int P_WC_SYN_ITEM_INFO_RES = 0x10048;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCSynWeaponInfoRES.class, needLogin = false)
	final public static int P_WC_SYN_WEAPON_INFO_RES = 0x10049;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WCSynFashionInfoRES.class, needLogin = false)
	final public static int P_WC_SYN_FASHION_INFO_RES = 0x1004a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.WCSynMoneyInfoRES.class, needLogin = false)
	final public static int P_WC_SYN_MONEY_INFO_RES = 0x1004b;

	//gateWay and Game Protocols for Clients 0x0002xxxx, xxxx start from 0x0000
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.WGAccountLoginREQ.class, needLogin = false)
	final public static int P_WG_ACCOUNT_LOGIN_REQ = 0x20001;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.GWAccountLoginRES.class, needLogin = false)
	final public static int P_GW_ACCOUNT_LOGIN_RES = 0x20002;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.GWAccountLogoutRES.class, needLogin = false)
	final public static int P_GW_ACCOUNT_LOGOUT_RES = 0x20003;
		final public static int GW_LOGOUT_KICKOFF = 0x1020003;
		final public static int GW_LOGOUT_FORBID = 0x1120003;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.WGRoomCreateREQ.class, needLogin = false)
	final public static int P_WG_ROOM_CREATE_REQ = 0x20004;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GWRoomCreateRES.class, needLogin = false)
	final public static int P_GW_ROOM_CREATE_RES = 0x20005;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.WGRoomEnterREQ.class, needLogin = false)
	final public static int P_WG_ROOM_ENTER_REQ = 0x20006;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.GWAccountModifySYN.class, needLogin = false)
	final public static int P_GW_ACCOUNT_MODIFY_SYN = 0x20007;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.WGRoomQuitREQ.class, needLogin = false)
	final public static int P_WG_ROOM_QUIT_REQ = 0x20008;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GWRoomQuitRES.class, needLogin = false)
	final public static int P_GW_ROOM_QUIT_RES = 0x20009;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.encrypt.EncryptProtoBuf.GWEncryptKeySYN.class, needLogin = false)
	final public static int P_GW_ENCRYPT_KEY_SYN = 0x2000a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.WGContinueBattleREQ.class, needLogin = false)
	final public static int P_WG_CONTINUE_BATTLE_REQ = 0x2000b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GWContinueBattleRES.class, needLogin = false)
	final public static int P_GW_CONTINUE_BATTLE_RES = 0x2000c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GWContinueBattleSYN.class, needLogin = false)
	final public static int P_GW_CONTINUE_BATTLE_SYN = 0x2000d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeWeaponSetREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_WEAPON_SET_REQ = 0x2000e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetRES.class, needLogin = false)
	final public static int P_GW_CHANGE_WEAPON_SET_RES = 0x2000f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeCurrentWeaponSetREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_CURRENT_WEAPON_SET_REQ = 0x20010;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeCurrentWeaponSetRES.class, needLogin = false)
	final public static int P_GW_CHANGE_CURRENT_WEAPON_SET_RES = 0x20011;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeWeaponSetNameREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_WEAPON_SET_NAME_REQ = 0x20012;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetNameRES.class, needLogin = false)
	final public static int P_GW_CHANGE_WEAPON_SET_NAME_RES = 0x20013;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeFashionSetREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_FASHION_SET_REQ = 0x20014;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionSetRES.class, needLogin = false)
	final public static int P_GW_CHANGE_FASHION_SET_RES = 0x20015;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeFashionUnlockREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_FASHION_UNLOCK_REQ = 0x20016;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionUnlockRES.class, needLogin = false)
	final public static int P_GW_CHANGE_FASHION_UNLOCK_RES = 0x20017;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeColourSetREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_COLOUR_SET_REQ = 0x20018;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourSetRES.class, needLogin = false)
	final public static int P_GW_CHANGE_COLOUR_SET_RES = 0x20019;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeColourUnlockREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_COLOUR_UNLOCK_REQ = 0x2001a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourUnlockRES.class, needLogin = false)
	final public static int P_GW_CHANGE_COLOUR_UNLOCK_RES = 0x2001b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.player.PlayerProtoBuf.WGChangePlayerInfoREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_PLAYER_INFO_REQ = 0x2001c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.player.PlayerProtoBuf.GWChangePlayerInfoRES.class, needLogin = false)
	final public static int P_GW_CHANGE_PLAYER_INFO_RES = 0x2001d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGGetTeamListREQ.class, needLogin = false)
	final public static int P_WG_GET_TEAM_LIST_REQ = 0x2001e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWGetTeamListRES.class, needLogin = false)
	final public static int P_GW_GET_TEAM_LIST_RES = 0x2001f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGGetTeamInfoREQ.class, needLogin = false)
	final public static int P_WG_GET_TEAM_INFO_REQ = 0x20020;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWGetTeamInfoRES.class, needLogin = false)
	final public static int P_GW_GET_TEAM_INFO_RES = 0x20021;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGJoinTeamREQ.class, needLogin = false)
	final public static int P_WG_JOIN_TEAM_REQ = 0x20022;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWJoinTeamRES.class, needLogin = false)
	final public static int P_GW_JOIN_TEAM_RES = 0x20023;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGCreateTeamREQ.class, needLogin = false)
	final public static int P_WG_CREATE_TEAM_REQ = 0x20024;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWCreateTeamRES.class, needLogin = false)
	final public static int P_GW_CREATE_TEAM_RES = 0x20025;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGKickTeamMemberREQ.class, needLogin = false)
	final public static int P_WG_KICK_TEAM_MEMBER_REQ = 0x20026;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWKickTeamMemberRES.class, needLogin = false)
	final public static int P_GW_KICK_TEAM_MEMBER_RES = 0x20027;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGChangeTeamPlayerStatusREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_TEAM_PLAYER_STATUS_REQ = 0x20028;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWChangeTeamPlayerStatusRES.class, needLogin = false)
	final public static int P_GW_CHANGE_TEAM_PLAYER_STATUS_RES = 0x20029;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGStartTeamBattleREQ.class, needLogin = false)
	final public static int P_WG_START_TEAM_BATTLE_REQ = 0x2002a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWStartTeamBattleRES.class, needLogin = false)
	final public static int P_GW_START_TEAM_BATTLE_RES = 0x2002b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGLeaveTeamREQ.class, needLogin = false)
	final public static int P_WG_LEAVE_TEAM_REQ = 0x2002c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWLeaveTeamRES.class, needLogin = false)
	final public static int P_GW_LEAVE_TEAM_RES = 0x2002d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGModifyTeamREQ.class, needLogin = false)
	final public static int P_WG_MODIFY_TEAM_REQ = 0x2002e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWModifyTeamRES.class, needLogin = false)
	final public static int P_GW_MODIFY_TEAM_RES = 0x2002f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGQuickJoinTeamREQ.class, needLogin = false)
	final public static int P_WG_QUICK_JOIN_TEAM_REQ = 0x20030;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWQuickJoinTeamRES.class, needLogin = false)
	final public static int P_GW_QUICK_JOIN_TEAM_RES = 0x20031;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.WGStartSingleBattleREQ.class, needLogin = false)
	final public static int P_WG_START_SINGLE_BATTLE_REQ = 0x20032;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWStartSingleBattleRES.class, needLogin = false)
	final public static int P_GW_START_SINGLE_BATTLE_RES = 0x20033;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GWFinishBattleRES.class, needLogin = false)
	final public static int P_GW_FINISH_BATTLE_RES = 0x20034;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeMainNodeUnlockREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_MAIN_NODE_UNLOCK_REQ = 0x20035;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeMainNodeUnlockRES.class, needLogin = false)
	final public static int P_GW_CHANGE_MAIN_NODE_UNLOCK_RES = 0x20036;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.WGChangeItemNodeUnlockREQ.class, needLogin = false)
	final public static int P_WG_CHANGE_ITEM_NODE_UNLOCK_REQ = 0x20037;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWChangeItemNodeUnlockRES.class, needLogin = false)
	final public static int P_GW_CHANGE_ITEM_NODE_UNLOCK_RES = 0x20038;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.WGMakeEquipmentREQ.class, needLogin = false)
	final public static int P_WG_MAKE_EQUIPMENT_REQ = 0x20039;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.GWMakeEquipmentRES.class, needLogin = false)
	final public static int P_GW_MAKE_EQUIPMENT_RES = 0x2003a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.WGSpeedEquipmentREQ.class, needLogin = false)
	final public static int P_WG_SPEED_EQUIPMENT_REQ = 0x2003b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.making.MakingProtoBuf.GWSpeedEquipmentRES.class, needLogin = false)
	final public static int P_GW_SPEED_EQUIPMENT_RES = 0x2003c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.item.ItemProtoBuf.GWSynItemInfoRES.class, needLogin = false)
	final public static int P_GW_SYN_ITEM_INFO_RES = 0x2003d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWSynWeaponInfoRES.class, needLogin = false)
	final public static int P_GW_SYN_WEAPON_INFO_RES = 0x2003e;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.weapon.WeaponProtoBuf.GWSynFashionInfoRES.class, needLogin = false)
	final public static int P_GW_SYN_FASHION_INFO_RES = 0x2003f;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.GWSynMoneyInfoRES.class, needLogin = false)
	final public static int P_GW_SYN_MONEY_INFO_RES = 0x20040;

	//gateWay and Battle Protocols for Clients 0x0003xxxx, xxxx start from 0x0000
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBSearchPathREQ.class, needLogin = false)
	final public static int P_WB_SEARCH_PATH = 0x30001;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWSearchPathRES.class, needLogin = false)
	final public static int P_BW_SEARCH_PATH = 0x30002;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ.class, needLogin = false)
	final public static int P_WB_ENTER_BATTLE = 0x30003;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWEnterBattleRES.class, needLogin = false)
	final public static int P_BW_ENTER_BATTLE = 0x30004;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBStartBattleREQ.class, needLogin = false)
	final public static int P_WB_START_BATTLE_REQ = 0x30005;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWStartBattleRES.class, needLogin = false)
	final public static int P_BW_START_BATTLE_RES = 0x30006;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBReadyBattleREQ.class, needLogin = false)
	final public static int P_WB_READY_BATTLE_REQ = 0x30007;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWReadyBattleRES.class, needLogin = false)
	final public static int P_BW_READY_BATTLE_RES = 0x30008;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBCancleOperateREQ.class, needLogin = false)
	final public static int P_WB_CANCLE_OPERATE_REQ = 0x30009;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWCancleOperateRES.class, needLogin = false)
	final public static int P_BW_CANCLE_OPERATE_RES = 0x3000a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBChangeWeaponREQ.class, needLogin = false)
	final public static int P_WB_CHANGE_WEAPON_REQ = 0x3000b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWChangeWeaponRES.class, needLogin = false)
	final public static int P_BW_CHANGE_WEAPON_RES = 0x3000c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.WBUseShieldREQ.class, needLogin = false)
	final public static int P_WB_USE_SHIELD_REQ = 0x3000d;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BWUseShieldRES.class, needLogin = false)
	final public static int P_BW_USE_SHIELD_RES = 0x3000e;

	//Game and Battle Protocols for Clients 0x0004xxxx, xxxx start from 0x0000
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GBRoomCreateREQ.class, needLogin = false)
	final public static int P_GB_ROOM_CREATE_REQ = 0x40001;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.BGRoomCreateRES.class, needLogin = false)
	final public static int P_BG_ROOM_CREATE_RES = 0x40002;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GBRoomEnterREQ.class, needLogin = false)
	final public static int P_GB_ROOM_ENTER_REQ = 0x40003;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.account.AccountProtoBuf.BGAccountModifyREQ.class, needLogin = false)
	final public static int P_BG_ACCOUNT_MODIFY_REQ = 0x40004;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.GBRoomQuitREQ.class, needLogin = false)
	final public static int P_GB_ROOM_QUIT_REQ = 0x40005;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.room.RoomProtoBuf.BGRoomQuitRES.class, needLogin = false)
	final public static int P_BG_ROOM_QUIT_RES = 0x40006;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GBStartTeamBattleREQ.class, needLogin = false)
	final public static int P_GB_START_TEAM_BATTLE_REQ = 0x40007;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.BGStartTeamBattleRES.class, needLogin = false)
	final public static int P_BG_START_TEAM_BATTLE_RES = 0x40008;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BGTeamGameOverRES.class, needLogin = false)
	final public static int P_BG_TEAM_GAME_OVER_RES = 0x40009;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.battle.BattleProtoBuf.BGSingleGameOverRES.class, needLogin = false)
	final public static int P_BG_SINGLE_GAME_OVER_RES = 0x4000a;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GBStartSingleBattleREQ.class, needLogin = false)
	final public static int P_GB_START_SINGLE_BATTLE_REQ = 0x4000b;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.BGStartSingleBattleRES.class, needLogin = false)
	final public static int P_BG_START_SINGLE_BATTLE_RES = 0x4000c;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.team.TeamProtoBuf.GBTmpDestoryRoomREQ.class, needLogin = false)
	final public static int P_GB_TMP_DESTORY_ROOM_REQ = 0x4000d;

	//Server Protocols 0x0005xxxx, xxxx start from 0x0000
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.server.ServerProtoBuf.SSPushServerInfoSYN.class, needLogin = false)
	final public static int P_SS_PUSH_SERVERINFO_SYN = 0x50001;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.server.ServerProtoBuf.SSPushBattleListSYN.class, needLogin = false)
	final public static int P_SS_PUSH_BATTLELIST_SYN = 0x50002;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.server.ServerProtoBuf.SSUpdateConfigSYN.class, needLogin = false)
	final public static int P_SS_UPDATE_CONFIG_SYN = 0x50003;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.server.ServerProtoBuf.SSRegisterServerREQ.class, needLogin = false)
	final public static int P_SS_REGISTER_SERVER_REQ = 0x50004;
	 @ProtocolConfigAnnotation(protocolClass = com.game.message.proto.server.ServerProtoBuf.SSRegisterServerRES.class, needLogin = false)
	final public static int P_SS_REGISTER_SERVER_RES = 0x50005;
		final public static int E_REGISTER_SERVER_SUCCESS = 0x150005;
		final public static int E_REGISTER_SERVER_INVALID_SERVER_NAME = 0x250005;

}
