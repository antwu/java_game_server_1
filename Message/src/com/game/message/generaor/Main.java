/*
  * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.game.message.generaor;

import java.io.IOException;

import org.dom4j.DocumentException;

public class Main 
{
	static public void main(String[] args) throws DocumentException, IOException
	{
		ClientProtocolsGenerator clientProtocolsGenerator = new ClientProtocolsGenerator("resources/ProtocolDesc.xml", "src/com/game/message/protocol/ProtocolsConfig.java");
		clientProtocolsGenerator.generateFile();

		ClientHelperProtocolsGenerator serverProtocolsGenerator = new ClientHelperProtocolsGenerator("resources/ProtocolDesc.xml", "resources/Protocols.cs");
		serverProtocolsGenerator.generateFile();
		System.out.println("message generator succeed!");
//		ClientLuaProtocolsGenerator luaProtocolsGenerator = new ClientLuaProtocolsGenerator("resource/ProtocolDesc.xml", "protocode/protocode.lua", "protocode/protocodetext.lua");
//		luaProtocolsGenerator.generateFile();
//		AlertMsgCreator creator = new AlertMsgCreator();
//		try
//        {
//	        creator.compileDescAll();
//        }
//        catch (Exception e)
//        {
//	        e.printStackTrace();
//        }
	}
	
}
