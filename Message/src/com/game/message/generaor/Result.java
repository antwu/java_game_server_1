package com.game.message.generaor;

public class Result
{
	private boolean isSuccess;
	private String name;
	private String desc;

	public Result(boolean isSuccess, String name, String desc)
	{
		super();
		this.isSuccess = isSuccess;
		this.name = name;
		this.desc = desc;
	}

	public boolean isSuccess()
	{
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess)
	{
		this.isSuccess = isSuccess;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDesc()
	{
		return desc;
	}
}
