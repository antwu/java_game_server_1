package com.game.core.session;

import com.game.core.net.common.RemoteNode;

public class Session
{
	private int sessionID;
	private RemoteNode remoteNode;

	private long upSequenceId;
	private long downSequenceId;
	
	private long clientUpSequenceId;
	private long clientDownSequenceId;

	public int getSessionID()
	{
		return sessionID;
	}
	void setSessionID(int sessionID)
	{
		this.sessionID = sessionID;
	}

	public RemoteNode getRemoteNode()
	{
		return remoteNode;
	}
	void setRemoteNode(RemoteNode remoteNode)
	{
		this.remoteNode = remoteNode;
	}
	public long getUpSequenceId()
	{
		return upSequenceId;
	}
	public void setUpSequenceId(long upSequenceId)
	{
		this.upSequenceId = upSequenceId;
	}
	public long getDownSequenceId()
	{
		return downSequenceId;
	}
	public void setDownSequenceId(long downSequenceId)
	{
		this.downSequenceId = downSequenceId;
	}
	public long getClientUpSequenceId()
	{
		return clientUpSequenceId;
	}
	public void setClientUpSequenceId(long clientUpSequenceId)
	{
		this.clientUpSequenceId = clientUpSequenceId;
	}
	public long getClientDownSequenceId()
	{
		return clientDownSequenceId;
	}
	public void setClientDownSequenceId(long clientDownSequenceId)
	{
		this.clientDownSequenceId = clientDownSequenceId;
	}
}
