package com.game.core.util;

import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class IOUtils {
	public static void writeLengthString(DataOutput dout, String content,
			String charset) throws IOException {
		if (content == null) {
			content = "";
		}
		byte[] buf = content.getBytes(charset);
		dout.writeShort(buf.length);
		dout.write(buf);
	}

	/**
	 * 关闭输入流，并忽略任何异常
	 * @param in
	 */
	public static void closeInputStream(InputStream in) {
		if (in != null) {
			try {
				in.close();
			} catch (Exception ignore) {
			}
		}
	}
	public static void closeQuietly(Socket sock) {
		// It's same thing as Apache Commons - IOUtils.closeQuietly()
		if (sock != null) {
		     try {
		        sock.close();
		     } catch (IOException e) {
		        // ignored
		    }
		}
	}
}
