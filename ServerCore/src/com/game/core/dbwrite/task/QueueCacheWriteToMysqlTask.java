package com.game.core.dbwrite.task;

import com.game.core.dbwrite.domain.QueueCache;

public class QueueCacheWriteToMysqlTask implements Task{

	@Override
	public void run() {
		QueueCache.writeToDatabase();
	}
}