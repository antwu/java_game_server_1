package com.game.core.dbwrite.task;

import com.game.core.dbwrite.domain.MemoryCache;

//周期性把数据从内存map中写入RocksDB
public class MemoryWriteToRocksDBTask implements Task{

	@Override
	public void run() {
		MemoryCache.writeToRocksDB();
	}
}