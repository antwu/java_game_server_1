package com.game.core.dbwrite.task;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TaskExecutor
{
	public static ScheduledThreadPoolExecutor startTask(Task task, long initalDelay, long delay)
	{
		ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
		executor.scheduleWithFixedDelay(task, initalDelay, delay, TimeUnit.MILLISECONDS);
		return executor;
	}

	public static void endTask(ScheduledThreadPoolExecutor executor)
	{
		if (executor != null)
			executor.shutdown();
	}
}