package com.game.core.dbwrite.task;

import com.game.core.dbwrite.domain.Storage;

//周期性把数据从RocksDB中存入MySQL
public class RocksDBWriteToMysqlTask implements Task{

	@Override
	public void run(){
		Storage.writeToDatabase();
	}
}