package com.game.core.dbwrite.task;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.dbcs.DBCS;
import com.game.core.dbcs.daoproxy.IDaoExecutor;
import com.game.core.dbwrite.constant.DBOperationType;

public class DirectWriteToDBUtil
{
	public static final Logger logger = LoggerFactory.getLogger(DirectWriteToDBUtil.class);
	private static Map<String, Method> methodMap = new HashMap<String, Method>();
	private static Map<String, Object> daoMap = new HashMap<String, Object>();

	@SuppressWarnings("unchecked")
	public static void directWriteTODB(Class<?> clazz, DBOperationType type, Object bean)
	{
		String beanPath = clazz.getName();
		String[] splitBeanPath = beanPath.split("\\.");
		if (splitBeanPath == null || splitBeanPath.length < 3)
		{
			logger.debug("Bean path is not correct.");
			return;
		}

		String daoPath = "com.game.dbpersistence." + splitBeanPath[splitBeanPath.length - 3] + ".dao.I" + splitBeanPath[splitBeanPath.length - 1] + "Dao";
		// logger.debug("IDao path: " + daoPath);
		String methodName = "";
		switch (type)
		{
		case INSERT:
			methodName = "insert" + splitBeanPath[splitBeanPath.length - 1];
			break;
		case UPDATE:
			methodName = "update" + splitBeanPath[splitBeanPath.length - 1];
			break;
		case DELETE:
			methodName = "delete" + splitBeanPath[splitBeanPath.length - 1];
			break;
		}

		if (methodMap.containsKey(methodName) && daoMap.containsKey(daoPath))
		{
			Method m = methodMap.get(methodName);
			Object dao = daoMap.get(daoPath);
			if (m != null && dao != null)
			{
				try
				{
					m.invoke(dao, bean);
					// logger.debug("Execute Map." + methodName);
				}
				catch (IllegalAccessException e)
				{
					// e.printStackTrace();
					logger.error("exception : ", e);
				}
				catch (IllegalArgumentException e)
				{
					// e.printStackTrace();
					logger.error("exception : ", e);
				}
				catch (InvocationTargetException e)
				{
					// e.printStackTrace();
					logger.error("exception : ", e);
				}
			}
			else
			{
				logger.debug(daoPath + "no such Method.");
			}
		}
		else
		{ // 如果方法不在Map中，通过反射获取
			Class<IDaoExecutor> daoClazz = null;
			try
			{
				daoClazz = (Class<IDaoExecutor>) Class.forName(daoPath);
			}
			catch (Exception e)
			{
				logger.error("exception : ", e);
			}
			if (daoClazz == null)
			{
				// logger.debug("Reflect IDao " + daoPath + " failed.");
				return;
			}
			Object dao = DBCS.getExector(daoClazz);
			if (dao == null)
			{
				logger.debug("DBCS getExecutor Dao is null.");
				return;
			}
			daoMap.put(daoPath, dao);

			try
			{
				Method m = daoClazz.getMethod(methodName, clazz);
				if (m != null)
				{
					methodMap.put(methodName, m);
					m.invoke(dao, bean);
				}
				else
				{
					logger.debug(daoClazz.getName() + "no such Method.");
				}
			}
			catch (Exception e)
			{
//				e.printStackTrace();
				logger.error(daoClazz.getName() + " get Method failed.",e);
			}
		}
	}
}
