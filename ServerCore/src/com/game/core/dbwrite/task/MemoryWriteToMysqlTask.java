package com.game.core.dbwrite.task;

import com.game.core.dbwrite.domain.MemoryCache;

/**
 * 周期性把数据从内存map中存入MySQL
 * @author koduser
 */
public class MemoryWriteToMysqlTask implements Task
{
	@Override
	public void run()
	{
		MemoryCache.writeToDatabase();
	}
}