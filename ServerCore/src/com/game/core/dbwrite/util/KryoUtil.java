package com.game.core.dbwrite.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

// 使用Kryo工具进行序列化和反序列化
public class KryoUtil
{
	private static Kryo kryo = new Kryo();
	public static final int BUFF_SIZE = 1024 * 1024;

	public static synchronized byte[] serilize(Class<?> clazz, Object obj)
	{
		Output output = new Output(new ByteArrayOutputStream(), BUFF_SIZE);
		kryo.register(clazz);
		kryo.writeObject(output, obj);
		byte[] b = output.toBytes();
		output.flush();
		output.close();
		return b;
	}

	public static synchronized Object deSerilize(Class<?> clazz, byte[] b)
	{
		Object obj = null;
		Input input = new Input(new ByteArrayInputStream(b), BUFF_SIZE);
		obj = kryo.readObject(input, clazz);
		input.close();
		return obj;
	}
}