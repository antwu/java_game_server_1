package com.game.core.dbwrite.constant;

/*
 * MEMORY_ONLY    在内存中做缓存，不通过RocksDB
 * ROCKSDB_ONLY   直接通过RocksDB做对象融合
 * MEMORY_ROCKSDB 先在内存中缓存，再通过RocksDB做对象融合
 * QUEUE_CACHE    队列缓存，通过队列的方式缓存Bean对象
 * DIRECT_DB	      直接存储到数据库
 * NONE           缓存开关，即不使用缓存
 */
public enum CacheOperationType {
	 MEMORY_ONLY,ROCKSDB_ONLY,MEMORY_ROCKSDB,QUEUE_CACHE,DIRECT_DB,NONE
}