package com.game.core.dbwrite.server;

public class DBServiceParemeter
{
	public static long memoryToMysqlUpdateTime 					= 1000;          	// 内存写入MySql的周期时间
	public static long memoryToRocksDBUpdateTime 				= 2000;        		// 内存写入RocksDB的周期时间
	public static long rocksDBToMysqlUpdateTime 				= 5000;         	// RocksDB写入MySql的周期时间
	public static long queueCacheToMysqlUpdateTime 				= 2000;      		// QueueCache写入MySql的周期时间
	
	public static final int WRITE_TO_DB_ITEM					= 500;				// 一次异步写入数据库的最大条数
}
