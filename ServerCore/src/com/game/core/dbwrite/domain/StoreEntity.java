package com.game.core.dbwrite.domain;

import com.game.core.dbwrite.constant.DBOperationType;

public class StoreEntity
{
	// 封装被序列化的Bean
	private Class<?> clazz;                       // 被序列化类类型
	private DBOperationType operationType;        // 数据库操作类型
	private byte[] serilizedClass;                // 序列化类的字节

	public StoreEntity()
	{
	}

	public StoreEntity(Class<?> clazz, DBOperationType operationType, byte[] serilizedClass)
	{
		this.clazz = clazz;
		this.operationType = operationType;
		this.serilizedClass = serilizedClass;
	}

	public Class<?> getClazz()
	{
		return clazz;
	}

	public void setClazz(Class<?> clazz)
	{
		this.clazz = clazz;
	}

	public DBOperationType getOperationType()
	{
		return operationType;
	}

	public void setOperationType(DBOperationType operationType)
	{
		this.operationType = operationType;
	}

	public byte[] getSerilizedClass()
	{
		return serilizedClass;
	}

	public void setSerilizedClass(byte[] serilizedClass)
	{
		this.serilizedClass = serilizedClass;
	}
}