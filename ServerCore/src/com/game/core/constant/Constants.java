package com.game.core.constant;

public class Constants
{
	public final static byte TRUE = 1;
	public final static byte FALSE = 0;
	
	/** 房间所有牌局结束 ,0, 房间所有牌局结束 */
	public final static int DESTORY_ROOM_TYPE_FINISH = 0;
	public final static String DESTORY_ROOM_TYPE_DES_FINISH = "房间所有牌局结束";
	/** 房间销毁方式 , 1, 玩家主动销毁 */
	public final static int DESTORY_ROOM_TYPE_OWNER = 1;
	public final static String DESTORY_ROOM_TYPE_DES_OWNER = "玩家主动销毁/强制解散房间";
	/** 房间销毁方式  , 2, 投票解散 */
	public final static int DESTORY_ROOM_TYPE_VOTE = 2;
	public final static String DESTORY_ROOM_TYPE_DES_VOTE = "投票解散";
	
	/**
	 * @author kod
	 *
	 */
	public enum RoomRoundCount
	{
		ONE_GAMES(0, 0, 8, 1), // 贵阳玩法，8局
		TWO_GAMES(0, 1, 16, 2), // 贵阳玩法，16局
		THREE_GAMES(1, 0, 8, 1), // 遵义玩法，8局
		FOUR_GAMES(1, 1, 16, 2), // 遵义玩法，16局
		ANSHUN_GAMES_8(2, 0 , 8 , 1), // 安顺8局
		ANSHUN_GAMES_16(2, 1, 16, 2), // 安顺16局
		TIANJIN_GAMES_8(3, 0, 8, 1), // 天津玩法
		;
		
		private int battleType = 0;
		private int countType = 0;
		private int roundCount = 0;
		private int cardCount = 0;

		private RoomRoundCount(int battleType, int countType, int roundCount, int cardCount)
		{
			this.battleType = battleType;
			this.countType = countType;
			this.roundCount = roundCount;
			this.cardCount = cardCount;
		}

		public static RoomRoundCount getValue(int battleType, int countType)
		{
			for (RoomRoundCount value : RoomRoundCount.values())
			{
				if (value.battleType == battleType && value.countType == countType)
					return value;
			}

			return null;
		}

		public int getCountType()
		{
			return countType;
		}

		public int getRoundCount()
		{
			return roundCount;
		}

		public int getCardCount()
		{
			return cardCount;
		}
	}
}
