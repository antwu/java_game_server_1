package com.game.core.constant;

public class RoomRuleBI
{
	private final static String SPLIT_CHAR = ",";
	private String name;
	private String group;
	private int trueValue;
	private int falseValue;

	public RoomRuleBI(String name, String value)
	{
		this.name = name;
		if (value.contains(SPLIT_CHAR))
		{
			String[] temp = value.split(SPLIT_CHAR);
			this.group = temp[0];
			this.trueValue = Integer.parseInt(temp[1]);
			this.falseValue = Integer.parseInt(temp[2]);
		}
		else
		{
			this.trueValue = Integer.parseInt(value);
			this.falseValue = trueValue;
		}
		this.group = String.valueOf(trueValue / 100 * 100);

	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getGroup()
	{
		return group;
	}

	public void setGroup(String group)
	{
		this.group = group;
	}

	public int getTrueValue()
	{
		return trueValue;
	}

	public void setTrueValue(int trueValue)
	{
		this.trueValue = trueValue;
	}

	public int getFalseValue()
	{
		return falseValue;
	}

	public void setFalseValue(int falseValue)
	{
		this.falseValue = falseValue;
	}

//	@Override
//	public String toString()
//	{
//		StringBuffer sb = new StringBuffer();
//		
//		sb.append("[name=").append(name);
//		sb.append(",group=").append(group);
//		sb.append(",trueValue=").append(trueValue);
//		sb.append(",falseValue=").append(falseValue);
//		sb.append("]");
//		
//		return  sb.toString();
//	}
}
