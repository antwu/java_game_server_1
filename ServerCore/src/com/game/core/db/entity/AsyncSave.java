package com.game.core.db.entity;

import com.game.core.db.service.proxy.EntityProxyWrapper;

/**
 * Created by  on 17/3/29. 
 * 异步存储
 */
public interface AsyncSave {
	// 用于记录数据库封装对象
	public EntityProxyWrapper getEntityProxyWrapper();

	public void setEntityProxyWrapper(EntityProxyWrapper entityProxyWrapper);
}
