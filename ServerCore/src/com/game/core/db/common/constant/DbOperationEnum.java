package com.game.core.db.common.constant;

/**
 * Created by  on 2017/3/23. 
 * 数据存储操作
 */
public enum DbOperationEnum {
	insert, 
	update, 
	query, 
	queryList, 
	delete, 
	insertBatch, 
	updateBatch, 
	deleteBatch,;
}
