package com.game.core.db.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.game.core.db.common.constant.DbOperationEnum;

/**
 * Created by  on 2017/3/23.
 * 数据存储操作
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DbOperation {
	/**
	 * @return
	 */
	DbOperationEnum operation();
}
