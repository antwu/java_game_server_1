package com.game.core.db.service.jedis.exceptions;

import org.slf4j.Logger;

import com.game.core.db.common.util.Loggers;

public class JedisException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	protected static Logger logger = Loggers.jedisLogger;

	public JedisException(String message) {
		super(message);
		logger.error(message);
	}

	public JedisException(Throwable e) {
		super(e);
		logger.error(e.toString());
	}

	public JedisException(String message, Throwable cause) {
		super(message, cause);
		logger.error(message + cause.toString());
	}
}
