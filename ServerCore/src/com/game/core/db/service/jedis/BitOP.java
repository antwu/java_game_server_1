package com.game.core.db.service.jedis;

public enum BitOP {
	AND, OR, XOR, NOT;
}
