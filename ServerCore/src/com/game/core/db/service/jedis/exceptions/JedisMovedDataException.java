package com.game.core.db.service.jedis.exceptions;

import com.game.core.db.service.jedis.HostAndPort;

public class JedisMovedDataException extends JedisRedirectionException {
	private static final long serialVersionUID = 1L;

	public JedisMovedDataException(String message, HostAndPort targetNode, int slot) {
		super(message, targetNode, slot);
	}

	public JedisMovedDataException(Throwable cause, HostAndPort targetNode, int slot) {
		super(cause, targetNode, slot);
	}

	public JedisMovedDataException(String message, Throwable cause, HostAndPort targetNode, int slot) {
		super(message, cause, targetNode, slot);
	}
}
