package com.game.core.db.service.jedis;

public abstract class Builder<T> {
	public abstract T build(Object data);
}
