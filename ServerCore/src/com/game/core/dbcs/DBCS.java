package com.game.core.dbcs;

import com.game.core.dbcs.config.DbConfigInfo;
import com.game.core.dbcs.daoproxy.IDaoExecutor;
import com.game.core.dbcs.dbspace.DBSpace;
import com.game.core.dbcs.executor.ExecutorManager;
import com.game.core.dbcs.interceptor.IDbOprateIntereptor;
/**
 * 分布式数据库代理服务
 * @author 
 * 
 */
public class DBCS
{
	private final static String DB_CONFIG_PATH = "";
	private static IDbOprateIntereptor _intereptor;
	
	public static void initialize(IDbOprateIntereptor intereptor, DbConfigInfo... dbConfigInfo)
	{
		_intereptor = intereptor;
		DBSpace.initialize(DB_CONFIG_PATH, dbConfigInfo);
		ExecutorManager.initialize(dbConfigInfo);
	}

	public static <T extends IDaoExecutor> T getExector(Class<T> interfaceClass)
	{
		return ExecutorManager.getExector(interfaceClass);
	}
	
	public static void startTransaction()
	{
		ExecutorManager.getExector(IDaoExecutor.class).startTransaction();
	}
	
	public static void commitTransaction()
	{
		ExecutorManager.getExector(IDaoExecutor.class).commitTransaction();
	}
	
	public static void endTransaction()
	{
		ExecutorManager.getExector(IDaoExecutor.class).endTransaction();
	}
	
	public static IDbOprateIntereptor getIntereptor()
	{
		return _intereptor;
	}
	
}
