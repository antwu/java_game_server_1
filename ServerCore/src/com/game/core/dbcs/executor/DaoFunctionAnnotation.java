package com.game.core.dbcs.executor;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DAO接口函数数据库映射处理器 包含参数列表、数据库映射规则信息
 * 
 * @author 
 * 
 */
public class DaoFunctionAnnotation
{
	private static final Logger logger = LoggerFactory.getLogger(DaoFunctionAnnotation.class);

	private String functionName;
	private int paramCount;
	private String[] paramList;
	private boolean valid;

	public DaoFunctionAnnotation(String functionname, String annotationinfo, Class<?>[] args)
	{
		this.functionName = functionname;

		int argCount = (args != null) ? args.length : 0;
		if (annotationinfo == null || annotationinfo.length() <= 0)
			paramList = new String[0];
		else
			paramList = annotationinfo.split(",");

		if (argCount > 1 && paramList.length != args.length)
		{
			logger.error("annotation syntax of " + functionName + " error,field count not equal args count");
			paramCount = 0;
			valid = false;
			return;
		}

		if (paramList.length == 0)
		{
			paramCount = args.length;
			valid = true;
			return;
		}
		else
		{
			paramCount = args.length;
			valid = true;

			int i = 0;
			while (valid && i < paramCount)
			{
				if (paramList[i] == null || paramList[i].length() <= 0)
					valid = false;
				i++;
			}
		}
	}

	@SuppressWarnings("unused")
	private boolean isSimpleClass(Class<?> classtype)
	{
		return (classtype != String.class && classtype != Integer.class && classtype != int.class
				&& classtype != Byte.class && classtype != byte.class && classtype != Long.class
				&& classtype != long.class && classtype != Short.class && classtype != short.class);
	}

	public int getParamCount()
	{
		return paramCount;
	}

	public String[] getParamList()
	{
		return paramList;
	}

	public boolean isValid()
	{
		return valid;
	}

	public Object createStatementMap(Object[] args)
	{
		Object tmpParam = null;
		if (this.paramCount > 0 && paramList != null)
		{
			Map<String, Object> tmpMap = new HashMap<String, Object>();
			for (int i = 0; i < paramList.length; i++)
			{
				tmpMap.put(paramList[i], args[i]);
			}
			tmpParam = tmpMap;
			tmpMap = null;
		}
		return tmpParam;
	}
}
