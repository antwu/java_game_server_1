package com.game.core.dbcs.config;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.dbcs.dbspace.DBZone;

/**
 * 数据库配置文件列表解析器
 * 
 * @author 
 * 
 */
public class DbConfigXmlReader extends AbstractTagFileReader
{
	private static final Logger logger = LoggerFactory.getLogger(DbConfigXmlReader.class);

	private static final String MYSQL_IP_TAG = "mysqlIp";
	private static final String MYSQL_DB_NAME_TAG = "mysqlDbName";
	private static final String MYSQL_USER_NAME_TAG = "mysqlUserName";
	private static final String MYSQL_PASSWORD_TAG = "mysqlPassword";

	public static final String SUFFIX = ".xml";

	private DBZone refDbZone;
	private String content;

	public DbConfigXmlReader(DBZone dbzone)
	{
		this.refDbZone = dbzone;
		this.content = null;
	}

	@Override
	public String getContent()
	{
		if (content == null)
		{
			try
			{
				content = this.read(refDbZone.getDataSourceConfig());
			}
			catch (IOException e)
			{
				content = null;
//				e.printStackTrace();
				logger.error("failed to getContent ", e);
			}
		}
		return content;
	}

	@Override
	public String onTag(String tag)
	{
		if (MYSQL_IP_TAG.equals(tag))
			return refDbZone.getIp();
		else if (MYSQL_DB_NAME_TAG.equals(tag))
			return refDbZone.getDbName();
		else if (MYSQL_USER_NAME_TAG.equals(tag))
			return refDbZone.getUserName();
		else if (MYSQL_PASSWORD_TAG.equals(tag))
			return refDbZone.getPassword();
		else
			return null;
	}
}
