package com.game.core.dbcs.config;

public class DbConfigInfo {
	private String ip;
	private String dbName;
	private String name;
	private String password;
	private String sqlMapXMLPath;
	
	public DbConfigInfo(String ip,String dbName,String name,String password,String sqlMapXMLPath){
		this.ip=ip;
		this.dbName=dbName;
		this.name=name;
		this.password=password;
		this.sqlMapXMLPath=sqlMapXMLPath;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSqlMapXMLPath() {
		return sqlMapXMLPath;
	}

	public void setSqlMapXMLPath(String sqlMapXMLPath) {
		this.sqlMapXMLPath = sqlMapXMLPath;
	}

}