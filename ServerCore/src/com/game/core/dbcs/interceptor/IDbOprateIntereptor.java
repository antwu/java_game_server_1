package com.game.core.dbcs.interceptor;

public interface IDbOprateIntereptor
{
	public void before(String statementId,Object[] params);
	
	public Object after(String statementId,Object[] params,Object result);
}
