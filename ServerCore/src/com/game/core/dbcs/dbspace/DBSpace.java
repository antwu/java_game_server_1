package com.game.core.dbcs.dbspace;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.dbcs.DBCSConst;
import com.game.core.dbcs.config.DbConfigInfo;

/**
 * 数据库连接信息集合 持有连接、映射规则等信息
 * @author
 */
public class DBSpace
{
	private static Logger logger = LoggerFactory.getLogger(DBSpace.class);
	private static String DB_CONFIG_PATH = DBCSConst.DB_CONFIG_PATH;

	private static Map<String,DBZone> dbZones = new HashMap<String,DBZone>();

	public static DBZone getDBZone(String path)
	{
		return dbZones.get(path);
	}

	public static boolean initialize(String dbconfigpath, DbConfigInfo... dbConfigInfo)
	{
		try
		{
			if (dbconfigpath != null)
				DB_CONFIG_PATH = dbconfigpath;
			System.setProperty("java.naming.factory.initial", "org.apache.naming.java.javaURLContextFactory");
			System.setProperty("java.naming.factory.url.pkgs", "org.apache.naming");
            
			for(DbConfigInfo config : dbConfigInfo){
				DBZone dbZone = new DBZone(config.getIp(), config.getDbName(), config.getName(), config.getPassword());
				if(dbZone!=null){
					dbZone.loadConfig(DB_CONFIG_PATH + "/" + config.getSqlMapXMLPath());
					dbZone.loadSqlMap();
					dbZones.put(config.getSqlMapXMLPath(), dbZone);
				}
			}
			return true;
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			logger.error("failed to initialize DBSpace, ",e);
			
		}
		return false;
	}

}
