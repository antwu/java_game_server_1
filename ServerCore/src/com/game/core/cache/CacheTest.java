package com.game.core.cache;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CacheTest {
	
	public static class BasePlayer implements IBasePlayer
	{
		private long playerID;
		private String name;
		private int level;

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getLevel() {
			return level;
		}
		public void setLevel(int level) {
			this.level = level;
		}

		@Override
        public long getPlayerId()
        {
	        return playerID;
        }

        @Override
		public void setPlayerId(long playerId)
        {
	        // TODO Auto-generated method stub
			this.playerID = playerId;
	        
        }
		@Override
        public String getPlayerName()
        {
	        // TODO Auto-generated method stub
	        return name;
        }
		@Override
		public void setPlayerName(String playerName)
		{
			// TODO Auto-generated method stub
			
		}
	}
	
	public static class FullPlayer implements IFullPlayer<BasePlayer>
	{
		BasePlayer basePlayer;
		
		public long getPlayerID() {
			return basePlayer.getPlayerId();
		}
		public void setPlayerID(long playerID) {
			basePlayer.setPlayerId(playerID);;
		}
		public String getName() {
			return basePlayer.getName();
		}
		public void setName(String name) {
			basePlayer.setName(name);;
		}
		public int getLevel() {
			return basePlayer.getLevel();
		}
		public void setLevel(int level) {
			basePlayer.setLevel(level);;
		}

	
		@Override
        public void setBasePlayer(BasePlayer basePlayer)
        {
	        // TODO Auto-generated method stub
			this.basePlayer = basePlayer;
	        
        }	
	}
	
	public class ConcurrentPlayer implements IConcurrentPlayer
	{
		
	}
	
	public static class CacheLoaderFromDB implements ICacheLoader<BasePlayer, FullPlayer, ConcurrentPlayer>
	{

		@Override
		public List<BasePlayer> loadAllBasePlayer() {
			// TODO Auto-generated method stub
			List<BasePlayer> players = new ArrayList<BasePlayer>();
			BasePlayer basePlayer1 = new BasePlayer();
			basePlayer1.setLevel(1);
			basePlayer1.setPlayerId(1);
			basePlayer1.setName("player1");
			BasePlayer basePlayer2 = new BasePlayer();
			basePlayer2.setLevel(1);
			basePlayer2.setPlayerId(2);
			basePlayer2.setName("player2");
			players.add(basePlayer1);
			players.add(basePlayer2);
			return players;
		}

		@Override
		public BasePlayer loadBasePlayer(long playerID) {
			// TODO Auto-generated method stub
			BasePlayer basePlayer = new BasePlayer();
			basePlayer.setPlayerId(playerID);
			basePlayer.setName("player"+playerID);
			basePlayer.setLevel(1);
			return basePlayer;
		}


		@Override
		public FullPlayer loadFullPlayer(long playerID) {
			// TODO Auto-generated method stub
			FullPlayer fullPlayer = new FullPlayer();
			return fullPlayer;
		}


		@Override
        public ConcurrentPlayer loadConcurrentPlayer(long playerID)
        {
	        // TODO Auto-generated method stub
	        return null;
        }

		@Override
        public BasePlayer loadBasePlayer(String name)
        {
	        // TODO Auto-generated method stub
	        return null;
        }

		@Override
        public FullPlayer loadFullPlayer(String name)
        {
	        // TODO Auto-generated method stub
	        return null;
        }

		@Override
        public ConcurrentPlayer loadConcurrentPlayer(String name)
        {
	        // TODO Auto-generated method stub
	        return null;
        }
		
	}
	
	public static class Thread1 extends Thread
	{
		public PlayerCache<BasePlayer,FullPlayer, ConcurrentPlayer> cache;
		@Override
		public void run()
		{
			cache.getWritablePlayer(1);
			System.out.println("Got writable player 1");
			cache.releasePlayer(1);
		}
	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws InterruptedException, IOException {
		CacheLoaderFromDB loaderFromDB = new CacheLoaderFromDB();
		PlayerCache<BasePlayer,FullPlayer, ConcurrentPlayer> cache = new PlayerCache<BasePlayer, FullPlayer, ConcurrentPlayer>();
		
		cache.init(10, 100, loaderFromDB,true);
		cache.setMaxLockTime(100);
		cache.loadAllBasePlayer();
		//cache.getReadOnlyPlayer(1);
		cache.getWritablePlayer(2);
		cache.getWritablePlayer(2);
		cache.releaseAllPlayersInCurrentThread();
		cache.getWritablePlayer(1).getFullPlayer();
		Thread1 t1 = new Thread1();
		t1.cache = cache;
		t1.start();
		Thread.currentThread().sleep(10000);
		cache.releasePlayer(1);
		cache.getReadOnlyPlayer(10).getFullPlayer();
		cache.getReadOnlyPlayer(11).getFullPlayer();
		cache.getReadOnlyPlayer(12).getFullPlayer();
		cache.getReadOnlyPlayer(13).getFullPlayer();
		cache.getReadOnlyPlayer(14).getFullPlayer();
		cache.getReadOnlyPlayer(15).getFullPlayer();
		cache.getReadOnlyPlayer(16).getFullPlayer();
		cache.getReadOnlyPlayer(17).getFullPlayer();
		cache.getReadOnlyPlayer(18).getFullPlayer();
		cache.getReadOnlyPlayer(19).getFullPlayer();
		cache.getReadOnlyPlayer(20).getFullPlayer();
		cache.getReadOnlyPlayer(21).getFullPlayer();
		cache.getReadOnlyPlayer(22).getFullPlayer();
		cache.getReadOnlyPlayer(23).getFullPlayer();
		cache.getReadOnlyPlayer(24).getFullPlayer();
		cache.getReadOnlyPlayer(25).getFullPlayer();
    	cache.releaseAllPlayersInCurrentThread();
    	
		cache.containPlayer("player20");
		Thread.currentThread().sleep(10000);
		cache.getReadOnlyPlayer(20).getFullPlayer();
		
		System.in.read();
		
	}
	

}
