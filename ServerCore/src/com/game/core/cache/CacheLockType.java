package com.game.core.cache;

public enum CacheLockType
{
	READ,
	WRITE,
	UNLOCK,
	;
}
