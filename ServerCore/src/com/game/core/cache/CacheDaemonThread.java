package com.game.core.cache;

import java.util.Map.Entry;
import java.util.Set;

public class CacheDaemonThread<B extends IBasePlayer,F extends IFullPlayer<B>, C extends IConcurrentPlayer> implements Runnable{
	private PlayerCache<B, F, C> playerCache;
	private Long oldestTime = System.currentTimeMillis();

	public CacheDaemonThread(PlayerCache<B, F, C> playerCache) {
		this.playerCache = playerCache;
	}
	
	@Override
	public void run() {
		int currentOfflinePlayersNumber =playerCache.getOfflinePlayers().size(); 
		if (currentOfflinePlayersNumber < this.playerCache.getMaxOfflinePlayerCount())
		{
			return;
		}
		
		long interval = 1000*150; //2.5m
		int removedNumber = 0;
		int needToRemoveNumber = (int)(currentOfflinePlayersNumber - this.playerCache.getMaxOfflinePlayerCount() *0.9);		
		
		do{
			needToRemoveNumber -= removedNumber;
			interval = interval*2;
			if (interval > 1000*3600*24) // > 24h
			{
				break;
			}
			removedNumber = removeOldPlayers(needToRemoveNumber, interval);
		}while (removedNumber < needToRemoveNumber);
	
	}
	
	public int removeOldPlayers(int needToRemoveNumber, long interval)
	{
		int removedNumber = 0;
		long _oldestTime = System.currentTimeMillis();;
		Set<Entry<Long, Long>> entries = playerCache.getOfflinePlayers().entrySet();
		for (Entry<Long, Long> entry : entries)
		{
			if (removedNumber < needToRemoveNumber)
			{
				if ( entry.getValue() - oldestTime < interval)
				{
					PlayerNode<B, F, C> node = playerCache.getWritablePlayer(entry.getKey());
					
					if ( node.isOnline() == false )
					{
						F fp = node.removeFullPlayer();					
						playerCache.getOfflinePlayers().remove(entry.getKey());
						playerCache.getRockDBUtil().saveFullPlayer(fp);
						removedNumber++;
						playerCache.releasePlayer(entry.getKey());
						continue;
					}
					playerCache.releasePlayer(entry.getKey());
				}
			}
			
			if (entry.getValue()<_oldestTime)
			{
				_oldestTime = entry.getValue();
			}
		}
		oldestTime = _oldestTime;	
		
		return removedNumber;
	}

}
