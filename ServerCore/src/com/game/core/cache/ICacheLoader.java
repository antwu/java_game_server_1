package com.game.core.cache;

import java.util.List;

public interface ICacheLoader<B extends IBasePlayer, F extends IFullPlayer<B>, C extends IConcurrentPlayer>
{
	/**
	 * invoke the PlayerCache.addBasePlayer in the loadAllBasePlayer
	 */
	List<B> loadAllBasePlayer();

	B loadBasePlayer(long playerID);

	F loadFullPlayer(long playerID);
	
	C loadConcurrentPlayer(long playerID);
	
	B loadBasePlayer(String name);

	F loadFullPlayer(String name);
	
	C loadConcurrentPlayer(String name);
}
