package com.game.core.cache;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * <P>
 * 在游戏逻辑中用的玩家数据都从PlayerCache中获取
 * 
 * <p>
 * 如果SimplePlayer中用到BasePlayer中的数据，必须用同一份，不能有两份拷贝；FullPlayer也是同理<br>
 * 在使用Cache前，必须先调用init，再调用loadAllBasePlayer加载所有玩家的BasePlayer。如果运行中途，增加了新的Player，
 * 可以调用addBasePlayer添加到cache中<br>
 * 在一个线程里，如果需要对玩家数据进行修改，请在一开始就调用
 * getWritablePlayer获取对应的玩家，并且按照playerID从小到大的顺序获取，否则会导致 CacheLockException<br>
 * 如果只是对player进行读操作，可以随时
 * 调用getReadOnlyPlayer或者玩家数据，对playerID的顺序没有要求。在同一个线程里，如果开始调用getReadOnlyPlayer后
 * ，必须把所有的readonlyPlayer都release后，才能再调用getWritablePlayer，否则会导致
 * CacheLockException<br>
 * 在一个Request处理完后，
 * 要记得调用releaseAllPlayersInCurrentThread或者releasePlayer将PlayerNode释放回cache
 * 。长时间得到PlayerNode而不释放，会导致CacheLockException<br>
 * 在一个线程里，可以对同一个PlayerID，调用多次getReadOnlyPlayer<br>
 * 在一个线程里，可以对同一个PlayerID，调用多次getWritablePlayer<br>
 * 在一个线程里，可以对同一个PlayerID，先调用getWritablePlayer，再调用getReadOnlyPlayer，反之则不行
 * 
 * @author marui
 * 
 * @param <BasePlayer>
 *            所有的BasePlayer都是在服务器初始化的时候，从数据库中加载，并且常驻内存
 * @param <SimplePlayer>
 *            SimplePlayer是需要的时候从内存加载，一旦加载就常驻内存，不管这个玩家是否在线
 * @param <FullPlayer>
 *            FullPlayer是需要的时候再从二级缓存中加载，如果二级缓存中不存在就从数据库中加载。当玩家离线的时候，
 *            通过maxOfflinePlayerCount和offlinePlayerExpiredTime的控制
 *            ，Fullplayer可能会从内存cahce中移除到二级缓存中
 * 
 */
public class PlayerCache<B extends IBasePlayer, F extends IFullPlayer<B>, C extends IConcurrentPlayer>
{
	//TODO:使用logger出错
	//private static final Logger logger = Logger.getLogger(PlayerCache.class);
	private ConcurrentHashMap<Long, PlayerNode<B, F, C>> players;
	private ConcurrentHashMap<String, PlayerNode<B, F, C>> players2;
	private ConcurrentHashMap<Long, Long> onlinePlayers;
	private ConcurrentHashMap<Long, Long> offlinePlayers;
	ScheduledExecutorService scheduler = Executors
	        .newSingleThreadScheduledExecutor();

	private int maxOfflinePlayerCount = 1000;
	private int offlinePlayerExpiredTime = 600;// TimeUnit.second
	private int maxLockTime = 10; // TimeUnit.second
	private boolean isDebug = false;
	private ICacheLoader<B, F, C> dbCacheLoader;
	private RockDBUtil<F> level2CachUtil = new RockDBUtil<F>();

	private ThreadLocal<HashSet<Long>> readLockedPlayersInCurrentThread = new ThreadLocal<HashSet<Long>>();
	private ThreadLocal<HashSet<Long>> writeLockedPlayersInCurrentThread = new ThreadLocal<HashSet<Long>>();
	private ThreadLocal<Long> firstLockTimestamp = new ThreadLocal<Long>();

	public void setDBCacheLoader(ICacheLoader<B, F, C> cacheLoader)
	{
		this.dbCacheLoader = cacheLoader;
		Executors.newSingleThreadScheduledExecutor();
	}

	ICacheLoader<B, F, C> getDBCacheLoader()
	{
		return this.dbCacheLoader;
	}

	RockDBUtil<F> getRockDBUtil()
	{
		return this.level2CachUtil;
	}

	public void setMaxOfflinePlayerCount(int count)
	{
		this.maxOfflinePlayerCount = count;
	}

	public int getMaxOfflinePlayerCount()
	{
		return this.maxOfflinePlayerCount;
	}

	/**
	 * 
	 * @param expiredTime
	 *            TimeUnit.second
	 */
	public void setOfflinePlayerExpiredTime(int expiredTime)
	{
		this.offlinePlayerExpiredTime = expiredTime;
	}

	public int getOfflinePlayerExpiredTime()
	{
		return this.offlinePlayerExpiredTime;
	}

	/**
	 * 设置锁可以持有的最大时间，如果持有时间大于最大持有时间，则认为是忘记释放锁了，会出发异常
	 * @return
	 */
	public int getMaxLockTime()
	{
		return this.maxLockTime;
	}

	/**
	 * 
	 * @param maxTime
	 *            TimeUnit.second
	 */
	public void setMaxLockTime(int maxTime)
	{
		this.maxLockTime = maxTime;
	}

	ConcurrentHashMap<Long, Long> getOfflinePlayers()
	{
		return offlinePlayers;
	}

	ConcurrentHashMap<Long, Long> getOnlinePlayers()
	{
		return onlinePlayers;
	}

	/**
	 * 在使用Cache前，必须先init，再loadAllBasePlayer
	 */
	public void init(int maxOfflinePlayerCount, int offlinePlayerExpiredTime,
	        ICacheLoader<B, F, C> dbCacheLoader, boolean isDebug)
	{
		this.maxOfflinePlayerCount = maxOfflinePlayerCount;
		this.offlinePlayerExpiredTime = offlinePlayerExpiredTime;
		this.isDebug = isDebug;
		this.dbCacheLoader = dbCacheLoader;
		this.players = new ConcurrentHashMap<Long, PlayerNode<B, F, C>>();
		this.players2 = new ConcurrentHashMap<String, PlayerNode<B, F, C>>();
		this.onlinePlayers = new ConcurrentHashMap<Long, Long>();
		this.offlinePlayers = new ConcurrentHashMap<Long, Long>();
		scheduler.scheduleAtFixedRate(new CacheDaemonThread<B, F, C>(this), 5,
		        1, TimeUnit.MINUTES);
	}

	/**
	 * 在使用Cache前，必须先init，再loadAllBasePlayer
	 */
	public void loadAllBasePlayer()
	{
		List<B> basePlayers = this.dbCacheLoader.loadAllBasePlayer();
		long currentTime = System.currentTimeMillis();
		if (basePlayers == null || basePlayers.size() <= 0)
			return;
		for (B b : basePlayers)
		{
			PlayerNode<B, F, C> node = new PlayerNode<B, F, C>(this, b);
			players.put(node.getPlayerID(), node);
			players2.put(node.getPlayerName(), node);
			offlinePlayers.put(node.getPlayerID(), currentTime);
		}
	}

	public void addBasePlayer(B basePlayer)
	{
		PlayerNode<B, F, C> node = new PlayerNode<B, F, C>(this, basePlayer);
		if (players.putIfAbsent(basePlayer.getPlayerId(), node) == null)
		{
			offlinePlayers.put(basePlayer.getPlayerId(),
			        System.currentTimeMillis());
		}
	}

	private PlayerNode<B, F, C> loadPlayer(long playerID)
	{
		B basePlayer = dbCacheLoader.loadBasePlayer(playerID);
		if (basePlayer == null)
		{
			return null;
		}
		PlayerNode<B, F, C> node = new PlayerNode<B, F, C>(this, basePlayer);

		PlayerNode<B, F, C> _node = players.putIfAbsent(playerID, node);

		if (_node != null)
		{
			node = _node;
		}
		else
		// means adding a new playerNode, so move it to offline players
		{
			offlinePlayers.put(playerID, System.currentTimeMillis());
		}

		players2.putIfAbsent(node.getPlayerName(), node);

		return node;
	}

	private PlayerNode<B, F, C> loadPlayer(String name)
	{
		B basePlayer = dbCacheLoader.loadBasePlayer(name);
		if (basePlayer == null)
		{
			return null;
		}
		PlayerNode<B, F, C> node = new PlayerNode<B, F, C>(this, basePlayer);

		PlayerNode<B, F, C> _node = players.putIfAbsent(node.getPlayerID(),
		        node);

		if (_node != null)
		{
			node = _node;
		}
		else
		// means adding a new playerNode, so move it to offline players
		{
			offlinePlayers.put(node.getPlayerID(), System.currentTimeMillis());
		}

		players2.putIfAbsent(node.getPlayerName(), node);

		return node;
	}
	
	@Deprecated
	public PlayerNode<B, F, C> getUnlockPlayer(long playerID)
	{
		PlayerNode<B, F, C> node = players.get(playerID);
		if (node == null)
		{
			node = loadPlayer(playerID);
		}
		return node;
	}
	
	@Deprecated
	public PlayerNode<B, F, C> getUnlockPlayer(String name)
	{
		PlayerNode<B, F, C> node = players2.get(name);
		if (node != null)
		{
			node = loadPlayer(name);
		}
		return node;
	}

	public PlayerNode<B, F, C> getReadOnlyPlayer(long playerID)
	{
		PlayerNode<B, F, C> node = players.get(playerID);
		if (node == null)
		{
//			node = loadPlayer(playerID);
			return null;
		}
		addReadLock(node);
		return node;
	}

	public PlayerNode<B, F, C> getReadOnlyPlayer(String name)
	{
		PlayerNode<B, F, C> node = players2.get(name);
		if (node == null)
		{
			node = loadPlayer(name);
			return null;
		}
		addReadLock(node);
		return node;
	}

	public PlayerNode<B, F, C> getWritablePlayer(long playerID)
	{
		PlayerNode<B, F, C> node = players.get(playerID);
		if (node == null)
		{
//			node = loadPlayer(playerID);
			return null;
		}
		addWriteLock(node);
		return node;
	}

	public PlayerNode<B, F, C> getWritablePlayer(String name)
	{
		PlayerNode<B, F, C> node = players2.get(name);
		if (node == null)
		{
//			node = loadPlayer(name);
		}
		addWriteLock(node);
		return node;
	}

	public boolean containPlayer(String name)
	{
		return players2.containsKey(name);
	}

	/**
	 * The method only return the concurrentPlayer of playerNode, and won't lock
	 * the player
	 * 
	 * @param playerID
	 * @return
	 */
	public C getConcurrentPlayer(long playerID)
	{
		PlayerNode<B, F, C> node = players.get(playerID);
		if (node == null)
		{
			B basePlayer = dbCacheLoader.loadBasePlayer(playerID);
			if (basePlayer == null)
			{
				return null;
			}
			node = new PlayerNode<B, F, C>(this, basePlayer);

			PlayerNode<B, F, C> _node = players.putIfAbsent(playerID, node);
			if (_node != null)
			{
				node = _node;
			}
			else
			{
				offlinePlayers.put(playerID, System.currentTimeMillis());
			}

			players2.putIfAbsent(node.getPlayerName(), node);
		}

		return node.getConcurrentPlayer();
	}

	public Enumeration<Long> getPlayerIDs()
	{
		return players.keys();
	}

	public Enumeration<Long> getOnlinePlayerIDs()
	{
		return onlinePlayers.keys();
	}

	public void releasePlayer(long playerID)
	{
		if(isDebug)
			System.out.println("releaseLockById==============nodeId:"+playerID+";"+logStack());
		PlayerNode<B, F, C> node = players.get(playerID);
		HashSet<Long> readerLocks = getreadLockedPlayersInCurrentThread();
		if (readerLocks.remove(playerID))
		{
			node.getRWLock().readLock().unlock();
		}

		HashSet<Long> writeLocks = getWriteLockedPlayersInCurrentThread();
		if (writeLocks.remove(playerID))
		{
			node.getRWLock().writeLock().unlock();
			node.getWriteLock().unlock();
		}
		clearFirstLockTimestamp();
	}

	public void releaseAllPlayersInCurrentThread()
	{
		if(isDebug)
			System.out.println("relaseAllPlayer:"+logStack());
		HashSet<Long> readerLocks = getreadLockedPlayersInCurrentThread();
		for (Long playerID : readerLocks)
		{
			if(isDebug)
				System.out.println("releaseAllReadById==============nodeId:"+playerID);
			players.get(playerID).getRWLock().readLock().unlock();
		}
		readerLocks.clear();

		HashSet<Long> writeLocks = getWriteLockedPlayersInCurrentThread();
		for (Long playerID : writeLocks)
		{
			if(isDebug)
				System.out.println("releaseAllWriteById==============nodeId:"+playerID);
			players.get(playerID).getRWLock().writeLock().unlock();
			players.get(playerID).getWriteLock().unlock();
		}
		writeLocks.clear();
		clearFirstLockTimestamp();
	}

	void removePlayer(int playerID)
	{
		if(isDebug)
			System.out.println("removeLockById==============nodeId:"+playerID+";"+logStack());
		PlayerNode<B, F, C> node = players.get(playerID);
		if ( node == null)
		{
			return;
		}
		addWriteLock(node);
		players.remove(playerID);
		players2.remove(node.getPlayerName());
		onlinePlayers.remove(playerID);
		offlinePlayers.remove(playerID);
		HashSet<Long> writeLocks = getWriteLockedPlayersInCurrentThread();
		if (writeLocks.remove(playerID))
		{
			node.getRWLock().writeLock().unlock();
			node.getWriteLock().unlock();
		}
	}

	private HashSet<Long> getWriteLockedPlayersInCurrentThread()
	{
		HashSet<Long> writeLockRecords = writeLockedPlayersInCurrentThread.get();
		if (writeLockRecords == null)
		{
			writeLockRecords = new HashSet<Long>();
			writeLockedPlayersInCurrentThread.set(writeLockRecords);
		}
		return writeLockRecords;
	}

	private HashSet<Long> getreadLockedPlayersInCurrentThread()
	{
		HashSet<Long> readLockRecords = readLockedPlayersInCurrentThread.get();
		if (readLockRecords == null)
		{
			readLockRecords = new HashSet<Long>();
			readLockedPlayersInCurrentThread.set(readLockRecords);
		}
		return readLockRecords;
	}

	private void updateFirstLockTimestamp()
	{
		Long timestamp = firstLockTimestamp.get();
		long currentTime = System.currentTimeMillis();
		if (timestamp == null)
		{
			firstLockTimestamp.set(currentTime);
		}
		else if (currentTime - timestamp > 1000 * maxLockTime) // means
		                                                       // someone
		                                                       // forget to
		                                                       // unlock
		{
			//为了方便调试
			if (isDebug)
			{
				System.out.println("The thread "
				        + Thread.currentThread().getName()
				        + " hold a lock than " + (currentTime - timestamp)
				        + " ms");
				//TODO:
				//使用logger出错
//				logger.warn("The thread "
//			        + Thread.currentThread().getName() + " hold a lock than "
//			        + (currentTime - timestamp) + " ms");
			}
			else
			{
				throw new CacheLockException("The thread "
				        + Thread.currentThread().getName()
				        + " hold a lock than " + (currentTime - timestamp)
				        + " ms");
			}
		}
	}

	private void clearFirstLockTimestamp()
	{
		if (getreadLockedPlayersInCurrentThread().isEmpty() && getWriteLockedPlayersInCurrentThread().isEmpty())
		{
			firstLockTimestamp.set(null);
		}
	}

	private void addReadLock(PlayerNode<B, F, C> node)
	{
		if(node == null)
			return;
		if(isDebug)
			System.out.println("addReadLock==============nodeId:"+node.getPlayerID()+";"+logStack());
		
		HashSet<Long> readerLocks = getreadLockedPlayersInCurrentThread();
		if (readerLocks.contains(node.getPlayerID()))
		{
			return;
		}

		HashSet<Long> writeLocks = getWriteLockedPlayersInCurrentThread();
		for (Long playerID : writeLocks)
		{
			if (playerID > node.getPlayerID())
			{
				players.get(playerID).getRWLock().writeLock().unlock();
			}
			else if (playerID == node.getPlayerID())
			{
				return;
			}
		}

		if (readerLocks.add(node.getPlayerID()))
		{
			updateFirstLockTimestamp();
			node.getRWLock().readLock().lock();
		}

		for (Long playerID : writeLocks)
		{
			if (playerID > node.getPlayerID())
			{
				players.get(playerID).getRWLock().writeLock().lock();
			}
		}
	}

	private void addWriteLock(PlayerNode<B, F, C> node)
	{
		if(node == null)
			return;
		
		if(isDebug)
			System.out.println("addWriteLock==============nodeId:"+node.getPlayerID()+";"+logStack());
		HashSet<Long> writeLocks = getWriteLockedPlayersInCurrentThread();

		for (Long playerID : writeLocks)
		{
			if (playerID > node.getPlayerID())
			{
				StringBuilder sb = new StringBuilder();
				sb.append("The thread ");
				sb.append(Thread.currentThread().getName());
				sb.append(" has a write lock ");
				sb.append(playerID);
				sb.append(" > current player ");
				sb.append(node.getPlayerID());
				sb.append(" that need to add write lock");
				throw new CacheLockException(sb.toString());
			}
			else if (playerID == node.getPlayerID())
			{
				return;
			}
		}

		HashSet<Long> readerLocks = getreadLockedPlayersInCurrentThread();
		if (readerLocks.size() > 0)
		{
			throw new CacheLockException(
			        "There is readlock before adding write lock in the thread "
			                + Thread.currentThread().getName());
		}

		if (writeLocks.add(node.getPlayerID()))
		{
			updateFirstLockTimestamp();
			node.getWriteLock().lock();
			node.getRWLock().writeLock().lock();
		}
	}
	
	private static String logStack()
	{
		StringBuffer sb = new StringBuffer();
		for (StackTraceElement e:Thread.currentThread().getStackTrace())
		{
			sb.append(e.toString());
			sb.append("\r\n");
		}
		sb.append("\r\n");
		sb.append("\r\n");
		return sb.toString();
	}

}
