package com.game.core.cache;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.kodgames.collections.factory.KodList;

public class Test 
{
	
	static public class Skill
	{
		ArrayList<String> kk = new ArrayList<String>();
//		private int i = 5;
		public String name = "h";
	}
	
	static public class PlayerNode
	{
		List<String> kk = new ArrayList<String>();
		public int i = 5;
		public String name = "h";
		private String value = "kkkkk";
		ConcurrentHashMap<Integer, Skill> map = new ConcurrentHashMap<Integer, Skill>();
		public void setValue(String v)
		{
			value = v;
		}
		public void printValue()
		{
			System.out.println(value);
		}

	}
	
	/**
	 * 使用了KODList
	 * @author marui
	 *
	 */
	static public class PlayerNode2
	{
		List<Integer> list = KodList.createSyncArrayList(Integer.class);
	}
	
	/**
	 * writeClassAndObject会保存class信息，序列化的结果会比较大
	 * @throws FileNotFoundException
	 */
	public static void test0() throws FileNotFoundException
	{
		Kryo kryo = new Kryo();
		Output output = new Output(new FileOutputStream("c:\\kdkd.bin"));
		PlayerNode node = new PlayerNode();
		node.name = "hello world!";
		node.kk.add("kk");
		node.setValue("kodkod");
		kryo.writeClassAndObject(output, node);
		System.out.println( output.total() );
		output.flush();
		output.close();
	
		
		Input input = new Input(new FileInputStream("c:\\kdkd.bin"));
		PlayerNode node2 = (PlayerNode)kryo.readClassAndObject(input);

		System.out.println(node2.name);
		System.out.println(node2.kk.get(0));
		node2.printValue();
		
	}
	
	public static void test1() throws FileNotFoundException
	{
		Kryo kryo = new Kryo();
		Output output = new Output(new FileOutputStream("c:\\kdkd.bin"));
		PlayerNode node = new PlayerNode();
		node.name = "hello world!";
		node.kk.add("kk");
		node.setValue("kodkod");
		kryo.writeObject(output, node);
		
		System.out.println( output.total() );
		output.flush();
		output.close();
	
		
		Input input = new Input(new FileInputStream("c:\\kdkd.bin"));
		PlayerNode node2 = (PlayerNode)kryo.readObject(input, PlayerNode.class);

		System.out.println(node2.name);
		System.out.println(node2.kk.get(0));
		node2.printValue();		
	}
	
	/**
	 * 对class指定一个int标识，序列结果中会用使用int标识来代替class名字，使序列结果变小
	 * @throws FileNotFoundException
	 */
	public static void test2() throws FileNotFoundException
	{
		Kryo kryo = new Kryo();
		Output output = new Output(new FileOutputStream("c:\\kdkd.bin"));
		PlayerNode node = new PlayerNode();
		node.name = "hello world!";
		node.kk.add("kk");
		node.setValue("kodkod");
		kryo.register(PlayerNode.class, 1);
		kryo.register(ArrayList.class, 2);
		kryo.writeObject(output, node);
		System.out.println( output.total() );
		output.flush();
		output.close();
	
		
		Input input = new Input(new FileInputStream("c:\\kdkd.bin"));
		//PlayerNode node2 = (PlayerNode)kryo.readClassAndObject(input);
		PlayerNode node2 = (PlayerNode)kryo.readObject(input, PlayerNode.class);

		System.out.println(node2.name);
		System.out.println(node2.kk.get(0));
		node2.printValue();
		
	}
	
	/**
	 * 对成员变量class指定一个int标识，序列结果中会用使用int标识来代替class名字，使序列结果变小
	 * @throws FileNotFoundException
	 */
	public static void test3() throws FileNotFoundException
	{
		Kryo kryo = new Kryo();
		Output output = new Output(new FileOutputStream("c:\\kdkd.bin"));
		PlayerNode node = new PlayerNode();
		node.name = "hello world!";
		node.kk.add("kk");
		node.setValue("kodkod");
		kryo.register(PlayerNode.class, 1);
		kryo.register(ArrayList.class, 2);
		kryo.register(ConcurrentHashMap.class, 3);
		kryo.writeObject(output, node);
		System.out.println( output.total() );
		output.flush();
		output.close();
	
		
		Input input = new Input(new FileInputStream("c:\\kdkd.bin"));
		PlayerNode node2 = (PlayerNode)kryo.readObject(input, PlayerNode.class);

		System.out.println(node2.name);
		System.out.println(node2.kk.get(0));
		node2.printValue();		
	}
	
	/**
	 * 目前Kryo不能序列化KOD容器，需要看一下原因
	 * @throws FileNotFoundException
	 */
	public static void test4() throws FileNotFoundException
	{
		Kryo kryo = new Kryo();
		Output output = new Output(new FileOutputStream("c:\\kdkd.bin"));
		PlayerNode2 node = new PlayerNode2();
		node.list.add(3);
		kryo.writeObject(output, node);
		System.out.println( output.total() );
		output.flush();
		output.close();
	
		
		Input input = new Input(new FileInputStream("c:\\kdkd.bin"));
		PlayerNode2 node2 = (PlayerNode2)kryo.readObject(input, PlayerNode2.class);

		System.out.println(node2.list.get(0));
	}
	
	



	
	public static void main(String[] args) throws FileNotFoundException {
		
		// TODO Auto-generated method stub
		test0();
		test1();
		test2();
		test3();
		test4();
		
	}

}
