package com.game.core.cache;

public interface IBasePlayer
{	
	public long getPlayerId();
	
	public String getPlayerName();
	
	public void setPlayerId(long roleId);
	
	public void setPlayerName(String playerName);
}
