package com.game.core.cache;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PlayerNode<B extends IBasePlayer, F extends IFullPlayer<B>, C extends IConcurrentPlayer>
{
	private long playerID;
	private boolean online = false;

	private long lastFullPlayerAccessTime;
	
	private B basePlayer;
	private F fullPlayer;
	private C concurrentPlayer;
	
	PlayerCache<B,F,C> playerCache;

	private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
	private ReentrantLock writeLock = new ReentrantLock();

	public Long getPlayerID()
	{
		return playerID;
	}
	
	public String getPlayerName()
	{
		return basePlayer.getPlayerName();
	}

	public PlayerNode(PlayerCache<B,F,C> playerCache, B basePlayer)
	{
		if(basePlayer == null || playerCache == null)
			throw new IllegalArgumentException("playerCache and baseplayer can't be null !!!");
		this.playerCache = playerCache;
		this.basePlayer = basePlayer;
		this.playerID = basePlayer.getPlayerId();
	}
	
	public synchronized C getConcurrentPlayer()
	{
		if (concurrentPlayer == null)
		{
			concurrentPlayer = playerCache.getDBCacheLoader().loadConcurrentPlayer(playerID);
		}
		return concurrentPlayer;
	}

	public synchronized B getBasePlayer()
	{
		return basePlayer;
	}
	
	public synchronized F getFullPlayer() {
		if(fullPlayer == null)
		{
			fullPlayer = playerCache.getRockDBUtil().loadFullPlayer(playerID);
			if (fullPlayer == null)
			{
				fullPlayer =  playerCache.getDBCacheLoader().loadFullPlayer(playerID);
			}
			if (fullPlayer != null)
			{
				fullPlayer.setBasePlayer(basePlayer);
			}
		}
		lastFullPlayerAccessTime = System.currentTimeMillis();
		updateOnlineOfflineTime();
		return fullPlayer;
	}
	
	void updateOnlineOfflineTime()
	{
		if(online)
		{
			playerCache.getOnlinePlayers().put(playerID, lastFullPlayerAccessTime);
		}
		else if(fullPlayer != null)
		{
			playerCache.getOfflinePlayers().put(playerID, lastFullPlayerAccessTime);
		}
	}
	
	ReentrantReadWriteLock getRWLock()
	{
		return rwLock;
	}
	
	ReentrantLock getWriteLock()
	{
		return writeLock;
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		if(this.online != online)
		{
			this.online = online;
			if(this.online)
			{
				playerCache.getOfflinePlayers().remove(playerID);
			}else
			{
				playerCache.getOnlinePlayers().remove(playerID);		
			}
			updateOnlineOfflineTime();
		}
	}
	
	public F removeFullPlayer()
	{
		F fp = this.fullPlayer;
		this.fullPlayer = null;
		return fp;
	}
}
