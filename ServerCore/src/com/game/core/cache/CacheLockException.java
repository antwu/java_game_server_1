package com.game.core.cache;

public class CacheLockException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4469567166624346613L;

	public CacheLockException()
	{
		super();
	}

	public CacheLockException(String message)
	{
		super(message);
	}
}
