package com.game.core.net.common;

import com.game.core.net.message.AbstractCustomizeMessage;

public interface ISendMessageInterceptor
{
	public boolean before(int serverType, AbstractCustomizeMessage message,
			RemoteNode session);

	public void after(int serverType, AbstractCustomizeMessage message,
			RemoteNode session);

	public void onException(int serverType, AbstractCustomizeMessage message,
			RemoteNode session, Exception e);
}
