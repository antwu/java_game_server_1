package com.game.core.net.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.atomic.AtomicInteger;

import com.game.core.net.NetParameter;
import com.game.core.net.handler.AbstractMessageInitializer;
import com.game.core.net.handler.BaseMessageHandler;
import com.game.core.net.handler.EncryptHandler;
import com.game.core.net.handler.HeartBeatHandler;
import com.game.core.net.handler.LengthLogicHandler;
import com.game.core.net.handler.MessageProcessor;
import com.game.core.net.handler.SnappyFramedCodec;
import com.game.core.net.message.InternalMessage;

public class SimpleCSNettyInitializer extends ChannelInitializer<Channel>
{
	static AtomicInteger handlerId = new AtomicInteger(0);
	AbstractMessageInitializer messageInitializer;
	LengthFieldPrepender lengthFieldPrepender;
	SnappyFramedCodec snappyFramedCodec;
	MessageProcessor messageProcessor;

	public SimpleCSNettyInitializer(AbstractMessageInitializer messageInitializer)
	{
		this.messageInitializer = messageInitializer;
		lengthFieldPrepender = new LengthFieldPrepender(4, false);
		snappyFramedCodec = new SnappyFramedCodec();
		messageProcessor = new MessageProcessor(messageInitializer);
	}

	@Override
	protected void initChannel(Channel ch) throws Exception
	{
		ChannelPipeline p = ch.pipeline();
		String nameId = "-" + handlerId.incrementAndGet();
		// length excludes the lenghFieldlength and the data decoded exclude the length field
		p.addLast("FrameDecoder" + nameId, new LengthFieldBasedFrameDecoder(204800, 0, 4, 0, 4));
		p.addLast("LengthFieldPrepender" + nameId, lengthFieldPrepender);
		p.addLast("LengthLogicHandler" + nameId, new LengthLogicHandler(messageInitializer));
		p.addLast("IdleHandler", new IdleStateHandler(NetParameter.CSHeartHalfTimeout, 0, 0));
//		p.addLast("HeartBeatHandler", new HeartBeatHandler());
		// p.addLast("EncryptHandler", new EncryptHandler());
		p.addLast("SnappyFramedCodec" + nameId, snappyFramedCodec);
		p.addLast("MessageProcessor" + nameId, messageProcessor);
	}

	public void readMessageBySelf(final BaseMessageHandler<?> handler, final InternalMessage protocol)
	{
		messageProcessor.readMessage(handler, protocol);
	}
}
