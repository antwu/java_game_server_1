package com.game.core.net.handler;

import com.game.core.net.common.RemoteNode;

public abstract class AbstractByteArrayHandler extends BaseMessageHandler<byte[]>{
	@Override
    final public void handleMessage(RemoteNode remoteNode, int protocolID, byte[]  message){};
	@Override
    abstract public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, byte[] message);
	@Override
    final public void handleConnectionActive(RemoteNode remoteNode){};
	@Override
    final public void handleConnectionInactive(RemoteNode remoteNode){};
	
	/**
	 * 如果在messageInitializer里设置了BeforeMessageExecutor或�?将此Handler和MessageExecutor绑定了，在线程池会保证相同MessageKey的消息先后执行顺�?
	 * @param remoteNode
	 * @param t
	 * @param buffer
	 * @return
	 */
	@Override
    abstract public Object getMessageKey(RemoteNode remoteNode, int protocoliD,  byte[] buffer);
	
	@Override
    final public Object getMessageKey(RemoteNode remoteNode){return null;};
}