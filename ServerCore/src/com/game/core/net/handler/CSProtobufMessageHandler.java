package com.game.core.net.handler;

import com.google.protobuf.GeneratedMessage;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.AbstractMessageService;
import com.game.core.service.PlayerService;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;

public abstract class CSProtobufMessageHandler<S extends AbstractMessageService, T extends GeneratedMessage> extends AbstractProtobufMessageHandler<T>
{
	@SuppressWarnings("unchecked")
	@Override
    public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, T message)
    {
		assert(serviceClass != null);
		S service = null;
		if ( PlayerService.class.isAssignableFrom(serviceClass) )
		{
			service = (S)ServiceContainer.getInstance().getPlayerService(remoteNode.getRoleId(), serviceClass);
		}
		else if ( PublicService.class.isAssignableFrom(serviceClass) )
		{
			service =(S)ServiceContainer.getInstance().getPublicService(serviceClass);		
		}

		handleMessage(remoteNode, service, message, callback);
    }
	
	abstract public void handleMessage(RemoteNode remoteNode, S service, T message, int callback);

	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, T t)
    {
	    return remoteNode.getKey();
    }
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode)
    {
	    return remoteNode.getKey();
    }
}
