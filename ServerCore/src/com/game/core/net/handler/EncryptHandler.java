package com.game.core.net.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

/**
 * 加密handler，利用异或进行最简单的加密
 * 此处无法判断协议ID，需要在MessageProcessor中激活新的key
 */
public class EncryptHandler extends ChannelDuplexHandler
{

	private static final Logger logger = LoggerFactory.getLogger(EncryptHandler.class);
	/**
	 * 判断协议类型，如果是更新key协议，在协议发送完毕之后，激活新的加密Key
	 */
	@Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception
    {
		RemoteNode remoteNode = ctx.channel().attr(RemoteNode.REMOTENODE).get();
		if ((remoteNode != null) && (msg instanceof ByteBuf) && remoteNode.getEncryptCoder().hasEncryptKey())
		{
//			logger.error("----EncryptHandler write using key={}-", remoteNode.getEncryptCoder().getEncryptKey());
			//加密数据
			ByteBuf buf = (ByteBuf)msg;
			int length = buf.readableBytes();
			byte[] data = new byte[length];
			buf.getBytes(buf.readerIndex(), data, 0, length);
			
			remoteNode.getEncryptCoder().encryptData(data, 0, data.length);
			ByteBuf outBuf = ctx.alloc().buffer();
			outBuf.writeBytes(data);
			
			ctx.write(outBuf, promise);
		}
		else
		{		
			ctx.write(msg, promise);
		}
    }

	/**
	 * 判断协议类型，如果是更新key应答协议，处理完毕协议后，激活新的解密key
	 */
	@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception
    {
		RemoteNode remoteNode = ctx.channel().attr(RemoteNode.REMOTENODE).get();
		if ((remoteNode != null) && (msg instanceof ByteBuf) && remoteNode.getEncryptCoder().hasDecryptKey())
		{
			//解密数据
			ByteBuf buf = (ByteBuf)msg;
			int length = buf.readableBytes();
			byte[] data = new byte[length];
			buf.getBytes(buf.readerIndex(), data, 0, length);
			
			remoteNode.getEncryptCoder().decryptData(data, 0, data.length);
			ByteBuf outBuf = ctx.alloc().buffer();
			outBuf.writeBytes(data);
			
			ctx.fireChannelRead(outBuf);
		}
		else
		{		
			ctx.fireChannelRead(msg);
		}
    }
	
}
