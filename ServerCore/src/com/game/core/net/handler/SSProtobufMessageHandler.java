package com.game.core.net.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.GeneratedMessage;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.AbstractMessageService;
import com.game.core.service.PlayerService;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;

public abstract class SSProtobufMessageHandler<S extends AbstractMessageService, T extends GeneratedMessage> extends AbstractProtobufMessageHandler<T>
{
	private static Logger logger = LoggerFactory.getLogger(SSProtobufMessageHandler.class);
	
	@SuppressWarnings("unchecked")
	@Override
    public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, T message)
    {
		assert(serviceClass != null);
		S service = null;
		if ( PlayerService.class.isAssignableFrom(serviceClass) )
		{
			service = (S)ServiceContainer.getInstance().getPlayerService(remoteNode.getRoleId(), serviceClass);
		}
		else if ( PublicService.class.isAssignableFrom(serviceClass) )
		{
			service =(S)ServiceContainer.getInstance().getPublicService(serviceClass);		
		}
		logger.debug("Receive protocolID:{}  message:{} from server:{}", protocolID, message.getClass().getSimpleName(), remoteNode.getRoleId());
		handleMessage(remoteNode, service, message, callback);    
    }
	
	abstract public void handleMessage(RemoteNode remoteNode, S service, T message, int callback);

	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, T t)
    {
	    return remoteNode.getKey();
    }
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode)
    {
	    return null;
    }
}
