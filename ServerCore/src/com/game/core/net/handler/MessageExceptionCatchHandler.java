package com.game.core.net.handler;

import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.util.PackageScaner;

public class MessageExceptionCatchHandler extends HashSet<IMessageExceptionCatchHandler> implements IMessageExceptionCatchHandler
{
	private static final long serialVersionUID = -8804586383858004697L;

	private Logger logger = LoggerFactory.getLogger(MessageExceptionCatchHandler.class);
	private static MessageExceptionCatchHandler instance = new MessageExceptionCatchHandler();
	private MessageExceptionCatchHandler()
	{
	}

	public static MessageExceptionCatchHandler getInstance()
	{
		return instance;
	}
	
	@SuppressWarnings("rawtypes")
	public void init(String handlerPackage) throws ReflectiveOperationException
	{
        try 
        {
			List<Class> list = PackageScaner.getClasses(handlerPackage, ".class", true);
			for (Class cls : list) 
			{
				if(cls!=null && cls!=IMessageExceptionCatchHandler.class && cls!=MessageExceptionCatchHandler.class
						&& IMessageExceptionCatchHandler.class.isAssignableFrom(cls))
				{
					add((IMessageExceptionCatchHandler) cls.newInstance());
				}
			}
		} catch (ReflectiveOperationException e) {
			logger.error("init failed. ");
			throw e;
		}
	}

	@Override
	public void handleMessage(RemoteNode remoteNode, int protocolID,
			int callback, Object buffer) 
	{
		for (IMessageExceptionCatchHandler handler : this) 
		{
			handler.handleMessage(remoteNode, protocolID, callback, buffer);
		}
	}
}
