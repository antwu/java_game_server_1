package com.game.core.net.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;
import java.util.Map;

import com.google.protobuf.MessageLite;
import com.game.core.net.message.InternalMessage;

public class ProtobufCodec extends ByteToMessageCodec<InternalMessage>
{
	@SuppressWarnings("unused")
	private final Map<Integer, MessageLite> prototypes;

	public ProtobufCodec(Map<Integer, MessageLite> prototypes)
	{
		this.prototypes = prototypes;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, InternalMessage msg, ByteBuf out) throws Exception
	{
		// write the protocol id first
		// out.writeInt(msg.getId());
		// if (msg.getPrototype() == null) {
		// out.writeBytes(msg.getBuffer());
		// } else {
		// out.writeBytes(msg.getPrototype().toByteArray());
		// }

	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception
	{

		// byte[] array;
		// final int offset;
		// final int protocolID = in.readInt();
		// final int length = in.readableBytes();
		// if (in.hasArray()) {
		// array = in.array();
		// offset = in.arrayOffset() + in.readerIndex();
		// } else {
		// array = new byte[length];
		// in.getBytes(in.readerIndex(), array, 0, length);
		// offset = 0;
		// }
		//
		// MessageLite prototype = null;
		// if ( prototypes != null ){
		// prototype = prototypes.get(protocolID);
		// }
		// InteralMessage p = new InteralMessage();
		// if (prototype == null) {
		// //create a new byte array because the parameters of Protocol.SetBuffer has no offset and
		// length
		// if ( offset != 0 || array.length != length){
		// array = new byte[length];
		// in.getBytes(in.readerIndex(), array, 0, length);
		// }
		// p.setBuffer(array);
		// } else{
		// p.setPrototype(prototype.getParserForType().parseFrom(array, offset,
		// length));
		// }
		// in.readerIndex(in.readerIndex()+length);
		// out.add(p);
	}

}
