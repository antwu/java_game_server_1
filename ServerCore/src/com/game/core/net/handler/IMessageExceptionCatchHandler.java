package com.game.core.net.handler;

import com.game.core.net.common.RemoteNode;

public interface IMessageExceptionCatchHandler {
	public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, Object buffer);
}
