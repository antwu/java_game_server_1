package com.game.core.net.handler;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.message.AbstractCustomizeMessage;

public abstract class AbstractCustomizeMessageHandler<T extends AbstractCustomizeMessage> extends BaseMessageHandler<T>{
	@Override
    public abstract void handleMessage(RemoteNode remoteNode, int protocolID, T  message);
	@Override
    final public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, T message){};
	@Override
    final public void handleConnectionActive(RemoteNode remoteNode){};
	@Override
    final public void handleConnectionInactive(RemoteNode remoteNode){};
	
	/**
	 * 如果在messageInitializer里设置了BeforeMessageExecutor或�?将此Handler和MessageExecutor绑定了，在线程池会保证相同MessageKey的消息先后执行顺�?
	 * @param remoteNode
	 * @param t
	 * @param buffer
	 * @return
	 */
	@Override
    abstract public Object getMessageKey(RemoteNode remoteNode, int protocoliD, T t);

	@Override
    final public Object getMessageKey(RemoteNode remoteNode){return null;};
}
