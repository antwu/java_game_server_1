package com.game.core.net.handler;

import com.google.protobuf.GeneratedMessage;
import com.game.core.net.common.RemoteNode;

public abstract class AbstractProtobufMessageHandler <T extends GeneratedMessage> extends BaseMessageHandler<T>{
	@Override
    final public void handleMessage(RemoteNode remoteNode, int protocolID, T  message){};
	@Override
    abstract public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, T  message);
	@Override
    final public void handleConnectionActive(RemoteNode remoteNode){};
	@Override
    final public void handleConnectionInactive(RemoteNode remoteNode){};
	
	/**
	 * 如果在messageInitializer里设置了BeforeMessageExecutor或�?将此Handler和MessageExecutor绑定了，在线程池会保证相同MessageKey的消息先后执行顺�?
	 * @param remoteNode
	 * @param t
	 * @param buffer
	 * @return
	 */
	@Override
    abstract public Object getMessageKey(RemoteNode remoteNode, int protocolID, T t);

	@Override
    abstract public Object getMessageKey(RemoteNode remoteNode);
}
