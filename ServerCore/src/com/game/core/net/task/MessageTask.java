package com.game.core.net.task;

import com.kodgames.core.threadPool.task.Task;
import com.game.core.net.handler.AbstractMessageInitializer;
import com.game.core.net.handler.BaseMessageHandler;
import com.game.core.net.message.InternalMessage;

public abstract class MessageTask extends Task
{
	AbstractMessageInitializer msgInitializer;
	InternalMessage message;
	
	@SuppressWarnings("rawtypes")
	BaseMessageHandler handler;

	public MessageTask(AbstractMessageInitializer msgInitializer, InternalMessage message, BaseMessageHandler<?> handler)
	{
		this.msgInitializer = msgInitializer;
		this.message = message;
		this.handler = handler;
	}

	@Override
    public Object getKey()
	{
		return handler.getMessageKey(msgInitializer, message);
	}
	
	public String getKeyName()
	{
		if(getKey() == null)
			return "Default";
		else
			return getKey().toString();
	}

	@Override
    public boolean isUrgency()
	{
		return message.isUrgency();
	}
}
