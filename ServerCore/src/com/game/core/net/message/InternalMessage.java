package com.game.core.net.message;

import com.game.core.net.common.RemoteNode;

/**
 * 
 * @author mrui
 */
public class InternalMessage
{
	private int protocolID;
	private int callback;
	private RemoteNode remoteNode;
	private int channelId;				//udp使用的channelId
	private Object message;
	boolean urgency = false;
	
	public int getProtocolID() {
		return protocolID;
	}
	public void setProtocolID(int protocolID) {
		this.protocolID = protocolID;
	}
	public int getCallback() {
		return callback;
	}
	public void setCallback(int callback) {
		this.callback = callback;
	}

	public RemoteNode getRemoteNode() {
		return remoteNode;
	}
	public void setRemoteNode(RemoteNode remoteNode) {
		this.remoteNode = remoteNode;
	}
	
	public int getChannelId()
	{
		return channelId;
	}
	
	public void setChannelId(int channelId)
	{
		this.channelId = channelId;
	}

	public Object getMessage()
    {
	    return message;
    }
	public void setMessage(Object message)
    {
	    this.message = message;
    }
	public boolean isUrgency() {
		return urgency;
	}
	public void setUrgency(boolean urgency) {
		this.urgency = urgency;
	}
	
}
