use `game`;
ALTER TABLE `agency_card_record` ADD COLUMN `doubi_count`  int(11) NOT NULL AFTER `rewardLevel`;
ALTER TABLE `agency_card_record` ADD COLUMN `cancel_cause`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `doubi_count`;
ALTER TABLE `agency_card_record` ADD COLUMN `isCancel`  int(2) NOT NULL AFTER `cancel_cause`;
ALTER TABLE `agency_card_record` ADD COLUMN `price_id`  int(10) NOT NULL AFTER `isCancel`;
