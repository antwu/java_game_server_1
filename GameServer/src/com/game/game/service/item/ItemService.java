package com.game.game.service.item;

import java.util.Map;

import com.google.protobuf.GeneratedMessage;
import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.db.service.proxy.EntityProxyFactory;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.dbpersistence.game.entity.ItemEntity;
import com.game.game.GameServer;
import com.game.game.config.ItemConfig;
import com.game.game.config.XMLTemplateService;
import com.game.game.service.server.ServerService;
import com.game.message.proto.item.ItemProtoBuf;
import com.game.message.proto.item.ItemProtoBuf.GWSynItemInfoRES;

public class ItemService extends PublicService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	enum ItemType {
		/** 预留0位置 */
		OTHER,
		/** 强化道具 */
		IMPROVE_ITEM,
		/** 消耗道具 */
		CONSUME_ITEM,
		/** 制作材料 */
		PRODUCE_ITEM,
		/** 武器部件 */
		WEAPON_PART,
		/** 图纸 */
		BLUEPRINT
	}
	
	public int addItem(String account, Map<Integer, Integer> addMap, int source) {
		int result = ItemProtoBuf.AddResultType.AddSuccess.ordinal();
		for (Map.Entry<Integer, Integer> entry : addMap.entrySet()) {
			ItemConfig itemConfig = XMLTemplateService.itemConfigMap.get(entry.getKey());
			if (itemConfig == null) {
				result = ItemProtoBuf.AddResultType.AddFail.ordinal();
				return result;
			}
			ItemEntity itemEntity = GameServer.itemEntityService.getItem(account, entry.getKey());
			if (itemEntity == null) {
				itemEntity = new ItemEntity(account, entry.getKey(), itemConfig.getType(), entry.getValue());
				result = this.insertItem(itemEntity);
			} else {
				try {
					EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
					ItemEntity itemProxyEntity = entityProxyFactory.createProxyEntity(itemEntity);
					itemProxyEntity.setCount(itemProxyEntity.getCount() + entry.getValue());
					GameServer.itemEntityService.updateEntity(itemProxyEntity);
				} catch(Exception e) {
					result = ItemProtoBuf.AddResultType.AddFail.ordinal();
					return result;
				}
			}
		}
		// 推送道具变更
		this.pushSynItemInfo(account, addMap, source);
		return result;
	}
	
	public int consumeItem(String account, Map<Integer, Integer> consumeMap, int source) {
		for (Map.Entry<Integer, Integer> entry : consumeMap.entrySet()) {
			ItemEntity itemEntity = GameServer.itemEntityService.getItem(account, entry.getKey());
			if (itemEntity == null || itemEntity.getCount() < entry.getValue()) {
				return ItemProtoBuf.ConsumeResultType.NotEnoughItem.ordinal();
			}
		}
		for (Map.Entry<Integer, Integer> entry : consumeMap.entrySet()) {
			ItemEntity itemEntity = GameServer.itemEntityService.getItem(account, entry.getKey());
			if ((itemEntity != null) && (itemEntity.getCount() >= entry.getValue())) {
				try {
					EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
					ItemEntity itemProxyEntity = entityProxyFactory.createProxyEntity(itemEntity);
					itemProxyEntity.setCount(itemProxyEntity.getCount() - entry.getValue());
					if(false == GameServer.itemEntityService.updateEntity(itemProxyEntity)) {
						return ItemProtoBuf.ConsumeResultType.ConsumeFail.ordinal();
					}
				} catch(Exception e) {
					return ItemProtoBuf.ConsumeResultType.ConsumeFail.ordinal();
				}
			}
		}
		// 推送道具变更
		this.pushSynItemInfo(account, consumeMap, source);
		return ItemProtoBuf.ConsumeResultType.ConsumeSuccess.ordinal();
	}
	
	public int insertItem(ItemEntity itemEntity) {
		int result = ItemProtoBuf.AddResultType.AddSuccess.ordinal();
		GameServer.itemEntityService.insertEntity(itemEntity);
		return result;
	}
	
	public void pushSynItemInfo(String account, Map<Integer, Integer> changeItemMap, int source) {
		GWSynItemInfoRES.Builder sendBuilder = GWSynItemInfoRES.newBuilder();
		sendBuilder.setAccount(account);
		sendBuilder.setSynSource(source);
		for (Map.Entry<Integer, Integer> entry : changeItemMap.entrySet()) {
			ItemEntity itemEntity = GameServer.itemEntityService.getItem(account, entry.getKey());
			if( itemEntity!=null ) {
				sendBuilder.addItemInfos(itemEntity.toProto());
			}
		}
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		RemoteNode remoteNode = service.getGatewayNode(3000);
		this.sendMesToRemoteNode(remoteNode, sendBuilder.build());
	}
	
	public void sendMesToRemoteNode(RemoteNode remoteNode, GeneratedMessage message) {
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, message);
	}
	
}
