package com.game.game.service.weapon;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.db.service.proxy.EntityProxyFactory;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.dbpersistence.game.entity.AccountEntity;
import com.game.dbpersistence.game.entity.BuncherEntity;
import com.game.dbpersistence.game.entity.FashionEntity;
import com.game.dbpersistence.game.entity.WeaponEntity;
import com.game.dbpersistence.game.entity.WeaponsetEntity;
import com.game.game.GameServer;
import com.game.game.service.server.ServerService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeBuncherSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeBuncherUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeCurrentWeaponSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeItemNodeUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeMainNodeUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetNameRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWSynFashionInfoRES;
import com.game.message.proto.weapon.WeaponProtoBuf.GWSynWeaponInfoRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeBuncherSetREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeBuncherUnlockREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeColourSetREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeColourUnlockREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeCurrentWeaponSetREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeFashionSetREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeFashionUnlockREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeItemNodeUnlockREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeMainNodeUnlockREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeWeaponSetNameREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeWeaponSetREQ;
import com.game.message.proto.weapon.WeaponProtoBuf.WeaponInfo;
import com.game.message.protocol.ProtocolsConfig;

public class WeaponService extends PublicService{
	final static Logger logger = LoggerFactory.getLogger(WeaponService.class);
	
	public static List<WeaponInfo.Builder> getCurrentWeaponList(String account) {
		List<WeaponInfo.Builder> weaponList = new ArrayList<> ();
		AccountEntity accountEntity = GameServer.accountEntityService.getAccount(account);
		int weaponSet = accountEntity.getWeaponSet();
		WeaponsetEntity weaponSetEntity = GameServer.weaponsetEntityService.getWeaponset(account, weaponSet);
		String weapon1 = weaponSetEntity.getWeapon1();
		String weapon2 = weaponSetEntity.getWeapon2();
		String weapon3 = weaponSetEntity.getWeapon3();
		String weapon4 = weaponSetEntity.getWeapon4();
		if (weapon1 != null && !weapon1.isEmpty() && !"0".equals(weapon1)) {
			WeaponEntity weaponEntity1 = GameServer.weaponEntityService.getWeaponByUID(account, weapon1);
			weaponList.add(weaponEntity1.toProto());
		}
		if (weapon2 != null && !weapon2.isEmpty() && !"0".equals(weapon2)) {
			WeaponEntity weaponEntity2 = GameServer.weaponEntityService.getWeaponByUID(account, weapon2);
			weaponList.add(weaponEntity2.toProto());
		}
		if (weapon3 != null && !weapon3.isEmpty() && !"0".equals(weapon3)) {
			WeaponEntity weaponEntity3 = GameServer.weaponEntityService.getWeaponByUID(account, weapon3);
			weaponList.add(weaponEntity3.toProto());
		}
		if (weapon4 != null && !weapon4.isEmpty() && !"0".equals(weapon4)) {
			WeaponEntity weaponEntity4 = GameServer.weaponEntityService.getWeaponByUID(account, weapon4);
			weaponList.add(weaponEntity4.toProto());
		}
		return weaponList;
	}
	
	/**
	 * 更改当前选择的武器方案
	 * @param remoteNode
	 * @param message
	 * @param callback
	 * @return
	 */
	public GWChangeCurrentWeaponSetRES changeCurrentWeaponSet(RemoteNode remoteNode, WGChangeCurrentWeaponSetREQ message, int callback) {
		String account = message.getAccount();
		int newWeaponSet = message.getChangeWeaponSet();
		GWChangeCurrentWeaponSetRES.Builder res = GWChangeCurrentWeaponSetRES.newBuilder();
		res.setAccount(account);
		if (newWeaponSet < 1 || newWeaponSet > 5) {
			res.setResult(ProtocolsConfig.INVALID_WEAPON_SET);
		} else {
			try {
				if (GameServer.accountEntityService == null) {
					GameServer.accountEntityService = GameServer
							.getAccountEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				AccountEntity accountEntity = GameServer.accountEntityService.getAccount(account);
				if (account != null) {
					EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
					AccountEntity proxyAccount = entityProxyFactory.createProxyEntity(accountEntity);
					proxyAccount.setWeaponSet(newWeaponSet);
					GameServer.accountEntityService.updateAccount(proxyAccount);
				}
			} catch (Exception e) {
				logger.error(e.toString());
			}
		}
		res.setResult(ProtocolsConfig.CHANGE_CURRENT_WEAPON_SET_SUCCESS);
		return res.build();
	}
	
	public GWChangeWeaponSetNameRES changeWeaponSetName(RemoteNode remoteNode, WGChangeWeaponSetNameREQ message, int callback) {
		String account = message.getAccount();
		int weaponSet = message.getChangeWeaponSet();
		String newName = message.getNewName();
		GWChangeWeaponSetNameRES.Builder res = GWChangeWeaponSetNameRES.newBuilder();
		res.setAccount(account);
		if (weaponSet < 1 || weaponSet > 5) {
			res.setResult(ProtocolsConfig.INVALID_WEAPON_SET);
		} else {
			try {
				WeaponsetEntity weaponsetEntity = GameServer.weaponsetEntityService.getWeaponset(account, weaponSet);
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				WeaponsetEntity proxyWeaponsetEntity = entityProxyFactory.createProxyEntity(weaponsetEntity);
				proxyWeaponsetEntity.setSetName(newName);
				GameServer.weaponsetEntityService.updateWeaponset(proxyWeaponsetEntity);
			} catch (Exception e) {
				logger.error(e.toString());
			}
		}
		res.setResult(ProtocolsConfig.CHANGE_WEAPON_SET_NAME_SUCCESS);
		return res.build();
	}
	
	
	public GWChangeWeaponSetRES changeWeaponSet(RemoteNode remoteNode, WGChangeWeaponSetREQ message, int callback) {
		String account = message.getAccount();
		String newWeaponUid = message.getNewWeapon();
		int changeWeaponSet = message.getChangeWeaponSet();
		int changePosition = message.getChangePosition();
		int result = this.ChangeEquipWeaponEntity(account, newWeaponUid, changeWeaponSet, changePosition);
		GWChangeWeaponSetRES.Builder res = GWChangeWeaponSetRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}

	public GWChangeBuncherSetRES changeBuncherSet(RemoteNode remoteNode, WGChangeBuncherSetREQ message, int callback) {
		String account = message.getAccount();
		int oldBuncherUid = message.getOldBuncherId();
		int newBuncherUid = message.getNewBuncherId();
		int changeWeaponSet = message.getChangeWeaponSet();
		int result = this.ChangeEquipBuncherEntitySet(account, oldBuncherUid, newBuncherUid, changeWeaponSet);
		// put on the new weapon
		GWChangeBuncherSetRES.Builder res = GWChangeBuncherSetRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}


	public GWChangeBuncherUnlockRES changeBuncherUnlock(RemoteNode remoteNode, WGChangeBuncherUnlockREQ message,
			int callback) {
		String account = message.getAccount();
		int buncherId = message.getBuncherId();
		int unlocked = message.getUnlocked();
		int changeWeaponSet = message.getChangeWeaponSet();
		int result = this.ChangeEquipBuncherEntityUnlock(account, buncherId, unlocked, changeWeaponSet);
		// put on the new weapon
		GWChangeBuncherUnlockRES.Builder res = GWChangeBuncherUnlockRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}
	
	public GWChangeColourSetRES changeColourSet(RemoteNode remoteNode, WGChangeColourSetREQ message, int callback) {
		String account = message.getAccount();
		int fashionId = message.getFashionId();
		List<Integer> changeList = message.getNewColourList(); 
		int result = this.ChangeEquipColourEntitySet(account, fashionId, changeList);
		// put on the new weapon
		GWChangeColourSetRES.Builder res = GWChangeColourSetRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}

	public GWChangeColourUnlockRES changeColourUnlock(RemoteNode remoteNode, WGChangeColourUnlockREQ message,
			int callback) {
		String account = message.getAccount();
		int fashionId = message.getFashionId();
		List<Integer> unlockList = message.getUnlockColourList();
		int result = this.ChangeEquipColourEntityUnlock(account, fashionId, unlockList);
		// put on the new weapon
		GWChangeColourUnlockRES.Builder res = GWChangeColourUnlockRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}

	public GWChangeMainNodeUnlockRES changeMainNodeUnlock(RemoteNode remoteNode, WGChangeMainNodeUnlockREQ message,
			int callback) {
		String account = message.getAccount();
		int weaponId = message.getWeaponId();
		int mainId = message.getMainId();
		int status = message.getStatus();
		GWChangeMainNodeUnlockRES.Builder res = GWChangeMainNodeUnlockRES.newBuilder();
		res.setAccount(account);
		if(mainId==1) {//解锁id==1的节点，直接返回错误码
			res.setResult(ProtocolsConfig.CHANGE_ITEM_NODE_HAS_UNLOCKED);
		}else {
			try {	
				if (GameServer.classPathXmlApplicationContext == null) {
					GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
				}
				if (GameServer.weaponEntityService == null) {
					GameServer.weaponEntityService = GameServer.getWeaponEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				WeaponEntity weaponEntity = GameServer.weaponEntityService.getWeapon(account, weaponId);
				if (weaponEntity != null && weaponEntity.getOwnerId().equals(account)) {
					EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
					WeaponEntity proxyWeaponEntity = entityProxyFactory.createProxyEntity(weaponEntity);
					if(proxyWeaponEntity != null) {
						int result = proxyWeaponEntity.changeMainNodeStatus(mainId, status);
						if(result == 1) {
							//此处需要添加解锁扣币的代码
							proxyWeaponEntity.setMainNodes(proxyWeaponEntity.getMainNodes());
							proxyWeaponEntity.setTalents(proxyWeaponEntity.getTalents());
							boolean ret = GameServer.weaponEntityService.updateWeapon(proxyWeaponEntity);
							if(ret) {
								res.setResult(ProtocolsConfig.CHANGE_MAIN_NODE_UNLOCK_SUCCESS);
								res.setWeaponInfos(proxyWeaponEntity.toProto());
							}else {
								res.setResult(ProtocolsConfig.CHANGE_MAIN_NODE_UNLOCK_FAIL);
							}
						}else if(result == 2){
							res.setResult(ProtocolsConfig.UNLOCK_MAIN_NODE_CONDITION_NOT_CONTENT);
						}else if(result == 0){
							res.setResult(ProtocolsConfig.CHANGE_MAIN_NODE_HAS_UNLOCKED);
						}else {
							res.setResult(ProtocolsConfig.UNLOCK_MAIN_NODE_NOT_EXIST);
						}
					}else {
						res.setResult(ProtocolsConfig.CHANGE_MAIN_NODE_UNLOCK_FAIL);
					}
				}else {
					res.setResult(ProtocolsConfig.UNLOCK_MAIN_NODE_WEAPON_NOT_EXIST);
				}
			} catch (Exception e) {
				logger.error(e.toString());
				res.setResult(ProtocolsConfig.CHANGE_MAIN_NODE_UNLOCK_FAIL);
			}
		}
		return res.build();
	}
	
	public GWChangeItemNodeUnlockRES changeItemNodeUnlock(RemoteNode remoteNode, WGChangeItemNodeUnlockREQ message,
			int callback) {
		String account = message.getAccount();
		int weaponId = message.getWeaponId();
		int mainId = message.getMainId();
		int itemId = message.getItemId();
		int status = message.getStatus();
		GWChangeItemNodeUnlockRES.Builder res = GWChangeItemNodeUnlockRES.newBuilder();
		res.setAccount(account);
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.weaponEntityService == null) {
				GameServer.weaponEntityService = GameServer.getWeaponEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			WeaponEntity weaponEntity = GameServer.weaponEntityService.getWeapon(account, weaponId);
			if (weaponEntity != null && weaponEntity.getOwnerId().equals(account)) {
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				WeaponEntity proxyWeaponEntity = entityProxyFactory.createProxyEntity(weaponEntity);
				if(proxyWeaponEntity != null) {
					int result = proxyWeaponEntity.changeItemNodeStatus(mainId, itemId, status);
					if(result == 1) {
						//此处需要添加解锁扣币的代码
						proxyWeaponEntity.setMainNodes(proxyWeaponEntity.getMainNodes());
						proxyWeaponEntity.setTalents(proxyWeaponEntity.getTalents());
						boolean ret = GameServer.weaponEntityService.updateWeapon(proxyWeaponEntity);
						if(ret) {
							res.setResult(ProtocolsConfig.CHANGE_ITEM_NODE_UNLOCK_SUCCESS);
							res.setWeaponInfos(proxyWeaponEntity.toProto());
						}else {
							res.setResult(ProtocolsConfig.CHANGE_ITEM_NODE_UNLOCK_FAIL);
						}
					}else if(result == 2){
						res.setResult(ProtocolsConfig.UNLOCK_ITEM_NODE_CONDITION_NOT_CONTENT);
					}else if(result == 0){
						res.setResult(ProtocolsConfig.CHANGE_ITEM_NODE_HAS_UNLOCKED);
					}else {
						res.setResult(ProtocolsConfig.UNLOCK_ITEM_NODE_NOT_EXIST);
					}
				}else {
					res.setResult(ProtocolsConfig.CHANGE_ITEM_NODE_UNLOCK_FAIL);
				}
			}else {
				res.setResult(ProtocolsConfig.UNLOCK_ITEM_NODE_WEAPON_NOT_EXIST);
			}
		} catch (Exception e) {
			logger.error(e.toString());
			res.setResult(ProtocolsConfig.CHANGE_ITEM_NODE_UNLOCK_FAIL);
		}
		return res.build();
	}
	
	public int ChangeEquipWeaponEntity(String account, String newWeaponUid, int changeWeaponSet, int changePosition) {	
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.weaponEntityService == null) {
				GameServer.weaponEntityService = GameServer.getWeaponEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			if (GameServer.weaponsetEntityService == null) {
				GameServer.weaponsetEntityService = GameServer.getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			WeaponEntity weaponEntity = GameServer.weaponEntityService.getWeaponByUID(account, newWeaponUid);
			if (weaponEntity != null && weaponEntity.getOwnerId().equals(account)) {
				WeaponsetEntity weaponsetEntity = GameServer.weaponsetEntityService.getWeaponset(account, changeWeaponSet);
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				WeaponsetEntity proxyWeaponsetEntity = entityProxyFactory.createProxyEntity(weaponsetEntity);
				
				switch (changePosition) {
				case 1:
					proxyWeaponsetEntity.setWeapon1(newWeaponUid);
					break;
				case 2:
					proxyWeaponsetEntity.setWeapon2(newWeaponUid);
					break;
				case 3:
					proxyWeaponsetEntity.setWeapon3(newWeaponUid);
					break;
				case 4:
					proxyWeaponsetEntity.setWeapon4(newWeaponUid);
					break;
				default:
					return ProtocolsConfig.INVALID_WEAPON_SET;
				}
				GameServer.weaponsetEntityService.updateWeaponset(proxyWeaponsetEntity);
				return ProtocolsConfig.CHANGE_WEAPON_SET_SUCCESS;
			}
			else {
				return weaponEntity == null ? ProtocolsConfig.WEAPON_NOT_EXIST : ProtocolsConfig.NOT_HIS_WEAPON;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.NOT_HIS_WEAPON;
	}

	private int ChangeEquipBuncherEntitySet(String account, int oldBuncherId, int newBuncherId, int changeWeaponSet) {
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.buncherEntityService == null) {
				GameServer.buncherEntityService = GameServer.getBuncherEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			if (GameServer.weaponsetEntityService == null) {
				GameServer.weaponsetEntityService = GameServer.getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			BuncherEntity buncherEntity = GameServer.buncherEntityService.getBuncher(account, oldBuncherId);
			if (buncherEntity != null && buncherEntity.getOwnerId().equals(account)) {
				WeaponsetEntity weaponsetEntity = GameServer.weaponsetEntityService.getWeaponset(account, changeWeaponSet);
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				WeaponsetEntity proxyWeaponsetEntity = entityProxyFactory.createProxyEntity(weaponsetEntity);
				proxyWeaponsetEntity.setBuncherId(newBuncherId);
				GameServer.weaponsetEntityService.updateWeaponset(proxyWeaponsetEntity);
				return ProtocolsConfig.CHANGE_WEAPON_SET_SUCCESS;
			}
			else {
				return buncherEntity == null ? ProtocolsConfig.WEAPON_NOT_EXIST : ProtocolsConfig.NOT_HIS_WEAPON;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.NOT_HIS_WEAPON;
	}


	private int ChangeEquipBuncherEntityUnlock(String account, int buncherId, int unlocked, int changeWeaponSet) {
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.buncherEntityService == null) {
				GameServer.buncherEntityService = GameServer.getBuncherEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			if (GameServer.weaponsetEntityService == null) {
				GameServer.weaponsetEntityService = GameServer.getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			BuncherEntity buncherEntity = GameServer.buncherEntityService.getBuncher(account, buncherId);
			if (buncherEntity != null && buncherEntity.getOwnerId().equals(account)) {
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				BuncherEntity proxyBuncherEntity = entityProxyFactory.createProxyEntity(buncherEntity);
				proxyBuncherEntity.setUnlocked(unlocked);
				GameServer.buncherEntityService.updateBuncher(proxyBuncherEntity);
				return ProtocolsConfig.CHANGE_WEAPON_SET_SUCCESS;
			}
			else {
				return buncherEntity == null ? ProtocolsConfig.WEAPON_NOT_EXIST : ProtocolsConfig.NOT_HIS_WEAPON;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.NOT_HIS_WEAPON;
	}

	private int ChangeEquipColourEntitySet(String account, int fashionId, List<Integer> changeList) {
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.fashionEntityService == null) {
				GameServer.fashionEntityService = GameServer.getFashionEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			if (GameServer.weaponsetEntityService == null) {
				GameServer.weaponsetEntityService = GameServer.getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			FashionEntity fashionEntity = GameServer.fashionEntityService.getFashion(account, fashionId);
			if (fashionEntity != null && fashionEntity.getOwnerId().equals(account)) {
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				FashionEntity proxyFashionEntity = entityProxyFactory.createProxyEntity(fashionEntity);	
				proxyFashionEntity.setSkinColour(changeList.get(0));
				proxyFashionEntity.setHairColour(changeList.get(1));
				proxyFashionEntity.setMainbodyColour(changeList.get(2));
				proxyFashionEntity.setDecorateColour(changeList.get(3));
				GameServer.fashionEntityService.updateFashion(proxyFashionEntity);
				return ProtocolsConfig.CHANGE_COLOUR_SET_SUCCESS;
			}
			else {
				return ProtocolsConfig.CHANGE_COLOUR_SET_FAIL;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.CHANGE_COLOUR_SET_FAIL;
	}

	private int ChangeEquipColourEntityUnlock(String account,int fashionId, List<Integer> unlockList) {
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.fashionEntityService == null) {
				GameServer.fashionEntityService = GameServer.getFashionEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			if (GameServer.weaponsetEntityService == null) {
				GameServer.weaponsetEntityService = GameServer.getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			FashionEntity fashionEntity = GameServer.fashionEntityService.getFashion(account, fashionId);
			if (fashionEntity != null && fashionEntity.getOwnerId().equals(account)) {
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				FashionEntity proxyFashionEntity = entityProxyFactory.createProxyEntity(fashionEntity);
				int unlockIndex = unlockList.get(0);
				if(unlockIndex > 0) {
					String skinColours = fashionEntity.getSkinColours();
					if(skinColours.indexOf(String.valueOf(unlockIndex)) < 0){
						skinColours = skinColours.substring(0, skinColours.length() - 1);
						skinColours = skinColours.concat("," + unlockIndex +"]");
					}
					proxyFashionEntity.setSkinColours(skinColours);
				}
				unlockIndex = unlockList.get(1);
				if(unlockIndex > 0) {
					String hairColours = fashionEntity.getHairColours();
					if(hairColours.indexOf(String.valueOf(unlockIndex)) < 0){
						hairColours = hairColours.substring(0, hairColours.length() - 1);
						hairColours = hairColours.concat("," + unlockIndex +"]");
					}
					proxyFashionEntity.setHairColours(hairColours);
				}
				unlockIndex = unlockList.get(2);
				if(unlockIndex > 0) {
					String mainbodyColours = fashionEntity.getMainbodyColours();
					if(mainbodyColours.indexOf(String.valueOf(unlockIndex)) < 0){
						mainbodyColours = mainbodyColours.substring(0, mainbodyColours.length() - 1);
						mainbodyColours = mainbodyColours.concat("," + unlockIndex +"]");
					}
					proxyFashionEntity.setMainbodyColours(mainbodyColours);
				}
				unlockIndex = unlockList.get(3);
				if(unlockIndex > 0) {
					String decorateColours = fashionEntity.getDecorateColours();
					if(decorateColours.indexOf(String.valueOf(unlockIndex)) < 0){
						decorateColours = decorateColours.substring(0, decorateColours.length() - 1);
						decorateColours = decorateColours.concat("," + unlockIndex +"]");
					}
					proxyFashionEntity.setDecorateColours(decorateColours);
				}
				GameServer.fashionEntityService.updateFashion(proxyFashionEntity);
				return ProtocolsConfig.CHANGE_COLOUR_UNLOCK_SUCCESS;
			}
			else {
				return ProtocolsConfig.CHANGE_COLOUR_UNLOCK_FAIL;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.CHANGE_COLOUR_UNLOCK_FAIL;
	}
	
	public GWChangeFashionSetRES changeFashionSet(RemoteNode remoteNode, WGChangeFashionSetREQ message, int callback) {
		String account = message.getAccount();
		int oldFashionId = message.getOldFashionId();
		int newFashionId = message.getNewFashionId();
		int changeWeaponSet = message.getChangeWeaponSet();
		int result = this.ChangeEquipFashionEntitySet(account, oldFashionId, newFashionId , changeWeaponSet);
		// put on the new fashion
		GWChangeFashionSetRES.Builder res = GWChangeFashionSetRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}

	public GWChangeFashionUnlockRES changeFashionUnlock(RemoteNode remoteNode, WGChangeFashionUnlockREQ message,
			int callback) {
		String account = message.getAccount();
		int fashionId = message.getFashionId();
		int unlocked = message.getUnlocked();
		int result = this.ChangeEquipFashionEntityUnlock(account,fashionId , unlocked);
		// put on the new fashion
		GWChangeFashionUnlockRES.Builder res = GWChangeFashionUnlockRES.newBuilder();
		res.setAccount(account);
		res.setResult(result);
		return res.build();
	}
	
	private int ChangeEquipFashionEntitySet(String account, int oldFashionId, int newFashionId ,int changeWeaponSet) {
		
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.fashionEntityService == null) {
				GameServer.fashionEntityService = GameServer.getFashionEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			if (GameServer.weaponsetEntityService == null) {
				GameServer.weaponsetEntityService = GameServer.getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			FashionEntity fashionEntity = GameServer.fashionEntityService.getFashion(account, oldFashionId);
			if (fashionEntity != null && fashionEntity.getOwnerId().equals(account)) {
				WeaponsetEntity weaponSetEntity = GameServer.weaponsetEntityService.getWeaponset(account, changeWeaponSet);
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				WeaponsetEntity proxyWeaponsetEntity = entityProxyFactory.createProxyEntity(weaponSetEntity);
				proxyWeaponsetEntity.setFashionId(newFashionId);
				GameServer.weaponsetEntityService.updateWeaponset(proxyWeaponsetEntity);
				return ProtocolsConfig.CHANGE_FASHION_SET_SUCCESS;
			}
			else {
				fashionEntity = new FashionEntity();
				fashionEntity.setOwnerId(account);
				fashionEntity.setFashionId(newFashionId);
				fashionEntity.setUnlocked(1);
				fashionEntity.setHairColour(1);
				fashionEntity.setSkinColour(1);
				fashionEntity.setMainbodyColour(1);
				fashionEntity.setDecorateColour(1);
				fashionEntity.setSkinColours("[1]");
				fashionEntity.setHairColours("[1,2]");
				fashionEntity.setMainbodyColours("[1,2,3]");
				fashionEntity.setDecorateColours("[1,2,3,4]");
				long ret = GameServer.fashionEntityService.insertFashion(fashionEntity);
				if( ret > 0) {
					WeaponsetEntity weaponSetEntity = GameServer.weaponsetEntityService.getWeaponset(account, changeWeaponSet);
					EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
					WeaponsetEntity proxyWeaponsetEntity = entityProxyFactory.createProxyEntity(weaponSetEntity);
					proxyWeaponsetEntity.setFashionId(newFashionId);
					GameServer.weaponsetEntityService.updateWeaponset(proxyWeaponsetEntity);
					return ProtocolsConfig.CHANGE_FASHION_SET_SUCCESS;
				}else {
					return ProtocolsConfig.CHANGE_FASHION_SET_FAIL;
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.CHANGE_FASHION_SET_FAIL;
	}


	private int ChangeEquipFashionEntityUnlock(String account, int fashionId, int unlocked) {
		try {	
			if (GameServer.classPathXmlApplicationContext == null) {
				GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
			}
			if (GameServer.fashionEntityService == null) {
				GameServer.fashionEntityService = GameServer.getFashionEntityProxyService(GameServer.classPathXmlApplicationContext);
			}
			FashionEntity fashionEntity = GameServer.fashionEntityService.getFashion(account, fashionId);
			if (fashionEntity != null && fashionEntity.getOwnerId().equals(account)) {
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				FashionEntity proxyFashionEntity = entityProxyFactory.createProxyEntity(fashionEntity);
				proxyFashionEntity.setUnlocked(unlocked);
				GameServer.fashionEntityService.updateFashion(proxyFashionEntity);
				return ProtocolsConfig.CHANGE_FASHION_UNLOCK_SUCCESS;
			}
			else {
				return ProtocolsConfig.CHANGE_FASHION_UNLOCK_FAIL;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ProtocolsConfig.CHANGE_FASHION_UNLOCK_FAIL;
	}
		
	public void pushSynWeaponInfo(String account, WeaponEntity weaponEntity, int source) {
		GWSynWeaponInfoRES.Builder sendBuilder = GWSynWeaponInfoRES.newBuilder();
		sendBuilder.setAccount(account);
		sendBuilder.setSynSource(source);
		sendBuilder.setWeaponInfo(weaponEntity.toProto());
		
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		RemoteNode remoteNode = service.getGatewayNode(3000);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}

	public void pushSynFashionInfo(String account, FashionEntity fashionEntity, int source) {
		GWSynFashionInfoRES.Builder sendBuilder = GWSynFashionInfoRES.newBuilder();
		sendBuilder.setAccount(account);
		sendBuilder.setSynSource(source);
		sendBuilder.setFashionInfo(fashionEntity.toProto());
		
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		RemoteNode remoteNode = service.getGatewayNode(3000);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
}
