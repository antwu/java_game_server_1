package com.game.game.service.room;

public class RoomState
{
	private int slientRoomCount;
	private int positiveRoomCount;
	
	private static volatile RoomState instance = null;
	private RoomState()
	{
	}
	public static RoomState getInstance()
	{
		if(instance == null)
		{
			synchronized(RoomState.class)
			{
				if(instance == null)
					instance = new RoomState();
			}
		}
		return instance;
	}
	
	public int getPositiveRoomCount()
	{
		return positiveRoomCount;
	}

	public void setPositiveRoomCount(int positiveRoomCount)
	{
		this.positiveRoomCount = positiveRoomCount;
	}

	public int getSlientRoomCount()
	{
		return slientRoomCount;
	}

	public void setSlientRoomCount(int slientRoomCount)
	{
		this.slientRoomCount = slientRoomCount;
	}
}
