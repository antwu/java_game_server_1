package com.game.game.start;

import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.server.SimpleSSNettyInitializer;
import com.game.core.net.server.SimpleServer;

public class NetInitializer
{
	static private Logger logger = LoggerFactory.getLogger(NetInitializer.class);
	private SSMessageInitializer ssMsgInitializer;
	private SimpleSSNettyInitializer ssInitializer;
	private SimpleServer ggServer;	
	
	public void init() throws Exception
	{
		ssMsgInitializer = new SSMessageInitializer("com.game.game.action");
		ssMsgInitializer.initialize();
		ssInitializer = new SimpleSSNettyInitializer(ssMsgInitializer);
		ggServer = new SimpleServer();	
		ggServer.initialize(ssInitializer, ssMsgInitializer);
		Transmitter.getInstance().setMessageInitializer(ssMsgInitializer);
	}
	
	public void openPort4Server(int port)
	{
		boolean ret = ggServer.openPort(new InetSocketAddress(port));
		if (ret == false) {
			logger.error("Open Port Filed!",port);
		}
		logger.info("Begin to open port:{} for Server", port);
	}
}
