package com.game.game.start;

//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.game.common.config.BaseLocalConfigInitializer;
//import com.game.common.config.BaseServerConfig;
//import com.game.core.dbcs.DBCS;
//import com.game.core.dbcs.DBCSConst;
//import com.game.core.dbcs.config.DbConfigInfo;
//import com.game.dbpersistence.game.bean.ServerConfigBean;
//import com.game.dbpersistence.game.dao.IServerConfigBeanDao;

public class ServerConfigInitializer
{
//	static private Logger logger = LoggerFactory.getLogger(ServerConfigInitializer.class);
//	private BaseServerConfig config;
//
//	public void init(BaseServerConfig config) throws Exception
//	{
//		BaseLocalConfigInitializer baseLocalInitializer = new BaseLocalConfigInitializer();
//		baseLocalInitializer.init(config);
//		this.config = config;
//		loadDBConfig();
//		loadMSServerConfigFromDB();
//	}
//
//	private void loadMSServerConfigFromDB() throws Exception
//	{
//		IServerConfigBeanDao dao = DBCS.getExector(IServerConfigBeanDao.class);
//		ServerConfigBean bean = dao.selectServerConfigBeanByidxServerName(config.getServerName());
//		if (bean == null)
//		{
//			logger.error("Can't load the manage server config from DB");
//			throw new Exception("Can't load the manage server config from DB");
//		}
//
//		config.setServerID(bean.getUid());
//		config.setServerType(bean.getServerType());
//		config.setServerName(bean.getServerName());
//		config.setHostName4Server(bean.getIPForServer());
//		config.setPort4Server(bean.getPortForServer());
//		config.setHostName4Client(bean.getIPForClient() == null ? new String("") : bean.getIPForClient());
//		config.setPort4Client(bean.getPortForClient());
//		config.setHostName4GMTools(bean.getIPForGMT());
//		config.setPort4GMTools(bean.getPortForGMT());
//
//		logger.info("HostName4Server:{}  Port4Server:{}", config.getHostName4Server(), config.getPort4Server());
//		logger.info("Success to load server config from DB!");
//	}
//	
//	public static List<Integer> parseDependenceServerType(String dependenceString)
//	{
//		List<Integer> typeIDs = new ArrayList<Integer>();
//		if (dependenceString.isEmpty())
//			return typeIDs;
//		String[] types = dependenceString.split(";");
//		
//		for(String type : types)
//			typeIDs.add(Integer.parseInt(type));
//		return typeIDs;
//	}
//	
//	private void loadDBConfig() throws Exception
//	{                                                                                                  
//		InputStream propertiesStream = null;
//		propertiesStream = ServerConfigInitializer.class.getResourceAsStream("/db.properties");
//		
//		Properties properties = new Properties();
//		try
//		{
//			properties.load(propertiesStream);
//		} 
//		catch (IOException e)
//		{
//			logger.error("Failed load base local config from /db.properties");
//			throw e;
//		}
//
//		String dbHostName = properties.getProperty("DBHostName");
//		String dbName = properties.getProperty("DBName");
//		String userName = properties.getProperty("UserName");
//		String password = properties.getProperty("Password");
//		
//		if (dbHostName == null || dbName == null || userName == null || password == null)
//		{
//			logger.error("Illegal content in the /db.properties");
//			throw new Exception("Illegal content in the /db.properties");
//		}
//		
//		DbConfigInfo dbConfigInfo = new DbConfigInfo(dbHostName, dbName, userName,password,DBCSConst.SQL_MAP_CONFIG);
//		DBCS.initialize(null, dbConfigInfo);
//	}
}
