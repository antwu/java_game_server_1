package com.game.game.start;


import com.kodgames.core.timer.TimerMgr;
import com.game.game.util.DateTimeUtil;

public class TaskInitializer
{
	private static TaskInitializer instance = new TaskInitializer();
	private TaskInitializer()
	{
	}
	
	public static TaskInitializer getInstance()
	{
		return instance;
	}
	public int init()
	{
		TimerMgr.getTimerMgr().SetBaseTime(DateTimeUtil.getCurrentTimeMillis());
		TimerMgr.getTimerMgr().start();
		
		return 0;
	}
}
