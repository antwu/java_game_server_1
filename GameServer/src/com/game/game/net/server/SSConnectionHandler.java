package com.game.game.net.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.AbstractConnectionHandler;
import com.game.core.service.ServiceContainer;
import com.game.game.service.server.ServerService;

public class SSConnectionHandler extends AbstractConnectionHandler<Object> 
{
	static private Logger logger = LoggerFactory.getLogger(SSConnectionHandler.class);
	@Override
    public void handleConnectionActive(RemoteNode remoteNode)
    {
	    logger.info("Connection active. Remote Server Address:{}", remoteNode.getAddress());
    }

	@Override
    public void handleConnectionInactive(RemoteNode remoteNode)
    {
		logger.info("Connection inactive. Remote Server Address:{}", remoteNode.getAddress());  
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		service.onServerDisconnect(remoteNode);
    }

	@Override
    public Object getMessageKey(RemoteNode remoteNode)
    {
	    return null;
    }
}
