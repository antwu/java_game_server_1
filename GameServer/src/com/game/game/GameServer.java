package com.game.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.game.common.config.BaseLocalConfigInitializer;
import com.game.common.config.BaseServerConfig;
import com.game.core.db.service.proxy.EntityServiceProxyFactory;
import com.game.core.service.ServiceContainer;
import com.game.dbpersistence.game.service.entity.impl.AccountEntityService;
import com.game.dbpersistence.game.service.entity.impl.BuncherEntityService;
import com.game.dbpersistence.game.service.entity.impl.FashionEntityService;
import com.game.dbpersistence.game.service.entity.impl.ItemEntityService;
import com.game.dbpersistence.game.service.entity.impl.MakingEntityService;
import com.game.dbpersistence.game.service.entity.impl.MissionEntityService;
import com.game.dbpersistence.game.service.entity.impl.TeamEntityService;
import com.game.dbpersistence.game.service.entity.impl.WeaponEntityService;
import com.game.dbpersistence.game.service.entity.impl.WeaponsetEntityService;
import com.game.game.config.XMLTemplateService;
import com.game.game.service.account.AccountLoginService;
import com.game.game.service.player.PlayerService;
import com.game.game.service.server.ServerService;
import com.game.game.start.NetInitializer;
import com.game.game.start.TaskInitializer;

public class GameServer
{
	private static Logger logger = LoggerFactory.getLogger(GameServer.class);
	public static ClassPathXmlApplicationContext classPathXmlApplicationContext;
	public static AccountEntityService accountEntityService = null;
	public static BuncherEntityService buncherEntityService = null;
	public static FashionEntityService fashionEntityService = null;
	public static ItemEntityService itemEntityService = null;
	public static MakingEntityService makingEntityService = null;
	public static MissionEntityService missionEntityService = null;
	public static TeamEntityService teamEntityService = null;
	public static WeaponEntityService weaponEntityService = null;
	public static WeaponsetEntityService weaponsetEntityService = null;
	
	public static void main(String[] args)
	{
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
		if (GameServer.classPathXmlApplicationContext == null) {
			GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(
					new String[] { "bean/applicationContext.xml" });
		}
		if (GameServer.classPathXmlApplicationContext != null) {
			try {
				if (accountEntityService == null) {
					accountEntityService = getAccountEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (buncherEntityService == null) {
					buncherEntityService = getBuncherEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (fashionEntityService == null) {
					fashionEntityService = getFashionEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (itemEntityService == null) {
					itemEntityService = getItemEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (makingEntityService == null) {
					makingEntityService = getMakingEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (missionEntityService == null) {
					missionEntityService = getMissionEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (teamEntityService == null) {
					teamEntityService = getTeamEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (weaponEntityService == null) {
					weaponEntityService = getWeaponEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (weaponsetEntityService == null) {
					weaponsetEntityService = getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
			} catch (Exception e) {
				// e.printStackTrace();
				logger.error(e.toString());
			}
		}

		BaseServerConfig config = new BaseServerConfig();
		try {
			XMLTemplateService.initTemplateData();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		BaseLocalConfigInitializer baseLocalInitializer = new BaseLocalConfigInitializer();
		try
		{
			baseLocalInitializer.init(config);
			logger.info("Success to intialize configuration!");
		}
		catch (Exception e)
		{
			logger.error("Failed to initialize config Exception err={}" , e.toString());
			return;
		}

		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		service.setServerConfig(config);
		// init PlayerService
		ServiceContainer.getInstance().getPublicService(PlayerService.class);
		NetInitializer netInitializer = new NetInitializer();
		try
		{
			netInitializer.init();
			netInitializer.openPort4Server(config.getPort4Server());
			
			AccountLoginService accountLoginService = ServiceContainer.getInstance().getPublicService(AccountLoginService.class);
			accountLoginService.reloadFormDB();
			
			// task initial
			if (TaskInitializer.getInstance().init() != 0)
			{
				logger.error("initial task failed.");
				System.exit(-3);
			}
			
			logger.info("GameServer is ready!!!");
		}
		catch (Exception e)
		{
			logger.error("Failed to initialize network!!! Exception err={}", e.toString());
			return;
		}
//		try 
//		{
//		} 
//		catch (Exception e) 
//		{
//			logger.error("Failed to init BILogConstant {}", e);
//			return;
//		}
	}

	public static AccountEntityService getAccountEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		AccountEntityService accountEntityService = (AccountEntityService) classPathXmlApplicationContext.getBean("accountEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		accountEntityService = entityServiceProxyFactory.createProxyService(accountEntityService);
		return accountEntityService;
	}

	public static BuncherEntityService getBuncherEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception  {
		BuncherEntityService buncherEntityService = (BuncherEntityService) classPathXmlApplicationContext.getBean("buncherEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		buncherEntityService = entityServiceProxyFactory.createProxyService(buncherEntityService);
		return buncherEntityService;
	}

	public static FashionEntityService getFashionEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception  {
		FashionEntityService fashionEntityService = (FashionEntityService) classPathXmlApplicationContext.getBean("fashionEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		fashionEntityService = entityServiceProxyFactory.createProxyService(fashionEntityService);
		return fashionEntityService;
	}

	public static ItemEntityService getItemEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception  {
		ItemEntityService itemEntityService = (ItemEntityService) classPathXmlApplicationContext.getBean("itemEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		itemEntityService = entityServiceProxyFactory.createProxyService(itemEntityService);
		return itemEntityService;
	}
	
	public static MakingEntityService getMakingEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext2) throws Exception {
		MakingEntityService makingEntityService = (MakingEntityService) classPathXmlApplicationContext.getBean("makingEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		makingEntityService = entityServiceProxyFactory.createProxyService(makingEntityService);
		return makingEntityService;
	}

	public static MissionEntityService getMissionEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		MissionEntityService missionEntityService = (MissionEntityService) classPathXmlApplicationContext.getBean("missionEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		missionEntityService = entityServiceProxyFactory.createProxyService(missionEntityService);
		return missionEntityService;
	}	

	public static TeamEntityService getTeamEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		TeamEntityService teamEntityService = (TeamEntityService) classPathXmlApplicationContext.getBean("teamEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		teamEntityService = entityServiceProxyFactory.createProxyService(teamEntityService);
		return teamEntityService;
	}

	public static WeaponEntityService getWeaponEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		WeaponEntityService weaponEntityService = (WeaponEntityService) classPathXmlApplicationContext.getBean("weaponEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		weaponEntityService = entityServiceProxyFactory.createProxyService(weaponEntityService);
		return weaponEntityService;
	}


	public static WeaponsetEntityService getWeaponsetEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		WeaponsetEntityService weaponsetEntityService = (WeaponsetEntityService) classPathXmlApplicationContext.getBean("weaponsetEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		weaponsetEntityService = entityServiceProxyFactory.createProxyService(weaponsetEntityService);
		return weaponsetEntityService;
	}

}
