package com.game.game.util;

import java.util.Random;

public class RandomHelper
{
	/*
	 * 生成[low,up)之间的随机数
	 */
	public static int getRandom(int low, int up)
	{
		if(up <= low)
			throw new IllegalArgumentException("up should bigger than low");
		Random random = new Random();
		return random.nextInt(up - low) + low;
	}
}
