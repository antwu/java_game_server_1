package com.game.game.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileReaderUtil {

	public static String getFileString(String filePath) { 
		StringBuilder sb = new StringBuilder();
		try {
            File file=new File(filePath);
            if(file.isFile() && file.exists()){ //判断文件是否存在
                String encoding = getCharset(filePath);
                InputStreamReader read = new InputStreamReader(
                new FileInputStream(file),encoding);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = "";
                while((lineTxt = bufferedReader.readLine()) != null){
                    sb.append(lineTxt);
                }
                read.close();
		    }else{
		        System.out.println("找不到文件:" + filePath);
		    }
	    } catch (Exception e) {
	        System.out.println("读取文件内容出错:" + filePath);
	        e.printStackTrace();
	    }
		return sb.toString();
	}
	
	public static String getCharset(String filePath) throws IOException{  
        
//        BufferedInputStream bin = new BufferedInputStream(new FileInputStream(filePath));    
//        int p = (bin.read() << 8) + bin.read();    
//        bin.close();
//          
//        String code = null;    
//          
//        switch (p) {    
//            case 0xefbb:    
//                code = "UTF-8";    
//                break;    
//            case 0xfffe:    
//                code = "Unicode";    
//                break;    
//            case 0xfeff:    
//                code = "UTF-16BE";    
//                break;    
//            default:    
//                code = "GBK";    
//        }    
//        return code;  
		return "UTF-8";
} 
}
