package com.game.game.action.making;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.making.MakingService;
import com.game.message.proto.making.MakingProtoBuf.GWSpeedEquipmentRES;
import com.game.message.proto.making.MakingProtoBuf.WGSpeedEquipmentREQ;

@ActionAnnotation(actionClass = WGSpeedEquipmentREQAction.class, messageClass =  WGSpeedEquipmentREQ.class, serviceClass = MakingService.class)
public class WGSpeedEquipmentREQAction extends SSProtobufMessageHandler<MakingService,  WGSpeedEquipmentREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGSpeedEquipmentREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, MakingService service,  WGSpeedEquipmentREQ message, int callback) {
		logger.info("account={}, getMakingId={}", message.getAccount(),message.getMakingId());
		GWSpeedEquipmentRES res = service.speedEquipmentRequest(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}
}
