package com.game.game.action.making;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.making.MakingService;
import com.game.message.proto.making.MakingProtoBuf.GWMakeEquipmentRES;
import com.game.message.proto.making.MakingProtoBuf.WGMakeEquipmentREQ;

@ActionAnnotation(actionClass = WGMakeEquipmentREQAction.class, messageClass =  WGMakeEquipmentREQ.class, serviceClass = MakingService.class)
public class WGMakeEquipmentREQAction extends SSProtobufMessageHandler<MakingService,  WGMakeEquipmentREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGMakeEquipmentREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, MakingService service,  WGMakeEquipmentREQ message, int callback) {
		logger.info("account={}, getMakingId={} ,getOperate={}", message.getAccount(),message.getMakingId(),message.getOperate());
		GWMakeEquipmentRES res = service.makeEquipmentRequest(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}
}
