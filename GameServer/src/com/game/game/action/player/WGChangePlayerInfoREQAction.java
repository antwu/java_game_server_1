package com.game.game.action.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.player.PlayerService;
import com.game.message.proto.player.PlayerProtoBuf.GWChangePlayerInfoRES;
import com.game.message.proto.player.PlayerProtoBuf.WGChangePlayerInfoREQ;

@ActionAnnotation(actionClass = WGChangePlayerInfoREQAction.class, messageClass = WGChangePlayerInfoREQ.class, serviceClass = PlayerService.class)
public class WGChangePlayerInfoREQAction extends SSProtobufMessageHandler<PlayerService, WGChangePlayerInfoREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangePlayerInfoREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, PlayerService service, WGChangePlayerInfoREQ message,
			int callback) {
		logger.info("WGChangePlayerInfoREQAction account={}, newName={}, appearance={}, newSex={}", message.getAccount(), message.getName(), message.getAppearance(), message.getSex());
		GWChangePlayerInfoRES res = service.changePlayerInfo(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
