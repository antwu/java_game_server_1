package com.game.game.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.room.RoomService;
import com.game.message.proto.room.RoomProtoBuf.BGRoomCreateRES;

@ActionAnnotation(actionClass = BGRoomCreateRESAction.class, messageClass = BGRoomCreateRES.class, serviceClass = RoomService.class)
public class BGRoomCreateRESAction extends SSProtobufMessageHandler<RoomService, BGRoomCreateRES>
{
	final static Logger logger = LoggerFactory.getLogger(BGRoomCreateRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, BGRoomCreateRES message, int callback)
	{
		logger.info("BGRoomCreateRESAction accountID={}, roomID={}", message.getAccountID(), message.getRoomID());
		service.createRoomResult(message, remoteNode);
	}

	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BGRoomCreateRES message)
    {
	    return protocoliD;
    }
}
