package com.game.game.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.room.RoomService;
import com.game.message.proto.room.RoomProtoBuf.WGRoomEnterREQ;

@ActionAnnotation(actionClass = WGRoomEnterREQAction.class, messageClass = WGRoomEnterREQ.class, serviceClass = RoomService.class)
public class WGRoomEnterREQAction extends SSProtobufMessageHandler<RoomService, WGRoomEnterREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGRoomEnterREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, WGRoomEnterREQ message, int callback)
	{
		logger.info("WGRoomEnterREQAction account={} playerID={} Room={}", message.getAccountID(), message.getID(), message.getRoomID());
//		service.enterRoom(remoteNode, message, callback);
	}
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, WGRoomEnterREQ message)
    {
	    return message.getRoomID();
    }
}
