package com.game.game.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.room.RoomService;
import com.game.message.proto.room.RoomProtoBuf.WGRoomCreateREQ;

@ActionAnnotation(actionClass = WGRoomCreateREQAction.class, messageClass = WGRoomCreateREQ.class, serviceClass = RoomService.class)
public class WGRoomCreateREQAction extends SSProtobufMessageHandler<RoomService, WGRoomCreateREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGRoomCreateREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, WGRoomCreateREQ message, int callback)
	{
		logger.info("WGRoomCreateREQAction accountID={} playerID={}", message.getAccountID(), message.getID());
		service.createRoom(remoteNode, message, callback);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, WGRoomCreateREQ message)
	{
		return message.getAccountID();
	}
}