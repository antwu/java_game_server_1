package com.game.game.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.room.RoomHelper;
import com.game.game.service.room.RoomService;
import com.game.message.proto.room.RoomProtoBuf.BGRoomStateRES;

@ActionAnnotation(actionClass = BGRoomStateRESAction.class, messageClass = BGRoomStateRES.class, serviceClass = RoomService.class)
public class BGRoomStateRESAction extends SSProtobufMessageHandler<RoomService, BGRoomStateRES>
{
	final static Logger logger = LoggerFactory.getLogger(BGRoomStateRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, BGRoomStateRES message, int callback)
	{
		logger.info("BGRoomStateRESAction:  slientRoomCount={}, positiveRoomCount={}", 
			message.getSlientRoomCount(), message.getPositiveRoomCount());
		RoomHelper.addRoomState(message.getSlientRoomCount(), message.getPositiveRoomCount());
	}
}