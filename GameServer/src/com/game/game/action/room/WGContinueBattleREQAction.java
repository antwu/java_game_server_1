package com.game.game.action.room;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.room.RoomService;
import com.game.message.proto.room.RoomProtoBuf.WGContinueBattleREQ;

@ActionAnnotation(actionClass = WGContinueBattleREQAction.class, messageClass = WGContinueBattleREQ.class, serviceClass = RoomService.class)
public class WGContinueBattleREQAction extends SSProtobufMessageHandler<RoomService, WGContinueBattleREQ>
{
	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, WGContinueBattleREQ message, int callback)
	{
	}
}
