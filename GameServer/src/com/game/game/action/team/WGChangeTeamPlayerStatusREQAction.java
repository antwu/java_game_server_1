package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGChangeTeamPlayerStatusREQ;

@ActionAnnotation(actionClass = WGChangeTeamPlayerStatusREQAction.class, messageClass = WGChangeTeamPlayerStatusREQ.class, serviceClass = TeamService.class)
public class WGChangeTeamPlayerStatusREQAction extends SSProtobufMessageHandler<TeamService, WGChangeTeamPlayerStatusREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGChangeTeamPlayerStatusREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGChangeTeamPlayerStatusREQ message, int callback) {
		logger.info("WGChangeTeamPlayerStatusREQAction handleMessage account={}, missionId={}, teamId={},changeStatus={}",
					message.getAccount(), message.getMissionId(), message.getTeamId(), message.getChangeStatus());
		service.changeTeamPlayerStatus(remoteNode, message, callback);		
	}
}
