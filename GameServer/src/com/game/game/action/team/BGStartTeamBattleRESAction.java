package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.BGStartTeamBattleRES;

@ActionAnnotation(actionClass = BGStartTeamBattleRESAction.class, messageClass = BGStartTeamBattleRES.class, serviceClass = TeamService.class)
public class BGStartTeamBattleRESAction extends SSProtobufMessageHandler<TeamService, BGStartTeamBattleRES>
{
	final static Logger logger = LoggerFactory.getLogger(BGStartTeamBattleRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, BGStartTeamBattleRES message, int callback) {
		logger.info("BGStartTeamBattleRES handleMessage account={}, result={} ", 
				message.getAccount(), message.getResult());
		service.startTeamBattleResult(message);
	}
}
