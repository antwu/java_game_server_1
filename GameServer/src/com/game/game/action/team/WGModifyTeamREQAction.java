package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGModifyTeamREQ;

@ActionAnnotation(actionClass = WGModifyTeamREQAction.class, messageClass = WGModifyTeamREQ.class, serviceClass = TeamService.class)
public class WGModifyTeamREQAction extends SSProtobufMessageHandler<TeamService, WGModifyTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGModifyTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGModifyTeamREQ message, int callback) {
		logger.info("WGModifyTeamREQAction handleMessage account={}, missionId={}, teamId={}", 
				message.getAccount(), message.getMissionId(), message.getTeamId());
		service.modifyTeam(remoteNode, message, callback);
	}
}
