package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.battle.BattleProtoBuf.BGSingleGameOverRES;

@ActionAnnotation(actionClass = BGSingleGameOverRESAction.class, messageClass = BGSingleGameOverRES.class, serviceClass = TeamService.class)
public class BGSingleGameOverRESAction extends SSProtobufMessageHandler<TeamService, BGSingleGameOverRES>
{
	final static Logger logger = LoggerFactory.getLogger(BGSingleGameOverRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, BGSingleGameOverRES message, int callback) {
		logger.info("BGSingleGameOverRES handleMessage account={}, mission={} ", message.getAccount(), message.getMissionId());
				service.handleAfterSingleBattle(message);
	}
}
