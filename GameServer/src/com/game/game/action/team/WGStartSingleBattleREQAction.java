package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGStartSingleBattleREQ;

@ActionAnnotation(actionClass = WGStartSingleBattleREQAction.class, messageClass = WGStartSingleBattleREQ.class, serviceClass = TeamService.class)
public class WGStartSingleBattleREQAction extends SSProtobufMessageHandler<TeamService, WGStartSingleBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGStartSingleBattleREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGStartSingleBattleREQ message, int callback) {
		logger.info("WGStartTeamBattleREQAction handleMessage account={}, missionId={}", 
				message.getAccount(), message.getMissionId());
		service.startSingleBattle(remoteNode, message, callback);
	}
}
