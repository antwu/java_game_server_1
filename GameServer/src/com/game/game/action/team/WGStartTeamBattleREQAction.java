package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGStartTeamBattleREQ;

@ActionAnnotation(actionClass = WGStartTeamBattleREQAction.class, messageClass = WGStartTeamBattleREQ.class, serviceClass = TeamService.class)
public class WGStartTeamBattleREQAction extends SSProtobufMessageHandler<TeamService, WGStartTeamBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGStartTeamBattleREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGStartTeamBattleREQ message, int callback) {
		logger.info("WGStartTeamBattleREQAction handleMessage account={}, missionId={}, teamId={} ", 
				message.getAccount(), message.getMissionId(), message.getTeamId());
		service.startTeamBattle(remoteNode, message, callback);
	}
}
