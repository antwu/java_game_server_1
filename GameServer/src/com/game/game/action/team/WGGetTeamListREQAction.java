package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGGetTeamListREQ;

@ActionAnnotation(actionClass = WGGetTeamListREQAction.class, messageClass = WGGetTeamListREQ.class, serviceClass = TeamService.class)
public class WGGetTeamListREQAction extends SSProtobufMessageHandler<TeamService, WGGetTeamListREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGGetTeamListREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGGetTeamListREQ message, int callback) {
		logger.info("WGGetTeamListREQAction handleMessage account={}, missionId={}", 
				message.getAccount(), message.getMissionId());
		service.getTeamList(remoteNode, message, callback);		
	}
}
