package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGQuickJoinTeamREQ;

@ActionAnnotation(actionClass = WGQuickJoinTeamREQAction.class, messageClass = WGQuickJoinTeamREQ.class, serviceClass = TeamService.class)
public class WGQuickJoinTeamREQAction extends SSProtobufMessageHandler<TeamService, WGQuickJoinTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGQuickJoinTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGQuickJoinTeamREQ message, int callback) {
		logger.info("WGKickTeamMemberREQAction handleMessage account={}, missionId={}", 
				message.getAccount(), message.getMissionId());
		service.quickJoinTeam(remoteNode, message, callback);
	}
}
