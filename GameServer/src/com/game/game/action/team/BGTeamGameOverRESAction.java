package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.battle.BattleProtoBuf.BGTeamGameOverRES;

@ActionAnnotation(actionClass = BGTeamGameOverRESAction.class, messageClass = BGTeamGameOverRES.class, serviceClass = TeamService.class)
public class BGTeamGameOverRESAction extends SSProtobufMessageHandler<TeamService, BGTeamGameOverRES>
{
	final static Logger logger = LoggerFactory.getLogger(BGTeamGameOverRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, BGTeamGameOverRES message, int callback) {
		logger.info("BGTeamGameOverRESAction handleMessage account={}, result={} ", 
				message.getMissionId(), message.getTeamId());
				service.handleAfterTeamBattle(message);
	}
}
