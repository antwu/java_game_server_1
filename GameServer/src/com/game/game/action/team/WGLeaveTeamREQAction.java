package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGLeaveTeamREQ;

@ActionAnnotation(actionClass = WGLeaveTeamREQAction.class, messageClass = WGLeaveTeamREQ.class, serviceClass = TeamService.class)
public class WGLeaveTeamREQAction extends SSProtobufMessageHandler<TeamService, WGLeaveTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGLeaveTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGLeaveTeamREQ message, int callback) {
		logger.info("WGLeaveTeamREQAction handleMessage account={}, missionId={}, teamId={} ", 
				message.getAccount(), message.getMissionId(), message.getTeamId());
		service.leaveTeam(remoteNode, message, callback);
	}
}
