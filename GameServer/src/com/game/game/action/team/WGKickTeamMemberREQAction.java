package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGKickTeamMemberREQ;

@ActionAnnotation(actionClass = WGKickTeamMemberREQAction.class, messageClass = WGKickTeamMemberREQ.class, serviceClass = TeamService.class)
public class WGKickTeamMemberREQAction extends SSProtobufMessageHandler<TeamService, WGKickTeamMemberREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGKickTeamMemberREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGKickTeamMemberREQ message, int callback) {
		logger.info("WGKickTeamMemberREQAction handleMessage account={}, missionId={}, teamId={}, kickaccount={}", 
				message.getAccount(), message.getMissionId(), message.getTeamId(), message.getKickAccount());
		service.kickTeamMember(remoteNode, message, callback);
	}
}
