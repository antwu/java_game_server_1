package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.BGStartSingleBattleRES;

@ActionAnnotation(actionClass = BGStartSingleBattleRESAction.class, messageClass = BGStartSingleBattleRES.class, serviceClass = TeamService.class)
public class BGStartSingleBattleRESAction extends SSProtobufMessageHandler<TeamService, BGStartSingleBattleRES>
{
	final static Logger logger = LoggerFactory.getLogger(BGStartSingleBattleRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, BGStartSingleBattleRES message, int callback) {
		logger.info("BGStartTeamBattleRES handleMessage account={}, result={} ", 
				message.getAccount(), message.getResult());
		service.startSingleBattleResult(message);
	}
}
