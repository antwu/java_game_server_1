package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGCreateTeamREQ;

@ActionAnnotation(actionClass = WGCreateTeamREQAction.class, messageClass =  WGCreateTeamREQ.class, serviceClass = TeamService.class)
public class WGCreateTeamREQAction extends SSProtobufMessageHandler<TeamService,  WGCreateTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGCreateTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service,  WGCreateTeamREQ message, int callback) {
		logger.info("WGCreateTeamREQAction handleMessage account={}, missionid={}, pass={}", 
				message.getAccount(), message.getMissionId(), message.getPassword());
		service.createTeam(remoteNode, message, callback);				
	}
}
