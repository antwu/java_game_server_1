package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGJoinTeamREQ;

@ActionAnnotation(actionClass = WGJoinTeamREQAction.class, messageClass = WGJoinTeamREQ.class, serviceClass = TeamService.class)
public class WGJoinTeamREQAction extends SSProtobufMessageHandler<TeamService, WGJoinTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGJoinTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGJoinTeamREQ message, int callback) {
		logger.info("WGJoinTeamREQAction handleMessage account={}, missionId={}, teamId={}", 
				message.getAccount(), message.getMissionId(), message.getTeamId());
		service.joinTeam(remoteNode, message, callback);
	}
}
