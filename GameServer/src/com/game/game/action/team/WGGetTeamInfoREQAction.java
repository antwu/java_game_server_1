package com.game.game.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.WGGetTeamInfoREQ;

@ActionAnnotation(actionClass = WGGetTeamInfoREQAction.class, messageClass = WGGetTeamInfoREQ.class, serviceClass = TeamService.class)
public class WGGetTeamInfoREQAction extends SSProtobufMessageHandler<TeamService, WGGetTeamInfoREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGGetTeamInfoREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, WGGetTeamInfoREQ message, int callback) {
		logger.info("WGGetTeamInfoREQAction handleMessage account={}, missionId={}, teamId={} ", 
				message.getAccount(), message.getMissionId(), message.getTeamId());
		service.getTeamInfo(remoteNode, message, callback);	
	}
}
