package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeFashionUnlockREQ;

@ActionAnnotation(actionClass = WGChangeFashionUnlockREQAction.class, messageClass = WGChangeFashionUnlockREQ.class, serviceClass = WeaponService.class)
public class WGChangeFashionUnlockREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeFashionUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeFashionUnlockREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeFashionUnlockREQ message,
			int callback) {
		logger.info("account={}, getUnlocked={}, fashionId={}", message.getAccount(), message.getUnlocked(), message.getFashionId());
		GWChangeFashionUnlockRES res = service.changeFashionUnlock(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
