package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeWeaponSetREQ;

@ActionAnnotation(actionClass = WGChangeWeaponSetREQAction.class, messageClass = WGChangeWeaponSetREQ.class, serviceClass = WeaponService.class)
public class WGChangeWeaponSetREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeWeaponSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeWeaponSetREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeWeaponSetREQ message,
			int callback) {
		logger.info("account={}, newWeapon={}, changeSet={}, changePosition={}", message.getAccount(), message.getNewWeapon(), message.getChangeWeaponSet(), message.getChangePosition());
		GWChangeWeaponSetRES res = service.changeWeaponSet(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
