package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetNameRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeWeaponSetNameREQ;

@ActionAnnotation(actionClass = WGChangeWeaponSetNameREQAction.class, messageClass = WGChangeWeaponSetNameREQ.class, serviceClass = WeaponService.class)
public class WGChangeWeaponSetNameREQAction  extends SSProtobufMessageHandler<WeaponService, WGChangeWeaponSetNameREQ>{
	final static Logger logger = LoggerFactory.getLogger(WGChangeWeaponSetNameREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeWeaponSetNameREQ message,
			int callback) {
		logger.info("account={}, changeSet={}, name={}", message.getAccount(), message.getChangeWeaponSet(), message.getNewName());
		GWChangeWeaponSetNameRES res = service.changeWeaponSetName(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
