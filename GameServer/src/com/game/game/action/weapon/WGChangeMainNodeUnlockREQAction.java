package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeMainNodeUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeMainNodeUnlockREQ;

@ActionAnnotation(actionClass = WGChangeMainNodeUnlockREQAction.class, messageClass = WGChangeMainNodeUnlockREQ.class, serviceClass = WeaponService.class)
public class WGChangeMainNodeUnlockREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeMainNodeUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeMainNodeUnlockREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeMainNodeUnlockREQ message,
			int callback) {
		logger.info("account={}, getWeaponId={}, getMainId={}, getStatus={}", message.getAccount(), message.getWeaponId(), message.getMainId(), message.getStatus());
		GWChangeMainNodeUnlockRES res = service.changeMainNodeUnlock(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
