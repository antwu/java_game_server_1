package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeColourUnlockREQ;

@ActionAnnotation(actionClass = WGChangeColourUnlockREQAction.class, messageClass = WGChangeColourUnlockREQ.class, serviceClass = WeaponService.class)
public class WGChangeColourUnlockREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeColourUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeColourUnlockREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeColourUnlockREQ message,
			int callback) {
		//logger.info("account={}, fashionId={}, unlockColourList={}", message.getAccount(),message.getFashionId(), message.getUnlockColourList().toString());
		GWChangeColourUnlockRES res = service.changeColourUnlock(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
