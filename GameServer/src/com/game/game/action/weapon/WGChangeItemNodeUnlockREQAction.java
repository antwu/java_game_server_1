package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeItemNodeUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeItemNodeUnlockREQ;

@ActionAnnotation(actionClass = WGChangeItemNodeUnlockREQAction.class, messageClass = WGChangeItemNodeUnlockREQ.class, serviceClass = WeaponService.class)
public class WGChangeItemNodeUnlockREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeItemNodeUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeItemNodeUnlockREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeItemNodeUnlockREQ message,
			int callback) {
		logger.info("account={}, getWeaponId={}, getMainId={}, getItemId={}, getStatus={}", message.getAccount(), message.getWeaponId(), message.getMainId(), message.getItemId(), message.getStatus());
		GWChangeItemNodeUnlockRES res = service.changeItemNodeUnlock(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
