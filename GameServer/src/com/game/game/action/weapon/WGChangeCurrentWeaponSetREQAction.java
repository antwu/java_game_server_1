package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeCurrentWeaponSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeCurrentWeaponSetREQ;

@ActionAnnotation(actionClass = WGChangeCurrentWeaponSetREQAction.class, messageClass = WGChangeCurrentWeaponSetREQ.class, serviceClass = WeaponService.class)
public class WGChangeCurrentWeaponSetREQAction  extends SSProtobufMessageHandler<WeaponService, WGChangeCurrentWeaponSetREQ>{
	final static Logger logger = LoggerFactory.getLogger(WGChangeCurrentWeaponSetREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeCurrentWeaponSetREQ message,
			int callback) {
		logger.info("account={}, changeSet={}", message.getAccount(), message.getChangeWeaponSet());
//		
//		ItemService itemService = ServiceContainer.getInstance().getPublicService(ItemService.class);
//		int a = itemService.addItem(message.getAccount(), 2000, 100);
//		
//		int b = itemService.consumeItem(message.getAccount(), 2000, 50);
//		
//		int c = itemService.consumeItem(message.getAccount(), 400, 100);

		GWChangeCurrentWeaponSetRES res = service.changeCurrentWeaponSet(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
