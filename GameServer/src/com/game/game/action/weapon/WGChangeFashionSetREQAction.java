package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeFashionSetREQ;

@ActionAnnotation(actionClass = WGChangeFashionSetREQAction.class, messageClass = WGChangeFashionSetREQ.class, serviceClass = WeaponService.class)
public class WGChangeFashionSetREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeFashionSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeFashionSetREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeFashionSetREQ message,
			int callback) {
		logger.info("account={}, oldFashion={}, newFashion={}, changeSet={}", message.getAccount(), message.getOldFashionId(), message.getNewFashionId());
		GWChangeFashionSetRES res = service.changeFashionSet(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
