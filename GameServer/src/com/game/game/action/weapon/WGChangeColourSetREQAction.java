package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeColourSetREQ;

@ActionAnnotation(actionClass = WGChangeColourSetREQAction.class, messageClass = WGChangeColourSetREQ.class, serviceClass = WeaponService.class)
public class WGChangeColourSetREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeColourSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeColourSetREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeColourSetREQ message,
			int callback) {
		//logger.info("account={}, fashionId={}, newColour={}, changePosition={} ", message.getAccount(), message.getFashionId(), message.getNewColour(), message.getChangePosition());
		GWChangeColourSetRES res = service.changeColourSet(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
