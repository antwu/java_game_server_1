package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeBuncherSetRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeBuncherSetREQ;

@ActionAnnotation(actionClass = WGChangeBuncherSetREQAction.class, messageClass = WGChangeBuncherSetREQ.class, serviceClass = WeaponService.class)
public class WGChangeBuncherSetREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeBuncherSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeBuncherSetREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeBuncherSetREQ message,
			int callback) {
		logger.info("account={}, newBuncher={}, changeSet={}", message.getAccount(), message.getNewBuncherId(), message.getChangeWeaponSet());
		GWChangeBuncherSetRES res = service.changeBuncherSet(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
