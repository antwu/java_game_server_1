package com.game.game.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeBuncherUnlockRES;
import com.game.message.proto.weapon.WeaponProtoBuf.WGChangeBuncherUnlockREQ;

@ActionAnnotation(actionClass = WGChangeBuncherUnlockREQAction.class, messageClass = WGChangeBuncherUnlockREQ.class, serviceClass = WeaponService.class)
public class WGChangeBuncherUnlockREQAction extends SSProtobufMessageHandler<WeaponService, WGChangeBuncherUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(WGChangeBuncherUnlockREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, WGChangeBuncherUnlockREQ message,
			int callback) {
		logger.info("account={}, getUnlocked={}, changeSet={}", message.getAccount(), message.getUnlocked(), message.getChangeWeaponSet());
		GWChangeBuncherUnlockRES res = service.changeBuncherUnlock(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}

}
