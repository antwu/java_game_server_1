package com.game.game.action.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.config.BaseServerConfig;
import com.game.common.constants.ServerType;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.server.ServerService;
import com.game.message.proto.server.ServerProtoBuf.SSPushBattleListSYN;
import com.game.message.proto.server.ServerProtoBuf.SSPushServerInfoSYN;
import com.game.message.proto.server.ServerProtoBuf.SSRegisterServerREQ;
import com.game.message.proto.server.ServerProtoBuf.SSRegisterServerRES;
import com.game.message.proto.server.ServerProtoBuf.SSUpdateConfigSYN;
import com.game.message.proto.server.ServerProtoBuf.ServerConfigPROTO;
import com.game.message.protocol.ProtocolsConfig;

@ActionAnnotation(actionClass = SSRegisterServerREQAction.class, messageClass=SSRegisterServerREQ.class, serviceClass = ServerService.class)
public class SSRegisterServerREQAction extends SSProtobufMessageHandler<ServerService, SSRegisterServerREQ>
{
	private static Logger logger = LoggerFactory.getLogger(SSRegisterServerREQAction.class);
	@Override
    public void handleMessage(RemoteNode remoteNode, ServerService service, SSRegisterServerREQ message, int callback)
    {
		SSRegisterServerRES.Builder resBuilder = SSRegisterServerRES.newBuilder(); 
	    BaseServerConfig config = service.getServerConfig(message.getServerName());
	    if(config == null)
	    	config = service.getServerConfig(message.getServerType());
	    
	    if (config == null)
	    {
	    	logger.error("Invalid server name:{}", message.getServerName());
	    	resBuilder.setResult(ProtocolsConfig.E_REGISTER_SERVER_INVALID_SERVER_NAME);
	    	Transmitter.getInstance().write(remoteNode, callback, resBuilder.build());
	    	return;
	    }
	    try
	    {
	    	Thread.sleep(3000);
	    }
	    catch(Exception e)
	    {
	    	logger.error("exception arise. Exception err={}", e);
	    }
	    
	    ServerConfigPROTO.Builder configBuilder = config.toProtoBufBuilder();
		if (config.getServerType() == ServerType.BATTLE_SERVER)
		{
			//推送服务器配置
			SSPushServerInfoSYN.Builder serverBuilder = SSPushServerInfoSYN.newBuilder();
			serverBuilder.setServerConfig(configBuilder);
			Transmitter.getInstance().write(remoteNode, callback, serverBuilder.build());
			service.setBattleNode(config.getServerID(), remoteNode);
		}
		else if (config.getServerType() == ServerType.GATEWAY_SERVER)
	    {
			int serverID = message.getServerID();
			if(message.getServerName().equals(config.getServerName()))
				serverID = config.getServerID();
			
	    	//推送服务器配置
			configBuilder.setServerID(serverID);
			configBuilder.setServerName(message.getServerName());
			SSPushServerInfoSYN.Builder serverBuilder = SSPushServerInfoSYN.newBuilder();
			serverBuilder.setServerConfig(configBuilder);
			Transmitter.getInstance().write(remoteNode, callback, serverBuilder.build());
	    	
	    	//注册Gateway RemoteNode
	    	service.setGatewayNode(serverID, remoteNode);
	    	
	    	try
	    	{
	    		Thread.sleep(3000);
	    	}
	    	catch(Exception e)
	    	{
	    		logger.error("exception arise. Exception err={}", e);
	    	}
		    //给GatewayServer推送Battle列表
	    	SSPushBattleListSYN.Builder battleBuilder = SSPushBattleListSYN.newBuilder();
	    	battleBuilder.addAllBattles(service.getBattleList());
	    	Transmitter.getInstance().write(remoteNode, callback, battleBuilder.build());
		    //给GatewayServer推送更新配置
	    	SSUpdateConfigSYN.Builder updateBuilder = SSUpdateConfigSYN.newBuilder();
	    	updateBuilder.addAllUpdates(service.getUpdateConfigList());
	    	Transmitter.getInstance().write(remoteNode, callback, updateBuilder.build());
	    }
    }
}