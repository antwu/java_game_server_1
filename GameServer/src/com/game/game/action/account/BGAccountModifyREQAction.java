package com.game.game.action.account;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.account.AccountLoginService;
import com.game.message.proto.account.AccountProtoBuf.BGAccountModifyREQ;

@ActionAnnotation(actionClass = BGAccountModifyREQAction.class, messageClass = BGAccountModifyREQ.class, serviceClass = AccountLoginService.class)
public class BGAccountModifyREQAction extends SSProtobufMessageHandler<AccountLoginService, BGAccountModifyREQ>
{
	@Override
	public void handleMessage(RemoteNode remoteNode, AccountLoginService service, BGAccountModifyREQ message, int callback)
	{
		service.modifyAccountInfo(remoteNode, message, callback);
	}
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BGAccountModifyREQ message)
    {
	    return message.getAccountID();
    }
}
