package com.game.game.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.account.AccountLoginService;
import com.game.message.proto.account.AccountProtoBuf.GWAccountLoginRES;
import com.game.message.proto.account.AccountProtoBuf.WGAccountLoginREQ;

@ActionAnnotation(actionClass = WGAccountLoginREQAction.class, messageClass = WGAccountLoginREQ.class, serviceClass = AccountLoginService.class)
public class WGAccountLoginREQAction extends SSProtobufMessageHandler<AccountLoginService, WGAccountLoginREQ>
{
	final static Logger logger = LoggerFactory.getLogger(WGAccountLoginREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountLoginService service, WGAccountLoginREQ message, int callback)
	{
		logger.info("WGAccountLoginREQAction account={}, platform={}, session={}", message.getAccount(), message.getPlatform(), message.getSessionID());
		GWAccountLoginRES res = service.accountLogin(remoteNode, message, callback);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, res);
	}
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, WGAccountLoginREQ message)
    {
	    return message.getAccountID();
    }
}
