package com.game.game.action.account;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.game.service.account.AccountLoginService;
import com.game.message.proto.battle.BattleProtoBuf.WGPlayerOfflineSYN;

@ActionAnnotation(serviceClass = AccountLoginService.class, actionClass = WGPlayerOfflineSYNAction.class, messageClass = WGPlayerOfflineSYN.class)
public class WGPlayerOfflineSYNAction extends SSProtobufMessageHandler<AccountLoginService, WGPlayerOfflineSYN>
{
	@Override
	public void handleMessage(RemoteNode remoteNode, AccountLoginService service, WGPlayerOfflineSYN message, int callback)
	{
		service.removeOnlineAccountID(message);
	}
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, WGPlayerOfflineSYN message)
    {
	    return message.getAccountID();
    }
}