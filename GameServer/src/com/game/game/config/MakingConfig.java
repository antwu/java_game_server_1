package com.game.game.config;

public class MakingConfig {
	
	//制作装备的ID
	//private int id = 0;
	
	//制作装备的ID
	private int makingId = 0;
	
	//武器装备的名称
	private String name =""; 
	
	private int type = 0;
	
	private int drawing = 0;
	
	private int drawingNum = 0;
	
	private int partA = 0;
	
	private int numA = 0;
	
	private int partB = 0;
	
	private int numB = 0;
	
	private int partC = 0;
	
	private int numC = 0;
	
	private int time = 0;
	
	private String des = "";
	
	public MakingConfig() {
		init();
	}
	
	public void init() {
		//this.id = 0;
		this.makingId = 0;
		this.name = "";
		this.type = 0;
		this.drawing = 0;
		this.drawingNum = 0;
		this.partA = 0;
		this.numA = 0;
		this.partB = 0;
		this.numB = 0;
		this.partC = 0;
		this.numC = 0;
		this.time = 0;
		this.des = "";
	}

//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}

	public int getMakingId() {
		return makingId;
	}

	public void setMakingId(int makingId) {
		this.makingId = makingId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getDrawing() {
		return drawing;
	}

	public void setDrawing(int drawing) {
		this.drawing = drawing;
	}

	public int getPartA() {
		return partA;
	}

	public void setPartA(int partA) {
		this.partA = partA;
	}

	public int getNumA() {
		return numA;
	}

	public void setNumA(int numA) {
		this.numA = numA;
	}

	public int getPartB() {
		return partB;
	}

	public void setPartB(int partB) {
		this.partB = partB;
	}

	public int getNumB() {
		return numB;
	}

	public void setNumB(int numB) {
		this.numB = numB;
	}

	public int getPartC() {
		return partC;
	}

	public void setPartC(int partC) {
		this.partC = partC;
	}

	public int getNumC() {
		return numC;
	}

	public void setNumC(int numC) {
		this.numC = numC;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public int getDrawingNum() {
		return drawingNum;
	}

	public void setDrawingNum(int drawingNum) {
		this.drawingNum = drawingNum;
	}
	

}
