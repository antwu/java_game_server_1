package com.game.game.config;

import java.util.ArrayList;
import java.util.List;

import com.game.dbpersistence.game.entity.MainNode;

public class WeaponStrengthenConfig {
	
	//武器装备的ID
	private int weaponId;
	//武器装备的名称
	//private String name; 
	//档位（1.简单；2.普通；3.困难；4.噩梦；5.遗迹）
	//private int weaponLevel = 0;
	//武器装备强化主线节点列表
	private  List<MainNode> mainNodeList;
	
	public WeaponStrengthenConfig() {
		init();
	}
	
	public void init() {
		this.weaponId = 0;
		//this.name = "";
		//this.weaponLevel = 0;
		this.mainNodeList = new ArrayList<MainNode>();
	}
	
	public int getWeaponId() {
		return weaponId;
	}
	public void setWeaponId(int weaponId) {
		this.weaponId = weaponId;
	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public int getWeaponLevel() {
//		return weaponLevel;
//	}
//	public void setWeaponLevel(int weaponLevel) {
//		this.weaponLevel = weaponLevel;
//	}
	public List<MainNode> getMainNodeList() {
		return mainNodeList;
	}
	public void setMainNodeList(List<MainNode> mainNodeList) {
		this.mainNodeList = mainNodeList;
	}
}
