package com.game.game.config;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.dbpersistence.game.entity.ItemNode;
import com.game.dbpersistence.game.entity.MainNode;


public class XMLTemplateService {

	private static Logger logger = LoggerFactory.getLogger("ServerLog");

//	private static HashMap<String, Integer> xmlLoadTime = new HashMap<String, Integer>();
//	private static HashMap<String, String> configMd5 = new HashMap<String, String>();


	public static ConcurrentHashMap<Integer, AttributeConfig> attributeConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, FashionConfig> fashionConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, ItemConfig> itemConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, MakingConfig> makingConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, PlayerConfig> playerConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, WeaponConfig> weaponConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, WeaponLevelConfig> weaponLevelConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, WeaponStrengthenConfig> weaponStrengthenConfigMap = new ConcurrentHashMap<>();
	
	public static void initTemplateData() throws Exception {
		XMLConfiguration.setDefaultListDelimiter(' ');
		
		//加载属性配置
		loadAttributeConfig();
		//加载时装配置
		loadFashionConfig();
		//加载物品道具配置
		loadItemConfig();
		//加载装备制作配置
		loadMakingConfig();
		//加载玩家初始化设置
		loadPlayerConfig();
		//加载武器配置
		loadWeaponConfig();
		//加载武器品质等级配置
		//loadWeaponLevelConfig();
		//加载武器强化图配置
		loadWeaponStrengthenConfig();

	}

	/**
	 * 加载武器配置
	 */
	@SuppressWarnings("unchecked")
	private static void loadWeaponConfig() throws Exception{
		String configPath =System.getProperty("user.dir")+ "/xml/weapon.xml";
		logger.info("load weapon config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				WeaponConfig weaponConfig = new WeaponConfig();
				sub = fields.get(i);
				int id = sub.getInt("[@id]");
				weaponConfig.setId(id);
				weaponConfig.setName(sub.getString("[@Name]"));
				weaponConfig.setWeaponType(sub.getInt("[@WeaponType]"));
				weaponConfig.setWeaponLevel(sub.getInt("[@WeaponLevel]"));
//				weaponConfig.setWeaponMaxLevel(sub.getInt("[@WeaponMaxLevel]"));
//				weaponConfig.setWeaponSource(sub.getInt("[@WeaponSource]"));
//				weaponConfig.setWeaponDamageType(sub.getString("[@WeaponDamageType]"));
//				weaponConfig.setDamageParam(sub.getString("[@DamageParam]"));
//				weaponConfig.setCritical(sub.getString("[@Critical]"));
//				weaponConfig.setCriticalDamage(sub.getString("[@CriticalDamage]"));
//				weaponConfig.setBackHitParam(sub.getString("[@BackHitParam]"));
//				weaponConfig.setWeaponFeatureSkill(sub.getString("[@WeaponFeatureSkill]"));
				weaponConfig.setMainSkillId(sub.getInt("[@MainSkill]"));
//				weaponConfig.setTalent(sub.getString("[@talents]"));
//				weaponConfig.setMoveConsume(sub.getString("[@MoveConsume]"));
//				weaponConfig.setListIcon(sub.getString("[@listIcon]"));
//				weaponConfig.setBattleIcon(sub.getString("[@battleIcon]"));
//				weaponConfig.setDes(sub.getString("[@des]"));
//				weaponConfig.setZhuanHuaBi(sub.getString("[@ZhuanHuaBi]"));
//				weaponConfig.setActionGroup(sub.getInt("[@actionGroup]"));
//				weaponConfig.setWeaponPath(sub.getString("[@weaponPath]"));
//				weaponConfig.setSlot(sub.getInt("[@slot]"));
//				weaponConfig.setSequenceNumber(sub.getInt("[@sequenceNumber]"));
				
				weaponConfigMap.put(id, weaponConfig);
			}
		}
	}

	/**
	 * 加载武器等级配置
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private static void loadWeaponLevelConfig() throws Exception{
		String configPath =System.getProperty("user.dir")+ "/xml/weaponLevel.xml";
		logger.info("load weaponLevel config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				WeaponLevelConfig weaponlevelConfig = new WeaponLevelConfig();
				sub = fields.get(i);
				weaponlevelConfig.setId(sub.getInt("[@id]"));
				weaponlevelConfig.setLevels(sub.getInt("[@levels]"));
				weaponlevelConfig.setNodeNum(sub.getInt("[@nodeNum]"));
				weaponlevelConfig.setMainNode(sub.getInt("[@mainNode]"));
				weaponlevelConfig.setSkillNode(sub.getInt("[@skillNode]"));
				weaponlevelConfig.setCombNode(sub.getInt("[@combNode]"));
				weaponlevelConfig.setFinalSkillNode(sub.getInt("[@finalSkillNode]"));
				weaponlevelConfig.setExclusiveNode(sub.getInt("[@exclusiveNode]"));
				weaponlevelConfig.setQuestNode(sub.getInt("[@questNode]"));
				
				weaponLevelConfigMap.put(i, weaponlevelConfig);
			}
		}		
	}
	

	@SuppressWarnings("unchecked")
	private static void loadWeaponStrengthenConfig() throws Exception{
		String configPath =System.getProperty("user.dir")+ "/xml/weaponStrengthen.xml";
		logger.info("load weaponStrengthen config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		HierarchicalConfiguration group = conf.configurationAt("group");
		if(group == null) {	return;	}
		List<HierarchicalConfiguration> weaponFields = group.configurationsAt("weapon");
		if (weaponFields != null && weaponFields.size() > 0) {
			HierarchicalConfiguration weaponFieldsSub;
			for (int i = 0; i < weaponFields.size(); i++) {
				WeaponStrengthenConfig weaponStrengthenConfig = new WeaponStrengthenConfig();
				weaponFieldsSub = weaponFields.get(i);
				int weaponId = weaponFieldsSub.getInt("[@id]");
				weaponStrengthenConfig.setWeaponId(weaponId);
				//weaponStrengthenConfig.setName(weaponFieldsSub.getString("[@name]"));
				//weaponStrengthenConfig.setWeaponLevel(weaponFieldsSub.getInt("[@weaponLevel]"));
				List<HierarchicalConfiguration> mainFields = weaponFieldsSub.configurationsAt("main");
				if (mainFields != null && mainFields.size() > 0) {
					HierarchicalConfiguration mainFieldsSub;
					List<MainNode> mainNodeList = weaponStrengthenConfig.getMainNodeList();
					for (int j = 0; j < mainFields.size(); j++) {
						mainFieldsSub = mainFields.get(j);
						MainNode mainNode = new MainNode();
						int id = mainFieldsSub.getInt("[@id]");
						mainNode.setId(id);
						mainNode.setName(mainFieldsSub.getString("[@name]"));
						mainNode.setType(mainFieldsSub.getInt("[@type]"));
						mainNode.setDir(mainFieldsSub.getInt("[@dir]"));
						mainNode.setCost(mainFieldsSub.getInt("[@unlockNum]"));
						if(id == 1) {
							mainNode.setStatus(1);
						}
						List<HierarchicalConfiguration> itemFields = mainFieldsSub.configurationsAt("item");
						if (itemFields != null && itemFields.size() > 0) {
							HierarchicalConfiguration itemFieldsSub;
							List<ItemNode> itemNodeList = mainNode.getItemNodeList();
							for (int k = 0; k < itemFields.size(); k++) {
								itemFieldsSub = itemFields.get(k);
								ItemNode itemNode = new ItemNode();
								itemNode.setId(itemFieldsSub.getInt("[@id]"));
								itemNode.setName(itemFieldsSub.getString("[@name]"));
								itemNode.setType(itemFieldsSub.getInt("[@type]"));
								itemNode.setTalent(itemFieldsSub.getInt("[@talent]"));
								itemNode.setCost(itemFieldsSub.getInt("[@unlockNum]"));
								itemNodeList.add(itemNode);
							}
						}
						mainNodeList.add(mainNode);
					}
				}
				weaponStrengthenConfigMap.put(weaponId, weaponStrengthenConfig);
			}
		}
	}
	
	/**
	 * 加载属性配置
	 */
	@SuppressWarnings("unchecked")
	private static void loadAttributeConfig() throws Exception{
		String configPath =System.getProperty("user.dir")+ "/xml/attribute.xml";
		logger.info("load attribute config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				AttributeConfig attributeConfig = new AttributeConfig();
				sub = fields.get(i);
				int lv = sub.getInt("[@lv]");
				attributeConfig.setLv(lv);
				attributeConfig.setHp(sub.getInt("[@hp]"));
				attributeConfig.setAtt(sub.getInt("[@att]"));
				attributeConfig.setDef(sub.getInt("[@def]"));
				attributeConfig.setCrit(sub.getInt("[@crit]"));
				attributeConfig.setTen(sub.getInt("[@ten]"));
				attributeConfig.setCritDam(sub.getInt("[@critDam]"));
				attributeConfig.setCap(sub.getInt("[@cap]"));
				attributeConfig.setMoveCap(sub.getInt("[@moveCap]"));
				attributeConfig.setSpeed(sub.getInt("[@speed]"));
				attributeConfig.setOrg(sub.getInt("[@org]"));
				attributeConfig.setMac(sub.getInt("[@mac]"));
				attributeConfig.setAll(sub.getInt("[@all]"));
				attributeConfig.setFib(sub.getInt("[@fib]"));
				attributeConfig.setPun(sub.getInt("[@pun]"));
				attributeConfig.setSma(sub.getInt("[@sma]"));
				attributeConfig.setImp(sub.getInt("[@imp]"));
				attributeConfig.setInc(sub.getInt("[@inc]"));
				
				attributeConfigMap.put(lv, attributeConfig);
			}
		}
	}


	/**
	 * 加载时装配置
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static void loadFashionConfig() throws Exception{
		String configPath =System.getProperty("user.dir")+ "/xml/fashion.xml";
		logger.info("load fashion config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				FashionConfig fashionConfig = new FashionConfig();
				sub = fields.get(i);
				int id = sub.getInt("[@id]");
				fashionConfig.setFashionId(sub.getInt("[@id]"));
				fashionConfig.setName(sub.getString("[@Name]"));
				fashionConfig.setIconNv(sub.getString("[@iconNv]"));
				fashionConfig.setIconNan(sub.getString("[@iconNan]"));
				fashionConfig.setFashionPathNv(sub.getString("[@fashionPathNv]"));
				fashionConfig.setFashionPathNan(sub.getString("[@fashionPathNan]"));
				fashionConfig.setDes(sub.getString("[@des]"));
				fashionConfig.setType(sub.getInt("[@type]"));
				fashionConfig.setQuality(sub.getInt("[@quality]"));
				fashionConfig.setFashionDefenseType(sub.getInt("[@fashionDefenseType]"));
				fashionConfig.setFashionSource(sub.getInt("[@fashionSource]"));
				fashionConfig.setFashionType(sub.getInt("[@fashionType]"));
				fashionConfig.setBaseAttribute(sub.getString("[@baseAttribute]"));
				fashionConfig.setFashionFeatures(sub.getString("[@fashionFeatures]"));
				fashionConfig.setStatus(sub.getInt("[@status]"));
				fashionConfig.setSequenceNumber(sub.getInt("[@sequenceNumber]"));

				fashionConfigMap.put(id, fashionConfig);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static void loadItemConfig() throws Exception{
		String configPath =System.getProperty("user.dir")+ "/xml/item.xml";
		logger.info("load item config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				ItemConfig itemConfig = new ItemConfig();
				sub = fields.get(i);
				int id = Integer.parseInt(sub.getString("[@id]"));
				itemConfig.setId(id);
				itemConfig.setName(sub.getString("[@Name]"));
				itemConfig.setType(sub.getInt("[@type]"));
				itemConfig.setQuality(sub.getInt("[@quality]"));
				itemConfig.setTimeLimit(sub.getString("[@timeLimit]"));
				itemConfig.setUseage(sub.getString("[@useage]"));
				itemConfig.setIcon(sub.getString("[@icon]"));
				itemConfig.setTips1(sub.getString("[@tips1]"));
				itemConfigMap.put(id, itemConfig);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static void loadMakingConfig() throws Exception {
		String configPath =System.getProperty("user.dir")+ "/xml/making.xml";
		logger.info("load making config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				MakingConfig makingConfig = new MakingConfig();
				sub = fields.get(i);
				int makingId = sub.getInt("[@makingid]");
				makingConfig.setMakingId(makingId);
				makingConfig.setName(sub.getString("[@name]"));
				makingConfig.setType(sub.getInt("[@type]"));
				
				String drawingStr = sub.getString("[@drawing]");
				int drawingId = Integer.parseInt(drawingStr.split(",")[0]);
				int drawingNum = Integer.parseInt(drawingStr.split(",")[1]);	
				makingConfig.setDrawing(drawingId);
				makingConfig.setDrawingNum(drawingNum);
				
				String partAStr = sub.getString("[@partA]");
				int partA = Integer.parseInt(partAStr.split(",")[0]);
				int numA = Integer.parseInt(partAStr.split(",")[1]);
				makingConfig.setPartA(partA);
				makingConfig.setNumA(numA);
				String partBStr = sub.getString("[@partB]");
				int partB = Integer.parseInt(partBStr.split(",")[0]);
				int numB = Integer.parseInt(partBStr.split(",")[1]);
				makingConfig.setPartB(partB);
				makingConfig.setNumB(numB);
				String partCStr = sub.getString("[@partC]");
				int partC = Integer.parseInt(partCStr.split(",")[0]);
				int numC = Integer.parseInt(partCStr.split(",")[1]);
				makingConfig.setPartC(partC);
				makingConfig.setNumC(numC);
				makingConfig.setTime(sub.getInt("[@time]"));
				makingConfig.setDes(sub.getString("[@des]"));
				
				makingConfigMap.put(makingId, makingConfig);
			}
		}
	}

	/**
	 * 加载玩家初始化配置
	 * @throws Exception
	 */
	private static void loadPlayerConfig() throws Exception {
		String configPath =System.getProperty("user.dir")+ "/xml/player.xml";
		logger.info("load player config file path :{}", configPath);
		XMLConfiguration conf = new XMLConfiguration(configPath);
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				PlayerConfig playerConfig = new PlayerConfig();
				sub = fields.get(i);
				int id = sub.getInt("[@modeSex]");
				playerConfig.setModeSex(id);
				playerConfig.setExperience(sub.getInt("[@experience]"));
				playerConfig.setGoldCoin(sub.getInt("[@goldCoin]"));
				playerConfig.setDiamonds(sub.getInt("[@diamonds]"));
				playerConfig.setGiftCert(sub.getInt("[@giftCert]"));
				playerConfig.setFeats(sub.getInt("[@feats]"));
				
				playerConfig.setModeName(sub.getString("[@mode]"));
				playerConfig.setWeaponId(sub.getInt("[@weaponid]"));
				playerConfig.setFashionId(sub.getInt("[@fashionid]"));
				playerConfig.setColor(sub.getString("[@color]"));
				String weapons = sub.getString("[@weapons]");
				String weaponList[] = weapons.split("\\|");
				for (String weapon : weaponList) {
					playerConfig.weapons.add(Integer.parseInt(weapon));
				}
				
				String fashions = sub.getString("[@fashions]");
				String fashionList[] = fashions.split("\\|");
				for (String fashion : fashionList) {
					playerConfig.fashions.add(Integer.parseInt(fashion));
				}
				
				String items = sub.getString("[@items]");
				String itemList[] = items.split("\\|");
				for (String item : itemList) {
					playerConfig.items.add(item);
				}
				playerConfigMap.put(id, playerConfig);
			}
		}
	}

}
