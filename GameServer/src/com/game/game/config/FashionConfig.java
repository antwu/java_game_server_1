package com.game.game.config;

public class FashionConfig {
	//时装id
	private int fashionId;
	//时装名字
	private String name;
	//icon路径女
	private String iconNv;
	//icon路径男
	private String iconNan;
	//模型路径女
	private String fashionPathNv;
	//模型路径男
	private String fashionPathNan;
	//基础属性
	private String baseAttribute;
	//时装特性
	private String fashionFeatures;
	//物品描述
	private String des;
	//类型（1女2男3通用）
	private int type;
	//品质
	private int quality;
	//护甲类型（1.无类型；2.纤维；3.机械；4.合金；5.有机）
	private int fashionDefenseType;
	//来源（1.初始；2.副本；3.商城）
	private int fashionSource;
	//类别（1刀 2枪 3锤 4盾 5弓 6匕首）
	private int fashionType;
	//状态（1解锁2锁定）
	private int status;
	//排序
	private int sequenceNumber;

	public int getFashionId() {
		return fashionId;
	}

	public void setFashionId(int id) {
		this.fashionId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconNv() {
		return iconNv;
	}

	public void setIconNv(String iconNv) {
		this.iconNv = iconNv;
	}

	public String getIconNan() {
		return iconNan;
	}

	public void setIconNan(String iconNan) {
		this.iconNan = iconNan;
	}

	public String getFashionPathNv() {
		return fashionPathNv;
	}

	public void setFashionPathNv(String fashionPathNv) {
		this.fashionPathNv = fashionPathNv;
	}

	public String getFashionPathNan() {
		return fashionPathNan;
	}

	public void setFashionPathNan(String fashionPathNan) {
		this.fashionPathNan = fashionPathNan;
	}

	public String getBaseAttribute() {
		return baseAttribute;
	}

	public void setBaseAttribute(String baseAttribute) {
		this.baseAttribute = baseAttribute;
	}

	public String getFashionFeatures() {
		return fashionFeatures;
	}

	public void setFashionFeatures(String fashionFeatures) {
		this.fashionFeatures = fashionFeatures;
	}
	
	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public int getFashionDefenseType() {
		return fashionDefenseType;
	}

	public void setFashionDefenseType(int fashionDefenseType) {
		this.fashionDefenseType = fashionDefenseType;
	}

	public int getFashionSource() {
		return fashionSource;
	}

	public void setFashionSource(int fashionSource) {
		this.fashionSource = fashionSource;
	}

	public int getFashionType() {
		return fashionType;
	}

	public void setFashionType(int fashionType) {
		this.fashionType = fashionType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
}
