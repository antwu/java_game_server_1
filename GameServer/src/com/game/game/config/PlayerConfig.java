package com.game.game.config;

import java.util.ArrayList;
import java.util.List;

public class PlayerConfig {
	//玩家性别
	private int modeSex;
	//1.经验
	private int experience;
	//2.金币
	private int goldCoin;
	//3.钻石
	private int diamonds;
	//4.礼券
	private int giftCert;
	//5.功勋
	private int feats;
	//玩家模型名称
	private String modeName;
	//玩家初始化时手持武器ID
	private int weaponId;
	//玩家初始化穿戴时装ID
	private int fashionId;
	//玩家初始化穿戴时装颜色
	private String color;
	//玩家初始化时背包武器
	public List<Integer> weapons = new ArrayList<Integer>() ;
	//玩家初始化时背包时装
	public List<Integer> fashions = new ArrayList<Integer>() ;
	//玩家初始化时背包物品
	public List<String> items = new ArrayList<String>() ;

	public int getModeSex() {
		return modeSex;
	}
	public void setModeSex(int modeSex) {
		this.modeSex = modeSex;
	}
	public String getModeName() {
		return modeName;
	}
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public List<String> getItems() {
		return items;
	}
	public void setItems(List<String> items) {
		this.items = items;
	}
	public int getWeaponId() {
		return weaponId;
	}
	public void setWeaponId(int weaponId) {
		this.weaponId = weaponId;
	}
	public int getFashionId() {
		return fashionId;
	}
	public void setFashionId(int fashionId) {
		this.fashionId = fashionId;
	}
	public List<Integer> getWeapons() {
		return weapons;
	}
	public void setWeapons(List<Integer> weapons) {
		this.weapons = weapons;
	}
	public List<Integer> getFashions() {
		return fashions;
	}
	public void setFashions(List<Integer> fashions) {
		this.fashions = fashions;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public int getGoldCoin() {
		return goldCoin;
	}
	public void setGoldCoin(int goldCoin) {
		this.goldCoin = goldCoin;
	}
	public int getDiamonds() {
		return diamonds;
	}
	public void setDiamonds(int diamonds) {
		this.diamonds = diamonds;
	}
	public int getGiftCert() {
		return giftCert;
	}
	public void setGiftCert(int giftCert) {
		this.giftCert = giftCert;
	}
	public int getFeats() {
		return feats;
	}
	public void setFeats(int feats) {
		this.feats = feats;
	}

}
