package com.game.game.config;

public class WeaponLevelConfig {
	//物品等级ID
	private int id = 0;
	//等级
	private int levels = 0;
	//节点数量
	private int nodeNum = 0;
	//主节点
	private int mainNode = 0;
	//技能节点
	private int skillNode = 0;
	//组合节点
	private int combNode = 0;
	//终结技节点
	private int finalSkillNode = 0;
	//专属技节点
	private int exclusiveNode = 0;
	//任务节点
	private int questNode = 0;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLevels() {
		return levels;
	}
	public void setLevels(int levels) {
		this.levels = levels;
	}
	public int getNodeNum() {
		return nodeNum;
	}
	public void setNodeNum(int nodeNum) {
		this.nodeNum = nodeNum;
	}
	public int getMainNode() {
		return mainNode;
	}
	public void setMainNode(int mainNode) {
		this.mainNode = mainNode;
	}
	public int getSkillNode() {
		return skillNode;
	}
	public void setSkillNode(int skillNode) {
		this.skillNode = skillNode;
	}
	public int getCombNode() {
		return combNode;
	}
	public void setCombNode(int combNode) {
		this.combNode = combNode;
	}
	public int getFinalSkillNode() {
		return finalSkillNode;
	}
	public void setFinalSkillNode(int finalSkillNode) {
		this.finalSkillNode = finalSkillNode;
	}
	public int getExclusiveNode() {
		return exclusiveNode;
	}
	public void setExclusiveNode(int exclusiveNode) {
		this.exclusiveNode = exclusiveNode;
	}
	public int getQuestNode() {
		return questNode;
	}
	public void setQuestNode(int questNode) {
		this.questNode = questNode;
	}


}
