package com.game.game.config;

public class ItemConfig {
	
	//制作装备的ID
	private int id = 0;
	
	//武器装备的名称
	private String name =""; 
	
	private int type = 0;
	
	private String timeLimit = "";
	
	private int quality = 0;
	
	private String useage = "";
	
	private String icon = "";
	
	private String tips1 = "";
	
	
	public ItemConfig() {
		init();
	}
	
	public void init() {
		this.id = 0;
		this.name = "";
		this.type = 0;
		this.timeLimit = "";
		this.quality = 0;
		this.useage = "";
		this.icon = "";
		this.tips1 = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}

	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	public String getUseage() {
		return useage;
	}

	public void setUseage(String useage) {
		this.useage = useage;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTips1() {
		return tips1;
	}

	public void setTips1(String tips1) {
		this.tips1 = tips1;
	}



}
