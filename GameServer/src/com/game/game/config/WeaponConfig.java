package com.game.game.config;

public class WeaponConfig {
	//武器ID
	private int id = 0;
	//武器名字
	private String name = "";
	//类别（1刀剑2枪3锤4盾5弓6匕首）
	private int weaponType = 0;
	//档位（1.简单；2.普通；3.困难；4.噩梦；5.遗迹）
	private int weaponLevel = 0;
	//该武器强化的最大等级
	private int weaponMaxLevel = 0;
	//来源（1.初始；2.副本；3.公会；4.竞技场；5.遗迹）
	private int weaponSource = 0 ;
	//伤害类型（1.切割；2.穿刺；3.粉碎；4.冲击）
	private String weaponDamageType = "";
	//系数
	private String damageParam = "";
	//暴击率
	private String critical = "";
	//暴击伤害
	private String criticalDamage = "";
	//背击系数
	private String backHitParam = "";
	//武器特性
	private String weaponFeatureSkill = "";
	//主动技能
	private int mainSkillId = 0;
	//武器装备已解锁的天赋列表  中间用 "|" 分隔
	private String talents = "";
	//移动消耗
	private String moveConsume = "";
	//列表icon地址
	private String listIcon = "";
	//战斗icon地址
	private String battleIcon = "";
	//物品描述
	private String des = "";
	//转化比
	private String zhuanHuaBi =  "";
	//动作组
	private int actionGroup = 0;
	//模型路径
	private String weaponPath = "";
	//挂点
	private int slot = 0;
	//排序
	private int sequenceNumber = 0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeaponType() {
		return weaponType;
	}

	public void setWeaponType(int weaponType) {
		this.weaponType = weaponType;
	}

	public int getWeaponLevel() {
		return weaponLevel;
	}

	public void setWeaponLevel(int weaponLevel) {
		this.weaponLevel = weaponLevel;
	}

	public int getWeaponMaxLevel() {
		return weaponMaxLevel;
	}

	public void setWeaponMaxLevel(int weaponMaxLevel) {
		this.weaponMaxLevel = weaponMaxLevel;
	}

	public int getWeaponSource() {
		return weaponSource;
	}

	public void setWeaponSource(int weaponSource) {
		this.weaponSource = weaponSource;
	}

	public String getWeaponDamageType() {
		return weaponDamageType;
	}

	public void setWeaponDamageType(String weaponDamageType) {
		this.weaponDamageType = weaponDamageType;
	}

	public String getDamageParam() {
		return damageParam;
	}

	public void setDamageParam(String damageParam) {
		this.damageParam = damageParam;
	}

	public String getCritical() {
		return critical;
	}

	public void setCritical(String critical) {
		this.critical = critical;
	}

	public String getCriticalDamage() {
		return criticalDamage;
	}

	public void setCriticalDamage(String criticalDamage) {
		this.criticalDamage = criticalDamage;
	}

	public String getBackHitParam() {
		return backHitParam;
	}

	public void setBackHitParam(String backHitParam) {
		this.backHitParam = backHitParam;
	}

	public String getWeaponFeatureSkill() {
		return weaponFeatureSkill;
	}

	public void setWeaponFeatureSkill(String weaponFeatureSkill) {
		this.weaponFeatureSkill = weaponFeatureSkill;
	}

	public int getMainSkillId() {
		return mainSkillId;
	}

	public void setMainSkillId(int mainSkillId) {
		this.mainSkillId = mainSkillId;
	}

	public String getTalents() {
		return talents;
	}

	public void setTalents(String talents) {
		this.talents = talents;
	}

	public String getMoveConsume() {
		return moveConsume;
	}

	public void setMoveConsume(String moveConsume) {
		this.moveConsume = moveConsume;
	}

	public String getListIcon() {
		return listIcon;
	}

	public void setListIcon(String listIcon) {
		this.listIcon = listIcon;
	}

	public String getBattleIcon() {
		return battleIcon;
	}

	public void setBattleIcon(String battleIcon) {
		this.battleIcon = battleIcon;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getZhuanHuaBi() {
		return zhuanHuaBi;
	}

	public void setZhuanHuaBi(String zhuanHuaBi) {
		this.zhuanHuaBi = zhuanHuaBi;
	}

	public int getActionGroup() {
		return actionGroup;
	}

	public void setActionGroup(int actionGroup) {
		this.actionGroup = actionGroup;
	}

	public String getWeaponPath() {
		return weaponPath;
	}

	public void setWeaponPath(String weaponPath) {
		this.weaponPath = weaponPath;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
}
