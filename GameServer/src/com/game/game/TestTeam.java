package com.game.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.game.game.config.XMLTemplateService;
import com.game.common.config.BaseLocalConfigInitializer;
import com.game.common.config.BaseServerConfig;
import com.game.core.db.service.proxy.EntityProxyFactory;
import com.game.core.db.service.proxy.EntityServiceProxyFactory;
import com.game.core.service.ServiceContainer;
import com.game.dbpersistence.game.entity.TeamEntity;
import com.game.dbpersistence.game.service.entity.impl.AccountEntityService;
import com.game.dbpersistence.game.service.entity.impl.TeamEntityService;
import com.game.dbpersistence.game.service.entity.impl.WeaponEntityService;
import com.game.dbpersistence.game.service.entity.impl.WeaponsetEntityService;
import com.game.game.service.account.AccountLoginService;
import com.game.game.service.server.ServerService;
import com.game.game.start.NetInitializer;
import com.game.game.start.TaskInitializer;

public class TestTeam
{
	private static Logger logger = LoggerFactory.getLogger(GameServer.class);
	public static ClassPathXmlApplicationContext classPathXmlApplicationContext;
	public static AccountEntityService accountEntityService = null;
	public static WeaponEntityService weaponEntityService = null;
	public static WeaponsetEntityService weaponsetEntityService = null;
	public static TeamEntityService teamEntityService = null;
	
	public static void main(String[] args)
	{
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[] { "bean/applicationContext.xml" });
		if (GameServer.classPathXmlApplicationContext == null) {
			GameServer.classPathXmlApplicationContext = new ClassPathXmlApplicationContext(
					new String[] { "bean/applicationContext.xml" });
		}
		if (GameServer.classPathXmlApplicationContext != null) {
			try {
				if (accountEntityService == null) {
					accountEntityService = getAccountEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (weaponEntityService == null) {
					weaponEntityService = getWeaponEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (weaponsetEntityService == null) {
					weaponsetEntityService = getWeaponsetEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
				if (teamEntityService == null) {
					teamEntityService = getTeamEntityProxyService(GameServer.classPathXmlApplicationContext);
				}
			} catch (Exception e) {
				// e.printStackTrace();
				logger.error(e.toString());
			}
		}

		BaseServerConfig config = new BaseServerConfig();
		try {
			XMLTemplateService.initTemplateData();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		BaseLocalConfigInitializer baseLocalInitializer = new BaseLocalConfigInitializer();
		try
		{
			baseLocalInitializer.init(config);
			logger.info("Success to intialize configuration!");
		}
		catch (Exception e)
		{
			logger.error("Failed to initialize config!!! Exception err={}", e);
			return;
		}

		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		service.setServerConfig(config);
		NetInitializer netInitializer = new NetInitializer();
		try
		{
			netInitializer.init();
			netInitializer.openPort4Server(config.getPort4Server());
			
			AccountLoginService accountLoginService = ServiceContainer.getInstance().getPublicService(AccountLoginService.class);
			accountLoginService.reloadFormDB();
			
			int missionId  = 2;
			long id = 1 ;			
//			List<TeamEntity> teamList = new ArrayList<TeamEntity>();
//			for (int i = 1; i <= 6; i++) {
//				TeamEntity teamEntity = new TeamEntity(i);
//				teamEntity.setTeamName("123456"+i);
//				teamEntity.setCaptainId("123456789"+i);
//				teamEntity.setMaxPlayerNum(i);
//				teamEntity.setCurPlayerNum(i);
//				teamEntity.setMissionId(missionId);
//				teamEntity.setTeamPwd("123456"+i);
//				teamEntity.setRequirePower(i);
//				teamEntity.setStatus(i);
//				HashMap<String, String> teamPlayerInfos = new HashMap<String, String>();
//				teamPlayerInfos.put("123456", ""+i);
//				teamPlayerInfos.put("654321", ""+i);
//				teamPlayerInfos.put("123789", ""+i);
//				teamPlayerInfos.put("456789", ""+i);
//				teamEntity.setTeamPlayerInfos(teamPlayerInfos);
//				teamList.add(teamEntity);
//			}
//			if(teamList.size() > 0) {
//				List<Long> rets = teamEntityService.insertTeamList(teamList);
//				if(rets!=null){
//					int index = 0;
//					for (Long ret :rets ) {
//						if(ret == 0) {
//							logger.error("teamEntityService.insertTeamList() Filed ! teamEntity =", teamList.get(index).toString());
//						}
//						index++;
//					}
//				}
//			}
			
			List<TeamEntity> teamList = new ArrayList<TeamEntity>();
			List<TeamEntity> teamList1 = teamEntityService.getTeamList(missionId);
			for (TeamEntity teamEntity : teamList1) {
				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
				TeamEntity proxyTeam = entityProxyFactory.createProxyEntity(teamEntity);
				HashMap<String, String> teamPlayerInfos = proxyTeam.getTeamPlayerInfos();
				teamPlayerInfos.put("1234560", "1");
				teamPlayerInfos.put("6543210", "2");
				teamPlayerInfos.put("1237890", "1");
				teamPlayerInfos.put("4567890", "1");
				proxyTeam.setTeamPlayerInfos(teamPlayerInfos);
				teamList.add(proxyTeam);
			}
			
			List<Long> rets = teamEntityService.updateTeamList(teamList);
			if(rets!=null){
				int index = 0;
				for (Long ret :rets ) {
					if(ret == 0) {
						logger.error("teamEntityService.updateTeamList() Filed ! teamEntity={}",teamList.get(index).toString());
					}
					index++;
				}
			}
			List<Long> rets1 = teamEntityService.deleteTeamList(teamList);
			if(rets1!=null){
				int index = 0;
				for (Long ret :rets1 ) {
					if(ret == 0) {
						logger.error("teamEntityService.deleteTeamList() Filed ! teamEntity={}", teamList.get(index).toString());
					}
					index++;
				}
			}
			
//			TeamEntity teamEntity = teamEntityService.getTeam(missionId, id );
//			if (teamEntity == null) {
//				teamEntity = new TeamEntity(id);
//				teamEntity.setId(id);
//				teamEntity.setTeamName("1234567");
//				teamEntity.setCaptainId("1234567889");
//				teamEntity.setMaxPlayerNum(345);
//				teamEntity.setCurPlayerNum(154);
//				teamEntity.setTeamPwd("1234567");
//				teamEntity.setRequirePower(451);
//				teamEntity.setStatus(154);
//				HashMap<String, String> teamPlayerInfos = new HashMap<String, String>();
//				teamPlayerInfos.put("1234565", "1");
//				teamPlayerInfos.put("65432145", "1");
//				teamPlayerInfos.put("12378934", "1");
//				teamPlayerInfos.put("4567894", "1");
//				teamEntity.setTeamPlayerInfos(teamPlayerInfos);
//				long ret = teamEntityService.insertTeam(teamEntity);
//				if(ret > 0) {
//					// 初始化用户数据
//					logger.info("GameServer.teamEntityService.insertTeam() success ! team =", teamEntity.toString());
//				}
//			}
//			else {
//				if (true) {
//				// 测试更新操作
//				EntityProxyFactory entityProxyFactory = new EntityProxyFactory();
//				TeamEntity proxyTeam = entityProxyFactory.createProxyEntity(teamEntity);
//				proxyTeam.setTeamName("1234567890");
//				proxyTeam.setCaptainId("1234567890");
//				proxyTeam.setMaxPlayerNum(4);
//				proxyTeam.setCurPlayerNum(5);
//				proxyTeam.setTeamPwd("1234567");
//				proxyTeam.setRequirePower(7);
//				proxyTeam.setStatus(2);
//				HashMap<String, String> teamPlayerInfos = new HashMap<String, String>();
//				teamPlayerInfos.put("1234562", "2");
//				teamPlayerInfos.put("6543212", "2");
//				teamPlayerInfos.put("1237892", "2");
//				teamPlayerInfos.put("4567892", "2");
//				proxyTeam.setTeamPlayerInfos(teamPlayerInfos);
//				boolean ret = teamEntityService.updateTeam(proxyTeam);
//				if (ret) {
//					// 测试删除操作
//					TeamEntity teamDelete = teamEntityService.getTeam(missionId,id);
//					if (teamDelete != null) {
//						ret = teamEntityService.deleteTeam(teamDelete);
//					}
//				}
//				}
//			}			
			// task initial
			if (TaskInitializer.getInstance().init() != 0)
			{
				logger.error("initial task failed.");
				System.exit(-3);
			}
			
			logger.info("GameServer is ready!!!");
		}
		catch (Exception e)
		{
			logger.error("Failed to initialize network!!! Exception err={}", e);
			return;
		}
//		try 
//		{
//		} 
//		catch (Exception e) 
//		{
//			logger.error("Failed to init BILogConstant Exception err={}", e);
//			return;
//		}
	}
	
	public static AccountEntityService getAccountEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		AccountEntityService accountEntityService = (AccountEntityService) classPathXmlApplicationContext.getBean("accountEntityService");
		// EntityAysncServiceProxyFactory entityAysncServiceProxyFactory = (EntityAysncServiceProxyFactory) classPathXmlApplicationContext.getBean("entityAysncServiceProxyFactory");
		// accountService = entityAysncServiceProxyFactory.createProxyService(accountService);
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		accountEntityService = entityServiceProxyFactory.createProxyService(accountEntityService);
		return accountEntityService;
	}

	public static WeaponEntityService getWeaponEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		WeaponEntityService weaponEntityService = (WeaponEntityService) classPathXmlApplicationContext.getBean("weaponEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		weaponEntityService = entityServiceProxyFactory.createProxyService(weaponEntityService);
		return weaponEntityService;
	}

	public static WeaponsetEntityService getWeaponsetEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		WeaponsetEntityService weaponsetEntityService = (WeaponsetEntityService) classPathXmlApplicationContext.getBean("weaponsetEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		weaponsetEntityService = entityServiceProxyFactory.createProxyService(weaponsetEntityService);
		return weaponsetEntityService;
	}
	
	public static TeamEntityService getTeamEntityProxyService(ClassPathXmlApplicationContext classPathXmlApplicationContext) throws Exception {
		TeamEntityService teamEntityService = (TeamEntityService) classPathXmlApplicationContext.getBean("teamEntityService");
		EntityServiceProxyFactory entityServiceProxyFactory = (EntityServiceProxyFactory) classPathXmlApplicationContext.getBean("entityServiceProxyFactory");
		teamEntityService = entityServiceProxyFactory.createProxyService(teamEntityService);
		return teamEntityService;
	}
}
