package com.game.game.common;

import java.util.ArrayList;

public class ContinueRoomInfo
{
	public static class ContinueType
	{
		public static final int Refuse = 0;
		public static final int Agree = 1;
	}
	
	private String oldOwnerID;
	private String ip;
	private int roomType;
	private int roomCount;
	private int continueRoomID;
	private boolean refuse = false;
	private ArrayList<String> oldAccountIDs = new ArrayList<String>();
	
	public ContinueRoomInfo(String oldOwnerID)
	{
		this.oldOwnerID = oldOwnerID;
	}
	
	public int getContinueRoomID()
	{
		return continueRoomID;
	}

	public void setContinueRoomID(int continueRoomID)
	{
		this.continueRoomID = continueRoomID;
	}

	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public int getRoomType()
	{
		return roomType;
	}

	public void setRoomType(int roomType)
	{
		this.roomType = roomType;
	}

	public int getRoomCount()
	{
		return roomCount;
	}

	public void setRoomCount(int roomCount)
	{
		this.roomCount = roomCount;
	}

	public String getOldOwernID()
	{
		return oldOwnerID;
	}
	
	public boolean isRefuse()
	{
		return refuse;
	}

	public void setRefuse(boolean refuse)
	{
		this.refuse = refuse;
	}

	public void addAccountID(String accountID)
	{
		if(!oldAccountIDs.contains(accountID))
			oldAccountIDs.add(accountID);
	}
	
	public void removeAccountID(String accountID)
	{
		if(oldAccountIDs.contains(accountID))
			oldAccountIDs.remove(accountID);
	}
	
	public ArrayList<String> getPlayers()
	{
		return oldAccountIDs;
	}
}
