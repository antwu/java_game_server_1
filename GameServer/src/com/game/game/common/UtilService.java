package com.game.game.common;

import java.util.UUID;

public class UtilService {
	public static String genGUID() {
		String guid = UUID.randomUUID().toString();
		return guid.substring(0, 8) + guid.substring(9, 13)
		+ guid.substring(14, 18) + guid.substring(19, 23)
		+ guid.substring(24);
	}
}
