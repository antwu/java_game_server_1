package com.game.game.common;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/*
 * 静态信息类，比如邮件内容，需要从配置表中获得，然后进行解析
 * */

public class ConstCommon {
	private static ConstCommon instance = new ConstCommon();
	Logger logger = LoggerFactory.getLogger(ConstCommon.class);
	ConcurrentHashMap<Integer, String> constEmail = new ConcurrentHashMap<>();	//记录所有email号和string
	private ConstCommon()
	{
		
	}
}
