package com.game.game.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.constant.RoomRuleBI;

public class BILogConstant
{

	final static private Logger logger = LoggerFactory.getLogger(BILogConstant.class);

	private final static String BI_DIQU_SPLIT_CHAR = ":";
	private final static String BI_ARGS_SPLIT_CHAR = "-";
	private final static String DIQU_PROP = "diqu";
	private final static String JUSHU_PROP = "jushu";

	private static List<String> rulesStatisticsList = new ArrayList<>();
	private static Map<String, List<RoomRuleBI>> groupRuleBIMap = new HashMap<>();
	private static Map<String, RoomRuleBI> battle2BIMap = new HashMap<>();

	private static BILogConstant instance;

	private BILogConstant()
	{

	}

	public static BILogConstant getInstance()
	{
		if (null == instance)
			instance = new BILogConstant();

		return instance;
	}

	public void init()
		throws Exception
	{
		loadBIProp();
		loadGame2BiProp();
	}

	private Properties loadProperties(String fileName)
		throws Exception
	{
		InputStream propertiesStream = BILogConstant.class.getResourceAsStream(fileName);
		InputStreamReader reader = new InputStreamReader(propertiesStream, "UTF-8");

		Properties properties = new Properties();
		try
		{
			properties.load(reader);
		}
		catch (IOException e)
		{
			logger.error("Failed load base local config from " + fileName);
			throw e;
		}

		return properties;
	}

	/**
	 * add by dongbing 2016-12-09 ,BI埋点需求
	 */
	private void loadBIProp()
		throws Exception
	{

		Properties properties = loadProperties("/bi.properties");
		if (!properties.isEmpty())
		{
			String key = null;
			int mapKey = 0;
			Iterator<String> it = properties.stringPropertyNames().iterator();
			while (it.hasNext())
			{
				key = it.next();
				mapKey = Integer.parseInt(key);
				
				if (mapKey > 1000 && mapKey % 100 == 0 && !rulesStatisticsList.contains(mapKey))
						rulesStatisticsList.add(key);
			}
		}

//		System.out.println("rulesStatisticsList = " + rulesStatisticsList);
	}

	private void loadGame2BiProp()
		throws Exception
	{

		Properties properties = loadProperties("/battle2bi.properties");
		if (!properties.isEmpty())
		{

			RoomRuleBI roomRuleBI = null;
			List<RoomRuleBI> roomRuleList = null;

			String key = null;
			Iterator<String> it = properties.stringPropertyNames().iterator();
			while (it.hasNext())
			{
				key = it.next();
				roomRuleBI = new RoomRuleBI(key, properties.getProperty(key));
				if (key.startsWith(DIQU_PROP) || key.startsWith(JUSHU_PROP))
				{
					groupRuleBIMap.put(roomRuleBI.getName(), new ArrayList<>(Arrays.asList(roomRuleBI)));
				}
				else
				{
					 roomRuleList = groupRuleBIMap.get(roomRuleBI.getGroup());
					 if(null == roomRuleList) {
						 roomRuleList = new ArrayList<RoomRuleBI>();
						 groupRuleBIMap.put(roomRuleBI.getGroup(), roomRuleList);
					 }
					 roomRuleList.add(roomRuleBI);
					 
				}
				battle2BIMap.put(roomRuleBI.getName(), roomRuleBI);
			}
		}

//		System.out.println("roomRuleBIMap = " + groupRuleBIMap);
//		System.out.println("battle2BIMap = " + battle2BIMap);
	}

	/**
	 * 构建BIlog需要的参数
	 * 
	 * @param roomType
	 * @param count
	 * @param roomRules
	 * @return
	 */
	public String bulidBIArgs(int roomType, int count, List<Integer> roomRules)
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			// 地区
			sb.append(groupRuleBIMap.get(DIQU_PROP + roomType).get(0).getTrueValue());
			// 局数
			if(null != battle2BIMap.get(JUSHU_PROP + count))
			{
				sb.append(BI_DIQU_SPLIT_CHAR).append(battle2BIMap.get(JUSHU_PROP + count).getTrueValue());
			}
			// 玩法
			List<String> tempRuleStatisticsList = new ArrayList<>();
			tempRuleStatisticsList.addAll(rulesStatisticsList);

			RoomRuleBI roomRuleBI = null;
			/**
			 * 已选玩法
			 */
			for (Integer rule : roomRules)
			{
				roomRuleBI = battle2BIMap.get(String.valueOf(rule));
				if(null == roomRuleBI)
					continue;
				
				sb.append(BI_ARGS_SPLIT_CHAR).append(roomRuleBI.getTrueValue());
				tempRuleStatisticsList.remove(roomRuleBI.getGroup());
			}
			/**
			 * 需要统计的未选玩法
			 */
			for (String statistics : tempRuleStatisticsList)
			{
				if("10100".equals(statistics))
					continue;
				/**
				 * 处理麻将玩法
				 */
				if("10200".equals(statistics))
				{
					for(RoomRuleBI rule : groupRuleBIMap.get(statistics))
					{
						if(rule.getName().equals(String.valueOf(roomType)))
						{
							sb.append(BI_ARGS_SPLIT_CHAR).append(rule.getFalseValue());
							break;
						}
					}
				}
				else
				{
					for(RoomRuleBI rule : groupRuleBIMap.get(statistics))
					{
						sb.append(BI_ARGS_SPLIT_CHAR).append(rule.getFalseValue());
					}
				}
			}

//			System.out.println(sb.toString());
		}
		catch (Exception e)
		{
			logger.error("BILogConstant#Exception#bulidBIArgs : Exception err={}", e.toString());
		}
		return sb.toString();
	}
	
	public String bulidBIRoomRules(int roomType, List<Integer> roomRules)
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			// 玩法
			List<String> tempRuleStatisticsList = new ArrayList<>();
			tempRuleStatisticsList.addAll(rulesStatisticsList);

			RoomRuleBI roomRuleBI = null;
			/**
			 * 已选玩法
			 */
			for (Integer rule : roomRules)
			{
				roomRuleBI = battle2BIMap.get(String.valueOf(rule));
				if(null == roomRuleBI)
					continue;
				if(sb.length() > 0 )
					sb.append(BI_ARGS_SPLIT_CHAR);
				sb.append(roomRuleBI.getTrueValue());
				tempRuleStatisticsList.remove(roomRuleBI.getGroup());
			}
			/**
			 * 需要统计的未选玩法
			 */
			for (String statistics : tempRuleStatisticsList)
			{
				if("10100".equals(statistics))
					continue;
				/**
				 * 处理麻将玩法
				 */
				if("10200".equals(statistics))
				{
					for(RoomRuleBI rule : groupRuleBIMap.get(statistics))
					{
						if(rule.getName().equals(String.valueOf(roomType)))
						{
							sb.append(BI_ARGS_SPLIT_CHAR).append(rule.getFalseValue());
							break;
						}
					}
				}
				else
				{
					for(RoomRuleBI rule : groupRuleBIMap.get(statistics))
					{
						sb.append(BI_ARGS_SPLIT_CHAR).append(rule.getFalseValue());
					}
				}
			}

//			System.out.println(sb.toString());
		}
		catch (Exception e)
		{
			logger.error("BILogConstant#Exception#bulidBIRoomRules : Exception err={}", e.toString());
		}
		return sb.toString();
	}

}
