package com.game.dbpersistence.game.mapper;

import com.game.core.db.service.entity.IDBMapper;
import com.game.dbpersistence.game.entity.WeaponEntity;

/**
 * Created by  on 17/3/6.
 */
public interface WeaponEntityMapper extends IDBMapper<WeaponEntity> {

}