package com.game.dbpersistence.game.mapper;

import com.game.core.db.service.entity.IDBMapper;
import com.game.dbpersistence.game.entity.ItemEntity;;

public interface ItemEntityMapper extends IDBMapper<ItemEntity> {

}