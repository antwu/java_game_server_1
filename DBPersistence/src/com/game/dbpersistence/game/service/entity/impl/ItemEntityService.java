package com.game.dbpersistence.game.service.entity.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.ItemEntity;
import com.game.dbpersistence.game.service.entity.IItemEntityService;

@Service
public class ItemEntityService extends EntityService<ItemEntity> implements IItemEntityService {
	final static Logger logger = LoggerFactory.getLogger(ItemEntityService.class);
	
	@Override
	public long insertItem(ItemEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertItemList(List<ItemEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public ItemEntity getItemByUID(String ownerId, String uid) {
		List<ItemEntity> itemList = getItemList(ownerId);
		if(itemList.size()<=0) {
			return null;
		}
		for(ItemEntity entity : itemList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}

	@Override
	public ItemEntity getItem(String ownerId, int itemId) {
		try {
			ItemEntity entity = new ItemEntity();
			entity.setUid("");
			entity.setOwnerId(ownerId);
			entity.setItemId(itemId);
			return (ItemEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}
	
	@Override
	public List<ItemEntity> getItemList(String ownerId) {
		List<ItemEntity> retList = new ArrayList<>();
		ItemEntity entity = new ItemEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<ItemEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateItem(ItemEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateItemList(List<ItemEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public boolean deleteItem(ItemEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> deleteItemList(List<ItemEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
