package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.ItemEntity;

public interface IItemEntityService {

	public long insertItem(ItemEntity entity);

	public List<Long> insertItemList(List<ItemEntity> entityList);

	public ItemEntity getItem(String ownerId,int itemId);

	public ItemEntity getItemByUID(String ownerId, String uid);
	
	public List<ItemEntity> getItemList(String ownerId);

	boolean updateItem(ItemEntity entity);
	
	public List<Long> updateItemList(List<ItemEntity> entityList);
	
	boolean deleteItem(ItemEntity entity);

	public List<Long> deleteItemList(List<ItemEntity> entityList);

}
