package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.MissionEntity;
import com.game.dbpersistence.game.service.entity.IMissionEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class MissionEntityService extends EntityService<MissionEntity> implements IMissionEntityService {
	final static Logger logger = LoggerFactory.getLogger(MissionEntityService.class);
	
	@Override
	public long insertMission(MissionEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertMissionList(List<MissionEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public MissionEntity getMission(String ownerId, int missionId) {
		try {
			MissionEntity entity = new MissionEntity();
			entity.setUid("");
			entity.setOwnerId(ownerId);
			entity.setMissionId(missionId);
			return (MissionEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}
	
	@Override
	public MissionEntity getMissionByUID(String ownerId, String uid) {
		List<MissionEntity> missionList = getMissionList(ownerId);
		if(missionList.size()<=0) {
			return null;
		}
		for(MissionEntity entity : missionList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}
	
	@Override
	public List<MissionEntity> getMissionList(String ownerId) {
		List<MissionEntity> retList = new ArrayList<>();
		MissionEntity entity = new MissionEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<MissionEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateMission(MissionEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateMissionList(List<MissionEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public boolean deleteMission(MissionEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> deleteMissionList(List<MissionEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
