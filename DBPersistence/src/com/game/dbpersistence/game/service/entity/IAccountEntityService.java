package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.AccountEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IAccountEntityService {

	public long insertAccount(AccountEntity entity);

	public AccountEntity getAccount(String acc);

	public List<AccountEntity> getAccountList();

	public boolean updateAccount(AccountEntity entity);

	public boolean deleteAccount(AccountEntity entity);

}
