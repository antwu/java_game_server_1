package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.BuncherEntity;
import com.game.dbpersistence.game.service.entity.IBuncherEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class BuncherEntityService extends EntityService<BuncherEntity> implements IBuncherEntityService {
	final static Logger logger = LoggerFactory.getLogger(BuncherEntityService.class);
	
	@Override
	public long insertBuncher(BuncherEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertBuncherList(List<BuncherEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public BuncherEntity getBuncherByUID(String ownerId, String uid) {
		List<BuncherEntity> buncherList = getBuncherList(ownerId);
		if(buncherList.size()<=0) {
			return null;
		}
		for(BuncherEntity entity : buncherList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}
	
	@Override
	public BuncherEntity getBuncher(String ownerId, int buncherId) {
		try {
			BuncherEntity entity = new BuncherEntity();
			entity.setUid("");
			entity.setOwnerId(ownerId);
			entity.setBuncherId(buncherId);
			return (BuncherEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}

	@Override
	public List<BuncherEntity> getBuncherList(String ownerId) {
		List<BuncherEntity> retList = new ArrayList<>();
		BuncherEntity entity = new BuncherEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<BuncherEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateBuncher(BuncherEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateBuncherList(List<BuncherEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public boolean deleteBuncher(BuncherEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}
	
	@Override
	public List<Long> deleteBuncherList(List<BuncherEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
