package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.MakingEntity;
import com.game.dbpersistence.game.service.entity.IMakingEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class MakingEntityService extends EntityService<MakingEntity> implements IMakingEntityService {
	final static Logger logger = LoggerFactory.getLogger(MakingEntityService.class);
	
	@Override
	public long insertMaking(MakingEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertMakingList(List<MakingEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public MakingEntity getMakingByUID(String ownerId, String uid) {
		List<MakingEntity> weaponList = getMakingList(ownerId);
		if(weaponList.size()<=0) {
			return null;
		}
		for(MakingEntity entity : weaponList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}

	@Override
	public MakingEntity getMaking(String ownerId, int makingId) {
		try {
			MakingEntity entity = new MakingEntity();
			entity.setUid("");
			entity.setOwnerId(ownerId);
			entity.setMakingId(makingId);
			return (MakingEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}
	
	@Override
	public List<MakingEntity> getMakingList(String ownerId) {
		List<MakingEntity> retList = new ArrayList<>();
		MakingEntity entity = new MakingEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<MakingEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateMaking(MakingEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateMakingList(List<MakingEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public boolean deleteMaking(MakingEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> deleteMakingList(List<MakingEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
