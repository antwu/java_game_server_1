package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.WeaponsetEntity;
import com.game.dbpersistence.game.service.entity.IWeaponsetEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class WeaponsetEntityService extends EntityService<WeaponsetEntity> implements IWeaponsetEntityService {
	final static Logger logger = LoggerFactory.getLogger(WeaponsetEntityService.class);
	@Override
	public long insertWeaponset(WeaponsetEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertWeaponsetList(List<WeaponsetEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public WeaponsetEntity getWeaponset(String ownerId,int setId) {
		try {
			WeaponsetEntity entity = new WeaponsetEntity();
			entity.setOwnerId(ownerId);
			entity.setSetId(setId);
			return (WeaponsetEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}

	@Override
	public WeaponsetEntity getWeaponsetByUID(String ownerId,String uid) {
		List<WeaponsetEntity> weaponsetList = getWeaponsetList(ownerId);
		if(weaponsetList.size()<=0) {
			return null;
		}
		for(WeaponsetEntity entity : weaponsetList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}
	
	@Override
	public List<WeaponsetEntity> getWeaponsetList(String ownerId) {
		List<WeaponsetEntity> retList = new ArrayList<>();
		WeaponsetEntity entity = new WeaponsetEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<WeaponsetEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateWeaponset(WeaponsetEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateWeaponsetList(List<WeaponsetEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public boolean deleteWeaponset(WeaponsetEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> deleteWeaponsetList(List<WeaponsetEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
