package com.game.dbpersistence.game.service.entity;

import java.util.List;
import com.game.dbpersistence.game.entity.TeamEntity;

/**
 * Created by  on 17/3/20.
 */
public interface ITeamEntityService {
	
	public long insertTeam(TeamEntity entity);

	public List<Long> insertTeamList(List<TeamEntity> entityList);
	
	public TeamEntity getTeam(int missionId, long id);

	public List<TeamEntity> getTeamList(int missionId);

	public boolean updateTeam(TeamEntity entity);

	public List<Long> updateTeamList(List<TeamEntity> entityList);
	
	public boolean deleteTeam(TeamEntity entity);

	public List<Long> deleteTeamList(List<TeamEntity> entityList);
}
