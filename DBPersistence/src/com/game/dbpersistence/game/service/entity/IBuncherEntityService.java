package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.BuncherEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IBuncherEntityService {

	public long insertBuncher(BuncherEntity entity);

	public List<Long> insertBuncherList(List<BuncherEntity> entityList);

	public BuncherEntity getBuncherByUID(String ownerId, String uid);
	
	public BuncherEntity getBuncher(String ownerId,int buncherId);

	public List<BuncherEntity> getBuncherList(String ownerId);

	boolean updateBuncher(BuncherEntity entity);
	
	public List<Long> updateBuncherList(List<BuncherEntity> entityList);
	
	boolean deleteBuncher(BuncherEntity entity);

	public List<Long> deleteBuncherList(List<BuncherEntity> entityList);
}
