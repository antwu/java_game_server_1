package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.FashionEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IFashionEntityService {

	public long insertFashion(FashionEntity entity);

	public List<Long> insertFashionList(List<FashionEntity> entityList);

	public FashionEntity getFashionByUID(String ownerId, String uid);

	public FashionEntity getFashion(String ownerId, int fashionId);
	
	public List<FashionEntity> getFashionList(String ownerId);

	boolean updateFashion(FashionEntity entity);
	
	public List<Long> updateFashionList(List<FashionEntity> entityList);

	boolean deleteFashion(FashionEntity entity);
	
	public List<Long> deleteFashionList(List<FashionEntity> entityList);
}
