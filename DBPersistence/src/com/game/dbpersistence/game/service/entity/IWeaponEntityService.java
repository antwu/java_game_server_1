package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.WeaponEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IWeaponEntityService {

	public long insertWeapon(WeaponEntity entity);

	public List<Long> insertWeaponList(List<WeaponEntity> entityList);

	public WeaponEntity getWeapon(String ownerId,int weaponId);

	public WeaponEntity getWeaponByUID(String ownerId, String uid);
	
	public List<WeaponEntity> getWeaponList(String ownerId);

	boolean updateWeapon(WeaponEntity entity);
	
	public List<Long> updateWeaponList(List<WeaponEntity> entityList);
	
	boolean deleteWeapon(WeaponEntity entity);

	public List<Long> deleteWeaponList(List<WeaponEntity> entityList);

}
