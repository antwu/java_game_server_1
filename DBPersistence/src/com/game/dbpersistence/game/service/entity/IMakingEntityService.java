package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.MakingEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IMakingEntityService {

	public long insertMaking(MakingEntity entity);

	public List<Long> insertMakingList(List<MakingEntity> entityList);

	public MakingEntity getMaking(String ownerId, int makingId);

	public MakingEntity getMakingByUID(String ownerId, String uid);
	
	public List<MakingEntity> getMakingList(String ownerId);

	boolean updateMaking(MakingEntity entity);
	
	public List<Long> updateMakingList(List<MakingEntity> entityList);
	
	boolean deleteMaking(MakingEntity entity);

	public List<Long> deleteMakingList(List<MakingEntity> entityList);

}
