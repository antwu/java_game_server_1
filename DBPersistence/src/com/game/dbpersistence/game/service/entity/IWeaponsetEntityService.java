package com.game.dbpersistence.game.service.entity;

import java.util.List;
import com.game.dbpersistence.game.entity.WeaponsetEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IWeaponsetEntityService {
	
	public long insertWeaponset(WeaponsetEntity entity);
	
	public List<Long> insertWeaponsetList(List<WeaponsetEntity> entityList);
	
	public WeaponsetEntity getWeaponset(String ownerId,int setId);

	public WeaponsetEntity getWeaponsetByUID(String ownerId,String uid);
	
	public List<WeaponsetEntity> getWeaponsetList(String ownerId);

	boolean updateWeaponset(WeaponsetEntity entity);
	
	public List<Long> updateWeaponsetList(List<WeaponsetEntity> entityList);
	
	boolean deleteWeaponset(WeaponsetEntity entity);

	public List<Long> deleteWeaponsetList(List<WeaponsetEntity> entityList);
}
