package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.AccountEntity;
import com.game.dbpersistence.game.service.entity.IAccountEntityService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class AccountEntityService extends EntityService<AccountEntity> implements IAccountEntityService {
	final static Logger logger = LoggerFactory.getLogger(AccountEntityService.class);

	@Override
	public long insertAccount(AccountEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public AccountEntity getAccount(String acc) {
		try {
			if(acc==null || acc.isEmpty()) {
				logger.error("acc is Empty!");
				return null;
			}
			AccountEntity entity = new AccountEntity();
			entity.setUid("");
			entity.setAcc(acc);
			return (AccountEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}

	@Override
	public List<AccountEntity> getAccountList() {
		List<AccountEntity> retList = new ArrayList<>();
		AccountEntity entity = new AccountEntity();
		try {
			entity.setUid("");
			entity.setAcc("");
			retList = (List<AccountEntity>) getEntityList(entity);
			return retList;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}

	@Override
	public boolean updateAccount(AccountEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public boolean deleteAccount(AccountEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
