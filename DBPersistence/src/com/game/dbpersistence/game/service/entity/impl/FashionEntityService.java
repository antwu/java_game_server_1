package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.FashionEntity;
import com.game.dbpersistence.game.service.entity.IFashionEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class FashionEntityService extends EntityService<FashionEntity> implements IFashionEntityService {
	final static Logger logger = LoggerFactory.getLogger(FashionEntityService.class);
	
	@Override
	public long insertFashion(FashionEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertFashionList(List<FashionEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public FashionEntity getFashionByUID(String ownerId, String uid) {
		List<FashionEntity> fashionList = getFashionList(ownerId);
		if(fashionList.size()<=0) {
			return null;
		}
		for(FashionEntity entity : fashionList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}

	@Override
	public FashionEntity getFashion(String ownerId, int fashionId) {
		try {
			FashionEntity entity = new FashionEntity();
			entity.setUid("");
			entity.setOwnerId(ownerId);
			entity.setFashionId(fashionId);
			return (FashionEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}

	
	@Override
	public List<FashionEntity> getFashionList(String ownerId) {
		List<FashionEntity> retList = new ArrayList<>();
		FashionEntity entity = new FashionEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<FashionEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateFashion(FashionEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateFashionList(List<FashionEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	

	@Override
	public boolean deleteFashion(FashionEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> deleteFashionList(List<FashionEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
