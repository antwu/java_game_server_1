package com.game.dbpersistence.game.service.entity;

import java.util.List;

import com.game.dbpersistence.game.entity.MissionEntity;

/**
 * Created by  on 17/3/20.
 */
public interface IMissionEntityService {

	public long insertMission(MissionEntity entity);

	public List<Long> insertMissionList(List<MissionEntity> entityList);

	public MissionEntity getMissionByUID(String ownerId,String uid);

	public MissionEntity getMission(String ownerId,int missionId);
	
	public List<MissionEntity> getMissionList(String ownerId);

	boolean updateMission(MissionEntity entity);
	
	public List<Long> updateMissionList(List<MissionEntity> entityList);
	
	boolean deleteMission(MissionEntity entity);

	public List<Long> deleteMissionList(List<MissionEntity> entityList);
}
