package com.game.dbpersistence.game.service.entity.impl;

import com.game.core.db.service.entity.EntityService;
import com.game.core.db.sharding.EntityServiceShardingStrategy;
import com.game.dbpersistence.game.entity.WeaponEntity;
import com.game.dbpersistence.game.service.entity.IWeaponEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 17/3/20.
 */
@Service
public class WeaponEntityService extends EntityService<WeaponEntity> implements IWeaponEntityService {
	final static Logger logger = LoggerFactory.getLogger(WeaponEntityService.class);
	
	@Override
	public long insertWeapon(WeaponEntity entity) {
		try {
			return insertEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return -1;
	}

	@Override
	public List<Long> insertWeaponList(List<WeaponEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = insertEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public WeaponEntity getWeaponByUID(String ownerId, String uid) {
		List<WeaponEntity> weaponList = getWeaponList(ownerId);
		if(weaponList.size()<=0) {
			return null;
		}
		for(WeaponEntity entity : weaponList) {
			if(uid.equalsIgnoreCase(entity.getUid())) {
				return entity;
			}
		}
		return null;
	}

	@Override
	public WeaponEntity getWeapon(String ownerId, int weaponId) {
		try {
			WeaponEntity entity = new WeaponEntity();
			entity.setUid("");
			entity.setOwnerId(ownerId);
			entity.setWeaponId(weaponId);
			return (WeaponEntity) getEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return null;
	}
	
	@Override
	public List<WeaponEntity> getWeaponList(String ownerId) {
		List<WeaponEntity> retList = new ArrayList<>();
		WeaponEntity entity = new WeaponEntity();
		try {
			entity.setOwnerId(ownerId);
			retList = (List<WeaponEntity>)getEntityList(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public boolean updateWeapon(WeaponEntity entity) {
		try {
			return updateEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> updateWeaponList(List<WeaponEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = updateEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}
	
	@Override
	public boolean deleteWeapon(WeaponEntity entity) {
		try {
			return deleteEntity(entity);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return false;
	}

	@Override
	public List<Long> deleteWeaponList(List<WeaponEntity> entityList) {
		List<Long> retList = new ArrayList<>();
		try {
			retList = deleteEntityBatch(entityList);
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.toString());
		}
		return retList;
	}

	@Override
	public EntityServiceShardingStrategy getEntityServiceShardingStrategy() {
		return getDefaultEntityServiceShardingStrategy();
	}
}
