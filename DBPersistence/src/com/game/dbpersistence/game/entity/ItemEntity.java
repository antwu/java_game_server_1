/**
 * ItemEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.db.common.annotation.DbMapper;
import com.game.core.db.common.annotation.FieldSave;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseStringIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import com.game.dbpersistence.game.mapper.ItemEntityMapper;
import com.game.message.proto.item.ItemProtoBuf.MItemInfo;

@DbMapper(mapper = ItemEntityMapper.class)
public class ItemEntity extends BaseStringIDEntity implements RedisListInterface, Cloneable {
	final static Logger logger = LoggerFactory.getLogger(ItemEntity.class);
	/**
	 * 默认生成的账号实体的ID，每次创建自动加 1 *
	 */
	@FieldSave
	private long id;
	/**
	 * 创建唯一关键字索引UUID *
	 */
	@FieldSave
	private String uid;
	/**
	 * 该实体是否在过有效期后需要需要自动从缓存中删除 *
	 */
	@FieldSave
	private boolean deleted;
	/**
	 *  设置有效期时间，默认从当前创建时间加24小时合86400秒 *
	 */
	@FieldSave
	private Date deleteTime;
	/**
	 * 道具所属的玩家的UID *
	 */
	@FieldSave
	private String ownerId;
	/**
	 * 道具的配置ID *
	 */
	@FieldSave
	private int itemId;
	/**
	 * 类别
	 */
	@FieldSave
	private int itemType = 0;
	/**
	 * 物品数量
	 */
	@FieldSave
	private int count = 0;
	/**
	* 有效时间
	*/
	@FieldSave
	private long vanishTime;

	public ItemEntity() {
		init();
	}
	
	public ItemEntity(long id) {
		this.id = id;
		init();
	}
	
	public ItemEntity(String uid) {
		this.uid = uid;
		init();
	}
	
	public ItemEntity(String account, int itemId, int itemType, int count) {
		init();
		this.setOwnerId(account);
		this.setItemId(itemId);
		this.setItemType(itemType);
		this.setCount(count);
	}
	
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}

	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}

	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}

	/** Gets */
	public String getOwnerId() {
		return this.ownerId;
	}

	/** Gets */
	public int getItemId() {
		return this.itemId;
	}
	
	/** Gets */
	public int getItemType() {
		return this.itemType;
	}

	/** Gets */
	public int getCount() {
		return this.count;
	}
	
	/** Gets */
	public long getVanishTime() {
		return this.vanishTime;
	}
	
	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.ownerId = "";
		this.itemId = 0;
		this.itemType = 0;
		this.count = 0;
		this.vanishTime = 0;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "ownerId")
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "itemId")
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "itemType")
	public void setItemType(int itemType) {
		this.itemType = itemType;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "count")
	public void setCount(int count) {
		this.count = count;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "vanishTime")
	public void setVanishTime(long vanishTime) {
		this.vanishTime = vanishTime;
	}
	
	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(ItemEntity) "
				+ "id='" + id + "', "
				+ "uid='" + uid + "', "
				+ "ownerId='" + ownerId + "', "
				+ "itemId='" + itemId + "', "
				+ "itemType='" + itemType + "', "
				+ "count='" + count + "', "
				+ "vanishTime='" + vanishTime + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\""
				+ id + "\"," + "\""
				+ uid + "\"," + "\""
				+ ownerId + "\"," + "\""
				+ itemId + "\"," + "\""
				+ itemType + "\"," + "\""
				+ count + "\"," + "\""
				+ vanishTime + "\"";
	}

	@Override
	public String getShardingKey() {
		return getOwnerId();
	}

	@Override
	public String getSubUniqueKey() {
		return String.valueOf(this.getItemId());
	}

	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.ITEM.getKey();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public MItemInfo.Builder toProto () {
		MItemInfo.Builder builder = MItemInfo.newBuilder();
		builder.setUid(this.getUid());
		builder.setOwnerId(this.getOwnerId());
		builder.setItemId(this.getItemId());
		builder.setItemType(this.getItemType());
		builder.setCount(this.getCount());
		builder.setVanishTime(this.getVanishTime());
		return builder;
	}
}
