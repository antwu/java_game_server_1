/**
 * MissionEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.dbpersistence.game.mapper.MissionEntityMapper;
import com.game.message.proto.mission.MissionProtoBuf.MMissionInfo;
import com.game.core.db.common.annotation.DbMapper;
import com.game.core.db.common.annotation.FieldSave;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseStringIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.util.Date;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for MissionEntity
 *
 * @author CodeMaster v1.0
 */
@DbMapper(mapper = MissionEntityMapper.class)
public class MissionEntity extends BaseStringIDEntity implements RedisListInterface, Cloneable {
	/**
	 * 默认生成的账号实体的ID，每次创建自动加 1 *
	 */
	@FieldSave
	private long id;
	/**
	 * 创建唯一关键字索引UUID *
	 */
	@FieldSave
	private String uid;
	/**
	 * 该实体是否在过有效期后需要需要自动从缓存中删除 *
	 */
	@FieldSave
	private boolean deleted;
	/**
	 *  设置有效期时间，默认从当前创建时间加24小时合86400秒 *
	 */
	@FieldSave
	private Date deleteTime;
	/**
	 * 任务所属的玩家的UID *
	 */
	@FieldSave
	private String ownerId;
	/**
	 * 任务ID *
	 */
	@FieldSave
	private int missionId;
	/**
	 * 任务达成 *
	 */
	@FieldSave
	private String achievement;

	public MissionEntity() {
		init();
	}
	
	public MissionEntity(long id) {
		this.id = id;
		init();
	}
	
	public MissionEntity(String uid) {
		this.uid = uid;
		init();
	}
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}

	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}

	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}

	/** Gets */
	public String getOwnerId() {
		return this.ownerId;
	}

	/** Gets */
	public int getMissionId() {
		return this.missionId;
	}

	/** Gets */
	public String getAchievement() {
		return this.achievement;
	}

	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.ownerId = "";
		this.missionId = 0;
		this.achievement = "";
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "ownerId")
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "missionId")
	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "achievement")
	public void setAchievement(String achievement) {
		this.achievement = achievement;
	}

	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(MissionEntity) " 
				+ "id='" + id + "', " 
				+ "uid='" + uid + "', " 
				+ "ownerId='" + ownerId + "', "
				+ "missionId='" + missionId + "', " 
				+ "achievement='" + achievement + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\"" 
				+ id + "\",\"" 
				+ uid + "\",\"" 
				+ ownerId + "\",\"" 
				+ missionId + "\",\"" 
				+ achievement + "\"";
	}

	@Override
	public String getShardingKey() {
		return getOwnerId();
	}

	@Override
	public String getSubUniqueKey() {
		return String.valueOf(getMissionId());
	}

	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.MISSION.getKey();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public MMissionInfo.Builder toProto () {
		MMissionInfo.Builder builderInfo = MMissionInfo.newBuilder();
		builderInfo.setOwnerId(this.getOwnerId());
		builderInfo.setMissionId(this.getMissionId());
		builderInfo.setAchievement(this.getAchievement());
		return builderInfo;
	}
}