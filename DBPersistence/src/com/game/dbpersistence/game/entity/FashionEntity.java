/**
 * FashionEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.dbpersistence.game.mapper.FashionEntityMapper;
import com.game.message.proto.weapon.WeaponProtoBuf.FashionInfo;
import com.alibaba.fastjson.JSON;
import com.game.core.db.common.annotation.DbMapper;
import com.game.core.db.common.annotation.FieldSave;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseStringIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.util.Date;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for FashionEntity
 *
 * @author CodeMaster v1.0
 */
@DbMapper(mapper = FashionEntityMapper.class)
public class FashionEntity extends BaseStringIDEntity implements RedisListInterface, Cloneable {
	/**
	 * 默认生成的账号实体的ID，每次创建自动加 1 *
	 */
	@FieldSave
	private long id;
	/**
	 * 创建唯一关键字索引UUID *
	 */
	@FieldSave
	private String uid;
	/**
	 * 该实体是否在过有效期后需要需要自动从缓存中删除 *
	 */
	@FieldSave
	private boolean deleted;
	/**
	 *  设置有效期时间，默认从当前创建时间加24小时合86400秒 *
	 */
	@FieldSave
	private Date deleteTime;
	/**
	 * 时装外形所属的玩家的UID *
	 */
	@FieldSave
	private String ownerId;
	/**
	 * 时装外形的ID *
	 */
	@FieldSave
	private int fashionId;
	/**
	 * 是否解锁 0未解锁 1已解锁 *
	 */
	@FieldSave
	private int unlocked;

	/**
	 * 武器装备时装  肤色  默认的Position = 1*
	 */
	@FieldSave
	private int skinColour;
	
	/**
	 * 武器装备时装  发色  默认的Position = 2*
	 */
	@FieldSave
	private int hairColour;
	
	/**
	 * 武器装备时装  主体颜色  默认的Position = 3*
	 */
	@FieldSave
	private int mainbodyColour;
	/**
	 * 武器装备时装  修饰颜色  默认的Position = 4*
	 */
	@FieldSave
	private int decorateColour;
	
	/**
	 * 武器装备时装  肤色  默认的Position = 1*
	 */
	@FieldSave
	private String skinColours = null;
	/**
	 * 武器装备时装  发色  默认的Position = 2*
	 */
	@FieldSave
	private String hairColours = null;
	/**
	 * 武器装备时装  主体颜色  默认的Position = 3*
	 */
	@FieldSave
	private String mainbodyColours = null;
	/**
	 * 武器装备时装  修饰颜色  默认的Position = 4*
	 */
	@FieldSave
	private String decorateColours = null;
	
	public FashionEntity() {
		init();
	}
	
	public FashionEntity(long id) {
		this.id = id;
		init();
	}
	
	public FashionEntity(String uid) {
		this.uid = uid;
		init();
	}
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}

	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}

	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}

	/** Gets */
	public String getOwnerId() {
		return this.ownerId;
	}
	
	/** Gets */
	public int getFashionId() {
		return this.fashionId;
	}

	/** Gets */
	public int getUnlocked() {
		return this.unlocked;
	}
	
	/** Gets */
	public int getSkinColour() {
		return this.skinColour;
	}
	
	/** Gets */
	public int getHairColour() {
		return this.hairColour;
	}	
	
	/** Gets */
	public int getMainbodyColour() {
		return this.mainbodyColour;
	}	
	
	/** Gets */
	public int getDecorateColour() {
		return this.decorateColour;
	}
	
	/** Gets */
	public String getSkinColours() {
		return this.skinColours;
	}
	
	/** Gets */
	public String getHairColours() {
		return this.hairColours;
	}	
	
	/** Gets */
	public String getMainbodyColours() {
		return this.mainbodyColours;
	}	
	
	/** Gets */
	public String getDecorateColours() {
		return this.decorateColours;
	}	
	
	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.ownerId = "";
		this.fashionId = 0;
		this.unlocked = 0;
		this.skinColour = 0;
		this.hairColour = 0;
		this.mainbodyColour = 0;
		this.decorateColour = 0;
		this.skinColours = "";
		this.hairColours = "";
		this.mainbodyColours = "";
		this.decorateColours = "";
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "ownerId")
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "fashionId")
	public void setFashionId(int fashionId) {
		this.fashionId = fashionId;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "unlocked")
	public void setUnlocked(int unlocked) {
		this.unlocked = unlocked;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "skinColour")
	public void setSkinColour(int skinColour) {
		this.skinColour = skinColour;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "hairColour")
	public void setHairColour(int hairColour) {
		this.hairColour = hairColour;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "mainbodyColour")
	public void setMainbodyColour(int mainbodyColour) {
		this.mainbodyColour = mainbodyColour;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "decorateColour")
	public void setDecorateColour(int decorateColour) {
		this.decorateColour = decorateColour;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "skinColours")
	public void setSkinColours(String skinColours) {
		this.skinColours = skinColours;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "hairColours")
	public void setHairColours(String hairColours) {
		this.hairColours = hairColours;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "mainbodyColours")
	public void setMainbodyColours(String mainbodyColours) {
		this.mainbodyColours = mainbodyColours;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "decorateColours")
	public void setDecorateColours(String decorateColours) {
		this.decorateColours = decorateColours;
	}
	
	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(FashionEntity) "
				+ "id='" + id + "', "
				+ "uid='" + uid + "', "
				+ "ownerId='" + ownerId + "', "
				+ "fashionId='" + fashionId + "', "
				+ "unlocked='" + unlocked + "',"
				+ "skinColour='" + skinColour + "',"
				+ "hairColour='" + hairColour + "',"
				+ "mainbodyColour='" + mainbodyColour + "',"
				+ "decorateColour='" + decorateColour + "',"
				+ "skinColours='" + skinColours + "',"
				+ "hairColours='" + hairColours + "',"
				+ "mainbodyColours='" + mainbodyColours + "',"
				+ "decorateColours='" + decorateColours + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\"" 
				+ id + "\",\"" 
				+ uid + "\",\"" 
				+ ownerId + "\",\"" 
				+ fashionId + "\",\"" 
				+ unlocked + "\",\""
				+ skinColour + "\",\""
				+ hairColour + "\",\""
				+ mainbodyColour + "\",\""
				+ decorateColour + "\",\""
				+ skinColours + "\",\""
				+ hairColours + "\",\""
				+ mainbodyColours + "\",\""
				+ decorateColours + "\"";
	}

	@Override
	public String getShardingKey() {
		return getOwnerId();
	}

	@Override
	public String getSubUniqueKey() {
		return getFashionId()+"";
	}

	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.FASHION.getKey();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public FashionInfo.Builder toProto () {
		FashionInfo.Builder builderInfo = FashionInfo.newBuilder();
		builderInfo.setOwnerId(this.getOwnerId());
		builderInfo.setFashionId(this.getFashionId());
		builderInfo.setUnlocked(this.getUnlocked());
		builderInfo.setSkinColour(this.getSkinColour());
		builderInfo.setHairColour(this.getHairColour());
		builderInfo.setMainbodyColour(this.getMainbodyColour());
		builderInfo.setDecorateColour(this.getDecorateColour());
		List <Integer> skinColours = JSON.parseObject(this.skinColours, List.class);
		builderInfo.addAllSkinColours(skinColours);
		List <Integer> hairColours = JSON.parseObject(this.hairColours, List.class);
		builderInfo.addAllHairColours(hairColours);
		List <Integer> mainbodyColours = JSON.parseObject(this.mainbodyColours, List.class);
		builderInfo.addAllMainbodyColours(mainbodyColours);
		List <Integer> decorateColours = JSON.parseObject(this.decorateColours, List.class);
		builderInfo.addAllDecorateColours(decorateColours);
		return builderInfo;
	}
}