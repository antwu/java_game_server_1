/**
 * MakingEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.dbpersistence.game.mapper.MakingEntityMapper;
import com.game.message.proto.making.MakingProtoBuf.MakingInfo;
import com.game.core.db.common.annotation.DbMapper;
import com.game.core.db.common.annotation.FieldSave;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseStringIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for MakingEntity
 *
 * @author CodeMaster v1.0
 */
@DbMapper(mapper = MakingEntityMapper.class)
public class MakingEntity extends BaseStringIDEntity implements RedisListInterface, Cloneable {
	final static Logger logger = LoggerFactory.getLogger(MakingEntity.class);
	//默认生成的账号实体的ID，每次创建自动加 1
	@FieldSave
	private long id;
	
	//创建唯一关键字索引UUID
	@FieldSave
	private String uid;
	
	//该实体是否在过有效期后需要需要自动从缓存中删除
	@FieldSave
	private boolean deleted;
	
	//设置有效期时间，默认从当前创建时间加24小时合86400秒 
	@FieldSave
	private Date deleteTime;
	
	//制作物品所属的玩家的UID
	@FieldSave
	private String ownerId;
	
	//制作物品的ID
	@FieldSave
	private int makingId = 0;
	
	//当前状态: 0 不可制作; 1 可制作; 2 制作中; 3 制作完成可领取
	@FieldSave
	private int status = 0;
	
	//开始时间戳精确到秒
	@FieldSave
	private long startTime = 0;
	
	//剩余时间 剩余多少秒
	@FieldSave
	private long speedTime = 0;
	

	public MakingEntity() {
		init();
	}
	
	public MakingEntity(long id) {
		this.id = id;
		init();
	}
	
	public MakingEntity(String uid) {
		this.uid = uid;
		init();
	}
	
	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.ownerId = "";
		this.makingId = 0;
		this.status = 0;
		this.startTime = 0 ;
		this.speedTime = 0 ;
	}
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}
	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}
	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}
	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}
	/** Gets */
	public String getOwnerId() {
		return ownerId;
	}
	/** Gets */
	public int getMakingId() {
		return makingId;
	}
	/** Gets */
	public int getStatus() {
		return status;
	}
	/** Gets */
	public long getStartTime() {
		return startTime;
	}
	/** Gets */
	public long getSpeedTime() {
		return speedTime;
	}
	
	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}
	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}
	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}
	/** Sets */
	@MethodSaveProxy(proxy = "ownerId")
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/** Sets */
	@MethodSaveProxy(proxy = "makingId")
	public void setMakingId(int makingId) {
		this.makingId = makingId;
	}
	/** Sets */
	@MethodSaveProxy(proxy = "status")
	public void setStatus(int status) {
		this.status = status;
	}
	/** Sets */
	@MethodSaveProxy(proxy = "startTime")
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	/** Sets */
	@MethodSaveProxy(proxy = "speedTime")
	public void setSpeedTime(long speedTime) {
		this.speedTime = speedTime;
	}

	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(MakingEntity) "
				+ "id='" + id + "', "
				+ "uid='" + uid + "', "
				+ "ownerId='" + ownerId + "', "
				+ "makingId='" + makingId + "', "
				+ "status='" + status + "', "
				+ "startTime='" + startTime + "', "
				+ "speedTime='" + speedTime + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\""
				+ id + "\"," + "\""
				+ uid + "\"," + "\""
				+ ownerId + "\"," + "\""
				+ makingId + "\"," + "\""
				+ status + "\"," + "\""
				+ startTime + "\"," + "\""
				+ speedTime + "\"";
	}

	@Override
	public String getShardingKey() {
		return getOwnerId();
	}

	@Override
	public String getSubUniqueKey() {
		return String.valueOf(getMakingId());
	}

	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.MAKING.getKey();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public MakingInfo.Builder toProto () {
		MakingInfo.Builder builderInfo = MakingInfo.newBuilder();
		builderInfo.setUid(this.getUid());
		builderInfo.setMakingId(this.getMakingId());
		builderInfo.setStatus(this.getStatus());
		builderInfo.setStartTime(this.getStartTime());
		builderInfo.setSpeedTime(this.getSpeedTime());
		return builderInfo;
	}

}
