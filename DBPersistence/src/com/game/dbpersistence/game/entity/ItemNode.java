/**
 * ItemNode.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.message.proto.weapon.WeaponProtoBuf.ItemNodeInfo;

public class ItemNode {
	/**
	 * 武器强化分支节点的ID*
	 */
	private int id;
	/**
	 * 武器装备的名称 *
	 */
	private String name; 
	/**
	* 武器装备的类型 *
	*/
	private int type;
	/**
	 * 武器装备的天赋ID*
	 */
	private int talent;
	/**
	 * 武器装备的状态 1解锁0锁定*
	 */
	private int status;
	/**
	 * 武器强化的解锁花费的金币*
	 */
	private int cost;
	
	public ItemNode() {
		init();
	}

	public ItemNode(int id) {
		this.id = id;
		init();
	}

	/** Gets */
	public int getId() {
		return this.id;
	}

	/** Gets */
	public String getName() {
		return this.name;
	}

	/** Gets */
	public int getType() {
		return this.type;
	}
	
	/** Gets */
	public int getTalent() {
		return talent;
	}

	/** Gets */
	public int getStatus() {
		return status;
	}
	
	/** Gets */
	public int getCost() {
		return cost;
	}
	/** Initializes the values */
	public void init() {
		this.id = 0;
		this.name = "";
		this.type = 0;
		this.talent = 0;
		this.status = 0;
		this.cost = 0;
	}

	/** Sets */
	public void setId(int id) {
		this.id = id;
	}

	/** Sets */
	public void setName(String name) {
		this.name = name;
	}

	/** Sets */
	public void setType(int type) {
		this.type = type;
	}
	
	/** Sets */
	public void setTalent(int talent) {
		this.talent = talent;
	}
	
	/** Sets */
	public void setStatus(int status) {
		this.status = status;
	}

	/** Sets */
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	/** Returns the String representation */
	public String toString() {
		return "(ItemNode) " 
				+ "id='" + id + "', " 
				+ "name='" + name + "', " 
				+ "type='" + type + "', "
				+ "talent='" + talent + "', "
				+ "status='" + status + "',"
				+ "cost='" + cost + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\"" 
				+ id + "\"," + "\"" 
				+ name + "\"," + "\"" 
				+ type + "\"," + "\"" 
				+ talent + "\"," + "\""
				+ status + "\"," + "\""
				+ cost + "\"";
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public ItemNodeInfo.Builder toProto () {
		ItemNodeInfo.Builder builderInfo = ItemNodeInfo.newBuilder();
		builderInfo.setId(this.getId());
		builderInfo.setName(this.getName());
		builderInfo.setType(this.getType());
		builderInfo.setTalent(this.getTalent());
		builderInfo.setStatus(this.getStatus());
		//builderInfo.setCost(this.getCost());
		return builderInfo;
	}
}

