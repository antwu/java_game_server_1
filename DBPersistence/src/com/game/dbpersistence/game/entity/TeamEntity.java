/**
 * TeamEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;

import com.alibaba.fastjson.JSON;
import com.game.core.db.common.annotation.FieldSave2Redis;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseLongIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import com.game.message.proto.team.TeamProtoBuf.TeamInfo;

/**
 * Domain object class for TeamEntity
 *
 * @author CodeMaster v1.0
 */
public class TeamEntity extends BaseLongIDEntity implements RedisListInterface, Cloneable {
	/**
	 * 默认生成的队伍ID，每次创建自动加 1 *
	 */
	@FieldSave2Redis
	private long id = 0;
	/**
	 * 创建唯一关键字索引UUID *
	 */
	@FieldSave2Redis
	private String uid = "";
	/**
	 * 该实体是否在过有效期后需要需要自动从缓存中删除 *
	 */
	@FieldSave2Redis
	private boolean deleted = false;
	/**
	 *  设置有效期时间，默认从当前创建时间加24小时合86400秒 *
	 */
	@FieldSave2Redis
	private Date deleteTime;
	/**
	 * /队长ID 或 ACC *
	 */
	@FieldSave2Redis
	private String captainId = "";
	/**
	 * 队伍名称 *
	 */
	@FieldSave2Redis
	private String teamName = "";
	/**
	 * 关卡或任务ID *
	 */
	@FieldSave2Redis
	private int missionId = 0;
	/**
	 * 加入队伍是否需要密码 *
	 */
	@FieldSave2Redis
	private String teamPwd = "";
	/**
	 * 战力需求 *
	 */
	@FieldSave2Redis
	private int requirePower = 0;
	/**
	 * 最大人数 *
	 */
	@FieldSave2Redis
	private int maxPlayerNum = 0;
	/**
	 * 当前人数 *
	 */
	@FieldSave2Redis
	private int curPlayerNum = 0;
	/**
	 * 当前房间状态 1.空闲 2.战斗 *
	 */
	@FieldSave2Redis
	private int status;	
	/**
	 * 当前人数 *
	 */
	@FieldSave2Redis
	private HashMap<String, String> teamPlayerInfos = null;
	
	
	public TeamEntity() {
		init();
	}
	
	public TeamEntity(long id) {
		this.id = id;
		init();
	}
	
	public TeamEntity(String uid) {
		this.uid = uid;
		init();
	}
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}

	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}

	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}
	
	/** Gets */
	public String getCaptainId() {
		return this.captainId;
	}
	
	/** Gets */
	public String getTeamName() {
		return this.teamName;
	}

	/** Gets */
	public int getMissionId() {
		return this.missionId;
	}

	/** Gets */
	public String getTeamPwd() {
		return this.teamPwd;
	}

	/** Gets */
	public int getRequirePower() {
		return this.requirePower;
	}
	
	/** Gets */
	public int getMaxPlayerNum() {
		return this.maxPlayerNum;
	}
	
	/** Gets */
	public int getCurPlayerNum() {
		return this.curPlayerNum;
	}
	
	/** Gets */
	public int getStatus() {
		return this.status;
	}
	
	/** Gets */
	public HashMap<String, String> getTeamPlayerInfos() {
		return this.teamPlayerInfos;
	}
	
	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.captainId = "";
		this.teamName = "";
		this.missionId = 0;
		this.teamPwd = "";
		this.requirePower = 0;
		this.maxPlayerNum = 0;
		this.curPlayerNum = 0;
		this.status = 0;
		this.teamPlayerInfos = new HashMap<String, String>();
	}
	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "captainId")
	public void setCaptainId(String captainId) {
		this.captainId = captainId;
	}
	/** Sets */
	@MethodSaveProxy(proxy = "teamName")
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "missionId")
	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "teamPwd")
	public void setTeamPwd(String teamPwd) {
		this.teamPwd = teamPwd;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "requirePower")
	public void setRequirePower(int requirePower) {
		this.requirePower = requirePower;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "maxPlayerNum")
	public void setMaxPlayerNum(int maxPlayerNum) {
		this.maxPlayerNum = maxPlayerNum;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "curPlayerNum")
	public void setCurPlayerNum(int curPlayerNum) {
		this.curPlayerNum = curPlayerNum;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "status")
	public void setStatus(int status) {
		this.status = status;
	}	

	/** Sets */
	@MethodSaveProxy(proxy = "teamPlayerInfos")
	public void setTeamPlayerInfos(HashMap<String, String> teamPlayerInfos) {
		this.teamPlayerInfos = teamPlayerInfos;
	}

	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(TeamEntity) " 
				+ "id='" + id + "', " 
				+ "uid='" + uid + "', " 
				+ "captainId='" + captainId + "', " 
				+ "teamName='" + teamName + "', "
				+ "missionId='" + missionId + "', " 
				+ "teamPwd='" + teamPwd + "', " 
				+ "requirePower='" + requirePower + "', "
				+ "maxPlayerNum='" + maxPlayerNum + "', " 
				+ "curPlayerNum='" + curPlayerNum + "', " 
				+ "status='" + status + "'," 
				+ "teamPlayerInfos='" + JSON.toJSONString(teamPlayerInfos) + "'" ;
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\"" 
				+ id + "\",\"" 
				+ uid + "\",\"" 
				+ captainId + "\",\"" 
				+ teamName + "\",\"" 
				+ missionId + "\",\"" 
				+ teamPwd + "\",\"" 
				+ requirePower + "\",\"" 
				+ maxPlayerNum + "\",\"" 
				+ curPlayerNum + "\",\"" 
				+ status + "\",\"" 
				+ JSON.toJSONString(teamPlayerInfos) + "\"";
	}
	
	public TeamInfo.Builder toProto() {
		TeamInfo.Builder builderInfo = TeamInfo.newBuilder();
		builderInfo.setId(this.getId());
		builderInfo.setTeamName(this.getTeamName());
		builderInfo.setMissionId(this.getMissionId());
		builderInfo.setIsNeedPwd(this.getTeamPwd() == "" ? 0 : 1);
		builderInfo.setRequirePower(this.getRequirePower());
		builderInfo.setMaxPlayerNum(this.getMaxPlayerNum());
		builderInfo.setCurPlayerNum(this.getCurPlayerNum());
		builderInfo.setStatus(this.getStatus());
		builderInfo.setCaptainId(this.getCaptainId());
		return builderInfo;
	}

	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.TEAM.getKey();
	}

	@Override
	public String getSubUniqueKey() {
		return String.valueOf(getId());
	}

	@Override
	public String getShardingKey() {
		return String.valueOf(getMissionId());
	}

}