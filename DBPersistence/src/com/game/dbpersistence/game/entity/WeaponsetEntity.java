/**
 * WeaponsetEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.dbpersistence.game.mapper.WeaponsetEntityMapper;
import com.game.message.proto.weapon.WeaponProtoBuf.WeaponSetInfo;
import com.game.core.db.common.annotation.DbMapper;
import com.game.core.db.common.annotation.FieldSave;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseStringIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.util.Date;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for WeaponsetEntity
 *
 * @author CodeMaster v1.0
 */
@DbMapper(mapper = WeaponsetEntityMapper.class)
public class WeaponsetEntity extends BaseStringIDEntity implements RedisListInterface, Cloneable {
	/**
	 * 默认生成的账号实体的ID，每次创建自动加 1 *
	 */
	@FieldSave
	private long id;
	/**
	 * 创建唯一关键字索引UUID *
	 */
	@FieldSave
	private String uid;
	/**
	 * 该实体是否在过有效期后需要需要自动从缓存中删除 *
	 */
	@FieldSave
	private boolean deleted;
	/**
	 *  设置有效期时间，默认从当前创建时间加24小时合86400秒 *
	 */
	@FieldSave
	private Date deleteTime;
	/**
	 * 武器装备所属的玩家的UID *
	 */
	@FieldSave
	private String ownerId;
	/**
	 * 武器装备的设置页的Index *
	 */
	@FieldSave
	private int setId;
	/**
	 * 武器装备的设置名称 *
	 */
	@FieldSave
	private String setName;
	/**
	 * 武器装备1 *
	 */
	@FieldSave
	private String weapon1;
	/**
	 * 武器装备2 *
	 */
	@FieldSave
	private String weapon2;
	/**
	 * 武器装备3 *
	 */
	@FieldSave
	private String weapon3;
	/**
	 * 武器装备4 *
	 */
	@FieldSave
	private String weapon4;
	/**
	 * 武器装备时装衣服 *
	 */
	@FieldSave
	private int fashionId;
	
	/**
	 * 武器装备集束器 *
	 */
	@FieldSave
	private int buncherId;
	
	public WeaponsetEntity() {
		init();
	}
	
	public WeaponsetEntity(long id) {
		this.id = id;
		init();
	}
	
	public WeaponsetEntity(String uid) {
		this.uid = uid;
		init();
	}
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}

	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}

	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}

	/** Gets */
	public String getOwnerId() {
		return this.ownerId;
	}

	/** Gets */
	public int getSetId() {
		return this.setId;
	}

	/** Gets */
	public String getSetName() {
		return this.setName;
	}
	
	/** Gets */
	public String getWeapon1() {
		return this.weapon1;
	}

	/** Gets */
	public String getWeapon2() {
		return this.weapon2;
	}

	/** Gets */
	public String getWeapon3() {
		return this.weapon3;
	}

	/** Gets */
	public String getWeapon4() {
		return this.weapon4;
	}

	/** Gets */
	public int getFashionId() {
		return this.fashionId;
	}

	/** Gets */
	public int getBuncherId() {
		return this.buncherId;
	}
	
	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.ownerId = "";
		this.setId = 0;
		this.setName = "";
		this.weapon1 = "";
		this.weapon2 = "";
		this.weapon3 = "";
		this.weapon4 = "";
		this.fashionId = 0;
		this.buncherId = 0;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "ownerId")
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "setId")
	public void setSetId(int setId) {
		this.setId = setId;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "setName")
	public void setSetName(String setName) {
		this.setName = setName;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "weapon1")
	public void setWeapon1(String weapon1) {
		this.weapon1 = weapon1;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "weapon2")
	public void setWeapon2(String weapon2) {
		this.weapon2 = weapon2;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "weapon3")
	public void setWeapon3(String weapon3) {
		this.weapon3 = weapon3;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "weapon4")
	public void setWeapon4(String weapon4) {
		this.weapon4 = weapon4;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "fashionId")
	public void setFashionId(int fashionId) {
		this.fashionId = fashionId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "buncherId")
	public void setBuncherId(int buncherId) {
		this.buncherId = buncherId;
	}

	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(WeaponsetEntity) " 
				+ "id='" + id + "', " 
				+ "uid='" + uid + "', " 
				+ "ownerId='" + ownerId + "', " 
				+ "setId='" + setId + "', " 
				+ "setName='" + setName + "', " 
				+ "weapon1='" + weapon1 + "', " 
				+ "weapon2='" + weapon2 + "', "	
				+ "weapon3='" + weapon3 + "', " 
				+ "weapon4='" + weapon4 + "', " 
				+ "fashionId='" + fashionId + "', "	
				+ "buncherId='" + buncherId + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\"" 
				+ id + "\",\"" 
				+ uid + "\",\"" 
				+ ownerId + "\",\"" 
				+ setId + "\",\"" 
				+ setName + "\",\"" 
				+ weapon1 + "\",\"" 
				+ weapon2 + "\",\"" 
				+ weapon3 + "\",\"" 
				+ weapon4 + "\",\"" 
				+ fashionId	+ "\",\"" 
				+ buncherId + "\"";
	}

	@Override
	public String getShardingKey() {
		return getOwnerId();
	}

	@Override
	public String getSubUniqueKey() {
		return String.valueOf(getSetId());
	}
	
	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.WEAPONSET.getKey();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public WeaponSetInfo.Builder toProto () {
		WeaponSetInfo.Builder builderInfo = WeaponSetInfo.newBuilder();
		builderInfo.setOwnerId(this.getOwnerId());
		builderInfo.setSetId(this.getSetId());
		builderInfo.setSetName(this.getSetName());
		builderInfo.setWeapon1(this.getWeapon1());
		builderInfo.setWeapon2(this.getWeapon2());
		builderInfo.setWeapon3(this.getWeapon3());
		builderInfo.setWeapon4(this.getWeapon4());
		builderInfo.setFashionId(this.getFashionId());
		builderInfo.setBuncherId(this.getBuncherId());
		return builderInfo;
	}
	
}