/**
 * MainNode.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.message.proto.weapon.WeaponProtoBuf.MainNodeInfo;
import java.util.ArrayList;
import java.util.List;

public class MainNode {
	/**
	 * 武器强化主线节点的ID*
	 */
	private int id;
	/**
	 * 武器装备的名称 *
	 */
	private String name; 
	/**
	* 武器装备的类型 *
	*/
	private int type;
	/**
	 * 武器装备 方向*
	 */
	private int dir;
	/**
	 * 武器装备的状态 1解锁0锁定*
	 */
	private int status;
	/**
	 * 武器强化的解锁花费的金币*
	 */
	private int cost;
	
	
	/**
	 * 当前分支节点列表 *
	 */
	private List<ItemNode> itemNodeList = null;

	public MainNode() {
		init();
	}

	public MainNode(int id) {
		this.id = id;
		init();
	}

	/** Gets */
	public int getId() {
		return this.id;
	}

	/** Gets */
	public String getName() {
		return this.name;
	}

	/** Gets */
	public int getType() {
		return this.type;
	}

	/** Gets */
	public int getDir() {
		return this.dir;
	}
	
	/** Gets */
	public int getStatus() {
		return status;
	}
	
	/** Gets */
	public int getCost() {
		return cost;
	}
	
	/** Gets */
	public List<ItemNode> getItemNodeList() {
		return itemNodeList;
	}

	/** Initializes the values */
	public void init() {
		this.id = 0;
		this.name = "";
		this.type = 0;
		this.dir = 0;
		this.status = 0;
		this.cost = 0;
		this.itemNodeList = new ArrayList<ItemNode>();
	}

	/** Sets */
	public void setId(int id) {
		this.id = id;
	}

	/** Sets */
	public void setName(String name) {
		this.name = name;
	}

	/** Sets */
	public void setType(int type) {
		this.type = type;
	}

	/** Sets */
	public void setDir(int dir) {
		this.dir = dir;
	}
	
	/** Sets */
	public void setStatus(int status) {
		this.status = status;
	}

	/** Sets */
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	/** Sets */
	public void setItemNodeList(List<ItemNode> itemNodeList) {
		this.itemNodeList = itemNodeList;
	}

	/** Returns the String representation */
	public String toString() {
		return "(MainNode) "
				+ "id='" + id + "', "
				+ "name='" + name + "', "
				+ "type='" + type + "', "
				+ "dir='" + dir + "', "
				+ "status='" + status + "', "
				+ "cost='" + cost + "', "
				+ "itemList='" + itemNodeList.toString() + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\""
				+ id + "\"," + "\""
				+ name + "\"," + "\""
				+ type + "\"," + "\""
				+ dir + "\"," + "\""
				+ status + "\"," + "\""
				+ cost + "\"," + "\""
				+ itemNodeList + "\"";
	}
	
	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public MainNodeInfo.Builder toProto () {
		MainNodeInfo.Builder builderInfo = MainNodeInfo.newBuilder();
		builderInfo.setId(this.getId());
		builderInfo.setName(this.getName());
		builderInfo.setType(this.getType());
		builderInfo.setDir(this.getDir());
		builderInfo.setStatus(this.getStatus());
		//builderInfo.setCost(this.getCost());
		if((this.itemNodeList != null)&&(this.itemNodeList.size() > 0)) {
			for(ItemNode itemNode : this.itemNodeList) {
				builderInfo.addItemNodeList(itemNode.toProto());
			}
		}
		return builderInfo;
	}
	
}

