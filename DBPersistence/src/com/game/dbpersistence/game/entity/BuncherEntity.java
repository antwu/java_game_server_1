/**
 * BuncherEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.dbpersistence.game.entity;

import com.game.dbpersistence.game.mapper.BuncherEntityMapper;
import com.game.message.proto.weapon.WeaponProtoBuf.BuncherInfo;
import com.game.core.db.common.annotation.DbMapper;
import com.game.core.db.common.annotation.FieldSave;
import com.game.core.db.common.annotation.MethodSaveProxy;
import com.game.core.db.entity.BaseStringIDEntity;
import com.game.core.db.service.redis.RedisKeyEnum;
import com.game.core.db.service.redis.RedisListInterface;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.util.Date;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Domain object class for BuncherEntity
 *
 * @author CodeMaster v1.0
 */
@DbMapper(mapper = BuncherEntityMapper.class)
public class BuncherEntity extends BaseStringIDEntity implements RedisListInterface, Cloneable {
	/**
	 * 默认生成的账号实体的ID，每次创建自动加 1 *
	 */
	@FieldSave
	private long id;
	/**
	 * 创建唯一关键字索引UUID *
	 */
	@FieldSave
	private String uid;
	/**
	 * 该实体是否在过有效期后需要需要自动从缓存中删除 *
	 */
	@FieldSave
	private boolean deleted;
	/**
	 *  设置有效期时间，默认从当前创建时间加24小时合86400秒 *
	 */
	@FieldSave
	private Date deleteTime;
	/**
	 * 武器装备所属的玩家的UID *
	 */
	@FieldSave
	private String ownerId;

	/**
	 * 武器装备的ID *
	 */
	@FieldSave
	private int buncherId;
	/**
	 * 武器装备名称 *
	 */
	@FieldSave
	private String buncherName;
	/**
	 * 武器装备特征描述 *
	 */
	@FieldSave
	private String feature;
	/**
	 * 是否解锁 0未解锁 1已解锁 *
	 */
	@FieldSave
	private int unlocked;
	
	public BuncherEntity() {
		init();
	}
	
	public BuncherEntity(long id) {
		this.id = id;
		init();
	}
	
	public BuncherEntity(String uid) {
		this.uid = uid;
		init();
	}
	/** Gets */
	@Override
	public long getId() {
		return this.id;
	}

	/** Gets */
	@Override
	public String getUid() {
		return this.uid;
	}

	/** Gets */
	@Override
	public boolean isDeleted() {
		return this.deleted;
	}

	/** Gets */
	@Override
	public Date getDeleteTime() {
		return this.deleteTime;
	}

	/** Gets */
	public String getOwnerId() {
		return this.ownerId;
	}
	
	/** Gets */
	public int getBuncherId() {
		return this.buncherId;
	}

	/** Gets */
	public String getBuncherName() {
		return this.buncherName;
	}

	/** Gets */
	public String getFeature() {
		return this.feature;
	}
	
	/** Gets */
	public int getUnlocked() {
		return this.unlocked;
	}
	
	/** Initializes the values */
	public void init() {
		if (this.id == 0) {
			this.id = super.getId();
		}
		if (this.uid == null || this.uid == "") {
			this.uid = super.getUid();
		}
		this.deleted = false;
		this.deleteTime = new Date();
		this.ownerId = "";
		this.buncherId = 0;
		this.buncherName = "";
		this.feature = "";
		this.unlocked = 0;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "id")
	public void setId(long id) {
		this.id = id;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "uid")
	public void setUid(String uid) {
		this.uid = uid;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleted")
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/** Sets */
	@Override
	@MethodSaveProxy(proxy = "deleteTime")
	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "ownerId")
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "buncherId")
	public void setBuncherId(int buncherId) {
		this.buncherId = buncherId;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "buncherName")
	public void setBuncherName(String buncherName) {
		this.buncherName = buncherName;
	}

	/** Sets */
	@MethodSaveProxy(proxy = "feature")
	public void setFeature(String feature) {
		this.feature = feature;
	}
	
	/** Sets */
	@MethodSaveProxy(proxy = "unlocked")
	public void setUnlocked(int unlocked) {
		this.unlocked = unlocked;
	}

	/** depth clone **/
	public Object clone() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Returns the String representation */
	public String toString() {
		return "(BuncherEntity) " 
				+ "id='" + id + "', " 
				+ "uid='" + uid + "', " 
				+ "ownerId='" + ownerId + "', "
				+ "buncherId='" + buncherId + "', " 
				+ "buncherName='" + buncherName + "'," 
				+ "feature='" + feature + "'," 
				+ "unlocked='" + unlocked + "'";
	}

	/** Returns the CSV String */
	public String toCSVLine() {
		return "\"" 
				+ id + "\",\"" 
				+ uid + "\",\"" 
				+ ownerId + "\",\"" 
				+ buncherId + "\",\"" 
				+ buncherName + "\",\"" 
				+ feature + "\",\"" 
				+ unlocked + "\"";
	}

	@Override
	public String getShardingKey() {
		return getOwnerId();
	}

	@Override
	public String getSubUniqueKey() {
		return getBuncherId()+"";
	}

	@Override
	public String getRedisKeyEnumString() {
		return RedisKeyEnum.BUNCHER.getKey();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public BuncherInfo.Builder toProto () {
		BuncherInfo.Builder builderInfo = BuncherInfo.newBuilder();
		builderInfo.setOwnerId(this.getOwnerId());
		builderInfo.setBuncherId(this.getBuncherId());
		builderInfo.setBuncherName(this.getBuncherName());
		builderInfo.setFeature(this.getFeature());
		builderInfo.setUnlocked(this.getUnlocked());
		return builderInfo;
	}
}