package com.game.common.constants;

public class GlobalConstants
{
	final public static int DEFAULT_CALLBACK = 0;
	final public static int AREA_NO_ALLOCATION= 0;
	final public static int AREA_SHARE=-1;
	final public static String MYSQL_DB_IP = "MYSQL_DB_IP";
	final public static String MYSQL_DB_NAME = "MYSQL_DB_NAME";
	final public static String MYSQL_DB_USERNAME = "MYSQL_DB_USERNAME";
	final public static String MYSQL_DB_PASSWORD = "MYSQL_DB_PASSWORD";
	final public static String CONFIG_MYSQL_DB_IP = "CONFIG_MYSQL_DB_IP";
	final public static String CONFIG_MYSQL_DB_NAME = "CONFIG_MYSQL_DB_NAME";
	final public static String CONFIG_MYSQL_DB_USERNAME = "CONFIG_MYSQL_DB_USERNAME";
	final public static String CONFIG_MYSQL_DB_PASSWORD = "CONFIG_MYSQL_DB_PASSWORD";

	final public static String MYSQL_LOG_DB_IP = "MYSQL_LOG_DB_IP";
	final public static String MYSQL_LOG_DB_NAME = "MYSQL_LOG_DB_NAME";
	final public static String MYSQL_LOG_DB_USERNAME = "MYSQL_LOG_DB_USERNAME";
	final public static String MYSQL_LOG_DB_PASSWORD = "MYSQL_LOG_DB_PASSWORD";
	public final static String ACCOUNTID_SPLIT = "_";
}
