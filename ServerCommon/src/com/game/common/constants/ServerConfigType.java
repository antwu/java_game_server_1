package com.game.common.constants;

public class ServerConfigType
{
	public static final int SERVER_OPEN = 1;
	public static final int SERVER_TEST = 2;
}
