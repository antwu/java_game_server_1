package com.game.common.constants;

public class ServerType
{
	final static public int GAME_SERVER = 1;
	final static public int BATTLE_SERVER = 2;
	final static public int GATEWAY_SERVER = 3;
}
