package com.game.common.service;
//package com.game.common.service;
//
//import java.net.SocketAddress;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.game.common.constants.ServerType;
//import com.game.core.net.common.RemoteNode;
//import com.game.core.service.PublicService;
//
//public class ServerInfoService extends PublicService
//{
//	private static final long serialVersionUID = -4809149293523658481L;
//	private static Logger logger = LoggerFactory.getLogger(ServerInfoService.class);
//	private ServerInfo selfServerInfo;
//	private Map<Integer, ServerInfo> servers = new ConcurrentHashMap<Integer, ServerInfo>();
//
//	public void updateServerInfo(ServerInfo serverInfo, RemoteNode remoteNode)
//	{
//		serverInfo.setRemoteNode(remoteNode);
//		servers.put(serverInfo.getServerID(), serverInfo);
//	}
//
//	public void setSelfServerInfo(ServerInfo serverInfo)
//	{
//		selfServerInfo = serverInfo;
//	}
//	public ServerInfo getSelfServerInfo()
//	{
//		return selfServerInfo;
//	}
//
//	public List<ServerInfo> getBattleServers()
//	{
//		List<ServerInfo> battleServers = new ArrayList<ServerInfo>();
//		for (ServerInfo serverInfo : servers.values())
//			if (serverInfo.getServerType() == ServerType.BATTLE_SERVER)
//				battleServers.add(serverInfo);
//
//		return battleServers;
//	}
//
//	public void updateSelfServerStatus(int newStatus)
//	{
//		selfServerInfo.setServerStatus(newStatus);
////		PushServiceInfoSYNHelper.broadcastMessage(selfServerInfo, GlobalConstants.DEFAULT_CALLBACK);
//	}
//
//	public List<RemoteNode> getConnectedServerNodes()
//	{
//		List<RemoteNode> remoteNodes = new ArrayList<RemoteNode>();
//		for (ServerInfo serverInfo : servers.values())
//		{
//			if (serverInfo.getRemoteNode() != null)
//				remoteNodes.add(serverInfo.getRemoteNode());
//		}
//		return remoteNodes;
//	}
//
//	public List<ServerInfo> getUnconnectedServers()
//	{
//		List<ServerInfo> unconnectedServers = new ArrayList<ServerInfo>();
//		for (ServerInfo serverInfo : servers.values())
//		{
//			if (serverInfo.getRemoteNode() == null)
//			{
//				unconnectedServers.add(serverInfo);
//			}
//		}
//		return unconnectedServers;
//	}
//
//	public void disconnect(RemoteNode remoteNode)
//	{
//		for (ServerInfo serverInfo : servers.values())
//		{
//			if (serverInfo.getRemoteNode() == remoteNode)
//			{
//				serverInfo.setRemoteNode(null);
//				logger.info("disconnected with server:{}", serverInfo.getServerName());
//			}
//		}
//	}
//
//	public void connected(RemoteNode remoteNode)
//	{
//		for (ServerInfo serverInfo : servers.values())
//		{
//			if (remoteNode.getAddress().equals((SocketAddress) serverInfo.getAddress4Server()))
//			{
//				serverInfo.setRemoteNode(remoteNode);
//				logger.info("connected with server:{}", serverInfo.getServerName());
//			}
//		}
//	}
//
//	/**
//	 * manager server 单独使用
//	 * @param remoteNode
//	 */
//	public void managerConnected(RemoteNode remoteNode, ServerInfo serverInfo)
//	{
//		for (ServerInfo elem : servers.values())
//		{
//			if (elem.getServerID() == serverInfo.getServerID())
//			{
//				serverInfo.setRemoteNode(remoteNode);
//				logger.info("connected with server:{}", serverInfo.getServerName());
//			}
//		}
//	}
//
//	// 根据remoteNode判断该server是否是战斗服，并返回该战斗服的serverId
//	public int getBattleConfigId(RemoteNode remoteNode)
//	{
//		for (ServerInfo serverInfo : servers.values())
//		{
//			if (serverInfo.getRemoteNode() == remoteNode && serverInfo.getServerType() == ServerType.BATTLE_SERVER)
//				return serverInfo.getServerID();
//		}
//		return -1;
//	}
//	
//	public boolean isGameServer(RemoteNode remoteNode)
//	{
//		for (ServerInfo serverInfo : servers.values())
//		{
//			if (serverInfo.getRemoteNode() == remoteNode && serverInfo.getServerType() == ServerType.GAME_SERVER)
//				return true;
//		}
//		return false;
//	}
//}
