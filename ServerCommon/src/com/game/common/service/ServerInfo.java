package com.game.common.service;

import com.game.common.config.BaseServerConfig;
import com.game.core.net.common.RemoteNode;

public class ServerInfo
{
	private BaseServerConfig serverConfig;
	private RemoteNode remoteNode;

	public BaseServerConfig getServerConfig()
    {
	    return serverConfig;
    }
	void setServerConfig(BaseServerConfig serverConfig)
    {
	    this.serverConfig = serverConfig;
    }
	public RemoteNode getRemoteNode()
    {
	    return remoteNode;
    }
	void setRemoteNode(RemoteNode remoteNode)
    {
	    this.remoteNode = remoteNode;
    }
}
