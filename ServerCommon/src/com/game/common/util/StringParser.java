package com.game.common.util;

import java.util.HashMap;
import java.util.Map;

public class StringParser
{
	
	/**
	 * Format:  Name=hello;Type=1;Desc=world
	 * @param rawString
	 * @return
	 *TODO: need to refine
	 */
	static public Map<String, String> parseConfig(String rawString)
	{
		Map<String, String> configs = new HashMap<String, String>();
		String[] elements = rawString.split(";");
		for(String element : elements)
		{
			if (element.isEmpty())
			{
				continue;
			}
			String[] keyValue = element.split("=");
			configs.put(keyValue[0], keyValue[1]);
		}
		
		return configs;
	}
}
