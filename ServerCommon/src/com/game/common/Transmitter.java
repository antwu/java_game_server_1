package com.game.common;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.GeneratedMessage;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.AbstractMessageInitializer;
import com.game.core.net.message.AbstractCustomizeMessage;

public class Transmitter
{
	private static Logger logger = LoggerFactory.getLogger(Transmitter.class);
	static private Transmitter transmitter = new Transmitter();
	private AbstractMessageInitializer msgInitializer;

	private Transmitter()
	{
	}
	static public Transmitter getInstance()
	{
		return transmitter;
	}
	
	public void setMessageInitializer(AbstractMessageInitializer msgInitializer)
	{
		this.msgInitializer = msgInitializer;
	}
	
	public void write(RemoteNode remoteNode, AbstractCustomizeMessage msg)
	{
		remoteNode.writeAndFlush(msg);
	}

	public void write(RemoteNode remoteNode, int callback, int protocolID, byte[] buffer)
	{
		remoteNode.writeAndFlush(callback, protocolID, buffer);
	}

	public void write(RemoteNode remoteNode, int callback, GeneratedMessage msg)
	{
		int protocolID = msgInitializer.getProtocolID(msg.getClass());
		if (protocolID == 0)
		{
			logger.error("Please bind  the message:{} to corresponding protocolID", msg.getClass().getSimpleName());
			return;
		}
		if (remoteNode == null)
		{
			logger.error("Send message :{} to null remoteNode", msg.getClass().getSimpleName());
			return;
		}
		logger.debug("Send protocolID:{}  message:{} to role {} ip {}", protocolID, msg.getClass().getSimpleName(), remoteNode.getRoleId(), remoteNode.getAddress());
		remoteNode.writeAndFlush(callback, protocolID, msg);
	}
	
//	public void write(long playerID, int callback, GeneratedMessage msg)
//	{
//		SessionService service = ServiceContainer.getInstance().getPublicService(SessionService.class);
////		if(!service.isOnlineRoleId(playerID))
////			return;
//		Session session = service.getOnlineSession(playerID);
//		if(session != null)
//		{
//			write(session.getRemoteNode(), callback, msg);
//			return;
//		}
//		session = service.getOfflineSessionByRoleId(playerID);
//		if(session == null)
//		{
//			logger.debug("write: can't find session on offline session map. player id is " + playerID);
//			return;
//		}	
//
//		write(session.getRemoteNode(), callback, msg);
//	}
	
	public void writeAndClose(RemoteNode remoteNode, int callback, GeneratedMessage msg)
	{
		int protocolID = msgInitializer.getProtocolID(msg.getClass());
		if (protocolID == 0)
		{
			logger.error("Please bind  the message:{} to corresponding protocolID", msg.getClass().getSimpleName());
			return;
		}
		logger.debug("Send protocolID:{}  message:{} to {}", protocolID, msg.getClass().getSimpleName(), remoteNode.getRoleId());
		remoteNode.writeAndClose(callback, msgInitializer.getProtocolID(msg.getClass()), msg);
	}

	public void broadcast(List<RemoteNode> remoteNodes, AbstractCustomizeMessage msg)
	{
		//TODO:
	}

	public void broadcast(List<RemoteNode> remoteNodes, int callback, int protocolID, byte[] buffer)
	{
		//TODO:
	}

	public void broadcast(List<RemoteNode> remoteNodes, int callback, GeneratedMessage msg)
	{
		for(RemoteNode node : remoteNodes)
		{
			node.writeAndFlush(callback, msgInitializer.getProtocolID(msg.getClass()), msg);
		}
	}
	
	// 这里使用ArrayList，是为了和上面的方法重载
//	public void broadcast(ArrayList<Long> roleIds, int callback, GeneratedMessage msg)
//	{
//		for(Long roleId : roleIds)
//			write(roleId, callback, msg);
//	}

	public void broadcastToAllClient(AbstractCustomizeMessage msg)
	{
		//TODO:
	}

	public void broadcastToAllClient(int callback, int protocolID, byte[] buffer)
	{
		//TODO
	}

	public void broadcastToAllClient(int callback, int protocolID, GeneratedMessage msg)
	{
		//TODO:
	}

	public void broadcastToAllServer(AbstractCustomizeMessage msg)
	{
		//TODO:
	}

	public void broadcastToAllServer(int callback, int protocolID, byte[] buffer)
	{
		//TODO:
	}

//	public void broadcastToAllServer(int callback, GeneratedMessage msg)
//	{
//		SessionService ss = ServiceContainer.getInstance().getPublicService(SessionService.class);
//		ArrayList<Long> roleIds = new ArrayList<Long>(ss.getOnlineRoleId());
//		broadcast(roleIds, callback, msg);
//	}
}
