package com.game.common.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseLocalConfigInitializer {
	static private Logger logger = LoggerFactory.getLogger(BaseLocalConfigInitializer.class);

	public void init(BaseServerConfig config) throws Exception {
		init(new HashMap<String, String>(), config);
	}

	public void init(Map<String, String> retArguments, BaseServerConfig config) throws Exception {
		// 加载数据库配置
		InputStream propertiesStream = null;
		propertiesStream = BaseLocalConfigInitializer.class.getResourceAsStream("/baseInfo.properties");
		String ServerName = null;
		String MSHostName = null;
		int MSPort = 0;
		int serverID = 0;
		int ServerType = 0;
		String IPForServer = null;
		int PortForServer = 0;
		String IPForClient = null;
		int PortForClient = 0;
		String IPForGMTools = null;
		int PortForGMTools = 0;

		Properties properties = new Properties();
		try {
			properties.load(propertiesStream);
		} catch (IOException e) {
			logger.error("Failed load base local config from /baseInfo.properties");
			throw e;
		}

		try {
			if ((properties.containsKey("MSHostName") && (properties.containsKey("MSPort")))) {
				MSHostName = properties.getProperty("MSHostName").trim();
				MSPort = Integer.parseInt(properties.getProperty("MSPort").trim());
				InetSocketAddress manageServerAddress = new InetSocketAddress(MSHostName, MSPort);
				config.setManageServerAddress(manageServerAddress);
			} else {
				throw new Exception("The server host name or ip and port can't be null in the baseInfo.properties");
			}
			if (properties.containsKey("ServerName")) {
				ServerName = properties.getProperty("ServerName").trim();
				if (ServerName != null) {
					config.setServerName(ServerName);
				} else {
					throw new Exception("The server name can't be null in the baseInfo.properties");
				}
			}

			if (properties.containsKey("ServerID")) {
				serverID = Integer.parseInt(properties.getProperty("ServerID").trim());
				config.setServerID(serverID);
			}
			if (properties.containsKey("ServerType")) {
				ServerType = Integer.parseInt(properties.getProperty("ServerType").trim());
				config.setServerType(ServerType);
			}
			if (properties.containsKey("IPForServer")) {
				IPForServer = properties.getProperty("IPForServer").trim();
				config.setHostName4Server(IPForServer);
			}
			if (properties.containsKey("PortForServer")) {
				PortForServer = Integer.parseInt(properties.getProperty("PortForServer").trim());
				config.setPort4Server(PortForServer);
			}
			if (properties.containsKey("IPForClient")) {
				IPForClient = properties.getProperty("IPForClient").trim();
				config.setHostName4Client(IPForClient);
			}
			if (properties.containsKey("PortForClient")) {
				PortForClient = Integer.parseInt(properties.getProperty("PortForClient").trim());
				config.setPort4Client(PortForClient);
			}
			if (properties.containsKey("IPForGMTools")) {
				IPForGMTools = properties.getProperty("IPForGMTools").trim();
				config.setHostName4GMTools(IPForGMTools);
			}
			if (properties.containsKey("PortForGMTools")) {
				PortForGMTools = Integer.parseInt(properties.getProperty("PortForGMTools").trim());
				config.setPort4GMTools(PortForGMTools);
			}

			if ((ServerName != null) && (MSHostName != null)) {
				retArguments.put("ServerName", ServerName);
				retArguments.put("MSHostName", MSHostName);
				retArguments.put("MSPort", String.valueOf(MSPort));
				retArguments.put("Mode", properties.getProperty("Mode") == null ? new String() : properties.getProperty("Mode"));
			}

			logger.info("ServerName:{}  ManageServerAddress:{}  ManageServerPort:{}", ServerName, MSHostName, MSPort);
			logger.info("Success to load local config!");
		} catch (Exception e) {
			logger.error("Illegal content in the baseInfo.properties");
			throw e;
		}
	}

}