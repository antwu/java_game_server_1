package com.game.common.config;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import com.game.message.proto.server.ServerProtoBuf.ServerConfigPROTO;

public class BaseServerConfig
{
	private InetSocketAddress manageServerAddress;
	private int serverID;
	private String serverName;
	private int serverType;
	private String hostName4Server;
	private int port4Server;
	private String hostName4Client;
	private int port4Client;
	private String hostName4GMTools;
	private int port4GMTools;
	
	public BaseServerConfig()
	{
		init();
	}
	
	public void init()
	{
		manageServerAddress = null;
		serverID = 0;
		serverName = null;
		serverType = 0;
		hostName4Server = null;
		port4Server = 0;
		hostName4Client = null;
		port4Client = 0;
		hostName4GMTools = null;
		port4GMTools = 0;
	}
	
	public int getServerID()
	{
		return serverID;
	}
	public void setServerID(int serverID)
	{
		this.serverID = serverID;
	}
	public String getServerName()
	{
		return serverName;
	}
	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}
	public int getServerType()
    {
	    return serverType;
    }
	public void setServerType(int severType)
    {
	    this.serverType = severType;
    }
	public String getHostName4Server()
	{
		return hostName4Server;
	}
	public void setHostName4Server(String hostName4Server)
	{
		this.hostName4Server = hostName4Server;
	}
	public int getPort4Server()
	{
		return port4Server;
	}
	public void setPort4Server(int port4Server)
	{
		this.port4Server = port4Server;
	}
	public String getHostName4Client()
	{
		return hostName4Client;
	}
	public void setHostName4Client(String hostName4Client)
	{
		this.hostName4Client = hostName4Client;
	}
	public int getPort4Client()
	{
		return port4Client;
	}
	public void setPort4Client(int port4Client)
	{
		this.port4Client = port4Client;
	}
	public String getHostName4GMTools()
	{
		return hostName4GMTools;
	}
	public void setHostName4GMTools(String hostName4GMTools)
	{
		this.hostName4GMTools = hostName4GMTools;
	}
	public int getPort4GMTools()
	{
		return port4GMTools;
	}
	public void setPort4GMTools(int port4gmTools)
	{
		port4GMTools = port4gmTools;
	}
	public SocketAddress getManageServerAddress()
    {
	    return manageServerAddress;
    }
	public void setManageServerAddress(InetSocketAddress manageServerAddress)
    {
	    this.manageServerAddress = manageServerAddress;
    }
	public ServerConfigPROTO.Builder toProtoBufBuilder()
	{
		ServerConfigPROTO.Builder builder = ServerConfigPROTO.newBuilder();
		builder.setServerID(serverID);
		builder.setServerName(serverName);
		builder.setServerType(serverType);
		builder.setIpForServer(hostName4Server);
		builder.setPortForServer(port4Server);
		builder.setIpForClient(hostName4Client);
		builder.setPortForClient(port4Client);
		builder.setIpForGMTools(hostName4GMTools);
		builder.setPortForGMTools(port4GMTools);
		return builder;
	}
	
	public void fromProtoBuf(ServerConfigPROTO serverConfigPROTO)
	{
		serverID = serverConfigPROTO.getServerID();
		serverName = serverConfigPROTO.getServerName();
		serverType = serverConfigPROTO.getServerType();
		hostName4Server = serverConfigPROTO.getIpForServer();
		port4Server = serverConfigPROTO.getPortForServer();
		hostName4Client = serverConfigPROTO.getIpForClient();
		port4Client = serverConfigPROTO.getPortForClient();
		hostName4GMTools = serverConfigPROTO.getIpForGMTools();
		port4GMTools = serverConfigPROTO.getPortForGMTools();
	}
}
