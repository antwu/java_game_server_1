package com.game.battle.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.room.RoomService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.room.RoomProtoBuf.GBRoomCreateREQ;

@ActionAnnotation(actionClass = GBRoomCreateREQAction.class, messageClass = GBRoomCreateREQ.class, serviceClass = RoomService.class)
public class GBRoomCreateREQAction extends SSProtobufMessageHandler<RoomService, GBRoomCreateREQ>
{
	final static Logger logger = LoggerFactory.getLogger(GBRoomCreateREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, GBRoomCreateREQ message, int callback)
	{
		logger.info("GBRoomCreateREQAction accountID={} roomID={} rules={}", message.getAccountID(), message.getRoomID(), message.getRulesList());
		service.createRoom(remoteNode, message, callback);
	}

	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GBRoomCreateREQ message)
    {
	    return message.getRoomID();
    }
}
