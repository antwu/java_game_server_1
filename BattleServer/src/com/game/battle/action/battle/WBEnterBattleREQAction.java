package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ;

@ActionAnnotation(actionClass = WBEnterBattleREQAction.class, messageClass = WBEnterBattleREQ.class, serviceClass = BattleService.class)
public class WBEnterBattleREQAction extends SSProtobufMessageHandler<BattleService, WBEnterBattleREQ>
{
	Logger Logger = LoggerFactory.getLogger(WBEnterBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBEnterBattleREQ message,int callback)
	{
		service.enterBattle(message.getAccount());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
