package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.GBStartTeamBattleREQ;
import com.game.message.proto.team.TeamProtoBuf.GBTmpDestoryRoomREQ;

@ActionAnnotation(actionClass = GBTmpDestoryRoomREQAction.class, messageClass = GBTmpDestoryRoomREQ.class, serviceClass = BattleService.class)
public class GBTmpDestoryRoomREQAction extends SSProtobufMessageHandler<BattleService, GBTmpDestoryRoomREQ>
{
	Logger Logger = LoggerFactory.getLogger(GBStartTeamBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, GBTmpDestoryRoomREQ message,int callback)
	{
		service.tmpDestoryRoom(message);
	}
}
