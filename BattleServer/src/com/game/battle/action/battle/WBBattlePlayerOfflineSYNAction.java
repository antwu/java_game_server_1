package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBBattlePlayerOfflineSYN;

@ActionAnnotation(actionClass = WBBattlePlayerOfflineSYNAction.class, messageClass = WBBattlePlayerOfflineSYN.class, serviceClass = BattleService.class)
public class WBBattlePlayerOfflineSYNAction extends SSProtobufMessageHandler<BattleService, WBBattlePlayerOfflineSYN>
{
	Logger Logger = LoggerFactory.getLogger(WBBattlePlayerOfflineSYNAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBBattlePlayerOfflineSYN message,
			int callback)
	{
		service.playerOffline(message.getAccountID(), message.getRoom());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
