package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.GBStartSingleBattleREQ;
import com.game.message.proto.team.TeamProtoBuf.GBStartTeamBattleREQ;

@ActionAnnotation(actionClass = GBStartSingleBattleREQAction.class, messageClass = GBStartSingleBattleREQ.class, serviceClass = BattleService.class)
public class GBStartSingleBattleREQAction extends SSProtobufMessageHandler<BattleService, GBStartSingleBattleREQ>
{
	Logger Logger = LoggerFactory.getLogger(GBStartTeamBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, GBStartSingleBattleREQ message,int callback)
	{
		service.startSingleBattle(message);
	}
}
