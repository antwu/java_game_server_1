package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBCancleOperateREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ;

@ActionAnnotation(actionClass = WBCancleOperateREQAction.class, messageClass = WBCancleOperateREQ.class, serviceClass = BattleService.class)
public class WBCancleOperateREQAction extends SSProtobufMessageHandler<BattleService, WBCancleOperateREQ>
{
	Logger Logger = LoggerFactory.getLogger(WBEnterBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBCancleOperateREQ message,int callback)
	{
		service.cancleOperate(message.getAccount());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
