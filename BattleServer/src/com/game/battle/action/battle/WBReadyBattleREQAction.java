package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBReadyBattleREQ;

@ActionAnnotation(actionClass = WBReadyBattleREQAction.class, messageClass = WBReadyBattleREQ.class, serviceClass = BattleService.class)
public class WBReadyBattleREQAction extends SSProtobufMessageHandler<BattleService, WBReadyBattleREQ>
{
	Logger Logger = LoggerFactory.getLogger(WBEnterBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBReadyBattleREQ message,int callback)
	{
		service.readyBattle(message.getAccount());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
