package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBChangeWeaponREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ;

@ActionAnnotation(actionClass = WBChangeWeaponREQAction.class, messageClass = WBChangeWeaponREQ.class, serviceClass = BattleService.class)
public class WBChangeWeaponREQAction extends SSProtobufMessageHandler<BattleService, WBChangeWeaponREQ>
{
	Logger Logger = LoggerFactory.getLogger(WBEnterBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBChangeWeaponREQ message,int callback)
	{
		service.changeWeapon(message,callback);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
