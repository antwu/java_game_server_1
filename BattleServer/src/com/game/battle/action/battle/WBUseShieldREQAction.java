package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBUseShieldREQ;

@ActionAnnotation(actionClass = WBUseShieldREQAction.class, messageClass = WBUseShieldREQ.class, serviceClass = BattleService.class)
public class WBUseShieldREQAction extends SSProtobufMessageHandler<BattleService, WBUseShieldREQ>
{
	Logger Logger = LoggerFactory.getLogger(WBEnterBattleREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBUseShieldREQ message,int callback)
	{
		service.useShield(message,callback);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
