package com.game.battle.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.battle.BattleService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.WBSearchPathREQ;

@ActionAnnotation(actionClass = WBSearchPathREQAction.class, messageClass = WBSearchPathREQ.class, serviceClass = BattleService.class)
public class WBSearchPathREQAction extends SSProtobufMessageHandler<BattleService, WBSearchPathREQ>
{
	Logger Logger = LoggerFactory.getLogger(WBSearchPathREQ.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, WBSearchPathREQ message,int callback)
	{
		service.searchPath(message);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode)
	{
		return remoteNode.getRoomID();
	}
}
