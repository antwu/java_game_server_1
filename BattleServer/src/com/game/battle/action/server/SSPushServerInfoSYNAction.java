package com.game.battle.action.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.server.ServerService;
import com.game.common.config.BaseServerConfig;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.server.ServerProtoBuf.SSPushServerInfoSYN;

@ActionAnnotation(actionClass = SSPushServerInfoSYNAction.class, messageClass = SSPushServerInfoSYN.class, serviceClass = ServerService.class)
public class SSPushServerInfoSYNAction  extends SSProtobufMessageHandler<ServerService, SSPushServerInfoSYN>
{
	final static Logger logger = LoggerFactory.getLogger(SSPushServerInfoSYNAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, ServerService service, SSPushServerInfoSYN message, int callback)
	{
		BaseServerConfig config = service.getServerConfig();
		logger.debug("SSPushServerInfoSYNAction, servername:{} ", config.getServerName());
		config.fromProtoBuf(message.getServerConfig());
		service.getNetInitializer().openPort4Client(config.getPort4Server());
		logger.info("Battle server is ready!!!");
	}
}
