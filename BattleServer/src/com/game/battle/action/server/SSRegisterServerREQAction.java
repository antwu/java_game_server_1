package com.game.battle.action.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.server.ServerService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.message.proto.server.ServerProtoBuf.SSRegisterServerREQ;

@ActionAnnotation(actionClass = SSRegisterServerREQAction.class, messageClass=SSRegisterServerREQ.class, serviceClass = ServerService.class)
public class SSRegisterServerREQAction extends SSProtobufMessageHandler<ServerService, SSRegisterServerREQ>
{
	private static Logger logger = LoggerFactory.getLogger(SSRegisterServerREQAction.class);
	@Override
    public void handleMessage(RemoteNode remoteNode, ServerService service, SSRegisterServerREQ message, int callback)
    {
		service.setGatewayNode(message.getServerID(), remoteNode);
		logger.info("result : getServerID={}", message.getServerID());
    }
}