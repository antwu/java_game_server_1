package com.game.battle.start;

import com.game.battle.service.room.DeployCountDownTask;
import com.kodgames.core.timer.TimerMgr;;

public class TaskInitializer
{
	private static TaskInitializer instance = new TaskInitializer();
	private TaskInitializer()
	{
	}
	
	public static TaskInitializer getInstance()
	{
		return instance;
	}
	
	public int init()
	{
		TimerMgr.getTimerMgr().SetBaseTime(System.currentTimeMillis());
		TimerMgr.getTimerMgr().start();
		
		DeployCountDownTask.getInstance().init();
		return 0;
	}
}
