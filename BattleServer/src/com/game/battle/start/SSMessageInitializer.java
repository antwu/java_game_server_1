package com.game.battle.start;

import com.game.battle.net.server.SSConnectionHandler;
import com.game.common.Project4MessageInitializer;

public class SSMessageInitializer extends Project4MessageInitializer
{
	public SSMessageInitializer(String actionPackageName)
    {
	    super(actionPackageName);
    }

	@Override
    protected void initMessages() throws Exception
    {
		super.initMessages();
	    SSConnectionHandler ssConnectionHandler = new SSConnectionHandler();
	    setConnectionActiveHandler(ssConnectionHandler);
	    setConnectionInactiveHandler(ssConnectionHandler);
    }
}
