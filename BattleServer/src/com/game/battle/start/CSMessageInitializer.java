package com.game.battle.start;

import com.game.battle.net.server.CSConnectionHandler;
import com.game.common.Project4MessageInitializer;

public class CSMessageInitializer extends Project4MessageInitializer
{

	public CSMessageInitializer(String actionPackageName)
    {
	    super(actionPackageName);
    }

	@Override
    protected void initMessages() throws Exception
    {
		super.initMessages();
		CSConnectionHandler csConnectionHandler = new CSConnectionHandler();
	    setConnectionActiveHandler(csConnectionHandler);
	    setConnectionInactiveHandler(csConnectionHandler);
    }

}
