package com.game.battle.common.ai;

public class MonsterAI {
	/**
	 * aiID
	 */
	private int id;
	/**
	 * 是否激活
	 */
	private boolean active;
	/**
	 * 上次检测是否激活的回合数（绝对回合数）
	 */
	private int lastCheckActiveRound;
	/**
	 * 上次执行的回合数
	 */
	private int lastActionRound;
	/**
	 * ai执行次数
	 */
	private int actionCount;
	
	
	public MonsterAI(Integer aiId) {
		this.id = aiId;
		this.active = false;
		this.lastCheckActiveRound = 0;
		this.lastActionRound = -999;
		this.actionCount = 0;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public int getActionCount() {
		return actionCount;
	}
	public void setActionCount(int actionCount) {
		this.actionCount = actionCount;
	}
	
	public int getLastCheckActiveRound() {
		return lastCheckActiveRound;
	}
	public void setLastCheckActiveRound(int lastCheckActiveRound) {
		this.lastCheckActiveRound = lastCheckActiveRound;
	}
	public int getLastActionRound() {
		return lastActionRound;
	}
	public void setLastActionRound(int lastActionRound) {
		this.lastActionRound = lastActionRound;
	}
	public void addActionCount() {
		this.actionCount++;
	}
}
