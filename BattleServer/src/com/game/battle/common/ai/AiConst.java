package com.game.battle.common.ai;

public class AiConst {
	public static final int eventId1 = 1;
	public static final int eventId2 = 2;
	public static final int eventId3 = 3;
	//自身血量百分比范围 两个参数 minHP%	maxHP%
	public static final int eventId4 = 4;
	public static final int eventId5 = 5;
	public static final int eventId6 = 6;
	public static final int eventId7 = 7;
	public static final int eventId8 = 8;
	public static final int eventId9 = 9;
	public static final int eventId10 = 10;
	public static final int eventId11 = 11;
	//特定BUFF的叠加层数范围
	public static final int eventId12 = 12;
	//战场上存在的特定BUFF的数量范围
	public static final int eventId13 = 13;
	
	//像目标移动
	public static final int actionId1 = 1;
	public static final int actionId2 = 2;
	public static final int actionId3 = 3;
	//释放技能
	public static final int actionId4 = 4;
	//向目标移动并释放技能
	public static final int actionId5 = 5;
	public static final int actionId6 = 6;
	public static final int actionId7 = 7;
	public static final int actionId8 = 8;
	public static final int actionId9 = 9;
	
	public static final int targetId1 = 1;
	//找血量最低的目标
	public static final int targetId2 = 2;
	public static final int targetId3 = 3;
	//直线距离最近的目标
	public static final int targetId4 = 4;
	public static final int targetId5 = 5;
	public static final int targetId6 = 6;
	public static final int targetId7 = 7;
	public static final int targetId8 = 8;
	public static final int targetId9 = 9;
	public static final int targetId10 = 10;
	public static final int targetId11 = 11;
	public static final int targetId12 = 12;
	public static final int targetId13 = 13;
	public static final int targetId14 = 14;
	public static final int targetId15 = 15;
	public static final int targetId16 = 16;
	public static final int targetId17 = 17;
	public static final int targetId18 = 18;
	public static final int targetId19 = 19;
	public static final int targetId20 = 20;
	public static final int targetId21 = 21;
	public static final int targetId22 = 22;
}
