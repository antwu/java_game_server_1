package com.game.battle.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kodgames.core.task.Task;

public abstract class BattleServerTask implements Task
{
	Logger Logger = LoggerFactory.getLogger(BattleServerTask.class);

	@Override
	public void run(long currentTime, int state)
	{
		run(state);
	}

	/**
	 * @param state 状态， 一次性task该值为0
	 */
	public abstract void run(int state);
}

