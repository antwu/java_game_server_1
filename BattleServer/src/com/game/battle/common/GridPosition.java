package com.game.battle.common;
public class GridPosition implements Cloneable{
    private int x ;
    private int y ;
    
    
	public GridPosition(int nodeX, int nodeY) {
		this.x = nodeX;
		this.y = nodeY;
	}
	
	 @Override  
    public Object clone() {  
		 GridPosition gridPosition = null;  
        try{  
        	gridPosition = (GridPosition)super.clone();  
        }catch(CloneNotSupportedException e) {  
            e.printStackTrace();  
        }  
        return gridPosition;  
    }  
	 
    @Override
    public boolean equals(Object other){
    	 if (this == other)
             return true;
         if (other == null)
             return false;
         if (getClass() != other.getClass())
             return false;
         final GridPosition otherPos = (GridPosition) other;
         if(this.x !=otherPos.getX() || this.y != otherPos.getY())
             return false;
         return true;
    }
	    
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

}