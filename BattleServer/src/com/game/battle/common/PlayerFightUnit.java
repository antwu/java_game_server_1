package com.game.battle.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import com.game.battle.common.BattleEnum.EnumSceneUnitType;
import com.game.battle.common.Attr.AttributeTemplate;
import com.game.battle.common.buff.Buff;
import com.game.battle.common.fashion.Fashion;
import com.game.battle.common.weapon.Weapon;
import com.game.battle.config.AttributeConfig;
import com.game.battle.config.XMLTemplateService;
import com.game.message.proto.battle.BattleProtoBuf.MFightUnit;
import com.game.message.proto.battle.BattleProtoBuf.MPathNode;
import com.game.message.proto.battle.BattleProtoBuf.MRotation;
import com.game.message.proto.player.PlayerProtoBuf.PlayerInfo;
import com.game.message.proto.weapon.WeaponProtoBuf.FashionInfo;
import com.game.message.proto.weapon.WeaponProtoBuf.WeaponInfo;


public class PlayerFightUnit extends FightSceneUnit
{
	private List<Weapon> weapons = new ArrayList<>();
	private int selectWeaponId;
	private int initWeaponId;//进场初始的武器
	private Fashion fashion;
	private int sex = 1;
	private String name;
	public Stack<Shield> shieldStack = new Stack<Shield>();
	
	public AttributeTemplate weaponTotalAttribute = new  AttributeTemplate();
	
	public PlayerFightUnit(int bornx, int borny, PlayerInfo playerInfo) {
		this.id = SceneUnitIDGenerater.getId(EnumSceneUnitType.Monster.ordinal()+EnumSceneUnitType.Obstacle.ordinal());
    	this.unitType = EnumSceneUnitType.Player.ordinal();
    	this.level = 1;
    	gridPos = new GridPosition(bornx,borny);

		List<WeaponInfo> weaponInfos = playerInfo.getWeaponInfosList();
		FashionInfo fashionInfo = playerInfo.getFashionInfo();
		int sex = playerInfo.getSex();
		String name = playerInfo.getName();
    	for(int i=0; i<weaponInfos.size(); i++){
    		WeaponInfo weaponInfo = weaponInfos.get(i);
    		if(i == 0){
    			selectWeaponId = weaponInfo.getWeaponId();
    			initWeaponId = weaponInfo.getWeaponId();
    		}
    		Weapon weapon = new Weapon(weaponInfo.getWeaponId());
    		//String talents  = weaponInfo.getTalents();
    		//Weapon weapon = new Weapon(talents);
    		//weapon.setCfgId(weaponInfo.getWeaponId());
    		//weapon.setMainSkillId(weaponInfo.getMainSkillId());
    		//weapon.setWeaponType(weaponInfo.getWeaponType());
    		weapons.add(weapon);
    		//各个武器的属性累计到武器总map上
    		weaponTotalAttribute.addAttribute(weapon.weaponAttribute);
    	}
    	this.initAttr();
    	this.setFashion(new Fashion(fashionInfo));
    	this.setSex(sex);
    	this.setName(name);
    	this.armorType = BattleEnum.ArmorType.Organic.ordinal();
	}
	
	@Override
	public void resetByClone(Map<Integer, FightSceneUnit> effectOrigiFightSceneUnits, FightSceneUnit unit) {
		super.resetByClone(effectOrigiFightSceneUnits, unit);
		FightSceneUnit originUnit = effectOrigiFightSceneUnits.get(unit.getId());
		List<Weapon> newWeapons = new ArrayList<>();
		if(originUnit != null){
			for(int i=0 ; i<weapons.size(); i++){
				Weapon origiWeapon = ((PlayerFightUnit)originUnit).getWeapons().get(i);
				newWeapons.add(origiWeapon.clone());
			}
		}
		setWeapons(newWeapons);
	}
	
	 @Override  
	 public PlayerFightUnit clone() {  
	 	PlayerFightUnit fightSceneUnit = null;  
    	fightSceneUnit = (PlayerFightUnit)super.clone();  
    	
    	List<Weapon> cloneWeapons = new ArrayList<>();
    	for(Weapon weapon : weapons){
    		cloneWeapons.add(weapon.clone());
    	}
    	fightSceneUnit.setWeapons(cloneWeapons);
    	
        return fightSceneUnit;  
    }  


	public List<Weapon> getWeapons() {
		return weapons;
	}

	public void setWeapons(List<Weapon> weapons) {
		this.weapons = weapons;
	}

	public int getSelectWeaponId() {
		return selectWeaponId;
	}

	public void setSelectWeaponId(int selectWeaponId) {
		this.selectWeaponId = selectWeaponId;
	}
	
	/**
	 * 获取当前选中的武器
	 */
	public Weapon getSelectWeapon(){
		Weapon weapon = null;
		for(Weapon w : weapons){
			if(w.getCfgId() == selectWeaponId){
				weapon = w;
			}
		}
		return weapon;
	}
	
	/**
	 * 获取当前选中的武器
	 */
	public Weapon getWeaponByCfgId(int weaponCfgId){
		Weapon weapon = null;
		for(Weapon w : weapons){
			if(w.getCfgId() == weaponCfgId){
				weapon = w;
			}
		}
		return weapon;
	}
	
	public int getInitWeaponId() {
		return initWeaponId;
	}


	public void setInitWeaponId(int initWeaponId) {
		this.initWeaponId = initWeaponId;
	}
	
	public int getArmorType() {
		return armorType;
	}

	public void setArmorType(int armorType) {
		this.armorType = armorType;
	}

	private void initAttr() {
		AttributeConfig attributeConfig = XMLTemplateService.attributeConfigMap.get(this.level);
		this.totalHp = attributeConfig.getHp() + this.weaponTotalAttribute.getHp();
		this.currentHp = this.totalHp;
		this.tempCurrentHp = currentHp;
		this.attack = attributeConfig.getAtt() + this.weaponTotalAttribute.getAtt();
		this.defence = attributeConfig.getDef() + this.weaponTotalAttribute.getDef();
		this.crit = attributeConfig.getCrit() + this.weaponTotalAttribute.getCrit();
		this.tenacity = attributeConfig.getTen() + this.weaponTotalAttribute.getTen();
		this.critDamage = attributeConfig.getCritDam() + this.weaponTotalAttribute.getCritDam();
		this.totalActionPower = attributeConfig.getCap() + this.weaponTotalAttribute.getCap();
		this.currentActionPower = totalActionPower;
		this.moveCap = attributeConfig.getMoveCap() + this.weaponTotalAttribute.getMoveCap();
		this.priority = attributeConfig.getSpeed() + this.weaponTotalAttribute.getSpeed();
		this.resistance_organic = attributeConfig.getOrg() + this.weaponTotalAttribute.getOrg();
		this.resistance_machinery = attributeConfig.getMac() + this.weaponTotalAttribute.getMac();
		this.resistance_alloy = attributeConfig.getAll() + this.weaponTotalAttribute.getAll();
		this.resistance_fibre = attributeConfig.getFib() + this.weaponTotalAttribute.getFib();
		this.damage_puncture = attributeConfig.getPun() + this.weaponTotalAttribute.getPun();
		this.damage_smash = attributeConfig.getSma() + this.weaponTotalAttribute.getSma();
		this.damage_pound = attributeConfig.getImp() + this.weaponTotalAttribute.getImp();
		this.damage_cut = attributeConfig.getInc() + this.weaponTotalAttribute.getInc();
	}
	public MFightUnit.Builder toProto() {
		MFightUnit.Builder mfightUnit = MFightUnit.newBuilder();
		mfightUnit.setUnitType(this.unitType);
		mfightUnit.setCurrentHp(this.getCurrentHp());
		mfightUnit.setTotalHp(this.getTotalHp());
		mfightUnit.setId(this.id);
		MPathNode.Builder posBuilder = MPathNode.newBuilder();
		posBuilder.setX(this.gridPos.getX());
		posBuilder.setY(this.gridPos.getY());
		mfightUnit.setPos(posBuilder);
		mfightUnit.setCfgId(0);
		mfightUnit.setOldHp(tempCurrentHp);
		mfightUnit.setCurrentActionPower(this.getCurrentActionPower());
		mfightUnit.setTotalActionPower(totalActionPower);
		if(shield != null){
			mfightUnit.setShieldInfo(shield.toProto());
		}
		for(java.util.Map.Entry<Integer, Buff> entry : mutexBuffMap.entrySet()){
			mfightUnit.addBuffInfos(entry.getValue().toProto());
    	}
		
		for(java.util.Map.Entry<Integer, List<Buff>> entry : noneMutexBuffMap.entrySet()){
    		List<Buff> buffList = entry.getValue();
    		for(Buff buff : buffList){
    			mfightUnit.addBuffInfos(buff.toProto());
    		}
    	}
		if(this.getRotation() != null){
			MRotation.Builder rotation = MRotation.newBuilder();
			rotation.setX(this.getRotation().x);
			rotation.setY(this.getRotation().y);
			rotation.setZ(this.getRotation().z);
			mfightUnit.setRotation(rotation);
		}
		
		return mfightUnit;
	}
	
	public void addShield(Shield shield) {
		this.shieldStack.push(shield);
		this.shield = shield;
	}

	public void removeShield() {
		if(shieldStack.size() > 0){
			shieldStack.pop();
		}
		if(shieldStack.size() > 0){
			shield = shieldStack.peek();
		}else{
			shield = null;
		}
	}

	public Fashion getFashion() {
		return fashion;
	}

	public void setFashion(Fashion fashion) {
		this.fashion = fashion;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}