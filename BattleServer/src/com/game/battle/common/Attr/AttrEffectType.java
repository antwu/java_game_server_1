package com.game.battle.common.Attr;

public class AttrEffectType {
	/**
	 * 绝对值
	 */
	//生命
	public static final int hp = 101;
	//攻击
	public static final int att = 102;
	//防御
	public static final int def = 103;
	//暴击
	public static final int crit = 104;
	//韧性
	public static final int ten = 105;
	//暴击伤害
	public static final int critDam = 106;
	//行动力上限
	public static final int cap = 107;
	//单位距离消耗行动力
	public static final int moveCap = 108;
	//先手值
	public static final int speed = 109;
	//有机抗性
	public static final int org = 110;
	//机械抗性
	public static final int mac = 111;
	//合金抗性
	public static final int all = 112;
	//纤维抗性
	public static final int fib = 113;
	//穿刺伤害
	public static final int pun = 114;
	//粉碎伤害
	public static final int sma = 115;
	//冲击伤害
	public static final int imp = 116;
	//切割伤害
	public static final int inc = 117;
	
	/**
	 * 百分百
	 */
	//生命
	public static final int hp_per = 201;
	//攻击
	public static final int att_per = 202;
	//防御
	public static final int def_per = 203;
	//暴击
	public static final int crit_per = 204;
	//韧性
	public static final int ten_per = 205;
	//暴击伤害
	public static final int critDam_per = 206;
	//行动力上限
	public static final int cap_per = 207;
	//单位距离消耗行动力
	public static final int moveCap_per = 208;
	//先手值
	public static final int speed_per = 209;
	//有机抗性
	public static final int org_per = 210;
	//机械抗性
	public static final int mac_per = 211;
	//合金抗性
	public static final int all_per = 212;
	//纤维抗性
	public static final int fib_per = 213;
	//穿刺伤害
	public static final int pun_per = 214;
	//粉碎伤害
	public static final int sma_per = 215;
	//冲击伤害
	public static final int imp_per = 216;
	//切割伤害
	public static final int inc_per = 217;
}
