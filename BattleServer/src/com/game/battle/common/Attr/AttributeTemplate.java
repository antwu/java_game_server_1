package com.game.battle.common.Attr;

import com.game.battle.config.TalentEffectConfig;

public class AttributeTemplate {
		//生命
		private int hp;
		//攻击
		private int att;
		//防御
		private int def;
		//暴击
		private int crit;
		//韧性
		private int ten;
		//暴击伤害
		private int critDam;
		//行动力上限
		private int cap;
		//单位距离消耗行动力
		private int moveCap;
		//先手值
		private int speed;
		//有机抗性
		private int org;
		//机械抗性
		private int mac;
		//合金抗性
		private int all;
		//纤维抗性
		private int fib;
		//穿刺伤害
		private int pun;
		//粉碎伤害
		private int sma;
		//冲击伤害
		private int imp;
		//切割伤害
		private int inc;
		
		public int getHp() {
			return hp;
		}
		public void setHp(int hp) {
			this.hp = hp;
		}
		public int getAtt() {
			return att;
		}
		public void setAtt(int att) {
			this.att = att;
		}
		public int getDef() {
			return def;
		}
		public void setDef(int def) {
			this.def = def;
		}
		public int getCrit() {
			return crit;
		}
		public void setCrit(int crit) {
			this.crit = crit;
		}
		public int getTen() {
			return ten;
		}
		public void setTen(int ten) {
			this.ten = ten;
		}
		public int getCap() {
			return cap;
		}
		public void setCap(int cap) {
			this.cap = cap;
		}
		public int getCritDam() {
			return critDam;
		}
		public void setCritDam(int critDam) {
			this.critDam = critDam;
		}
		public int getMoveCap() {
			return moveCap;
		}
		public void setMoveCap(int moveCap) {
			this.moveCap = moveCap;
		}
		public int getSpeed() {
			return speed;
		}
		public void setSpeed(int speed) {
			this.speed = speed;
		}
		public int getOrg() {
			return org;
		}
		public void setOrg(int org) {
			this.org = org;
		}
		public int getMac() {
			return mac;
		}
		public void setMac(int mac) {
			this.mac = mac;
		}
		public int getAll() {
			return all;
		}
		public void setAll(int all) {
			this.all = all;
		}
		public int getFib() {
			return fib;
		}
		public void setFib(int fib) {
			this.fib = fib;
		}
		public int getPun() {
			return pun;
		}
		public void setPun(int pun) {
			this.pun = pun;
		}
		public int getSma() {
			return sma;
		}
		public void setSma(int sma) {
			this.sma = sma;
		}
		public int getImp() {
			return imp;
		}
		public void setImp(int imp) {
			this.imp = imp;
		}
		public int getInc() {
			return inc;
		}
		public void setInc(int inc) {
			this.inc = inc;
		}
		
		public void addAttribute(TalentEffectConfig talentEffectConfig) {
			this.setHp(hp + talentEffectConfig.getHp());
			this.setAtt(att + talentEffectConfig.getAtt());
			this.setDef(def + talentEffectConfig.getDef());
			this.setCrit(crit + talentEffectConfig.getCrit());
			this.setTen(ten + talentEffectConfig.getTen());
			this.setCritDam(critDam + talentEffectConfig.getCritDam());
			this.setCap(cap + talentEffectConfig.getCap());
			this.setMoveCap(moveCap + talentEffectConfig.getMoveCap());
			this.setSpeed(speed + talentEffectConfig.getSpeed());
			this.setOrg(org + talentEffectConfig.getOrg());
			this.setMac(mac + talentEffectConfig.getMac());
			this.setAll(all + talentEffectConfig.getAll());
			this.setFib(fib + talentEffectConfig.getFib());
			this.setPun(pun + talentEffectConfig.getPun());
			this.setSma(sma + talentEffectConfig.getSma());
			this.setImp(imp + talentEffectConfig.getImp());
			this.setInc(inc + talentEffectConfig.getInc());
		}
		
		public void addAttribute(AttributeTemplate AttributeTemplate) {
			this.setHp(hp + AttributeTemplate.getHp());
			this.setAtt(att + AttributeTemplate.getAtt());
			this.setDef(def + AttributeTemplate.getDef());
			this.setCrit(crit + AttributeTemplate.getCrit());
			this.setTen(ten + AttributeTemplate.getTen());
			this.setCritDam(critDam + AttributeTemplate.getCritDam());
			this.setCap(cap + AttributeTemplate.getCap());
			this.setMoveCap(moveCap + AttributeTemplate.getMoveCap());
			this.setSpeed(speed + AttributeTemplate.getSpeed());
			this.setOrg(org + AttributeTemplate.getOrg());
			this.setMac(mac + AttributeTemplate.getMac());
			this.setAll(all + AttributeTemplate.getAll());
			this.setFib(fib + AttributeTemplate.getFib());
			this.setPun(pun + AttributeTemplate.getPun());
			this.setSma(sma + AttributeTemplate.getSma());
			this.setImp(imp + AttributeTemplate.getImp());
			this.setInc(inc + AttributeTemplate.getInc());
		}
}
