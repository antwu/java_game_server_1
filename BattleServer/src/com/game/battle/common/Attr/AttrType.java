package com.game.battle.common.Attr;

public class AttrType {
	//生命
	public static final int hp = 1;
	//攻击
	public static final int att = 2;
	//防御
	public static final int def = 3;
	//暴击
	public static final int crit = 4;
	//韧性
	public static final int ten = 5;
	//暴击伤害
	public static final int critDam = 6;
	//行动力上限
	public static final int cap = 7;
	//单位距离消耗行动力
	public static final int moveCap = 8;
	//先手值
	public static final int speed = 9;
	//有机抗性
	public static final int org = 10;
	//机械抗性
	public static final int mac = 11;
	//合金抗性
	public static final int all = 12;
	//纤维抗性
	public static final int fib = 13;
	//穿刺伤害
	public static final int pun = 14;
	//粉碎伤害
	public static final int sma = 15;
	//冲击伤害
	public static final int imp = 16;
	//切割伤害
	public static final int inc = 17;
}
