package com.game.battle.common;

public class BattleEnum {
	public static enum EnumSceneUnitType
	{
	    None,
	    // 玩家
	    Player,
	    // 敌人
	    Monster,
	    // 障碍
	    Obstacle
	}
	
	public static enum EnumAttackRangeType
	{
		//直接选点
	    None,
	    // 扇形
	    Arc,

	    // 矩形
	    Rect,

	    // 圆
	    Circle,
	    //子弹
	    Bullet,	
	}
	
	public static enum EnumSkillType
	{
	    None,
	    // 伤害类型
	    Damage,
	    //护盾类型
	    Shield,
	    //治疗类型
	    Heal,
	    //瞬移
	    Teleport,
	    //召唤
	    Summon,
	    //变身
	    Transform,
	    //上buff
	    AddBuff,
	    ;
	}
	
	public static enum DamageAttrType
	{
		 	None,
		 	//穿刺
		 	Puncture,
		 	//粉碎
		 	Smash,
		 	//冲击
		 	Pound,
		    //切割
		 	Cut,
		 	
	}
	
	public static enum ArmorType
	{
		None,
	 	//有机
		Organic,
	 	//机械
		Machinery,
		//合金
		Alloy,
	 	//纤维
		Fibre,
	}
	
	public static enum TalentEffect
	{
		None(0),
		//修正刀剑系别武器伤害
		DaoJianDamage(1),
		//修正枪系别武器伤害
		QiangDamage(2),
		//修正锤系别武器伤害
		ChuiDamage(3),
		//修正匕首系别武器伤害
		BiShouDanage(4),
		//修正盾牌系别转化率
		ShieldConvert(5),
		//修正弩系别武器伤害
		NuDamage(6),
		//修正武器攻击消耗行动力
		WeaponAttactCostPower(7),
		//修正武器移动消耗行动力
		WeaponMoveCostPower(8),
		//修正角色全部伤害
		AllAttackDamage(9),
		//修正角色所承受的伤害
		AllDefenceDamage(10),
		//修正属性相克伤害
		KeZhiDamage(11),
		//修正当前行动力
		CurrentActionPower(12),
		//修正当前生命值
		CurrentHp(13),
		//修正当前能量值
		CurrentPower(14),
		//修正属性被克伤害
		Restrain(15),
		//穿刺
		ChuanCi(16),
		//粉碎
		FenSui(17),
		//冲击
		Chongji(18),
		//切割
		Qiege(19),
		
		
		
		//增加buff
		AddBuff(10000),
		;
		
		private int iNum = 0;  
		  
		private TalentEffect(int iNum) {  
		  this.iNum = iNum;  
		}  
		  
		public int toNumber() {  
		  return this.iNum;  
		}  
	}
}
