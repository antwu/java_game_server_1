package com.game.battle.common;

import com.game.message.proto.battle.BattleProtoBuf.MShieldInfo;

public class Shield implements Cloneable{
	private int totalPower;
	private int currentPower;
	
	public int getTotalPower() {
		return totalPower;
	}
	public void setTotalPower(int totalPower) {
		this.totalPower = totalPower;
	}
	public int getCurrentPower() {
		return currentPower;
	}
	public void setCurrentPower(int currentPower) {
		this.currentPower = currentPower;
	}
	
	 @Override  
    public Shield clone() {  
		 Shield shield = null;  
        try{  
        	shield = (Shield)super.clone();  
        }catch(CloneNotSupportedException e) {  
            e.printStackTrace();  
        }  
        return shield;  
    }  
	public MShieldInfo.Builder toProto() {
		MShieldInfo.Builder res = MShieldInfo.newBuilder();
		res.setTotalPower(this.getTotalPower());
		res.setCurrentPower(this.getCurrentPower());
		return res;
	}
}
