package com.game.battle.common.weapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.game.battle.common.Attr.AttributeTemplate;
import com.game.battle.config.TalentEffectConfig;
import com.game.battle.config.WeaponConfig;
import com.game.battle.config.XMLTemplateService;


public class Weapon implements Cloneable{
	private int cfgId;
	private int mainSkillId;
	//类别（1刀剑2枪3锤4盾5弓6匕首）
	private int weaponType;
	
	public AttributeTemplate weaponAttribute = new AttributeTemplate();
	/**
	 * 所有按攻击次数计算的天赋（触发天赋使用 每个天赋单独记次）
	 */
	private HashMap<Integer, Integer> addTalentMap = new HashMap<>();
	//连击触发的天赋
	private List<Integer> seriTalentIdList = new ArrayList<>();
	//攻击特定buff触发的天赋 key为天赋id value为携带buffid 
	private HashMap<Integer, Integer> attackBuffTalentMap = new HashMap<>();
	//攻击携带护盾目标触发的天赋
	private List<Integer> attackShieldTalentList = new ArrayList<>();
	
	@Override  
	public Weapon clone() { 
		 Weapon weapon = null;
		 try {
			weapon = (Weapon)super.clone();
			HashMap<Integer, Integer> cloneAddTalentMap = new HashMap<>();
			for(Entry<Integer, Integer> entry : addTalentMap.entrySet()){
				cloneAddTalentMap.put(entry.getKey(), entry.getValue());
			}
			weapon.setAddTalentMap(cloneAddTalentMap);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return weapon;
	 }
	/**
	 * 初始化的时候把天赋数据加载进来
	 * @param configId
	 */
	public Weapon(int configId){
		this.cfgId = configId;
		WeaponConfig weaponConfig = XMLTemplateService.weaponConfigMap.get(configId);
		String talents = weaponConfig.getTalents();
		if(talents != null && !"".equals(talents)){
			String[] talentsParam = talents.split("\\|");
			for(int i = 0; i<talentsParam.length;i++){
				int talentId = Integer.valueOf(talentsParam[i]);
				TalentEffectConfig talentConfig = XMLTemplateService.tallentEffectConfigMap.get(talentId);
				if(talentConfig != null){
					String trigger = talentConfig.getTrigger();
					String[] triggerParam = trigger.split(",");
					//"1、当前兵器每X次攻击  2、连击次数达到x  3、攻击携带特定buff的目标时 4攻击携带护盾的目标 5、被攻击x次时"
					int triggerType = Integer.valueOf(triggerParam[0]);
					if(triggerType == 1){
						addTalentMap.put(talentId, 0);
					}else if(triggerType == 2){
						seriTalentIdList.add(talentId);
					}else if(triggerType == 3){
						int buffid = Integer.valueOf(triggerParam[1]);
						attackBuffTalentMap.put(talentId, buffid);
					}else if(triggerType == 4){
						attackShieldTalentList.add(talentId);
					}
					
					// 属性部分
					this.weaponAttribute.addAttribute(talentConfig);
				}
			}
		}
	}

	public Weapon(String talents){
		if(talents != null && !"".equals(talents)){
			String[] talentsParam = talents.split("\\|");
			for(int i = 0; i<talentsParam.length;i++){
				int talentId = Integer.valueOf(talentsParam[i]);
				TalentEffectConfig talentConfig = XMLTemplateService.tallentEffectConfigMap.get(talentId);
				if(talentConfig != null){
					String trigger = talentConfig.getTrigger();
					String[] triggerParam = trigger.split(",");
					//"1、当前兵器每X次攻击  2、连击次数达到x  3、攻击携带特定buff的目标时 4、被攻击x次时"
					int triggerType = Integer.valueOf(triggerParam[0]);
					if(triggerType == 1){
						addTalentMap.put(talentId, 0);
					}else if(triggerType == 2){
						seriTalentIdList.add(talentId);
					}
				}
			}
		}
	}

	public int getCfgId() {
		return cfgId;
	}

	public void setCfgId(int cfgId) {
		this.cfgId = cfgId;
	}
	
	public HashMap<Integer, Integer> getAddTalentMap() {
		return addTalentMap;
	}

	public void setAddTalentMap(HashMap<Integer, Integer> addTalentMap) {
		this.addTalentMap = addTalentMap;
	}
	
	public List<Integer> getSeriTalentIdList() {
		return seriTalentIdList;
	}
	public void setSeriTalentIdList(List<Integer> seriTalentIdList) {
		this.seriTalentIdList = seriTalentIdList;
	}
	public int getMainSkillId() {
		return mainSkillId;
	}
	public void setMainSkillId(int mainSkillId) {
		this.mainSkillId = mainSkillId;
	}
	public int getWeaponType() {
		return weaponType;
	}
	public void setWeaponType(int weaponType) {
		this.weaponType = weaponType;
	}
	
	public HashMap<Integer, Integer> getAttackBuffTalentMap() {
		return attackBuffTalentMap;
	}
	public void setAttackBuffTalentMap(HashMap<Integer, Integer> attackBuffTalentMap) {
		this.attackBuffTalentMap = attackBuffTalentMap;
	}
	public List<Integer> getAttackShieldTalentList() {
		return attackShieldTalentList;
	}
	public void setAttackShieldTalentList(List<Integer> attackShieldTalentList) {
		this.attackShieldTalentList = attackShieldTalentList;
	}
}
