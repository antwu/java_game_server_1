package com.game.battle.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.game.battle.common.map.PathNode;
import com.game.message.proto.battle.BattleProtoBuf.AttackAddEffect;
import com.game.message.proto.battle.BattleProtoBuf.AttackAddEffectUnit;
import com.game.message.proto.battle.BattleProtoBuf.MBattleStep;
import com.game.message.proto.battle.BattleProtoBuf.MPathNode;
import com.game.message.proto.battle.BattleProtoBuf.BattleStepType;

public class BattleStep {
//	public enum BattleStepType{
//		Attack,//攻击
//		Modify,//修正路线
//		Search,//普通寻路
//		UseShield,//释放护盾
//	}
	public BattleStepType stepType;
	public int stepX;
	public int stepY;
	public String account;
	public GridPosition startPos;
	public GridPosition endPos;
	public int targetId;
	public int skillId;
//	public FightSceneUnit originAttacker;
	public FightSceneUnit attacker;
	public int stepNumber = 1;
	public int weaponCfgId = 0;
	public int actionCost = 0;
	public int searchType = 0;
	
	public List<PathNode> searchPathNodes = new ArrayList<>();
	public List<PathNode> teleportPathNodes = new ArrayList<>();
	
	public Map<Integer, FightSceneUnit> effectOrigiFightSceneUnits = new HashMap<Integer, FightSceneUnit>();
	public List<FightSceneUnit> effectUnits = new ArrayList<>();
	public List<FightSceneUnit> summonUnits = new ArrayList<>();
	
	public Map<Integer, List<AttackAddEffect>> attackAddEffectsMap = new HashMap<>();

	public void refreshUnits(List<FightSceneUnit> units) {
		effectUnits.clear();
		for(FightSceneUnit unit : units){
			effectUnits.add(unit.clone());
		}
	}

	public MBattleStep.Builder toProto(String account) {
		MBattleStep.Builder builder = MBattleStep.newBuilder();
		builder.setAccount(account);
		builder.setType(1);
		builder.setMonsterid(targetId);
		builder.setStep(stepNumber);
		for(PathNode node : searchPathNodes){
			MPathNode.Builder mPathNodeBuilder = MPathNode.newBuilder();
			mPathNodeBuilder.setX(node.getX());
			mPathNodeBuilder.setY(node.getY());
			builder.addPathNodes(mPathNodeBuilder);
		}
		for(FightSceneUnit unit : effectUnits){
			builder.addEffectUnits(unit.toProto());
		}
		for(FightSceneUnit unit : summonUnits){
			builder.addSummonUnits(unit.toProto());
		}
		for(PathNode node : teleportPathNodes){
			MPathNode.Builder mPathNodeBuilder = MPathNode.newBuilder();
			mPathNodeBuilder.setX(node.getX());
			mPathNodeBuilder.setY(node.getY());
			builder.addTeleportNodes(mPathNodeBuilder);
		}
		for(Entry<Integer, List<AttackAddEffect>> entry : attackAddEffectsMap.entrySet()){
			AttackAddEffectUnit.Builder attAddEffectUnitBuilder = AttackAddEffectUnit.newBuilder();
			attAddEffectUnitBuilder.setUnitId(entry.getKey());
			for(AttackAddEffect effect : entry.getValue()){
				attAddEffectUnitBuilder.addEffect(effect);
			}
			builder.addAddEffects(attAddEffectUnitBuilder);
		}
		builder.setAttackTarget(targetId);
		builder.setWeaponCfgId(weaponCfgId);
		builder.setStepType(this.stepType);
		return builder;
	}
	
	public MBattleStep.Builder mosterStepToProto(int mosterId) {
		MBattleStep.Builder builder = MBattleStep.newBuilder();
		builder.setMonsterid(mosterId);
		builder.setType(2);
		builder.setSkillId(skillId);
		if(searchPathNodes != null){
			for(PathNode node : searchPathNodes){
				MPathNode.Builder mPathNodeBuilder = MPathNode.newBuilder();
				mPathNodeBuilder.setX(node.getX());
				mPathNodeBuilder.setY(node.getY());
				builder.addPathNodes(mPathNodeBuilder);
			}
		}
		
		for(FightSceneUnit unit : effectUnits){
			builder.addEffectUnits(unit.toProto());
		}
		for(FightSceneUnit unit : summonUnits){
			builder.addSummonUnits(unit.toProto());
		}
		for(PathNode node : teleportPathNodes){
			MPathNode.Builder mPathNodeBuilder = MPathNode.newBuilder();
			mPathNodeBuilder.setX(node.getX());
			mPathNodeBuilder.setY(node.getY());
			builder.addTeleportNodes(mPathNodeBuilder);
		}
		for(Entry<Integer, List<AttackAddEffect>> entry : attackAddEffectsMap.entrySet()){
			AttackAddEffectUnit.Builder attAddEffectUnitBuilder = AttackAddEffectUnit.newBuilder();
			attAddEffectUnitBuilder.setUnitId(entry.getKey());
			for(AttackAddEffect effect : entry.getValue()){
				attAddEffectUnitBuilder.addEffect(effect);
			}
			builder.addAddEffects(attAddEffectUnitBuilder);
		}
		builder.setAttackTarget(targetId);
		return builder;
	}

	public void refreshPathNodes(List<PathNode> nodes) {
		searchPathNodes.clear();
		this.searchPathNodes = nodes;
	}
}
