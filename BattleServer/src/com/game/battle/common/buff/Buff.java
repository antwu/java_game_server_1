package com.game.battle.common.buff;

import java.util.ArrayList;
import java.util.List;

import com.game.battle.config.BuffConfig;
import com.game.message.proto.battle.BattleProtoBuf.MBuffInfo;

/**
 * buff
 * @author game
 *
 */
public class Buff implements Cloneable{
	private int id;
	//buff来源
	private int sourceUnitId;
	private int totalRound;
	private int leftRound;
	private boolean active;
	private List<String> effectList;
	private int group;
	
	public Buff(int sourceUnitId, BuffConfig buffConfig) {
		this.sourceUnitId = sourceUnitId;
		this.id = buffConfig.getId();
		this.totalRound = buffConfig.getTime();
		this.leftRound = this.totalRound;
		this.active = buffConfig.getIsImmediate() == 1 ? true:false ;
		this.group = buffConfig.getGroup();
		effectList = new ArrayList<>();
		String[] effectsStrings = buffConfig.getEffects().split("\\|");
		for(String str : effectsStrings){
			effectList.add(str);
		}
	}
	
	@Override
	public Buff clone(){
		Buff buff = null;
		try {
			buff = (Buff)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return buff;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getSourceUnitId() {
		return sourceUnitId;
	}

	public void setSourceUnitId(int sourceUnitId) {
		this.sourceUnitId = sourceUnitId;
	}
	
	
	public int getTotalRound() {
		return totalRound;
	}

	public void setTotalRound(int totalRound) {
		this.totalRound = totalRound;
	}
	
	public int getLeftRound() {
		return leftRound;
	}

	public void setLeftRound(int leftRound) {
		this.leftRound = leftRound;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public List<String> getEffectList() {
		return effectList;
	}

	public void setEffectList(List<String> effectList) {
		this.effectList = effectList;
	}
	
	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}



	public MBuffInfo.Builder toProto() {
		MBuffInfo.Builder builder = MBuffInfo.newBuilder();
		builder.setId(this.id);
		builder.setLeftRound(this.leftRound);
		return builder;
	}

}
