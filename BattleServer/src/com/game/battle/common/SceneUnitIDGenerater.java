package com.game.battle.common;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class SceneUnitIDGenerater {
	public static ConcurrentHashMap<Integer, AtomicInteger> idGeneratorMap = new ConcurrentHashMap<>();
	public static int getId(Integer unitType){
		AtomicInteger idGenerator;
		if(!idGeneratorMap.containsKey(unitType)){
			idGenerator = new AtomicInteger(0);
			idGeneratorMap.put(unitType, idGenerator);
		}else{
			idGenerator = idGeneratorMap.get(unitType);
		}
			
		
		return idGenerator.incrementAndGet();
	}
}
