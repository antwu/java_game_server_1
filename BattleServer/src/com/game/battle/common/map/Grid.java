package com.game.battle.common.map;

public class Grid {
    public int x;
    public int y;
    public boolean isWall;
    public Position pos;
    
    public Grid(int x,int y,boolean isWall,Position pos){
    	this.x = x;
    	this.y = y;
    	this.isWall = isWall;
    	this.pos = pos;
    }
    
}
