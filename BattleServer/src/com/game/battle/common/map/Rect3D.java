package com.game.battle.common.map;

import com.game.battle.common.math.Vector2;

public class Rect3D
{
    //    D-------C
    //    |       |
    //    |       |
    //    A-------B

    public Vector2 rectA;
    public Vector2 rectB;
    public Vector2 rectC;
    public Vector2 rectD;

    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;

    public Rect3D(Vector2 position, float size)
    {
        rectA = Vector2.add(position,new Vector2(-size / 2f, -size / 2f));
        rectB = Vector2.add(position,new Vector2(size / 2f, -size / 2f));
        rectC = Vector2.add(position,new Vector2(size / 2f, size / 2f));
        rectD = Vector2.add(position,new Vector2(-size / 2f, size / 2f));

        xMin = rectA.x;
        xMax = rectB.x;
        yMin = rectA.y;
        yMax = rectD.y;

    }

    public boolean isInRect(Vector2 position)
    {
        if(position.x <= xMax && position.x >= xMin && position.y <= yMax && position.y >= yMin)
        {
            return true;
        }

        return false;
    }
}