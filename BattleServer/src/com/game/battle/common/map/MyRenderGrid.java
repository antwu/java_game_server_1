package com.game.battle.common.map;

import com.game.battle.common.math.Box2;
import com.game.battle.common.math.Vector2;

public class MyRenderGrid
{
    public int id;

    public int x;
    public int y;

    //public Material mat = null;
    public Rect3D rect;
    public Box2 gridBox2;

    public Vector2 oriPos;

    // 阻挡
    private boolean isObstacle = false;
    // 在路线上
    private boolean isInTrack = false;
    // 影子位置
    //private bool isGhostPoint = false;
    // 命中目标
    private boolean IsInAim = false;

    private boolean IsInMonsterRange = false;
    private boolean IsInSafe = false;

    public MyRenderGrid(int id, int x, int y,Vector2 pos, float size)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        
        oriPos = pos;
        rect = new Rect3D(oriPos, size);

        gridBox2 = new Box2(new Vector2(oriPos.x, oriPos.y), 
                            new Vector2(-1, 0),//right
                            new Vector2(0, -1),//forward
                            new Vector2(size / 2, size / 2));
    }


    public void setObstacle(boolean isObstacle)
    {
        if(this.isObstacle != isObstacle)
        {
            this.isObstacle = isObstacle;
        }
    }

    public void setInTrack(boolean isInTrack)
    {
        if (this.isInTrack != isInTrack)
        {
            this.isInTrack = isInTrack;
        }
    }

    public void setInAim(boolean isInAim)
    {
        if (this.IsInAim != isInAim)
        {
            this.IsInAim = isInAim;
        }
    }

    public void setIsInMonsterRange(boolean isInMonsterRange)
    {
        if(this.IsInMonsterRange != isInMonsterRange)
        {
            this.IsInMonsterRange = isInMonsterRange;
        }
    }

    public void SetIsInSafe(boolean isInSafe)
    {
        if (this.IsInSafe != isInSafe)
        {
            this.IsInSafe = isInSafe;
        }
    }
}