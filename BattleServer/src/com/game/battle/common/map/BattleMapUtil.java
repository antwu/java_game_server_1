package com.game.battle.common.map;

import com.game.battle.config.DungeonConfig;
import com.game.battle.config.map.BattleMapConfig.BattleMapItemConfig;
import com.game.battle.config.map.MapLevelConfig.MapLevelItemConfig;

public class BattleMapUtil {
	public static BattleMap addBattleMap(MapLevelItemConfig mapLevelItemConfig ,BattleMapItemConfig battleMapItemConfig, DungeonConfig dungeonConfig){
		
		BattleMap battleMap = new BattleMap(mapLevelItemConfig ,battleMapItemConfig, dungeonConfig);
		battleMap.updateMapByStage(mapLevelItemConfig.getStap1GridIndex());
		battleMap.updateGridDatasByLevelCfg(1);
		return battleMap;
	}
	
}
