package com.game.battle.common.map;

/**
 * 地图节点
 * @author game
 *
 */
public class PathNode implements Comparable<PathNode>{
	/**
	 * 是否可达
	 */
	private boolean isWall;
	/**
	 * 位置
	 */
	private Position pos;

	// 与起点的长度
	private double gCost;
	// 与目标点的长度
	private double hCost;
	// 总的路径长度
	private double fCost;
	//二维数组坐标
	private int x;
	private int y;
	private PathNode parent;
	public PathNode userContext;
	
	public PathNode(boolean isWall,Position position,int x,int y){
		this.isWall = isWall;
		this.pos = position;
		this.x = x;
		this.y = y;
		
		userContext = this;
	}
	
	public boolean getIsWall() {
		return isWall;
	}
	public void setIsWall(boolean isWall) {
		this.isWall = isWall;
	}
	public Position getPos() {
		return pos;
	}
	public void setPos(Position pos) {
		this.pos = pos;
	}
	public double getgCost() {
		return gCost;
	}
	public void setgCost(double gCost) {
		this.gCost = gCost;
	}
	public double gethCost() {
		return hCost;
	}
	public void sethCost(double hCost) {
		this.hCost = hCost;
	}
	public double getfCost() {
		fCost = gCost + hCost;
		return fCost;
	}
	public void setfCost(double fCost) {
		this.fCost = fCost;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}	
	public PathNode getParent() {
		return parent;
	}

	public void setParent(PathNode parent) {
		this.parent = parent;
	}
	
	@Override
	public int compareTo(PathNode o) {
		if(this.getfCost() < o.getfCost()){
			return -1;
		}
		else if(this.getfCost() > o.getfCost()){
			return 1;
		}
		return 0;
	}
}
