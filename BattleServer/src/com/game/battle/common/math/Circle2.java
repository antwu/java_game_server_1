package com.game.battle.common.math;

public class Circle2 {
	/// <summary>
	/// Circle center
	/// </summary>
	public Vector2 center;

	/// <summary>
	/// Circle radius
	/// </summary>
	public float radius;

	public Circle2(){
		
	}

	/// <summary>
	/// Creates circle from center and radius
	/// </summary>
	public Circle2(Vector2 center, float radius)
	{
		this.center = center;
		this.radius = radius;
	}
}
