package com.game.battle.common.math;
public class Mathfex
{
	/// <summary>
	/// 1e-5f
	/// </summary>
	public static float ZeroTolerance = 1e-5f;

	/// <summary>
	/// -1e-5f
	/// </summary>
	public static float NegativeZeroTolerance = -ZeroTolerance;

	/// <summary>
	/// (1e-5f)^2
	/// </summary>
	public static float ZeroToleranceSqr = ZeroTolerance * ZeroTolerance;

	/// <summary>
	/// π
	/// </summary>
	public static float Pi = (float)Math.PI;

	/// <summary>
	/// π/2
	/// </summary>
	public static float HalfPi = 0.5f * Pi;

	/// <summary>
	/// 2*π
	/// </summary>
	public static float TwoPi = 2f * Pi;
}