package com.game.battle.common.math;

public class Vector2 {
	public float x;
	public float y;
	public float magnitude;

	public static Vector2 zero = new Vector2();
	
	
	public Vector2(){
		this.x = 0;
		this.y = 0;
	}
	
	public Vector2(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * 向量相加
	 * @param vec1
	 * @param vec2
	 * @return
	 */
	public static Vector2 add(Vector2 vec1,Vector2 vec2){
		Vector2 vector2 = new Vector2();
		vector2.x = vec1.x + vec2.x;
		vector2.y = vec1.y + vec2.y;
		
		return vector2;
	}
	
	public float getMagnitude() {
		magnitude = (float)Math.sqrt(this.x*this.x + this.y*this.y);
		return magnitude;
	}

	public void setMagnitude(float magnitude) {
		this.magnitude = magnitude;
	}
	/**
	 * 向量相减
	 * @param vec1
	 * @param vec2
	 * @return
	 */
	public static Vector2 sub(Vector2 vec1,Vector2 vec2){
		Vector2 vector2 = new Vector2();
		vector2.x = vec1.x - vec2.x;
		vector2.y = vec1.y - vec2.y;
		
		return vector2;
	}
	
	public void sub(Vector2 vec2){
		this.x -= vec2.x;
		this.y -= vec2.y;
	}
	
	public Vector2 right(){
		Vector2 right = rotateVectorByXZ(this,90);
		return right;
	}
	
  public static Vector2 rotateVectorByXZ(Vector2 v, float degrees)
    {
        float degToRad = (float)(Math.PI) / 180f;
        float radians = degrees * degToRad;

        float ca = (float)Math.cos(radians);
        float sa = (float)Math.sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);    
    }

	/**
	 * 向量和数字相乘
	 * @param vec1
	 * @param param
	 * @return
	 */
	public static Vector2 multi(Vector2 vec1,float param){
		Vector2 vector2 = new Vector2();
		vector2.x = vec1.x * param;
		vector2.y = vec1.y * param;
		
		return vector2;
	}
	
	/**
	 * 点乘
	 * @param other
	 * @return
	 */
	public float dot(Vector2 other){
		return this.x * other.x + this.y * other.y;
	}
	
	public static float dot(Vector2 v1, Vector2 v2){
		return v1.x * v2.x + v1.y * v2.y;
	}
	
	public float normalize(){
		float length = (float)Math.sqrt(this.x * this.x + this.y * this.y);

		if (length >= Mathfex.ZeroTolerance)
		{
			float invLength = 1f / length;
			this.x *= invLength;
			this.y *= invLength;
			return length;
		}
			
		this.x = 0f;
		this.y = 0f;
		return 0f;
	}
	
	public static Vector2 normalize(Vector2 target){
		float length = (float)Math.sqrt(target.x * target.x + target.y * target.y);
		Vector2 result = new Vector2();
		if (length >= Mathfex.ZeroTolerance)
		{
			float invLength = 1f / length;
			result.x = target.x * invLength;
			result.y = target.y * invLength;
		}
		return result;
	}
	
	/// <summary>
	/// Returns (y,-x)
	/// </summary>
	public static Vector2 perp(Vector2 vector)
	{
		return new Vector2(vector.y, -vector.x);
	}
	
	/// <summary>
	/// Returns (y,-x)
	/// </summary>
	public Vector2 perp()
	{
		return new Vector2(this.y, -this.x);
	}
	
	public float sqrMagnitude(){
		return x*x + y*y;
	}
	
	public void multi(float param){
		this.x *= param;
		this.y *= param;
	}
	
	/**
	 * 计算两个向量的夹角
	 * @param normalize
	 * @param forward
	 * @return
	 */
	public static float angle(Vector2 vec1, Vector2 vec2) {
		
		float dot = Vector2.dot(vec1,vec2);
		if(dot<0){
			dot  = Math.max(-0.99999f, dot);
		}else if(dot > 0){
			dot = Math.min(0.99999f, dot);
		}
		dot = dot/(vec1.getMagnitude()*vec2.getMagnitude());
		float acos = (float)Math.acos(dot);
		float angle = (float)(acos*180/Math.PI);
		return angle;
	}
}
