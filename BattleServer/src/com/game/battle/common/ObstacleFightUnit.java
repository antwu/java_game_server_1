package com.game.battle.common;

import com.game.battle.common.BattleEnum.EnumSceneUnitType;
import com.game.battle.common.map.BattleMap.Rotation;
import com.game.battle.config.ObstacleConfig;
import com.game.battle.config.XMLTemplateService;
import com.game.message.proto.battle.BattleProtoBuf.MFightUnit;
import com.game.message.proto.battle.BattleProtoBuf.MPathNode;
import com.game.message.proto.battle.BattleProtoBuf.MRotation;


public class ObstacleFightUnit extends FightSceneUnit
{
	private int cfgId = 0;
	private int isNotCanAtkObs;//1是不可被攻击
	//伤害减免
	private int damageDerate;
	public ObstacleFightUnit(int cfgID, int nodeX, int nodeY, int isNotCanAtkObs, Rotation rotation) {
		this.id = SceneUnitIDGenerater.getId(EnumSceneUnitType.Monster.ordinal()+EnumSceneUnitType.Obstacle.ordinal());
    	cfgId = cfgID;
    	this.level = 1;
    	this.isNotCanAtkObs = isNotCanAtkObs;
    	this.setRotation(rotation);
    	this.initAttr();
    	this.unitType = EnumSceneUnitType.Obstacle.ordinal();
    	gridPos = new GridPosition(nodeX,nodeY);
	}
	
	private void initAttr() {
		ObstacleConfig obstacleConfig = XMLTemplateService.obstacleConfigMap.get(cfgId);
		this.totalHp = obstacleConfig.getHp();
		this.currentHp = this.totalHp;
		this.tempCurrentHp = currentHp;
	}

	public int getCfgId() {
		return cfgId;
	}

	public void setCfgId(int cfgId) {
		this.cfgId = cfgId;
	}

	public int getDamageDerate() {
		return damageDerate;
	}

	public void setDamageDerate(int damageDerate) {
		this.damageDerate = damageDerate;
	}
	
	public int getIsNotCanAtkObs() {
		return isNotCanAtkObs;
	}

	public void setIsNotCanAtkObs(int isNotCanAtkObs) {
		this.isNotCanAtkObs = isNotCanAtkObs;
	}

	public MFightUnit.Builder toProto() {
		MFightUnit.Builder mfightUnit = MFightUnit.newBuilder();
		mfightUnit.setUnitType(this.unitType);
		mfightUnit.setCurrentHp(this.currentHp);
		mfightUnit.setTotalHp(this.totalHp);
		mfightUnit.setId(this.id);
		MPathNode.Builder posBuilder = MPathNode.newBuilder();
		posBuilder.setX(this.gridPos.getX());
		posBuilder.setY(this.gridPos.getY());
		mfightUnit.setPos(posBuilder);
		mfightUnit.setCfgId(this.cfgId);
		mfightUnit.setOldHp(tempCurrentHp);
		if(this.getRotation() != null){
			MRotation.Builder rotation = MRotation.newBuilder();
			rotation.setX(this.getRotation().x);
			rotation.setY(this.getRotation().y);
			rotation.setZ(this.getRotation().z);
			mfightUnit.setRotation(rotation);
		}
		return mfightUnit;
	}
   
}