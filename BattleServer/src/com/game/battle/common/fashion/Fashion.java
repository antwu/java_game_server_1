package com.game.battle.common.fashion;

import com.game.message.proto.weapon.WeaponProtoBuf.FashionInfo;

public class Fashion {
	
	private String ownerId;
	
	private int fashionId;
	
	private int unlocked;
	
	private int skinColour;
	
	private int hairColour;
	
	private int mainbodyColour;
	
	private int decorateColour;
	
	public Fashion(FashionInfo fashionInfo) {
		this.fashionId = fashionInfo.getFashionId();
		this.ownerId = fashionInfo.getOwnerId();
		this.unlocked = fashionInfo.getUnlocked();
		this.skinColour = fashionInfo.getSkinColour();
		this.hairColour = fashionInfo.getHairColour();
		this.mainbodyColour = fashionInfo.getMainbodyColour();
		this.decorateColour = fashionInfo.getDecorateColour();
	}
	
	public FashionInfo.Builder toProto() {
		FashionInfo.Builder fashionInfoBuilder = FashionInfo.newBuilder();
		fashionInfoBuilder.setOwnerId(this.getOwnerId());
		fashionInfoBuilder.setFashionId(this.getFashionId());
		fashionInfoBuilder.setUnlocked(this.getUnlocked());
		fashionInfoBuilder.setSkinColour(this.getSkinColour());
		fashionInfoBuilder.setHairColour(this.getHairColour());
		fashionInfoBuilder.setMainbodyColour(this.getMainbodyColour());
		fashionInfoBuilder.setDecorateColour(this.getDecorateColour());
		return fashionInfoBuilder;
	}


	public int getFashionId() {
		return fashionId;
	}

	public void setFashionId(int fashionId) {
		this.fashionId = fashionId;
	}

	public int getSkinColour() {
		return skinColour;
	}

	public void setSkinColour(int skinColour) {
		this.skinColour = skinColour;
	}

	public int getHairColour() {
		return hairColour;
	}

	public void setHairColour(int hairColour) {
		this.hairColour = hairColour;
	}

	public int getMainbodyColour() {
		return mainbodyColour;
	}

	public void setMainbodyColour(int mainbodyColour) {
		this.mainbodyColour = mainbodyColour;
	}

	public int getDecorateColour() {
		return decorateColour;
	}

	public void setDecorateColour(int decorateColour) {
		this.decorateColour = decorateColour;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public int getUnlocked() {
		return unlocked;
	}

	public void setUnlocked(int unlocked) {
		this.unlocked = unlocked;
	}
}
