package com.game.battle.common;

import com.game.battle.common.map.BattleMap;
import com.game.battle.common.map.BattleMapUtil;
import com.game.battle.config.DungeonConfig;
import com.game.battle.config.XMLTemplateService;
import com.game.battle.config.map.MapLevelConfig.MapLevelItemConfig;


public class BattleHelper {
	public BattleMap battleMap;
	public BattleHelper(int levelId){
		MapLevelItemConfig mapLevelItemConfig = XMLTemplateService.mapLevelConfig.levels.get(levelId);
		DungeonConfig dungeonConfig = XMLTemplateService.dungeonConfigMap.get(levelId);
		battleMap = BattleMapUtil.addBattleMap(mapLevelItemConfig,XMLTemplateService.battleMapConfig.maps.get(mapLevelItemConfig.getSceneID()), dungeonConfig);
//		battleMap.initGridDatasByLevelCfg(levelId);
	}

}
