package com.game.battle.common;

import java.util.ArrayList;
import java.util.List;

import com.game.battle.common.BattleEnum.EnumSceneUnitType;
import com.game.battle.common.ai.MonsterAI;
import com.game.battle.common.buff.Buff;
import com.game.battle.common.map.BattleMap.Rotation;
import com.game.battle.config.MonsterConfig;
import com.game.battle.config.XMLTemplateService;
import com.game.message.proto.battle.BattleProtoBuf.MFightUnit;
import com.game.message.proto.battle.BattleProtoBuf.MPathNode;
import com.game.message.proto.battle.BattleProtoBuf.MRotation;


public class MonsterFightUnit extends FightSceneUnit
{
	private int cfgId = 0;
	//伤害减免
	private int damageDerate;
	//暴击率
	private int critRate;
	//抗暴率
	private int critDerate;
	
	//视野
	private int scanRange;

	//AI
//	private List<Integer> aiList;
	private List<MonsterAI> aiList;
	
	private boolean isTransformed;
	private int transformRound;

	//暴击伤害倍率
	private float critMulti;
	//是否为激活状态
	private boolean active;
	/**
	 * 怪物在哪一回合激活的
	 */
	private int activeRound;
	
	private String drops;
	
	public MonsterFightUnit(int cfgID, int nodeX, int nodeY, Rotation rotation) {
		this.id = SceneUnitIDGenerater.getId(EnumSceneUnitType.Monster.ordinal()+EnumSceneUnitType.Obstacle.ordinal());
    	cfgId = cfgID;
    	this.unitType = EnumSceneUnitType.Monster.ordinal();
    	this.level = 1;
    	this.initAttr();
    	this.setRotation(rotation);
    	gridPos = new GridPosition(nodeX,nodeY);
	}
	
	private void initAttr() {
		MonsterConfig monsterConfig = XMLTemplateService.monsterConfigMap.get(cfgId);
		this.aiList = new ArrayList<>();
		for(Integer aiId : monsterConfig.getAiList()){
			MonsterAI monsterAI = new MonsterAI(aiId);
			this.aiList.add(monsterAI) ;
		}
		
		this.active = false;
		this.totalHp = monsterConfig.getHp();
		this.currentHp = this.totalHp;
		this.tempCurrentHp = currentHp;
		this.attack = monsterConfig.getAtt();
		this.defence = monsterConfig.getDef();
		this.crit = monsterConfig.getCrit();
		this.totalActionPower = monsterConfig.getCap();
		this.currentActionPower = totalActionPower;
		this.moveCap = monsterConfig.getMoveCap();
		this.armorType = monsterConfig.getArmorType();
		this.scanRange = monsterConfig.getScanRange();
		this.drops = monsterConfig.getDrops();
		
		if(monsterConfig.getShield() > 0){
			Shield shield = new Shield();
			int power = monsterConfig.getShield();
			shield.setTotalPower(power);
			shield.setCurrentPower(power);
			setShield(shield);
		}
	}
	
	public void updateAI(){
		this.aiList.clear();
		MonsterConfig monsterConfig = XMLTemplateService.monsterConfigMap.get(cfgId);
		this.aiList = new ArrayList<>();
		for(Integer aiId : monsterConfig.getAiList()){
			MonsterAI monsterAI = new MonsterAI(aiId);
			this.aiList.add(monsterAI) ;
		}
	}
	public int getCfgId() {
		return cfgId;
	}

	public void setCfgId(int cfgId) {
		this.cfgId = cfgId;
	}

	public int getDamageDerate() {
		return damageDerate;
	}

	public void setDamageDerate(int damageDerate) {
		this.damageDerate = damageDerate;
	}

	public int getCritRate() {
		return critRate;
	}

	public void setCritRate(int critRate) {
		this.critRate = critRate;
	}

	public int getCritDerate() {
		return critDerate;
	}

	public void setCritDerate(int critDerate) {
		this.critDerate = critDerate;
	}

	public float getCritMulti() {
		return critMulti;
	}

	public void setCritMulti(float critMulti) {
		this.critMulti = critMulti;
	}
	
//	public List<Integer> getAiList() {
//		return aiList;
//	}
//
//	public void setAiList(List<Integer> aiList) {
//		this.aiList = aiList;
//	}
	
	public List<MonsterAI> getAiList() {
		return aiList;
	}

	public void setAiList(List<MonsterAI> aiList) {
		this.aiList = aiList;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public int getActiveRound() {
		return activeRound;
	}

	public void setActiveRound(int activeRound) {
		this.activeRound = activeRound;
	}
	
	public int getScanRange() {
		return scanRange;
	}

	public void setScanRange(int scanRange) {
		this.scanRange = scanRange;
	}
	
	public int getTransformRound() {
		return transformRound;
	}

	public void setTransformRound(int transformRound) {
		this.transformRound = transformRound;
	}

	public boolean isTransformed() {
		return isTransformed;
	}

	public void setTransformed(boolean isTransformed) {
		this.isTransformed = isTransformed;
	}
	
	public String getDrops() {
		return drops;
	}

	public void setDrops(String drops) {
		this.drops = drops;
	}
	
	public MFightUnit.Builder toProto() {
		MFightUnit.Builder mfightUnit = MFightUnit.newBuilder();
		mfightUnit.setUnitType(this.unitType);
		mfightUnit.setCurrentHp(this.getCurrentHp());
		mfightUnit.setTotalHp(this.getTotalHp());
		mfightUnit.setId(this.id);
		MPathNode.Builder posBuilder = MPathNode.newBuilder();
		posBuilder.setX(this.gridPos.getX());
		posBuilder.setY(this.gridPos.getY());
		mfightUnit.setPos(posBuilder);
		mfightUnit.setCfgId(this.cfgId);
		mfightUnit.setOldHp(tempCurrentHp);
		mfightUnit.setCurrentActionPower(this.getCurrentActionPower());
		mfightUnit.setTotalActionPower(totalActionPower);
		if(shield != null){
			mfightUnit.setShieldInfo(shield.toProto());
		}
		for(java.util.Map.Entry<Integer, Buff> entry : mutexBuffMap.entrySet()){
			mfightUnit.addBuffInfos(entry.getValue().toProto());
    	}
		
		for(java.util.Map.Entry<Integer, List<Buff>> entry : noneMutexBuffMap.entrySet()){
    		List<Buff> buffList = entry.getValue();
    		for(Buff buff : buffList){
    			mfightUnit.addBuffInfos(buff.toProto());
    		}
    	}
		if(this.getRotation() != null){
			MRotation.Builder rotation = MRotation.newBuilder();
			rotation.setX(this.getRotation().x);
			rotation.setY(this.getRotation().y);
			rotation.setZ(this.getRotation().z);
			mfightUnit.setRotation(rotation);
		}
		return mfightUnit;
	}
   
}