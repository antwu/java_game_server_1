/**
 * WeaponEntity.java
 *
 *
 * $LastChangedBy:  $
 * $LastChangedDate:  $
 * $Revision:  $
 */
package com.game.battle.common.item;

import java.util.HashMap;
import java.util.Map.Entry;

import com.game.dbpersistence.game.entity.ItemEntity;
import com.game.message.proto.battle.BattleProtoBuf.MDropItem;

public class DropItem {
	/**
	 * 在这里的ID是指战斗中生成怪物的ID *
	 */
	private int monsterId;
	/**
	 * 道具掉落所属的玩家的UID *
	 */
	private HashMap <Integer, ItemEntity> itemInfos;

	public int getMonsterId() {
		return monsterId;
	}

	public void setMonsterId(int monsterId) {
		this.monsterId = monsterId;
	}
	
	public HashMap <Integer, ItemEntity> getItemInfos() {
		return itemInfos;
	}

	public void setItemInfos(HashMap <Integer, ItemEntity> itemInfos) {
		this.itemInfos = itemInfos;
	}

	public void addItemInfo(ItemEntity itemEntity) {
		ItemEntity item = this.itemInfos.get(itemEntity.getItemId());
		if(item == null) {
			this.itemInfos.put(itemEntity.getItemId(), itemEntity);
		}else {
			item.setCount(item.getCount() + itemEntity.getCount());
			this.itemInfos.put(itemEntity.getItemId(), itemEntity);
		}
	}
	
	public DropItem() {
		this.monsterId = 0;
		this.itemInfos = new HashMap<>();
	}
	
	public DropItem(int monsterId) {
		this.monsterId = monsterId;
		this.itemInfos = new HashMap<>();
	}

	/**
	 * Returns the proto value of the object
	 * @return
	 */
	public MDropItem.Builder toProto () {
		MDropItem.Builder builder = MDropItem.newBuilder();
		builder.setMonsterId(this.getMonsterId());
		for(Entry<Integer, ItemEntity> entry : itemInfos.entrySet()) {
			builder.addItemInfos(entry.getValue().toProto());
		}
		return builder;
	}


}
