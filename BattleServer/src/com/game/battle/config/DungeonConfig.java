package com.game.battle.config;

public class DungeonConfig {
	private int id;//关卡id
	private int chapter;//关卡章节
	private int option;//关卡难度(1简单2普通3困难4噩梦）
	private String name;//关卡名称
	private String condition;//评价条件
	private String win;//胜利条件
	private String fail;//失败条件
	private String reward;//关卡奖励

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getChapter() {
		return chapter;
	}
	public void setChapter(int chapter) {
		this.chapter = chapter;
	}
	public int getOption() {
		return option;
	}
	public void setOption(int option) {
		this.option = option;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getWin() {
		return win;
	}
	public void setWin(String win) {
		this.win = win;
	}
	public String getFail() {
		return fail;
	}
	public void setFail(String fail) {
		this.fail = fail;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}

}
