package com.game.battle.config;

public class AttributeNumberConfig {
	
	private int id;
	private int defRate;
	private int defWeight;
	private int defLevelRate;
	private int defBound;
	private int critRate;
	private int critRateWeight;
	private int tenRate;
	private int tenRateWeight;
	private int critDamRate;
	private int critDamWeight;
	//暴击等级系数
	private int critDamLevelRate;
	private int critRateMin;
	private int critRateBound;
	private int restrainRate;
	private int restrainedRate;
	private float defIngore;
	private float tenIngore;
	private float shieldAttackRate;
	private float shieldDefenceRate;
	private int shieldRate;
	private int shieldRateWeight;
	private int shieldRateMax;
	private int restraintRateMax;
	//暴击等级常数
	private int critLevelRate;
	//韧性等级系数
	private int tenLevelRate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDefRate() {
		return defRate;
	}
	public void setDefRate(int defRate) {
		this.defRate = defRate;
	}
	public int getDefWeight() {
		return defWeight;
	}
	public void setDefWeight(int defWeight) {
		this.defWeight = defWeight;
	}
	public int getDefLevelRate() {
		return defLevelRate;
	}
	public void setDefLevelRate(int defLevelRate) {
		this.defLevelRate = defLevelRate;
	}
	public int getDefBound() {
		return defBound;
	}
	public void setDefBound(int defBound) {
		this.defBound = defBound;
	}
	public int getCritRate() {
		return critRate;
	}
	public void setCritRate(int critRate) {
		this.critRate = critRate;
	}
	public int getCritRateWeight() {
		return critRateWeight;
	}
	public void setCritRateWeight(int critRateWeight) {
		this.critRateWeight = critRateWeight;
	}
	public int getTenRate() {
		return tenRate;
	}
	public void setTenRate(int tenRate) {
		this.tenRate = tenRate;
	}
	public int getTenRateWeight() {
		return tenRateWeight;
	}
	public void setTenRateWeight(int tenRateWeight) {
		this.tenRateWeight = tenRateWeight;
	}
	public int getCritDamRate() {
		return critDamRate;
	}
	public void setCritDamRate(int critDamRate) {
		this.critDamRate = critDamRate;
	}
	public int getCritDamWeight() {
		return critDamWeight;
	}
	public void setCritDamWeight(int critDamWeight) {
		this.critDamWeight = critDamWeight;
	}
	public int getCritDamLevelRate() {
		return critDamLevelRate;
	}
	public void setCritDamLevelRate(int critDamLevelRate) {
		this.critDamLevelRate = critDamLevelRate;
	}
	public int getCritRateMin() {
		return critRateMin;
	}
	public void setCritRateMin(int critRateMin) {
		this.critRateMin = critRateMin;
	}
	public int getCritRateBound() {
		return critRateBound;
	}
	public void setCritRateBound(int critRateBound) {
		this.critRateBound = critRateBound;
	}
	public int getRestrainRate() {
		return restrainRate;
	}
	public void setRestrainRate(int restrainRate) {
		this.restrainRate = restrainRate;
	}
	public int getRestrainedRate() {
		return restrainedRate;
	}
	public void setRestrainedRate(int restrainedRate) {
		this.restrainedRate = restrainedRate;
	}
	public float getDefIngore() {
		return defIngore;
	}
	public void setDefIngore(float defIngore) {
		this.defIngore = defIngore;
	}
	public float getTenIngore() {
		return tenIngore;
	}
	public void setTenIngore(float tenIngore) {
		this.tenIngore = tenIngore;
	}
	public float getShieldAttackRate() {
		return shieldAttackRate;
	}
	public void setShieldAttackRate(float shieldAttackRate) {
		this.shieldAttackRate = shieldAttackRate;
	}
	public float getShieldDefenceRate() {
		return shieldDefenceRate;
	}
	public void setShieldDefenceRate(float shieldDefenceRate) {
		this.shieldDefenceRate = shieldDefenceRate;
	}
	public int getShieldRate() {
		return shieldRate;
	}
	public void setShieldRate(int shieldRate) {
		this.shieldRate = shieldRate;
	}
	public int getShieldRateWeight() {
		return shieldRateWeight;
	}
	public void setShieldRateWeight(int shieldRateWeight) {
		this.shieldRateWeight = shieldRateWeight;
	}
	public int getShieldRateMax() {
		return shieldRateMax;
	}
	public void setShieldRateMax(int shieldRateMax) {
		this.shieldRateMax = shieldRateMax;
	}
	public int getRestraintRateMax() {
		return restraintRateMax;
	}
	public void setRestraintRateMax(int restraintRateMax) {
		this.restraintRateMax = restraintRateMax;
	}
	public int getCritLevelRate() {
		return critLevelRate;
	}
	public void setCritLevelRate(int critLevelRate) {
		this.critLevelRate = critLevelRate;
	}
	
	public int getTenLevelRate() {
		return tenLevelRate;
	}
	public void setTenLevelRate(int tenLevelRate) {
		this.tenLevelRate = tenLevelRate;
	}
}
