package com.game.battle.config;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.battle.common.map.BattleMap.LevelStepInfos;
import com.game.battle.config.map.BattleMapConfig;
import com.game.battle.config.map.BattleMapConfig.BattleMapItemConfig;
import com.game.battle.config.map.MapLevelConfig;
import com.game.battle.config.map.MapLevelConfig.MapLevelItemConfig;

public class XMLTemplateService {

	static private Logger logger = LoggerFactory.getLogger(XMLTemplateService.class);
//	private static HashMap<String, Integer> xmlLoadTime = new HashMap<String, Integer>();
//	private static HashMap<String, String> configMd5 = new HashMap<String, String>();
	
	public static BattleMapConfig battleMapConfig = new BattleMapConfig();
	public static MapLevelConfig mapLevelConfig = new MapLevelConfig();
	public static ConcurrentHashMap<Integer, SkillConfig> skillConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, AttributeConfig> attributeConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, MonsterConfig> monsterConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, ObstacleConfig> obstacleConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, AiConfig> aiConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, AiActionConfig> aiActionConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, WeaponConfig> weaponConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, BuffConfig> buffConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, CounterConfig> counterConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, TalentEffectConfig> tallentEffectConfigMap = new ConcurrentHashMap<>();

	public static ConcurrentHashMap<Integer, DropItemConfig> dropItemConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, DungeonConfig> dungeonConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, DungeonConditionConfig> dungeonConditionConfigMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, AttributeNumberConfig> attributeNumberConfigMap = new ConcurrentHashMap<>();
	
	public static void initTemplateData() throws Exception {
		XMLConfiguration.setDefaultListDelimiter(' ');
		
		//加载地图配置
		loadBattleMapConfig();
		//加载关卡设置
		loadLevelConfig();
		//加载技能设置
		loadSkillConfig();
		//加载属性配置
		loadAttributeConfig();
		//加载怪物配置
		loadMonsterConfig();
		//加载障碍物配置
		loadObstacleConfig();
		//加载ai配置
		loadAiConfig();
		//加载ai行为配置
		loadAiActionConfig();
		//加载武器配置
		loadWeaponConfig();
		//加载buff配置
		loadBuffConfig();
		//加载属性克制表
		loadCounterConfig();
		//加载天赋效果表
		loadTalentEffectConfig();
		//加载物品掉落表
		loadDropItemConfig();

		//加载关卡表
		loadDungeonConfig();
		//加载关卡条件表
		loadConditionConfig();
		
		loadAttributeNumberConfig();
	}

	/**
	 * 加载天赋效果表
	 */
	private static void loadTalentEffectConfig() throws Exception{
		String talentEffectConfPath =System.getProperty("user.dir")+ "/xml/talenteffect.xml";
		logger.info("load counter config file path :{}", talentEffectConfPath);
		XMLConfiguration conf = new XMLConfiguration(talentEffectConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				TalentEffectConfig talentEffectConfig = new TalentEffectConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				String trigger = sub.getString("[@trigger]");
				String effectid = sub.getString("[@effectid]");
				int hp = sub.getInt("[@hp]");
				int att = sub.getInt("[@att]");
				int def = sub.getInt("[@def]");
				int crit = sub.getInt("[@crit]");
				int ten = sub.getInt("[@ten]");
				int critDam = sub.getInt("[@critDam]");
				int cap = sub.getInt("[@cap]");
				int moveCap = sub.getInt("[@moveCap]");
				int speed = sub.getInt("[@speed]");
				int org = sub.getInt("[@org]");
				int mac = sub.getInt("[@mac]");
				int all = sub.getInt("[@all]");
				int fib = sub.getInt("[@fib]");
				int pun = sub.getInt("[@pun]");
				int sma = sub.getInt("[@sma]");
				int imp = sub.getInt("[@imp]");
				int inc = sub.getInt("[@inc]");
				
				talentEffectConfig.setId(id);
				talentEffectConfig.setTrigger(trigger);
				talentEffectConfig.setEffectid(effectid);
				talentEffectConfig.setHp(hp);
				talentEffectConfig.setAtt(att);
				talentEffectConfig.setDef(def);
				talentEffectConfig.setCrit(crit);
				talentEffectConfig.setTen(ten);
				talentEffectConfig.setCritDam(critDam);
				talentEffectConfig.setCap(cap);
				talentEffectConfig.setMoveCap(moveCap);
				talentEffectConfig.setSpeed(speed);
				talentEffectConfig.setOrg(org);
				talentEffectConfig.setMac(mac);
				talentEffectConfig.setAll(all);
				talentEffectConfig.setFib(fib);
				talentEffectConfig.setPun(pun);
				talentEffectConfig.setSma(sma);
				talentEffectConfig.setImp(imp);
				talentEffectConfig.setInc(inc);
				
				tallentEffectConfigMap.put(id, talentEffectConfig);
			}
		}
	}

	/**
	 * 加载属性克制表
	 */
	private static void loadCounterConfig() throws Exception{
		String counterConfPath =System.getProperty("user.dir")+ "/xml/counter.xml";
		logger.info("load counter config file path :{}", counterConfPath);
		XMLConfiguration conf = new XMLConfiguration(counterConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				CounterConfig counterConfig = new CounterConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				float number = sub.getFloat("[@number]");
				
				
				counterConfig.setId(id);
				counterConfig.setNumber(number);
				
				counterConfigMap.put(id, counterConfig);
			}
		}
	}

	/**
	 * 加载buff配置文件
	 */
	private static void loadBuffConfig() throws Exception{
		String buffConfPath =System.getProperty("user.dir")+ "/xml/buff.xml";
		logger.info("load buff config file path :{}", buffConfPath);
		XMLConfiguration conf = new XMLConfiguration(buffConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				BuffConfig buffConfig = new BuffConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int group = sub.getInt("[@group]");
				int mutex = sub.getInt("[@mutex]");
				int kind = sub.getInt("[@kind]"); 
				int isImmediate = sub.getInt("[@isImmediate]"); 
				int isHint = sub.getInt("[@isHint]"); 
				int isCount = sub.getInt("[@isCount]"); 
				int isWeaponLimit = sub.getInt("[@isWeaponLimit]"); 
				int time = sub.getInt("[@time]"); 
				int useNumber = sub.getInt("[@useNumber]");
				String effects = sub.getString("[@effects]");
				int dieCancle = sub.getInt("[@dieCancle]"); 
				int isShow = sub.getInt("[@isShow]"); 
				
				
				buffConfig.setId(id);
				buffConfig.setGroup(group);
				buffConfig.setMutex(mutex);
				buffConfig.setKind(kind);
				buffConfig.setIsImmediate(isImmediate);
				buffConfig.setIsHint(isHint);
				buffConfig.setIsCount(isCount);
				buffConfig.setIsWeaponLimit(isWeaponLimit);
				buffConfig.setTime(time);
				buffConfig.setUseNumber(useNumber);
				buffConfig.setEffects(effects);
				buffConfig.setDieCancle(dieCancle);
				buffConfig.setIsShow(isShow);
				
				
				buffConfigMap.put(id, buffConfig);
			}
		}
	}

	/**
	 * 加载武器配置
	 */
	private static void loadWeaponConfig() throws Exception{
		String weaponConfPath =System.getProperty("user.dir")+ "/xml/weapon.xml";
		logger.info("load aiAction config file path :{}", weaponConfPath);
		XMLConfiguration conf = new XMLConfiguration(weaponConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				WeaponConfig weaponConfig = new WeaponConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int mainSkillId = sub.getInt("[@MainSkill]");
				int weaponType = sub.getInt("[@WeaponType]");
				String talents = sub.getString("[@Talent]");
				
				weaponConfig.setId(id);
				weaponConfig.setMainSkillId(mainSkillId);
				weaponConfig.setWeaponType(weaponType);
				weaponConfig.setTalents(talents);
				
				weaponConfigMap.put(id, weaponConfig);
			}
		}
	}

	/**
	 * 加载ai行为配置
	 */
	private static void loadAiActionConfig() throws Exception{
		String aiActionConfPath =System.getProperty("user.dir")+ "/xml/aiAction.xml";
		logger.info("load aiAction config file path :{}", aiActionConfPath);
		XMLConfiguration conf = new XMLConfiguration(aiActionConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				AiActionConfig aiActionConfig = new AiActionConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				String target = sub.getString("[@target]");
				String action = sub.getString("[@action]");
				
				aiActionConfig.setId(id);
				aiActionConfig.setTarget(target);
				aiActionConfig.setAction(action);
				
				aiActionConfigMap.put(id, aiActionConfig);
			}
		}
	}

	/**
	 * 加载ai配置
	 */
	private static void loadAiConfig() throws Exception{
		String aiConfPath =System.getProperty("user.dir")+ "/xml/aiConfig.xml";
		logger.info("load ai config file path :{}", aiConfPath);
		XMLConfiguration conf = new XMLConfiguration(aiConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				AiConfig aiConfig = new AiConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				String events = sub.getString("[@events]");
				int actionType = sub.getInt("[@actionType]");
				String actionIds = sub.getString("[@actionId]");
				String actionRates = sub.getString("[@actionProb]");
				int startFireRound = sub.getInt("[@startFireRound]");
				int intervalRound = sub.getInt("[@intervalRound]");
				int useCount = sub.getInt("[@useCount]");
				
				aiConfig.setId(id);
				aiConfig.setEvents(events);
				aiConfig.setActionType(actionType);
				aiConfig.setActionIds(actionIds);
				aiConfig.setActionRates(actionRates);
				aiConfig.setStartFireRound(startFireRound);
				aiConfig.setIntervalRound(intervalRound);
				aiConfig.setUseCount(useCount);
				
				aiConfigMap.put(id, aiConfig);
			}
		}
	}

	/**
	 * 加载怪物配置
	 */
	private static void loadMonsterConfig() throws Exception{
		String monsterConfPath =System.getProperty("user.dir")+ "/xml/monster.xml";
		logger.info("load monster config file path :{}", monsterConfPath);
		XMLConfiguration conf = new XMLConfiguration(monsterConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				MonsterConfig monsterConfig = new MonsterConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int hp = sub.getInt("[@hp]");
				int lv = sub.getInt("[@level]");
				int att = sub.getInt("[@attack]");
				int def = sub.getInt("[@defence]");
				int crit = sub.getInt("[@crit]");
				int cap = sub.getInt("[@maxPower]");
				int moveCap = sub.getInt("[@moveConsume]");
				int shield = sub.getInt("[@shield]");
				String aiList = sub.getString("[@aiList]");
				int armorType = sub.getInt("[@armorType]");
				int scanRange = sub.getInt("[@scanRange]");
				String drops = sub.getString("[@drop]");
				
				monsterConfig.setId(id);
				monsterConfig.setHp(hp);
				monsterConfig.setLv(lv);
				monsterConfig.setAtt(att);
				monsterConfig.setDef(def);
				monsterConfig.setCrit(crit);
				monsterConfig.setCap(cap);
				monsterConfig.setAi(aiList);
				monsterConfig.setMoveCap(moveCap);
				monsterConfig.setShield(shield);
				monsterConfig.setArmorType(armorType);
				monsterConfig.setScanRange(scanRange);
				monsterConfig.setDrops(drops);
				
				monsterConfigMap.put(id, monsterConfig);
			}
		}
	}
	
	
	private static void loadObstacleConfig() throws Exception{
		String obstacleConfPath =System.getProperty("user.dir")+ "/xml/obstacle.xml";
		logger.info("load obstacle config file path :{}", obstacleConfPath);
		XMLConfiguration conf = new XMLConfiguration(obstacleConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				ObstacleConfig obstacleConfig = new ObstacleConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int hp = sub.getInt("[@maxHp]");
				
				
				obstacleConfig.setId(id);
				obstacleConfig.setHp(hp);
				
				obstacleConfigMap.put(id, obstacleConfig);
			}
		}
	}

	/**
	 * 加载属性配置
	 */
	private static void loadAttributeConfig() throws Exception{
		String atrributeConfPath =System.getProperty("user.dir")+ "/xml/attribute.xml";
		logger.info("load skill config file path :{}", atrributeConfPath);
		XMLConfiguration conf = new XMLConfiguration(atrributeConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				AttributeConfig attributeConfig = new AttributeConfig();
				sub = fields.get(i);

				int lv = sub.getInt("[@lv]");
				int hp = sub.getInt("[@hp]");
				int att = sub.getInt("[@att]");
				int def = sub.getInt("[@def]");
				int crit = sub.getInt("[@crit]");
				int ten = sub.getInt("[@ten]");
				int critDam = sub.getInt("[@critDam]");
				int cap = sub.getInt("[@cap]");
				int moveCap = sub.getInt("[@moveCap]");
				int speed = sub.getInt("[@speed]");
				int org = sub.getInt("[@org]");
				int mac = sub.getInt("[@mac]");
				int all = sub.getInt("[@all]");
				int fib = sub.getInt("[@fib]");
				int pun = sub.getInt("[@pun]");
				int sma = sub.getInt("[@sma]");
				int imp = sub.getInt("[@imp]");
				int inc = sub.getInt("[@inc]");
				
				attributeConfig.setLv(lv);
				attributeConfig.setHp(hp);
				attributeConfig.setAtt(att);
				attributeConfig.setDef(def);
				attributeConfig.setCrit(crit);
				attributeConfig.setTen(ten);
				attributeConfig.setCritDam(critDam);
				attributeConfig.setCap(cap);
				attributeConfig.setMoveCap(moveCap);
				attributeConfig.setSpeed(speed);
				attributeConfig.setOrg(org);
				attributeConfig.setMac(mac);
				attributeConfig.setAll(all);
				attributeConfig.setFib(fib);
				attributeConfig.setPun(pun);
				attributeConfig.setSma(sma);
				attributeConfig.setImp(imp);
				attributeConfig.setInc(inc);
				
				attributeConfigMap.put(lv, attributeConfig);
			}
		}
	}

	/**
	 * 加载技能配置
	 * @throws Exception
	 */
	private static void loadSkillConfig() throws Exception{
		String skillConfPath =System.getProperty("user.dir")+ "/xml/skill.xml";
		logger.info("load skill config file path :{}", skillConfPath);
		XMLConfiguration conf = new XMLConfiguration(skillConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				SkillConfig skillConfig = new SkillConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				String name = sub.getString("[@Name]");
				int castType = sub.getInt("[@castType]");
				int weaponType = sub.getInt("[@weaponType]"); 
				int consumePower = sub.getInt("[@consumePower]"); 
				int skillType = sub.getInt("[@skillType]"); 
				int relation = sub.getInt("[@relation]"); 
				int attackRangeType = sub.getInt("[@attackRangeType]"); 
				float maxRange = sub.getFloat("[@maxRange]"); 
				float radius = sub.getFloat("[@radius]"); 
				int angle = sub.getInt("[@angle]"); 
				int targetCount = sub.getInt("[@targetCount]"); 
				int throughObType = sub.getInt("[@throughObType]"); 
				int throughTerrainType = sub.getInt("[@throughTerrainType]"); 
				int damageAttType = sub.getInt("[@damageAttType]"); 
				String addEffect = sub.getString("[@addEffect]"); 
				float convertNum = sub.getFloat("[@convertNum]"); 
				float addDamage = sub.getFloat("[@addDamage]"); 
				float addShield = sub.getFloat("[@addShield]"); 
				float addEnergy = sub.getFloat("[@addEnergy]"); 
				float addAngry = sub.getFloat("[@addAngry]"); 
				int actionID = sub.getInt("[@actionID]"); 
				String sunmonUnits = sub.getString("[@summon]");
				int transformId = sub.getInt("[@transformId]");
				
				
				skillConfig.setId(id);
				skillConfig.setName(name);
				skillConfig.setCastType(castType);
				skillConfig.setWeaponType(weaponType);
				skillConfig.setConsumePower(consumePower);
				skillConfig.setSkillType(skillType);
				skillConfig.setRelation(relation);
				skillConfig.setAttackRangeType(attackRangeType);
				skillConfig.setMaxRange(maxRange);
				skillConfig.setRadius(radius);
				skillConfig.setAngle(angle);
				skillConfig.setTargetCount(targetCount);
				skillConfig.setThroughObType(throughObType);
				skillConfig.setThroughTerrainType(throughTerrainType);
				skillConfig.setDamageAttType(damageAttType);
				skillConfig.setAddEffect(addEffect);
				skillConfig.setConvertNum(convertNum);
				skillConfig.setAddDamage(addDamage);
				skillConfig.setAddShield(addShield);
				skillConfig.setAddEnergy(addEnergy);
				skillConfig.setAddAngry(addAngry);
				skillConfig.setActionID(actionID);
				skillConfig.setSunmmonUnits(sunmonUnits);
				skillConfig.setTransformId(transformId);
				
				skillConfigMap.put(id, skillConfig);
			}
		}
	}


	private static void loadLevelConfig() throws Exception{
		String itemConfPath =System.getProperty("user.dir")+ "/xml/level.xml";
		logger.info("load item config file path :{}", itemConfPath);
		XMLConfiguration conf = new XMLConfiguration(itemConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				MapLevelItemConfig itemConfig = new MapLevelItemConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int sceneID = sub.getInt("[@sceneId]");
				String step1 = sub.getString("[@step1]");
				String step2 = sub.getString("[@step2]");
				String step3 = sub.getString("[@step3]");
				
				LevelStepInfos levelStepInfos = null;
				ObjectMapper mapper = new ObjectMapper();
				
				itemConfig.setId(id);
				itemConfig.setSceneID(sceneID);
				itemConfig.setStep1(step1);
				if(step1 != null && !"".equals(step1)){
					levelStepInfos = (LevelStepInfos)mapper.readValue(step1, new TypeReference<LevelStepInfos>(){});
					itemConfig.setStap1GridIndex(levelStepInfos.gridID);
				}
				
				itemConfig.setStep2(step2);
				if(step2 != null && !"".equals(step2)){
					levelStepInfos = (LevelStepInfos)mapper.readValue(step2, new TypeReference<LevelStepInfos>(){});
					itemConfig.setStap2GridIndex(levelStepInfos.gridID);
				}
				
				itemConfig.setStep3(step3);
				if(step3 != null && !"".equals(step3)){
					levelStepInfos = (LevelStepInfos)mapper.readValue(step3, new TypeReference<LevelStepInfos>(){});
					itemConfig.setStap3GridIndex(levelStepInfos.gridID);
				}
				
				
				mapLevelConfig.levels.put(id, itemConfig);
			}
//			BattleMapUtil.battleMaps.get(1).updateGridDatasByLevelCfg();
		}
	}


	private static void loadBattleMapConfig() throws Exception{
		String itemConfPath =System.getProperty("user.dir")+ "/xml/map.xml";
		logger.info("load item config file path :{}", itemConfPath);
		XMLConfiguration conf = new XMLConfiguration(itemConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("battleMap");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				BattleMapItemConfig itemConfig = new BattleMapItemConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				String name = sub.getString("[@name]");
				String grids = sub.getString("[@grids]");
				
				itemConfig.setId(id);
				itemConfig.setName(name);
				itemConfig.setGrids(grids);
				
				battleMapConfig.maps.put(id, itemConfig);
//				BattleMapUtil.addBattleMap(itemConfig);
			}
		}
	}

	private static void loadDropItemConfig() throws Exception{
		String dropConfPath =System.getProperty("user.dir")+ "/xml/drop.xml";
		logger.info("load drop config file path :{}", dropConfPath);
		XMLConfiguration conf = new XMLConfiguration(dropConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				DropItemConfig itemConfig = new DropItemConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int type = sub.getInt("[@type]");
				String content = sub.getString("[@content]");
				
				itemConfig.setId(id);
				itemConfig.setType(type);
				itemConfig.setContent(content);
				
				dropItemConfigMap.put(id, itemConfig);
			}
		}
	}
	
	private static void loadDungeonConfig() throws Exception{
		String dropConfPath =System.getProperty("user.dir")+ "/xml/dungeon.xml";
		logger.info("load dungeon config file path :{}", dropConfPath);
		XMLConfiguration conf = new XMLConfiguration(dropConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				DungeonConfig dungeonConfig = new DungeonConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int chapter = sub.getInt("[@chapter]");
				int option = sub.getInt("[@option]");
				String name = sub.getString("[@name]");
				String condition = sub.getString("[@condition]");
				String win = sub.getString("[@win]");
				String fail = sub.getString("[@fail]");
				String reward = sub.getString("[@reward]");

				dungeonConfig.setId(id);
				dungeonConfig.setChapter(chapter);
				dungeonConfig.setOption(option);
				dungeonConfig.setName(name);
				dungeonConfig.setCondition(condition);
				dungeonConfig.setWin(win);
				dungeonConfig.setFail(fail);
				dungeonConfig.setReward(reward);

				dungeonConfigMap.put(id, dungeonConfig);
			}
		}
		
	}
	
	private static void loadConditionConfig() throws Exception{
		String itemConfPath = System.getProperty("user.dir")+ "/xml/dungeonCondition.xml";
		logger.info("load dungeonCondition config file path :{}", itemConfPath);
		XMLConfiguration conf = new XMLConfiguration(itemConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				DungeonConditionConfig conditionConfig = new DungeonConditionConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int type = sub.getInt("[@type]");
				String condition = sub.getString("[@condition]");
				String word = sub.getString("[@word]");
				conditionConfig.setId(id);
				conditionConfig.setType(type);
				conditionConfig.setCondition(condition);
				conditionConfig.setWord(word);
				dungeonConditionConfigMap.put(id, conditionConfig);
			}
		}
	}
	
	private static void loadAttributeNumberConfig() throws Exception{
		String itemConfPath = System.getProperty("user.dir") + "/xml/attributeNumber.xml";
		logger.info("load attributeNumber config file path :{}", itemConfPath);
		XMLConfiguration conf = new XMLConfiguration(itemConfPath);
		@SuppressWarnings("unchecked")
		List<HierarchicalConfiguration> fields = conf.configurationsAt("data");
		if (fields != null && fields.size() > 0) {
			HierarchicalConfiguration sub;
			for (int i = 0; i < fields.size(); i++) {
				AttributeNumberConfig attributeNumberConfig = new AttributeNumberConfig();
				sub = fields.get(i);

				int id = sub.getInt("[@id]");
				int defWeight = sub.getInt("[@defWeight]");
				int defRate = sub.getInt("[@defRate]");
				int defLevelRate = sub.getInt("[@defLevelRate]");
				int defBound = sub.getInt("[@defBound]");
				int critRate = sub.getInt("[@critRate]");
				int critRateWeight = sub.getInt("[@critRateWeight]");
				int tenRate = sub.getInt("[@tenRate]");
				int tenRateWeight = sub.getInt("[@tenRateWeight]");
				int critDamRate = sub.getInt("[@critDamRate]");
				int critDamWeight = sub.getInt("[@critDamWeight]");
				int critDamLevelRate = sub.getInt("[@critDamLevelRate]");
				int critRateMin = sub.getInt("[@critRateMin]");
				int critRateBound = sub.getInt("[@critRateBound]");
				int restrainRate = sub.getInt("[@restrainRate]");
				int restrainedRate = sub.getInt("[@restrainedRate]");
				float defIngore = sub.getFloat("[@defIngore]");
				float tenIngore = sub.getFloat("[@tenIngore]");
				float shieldAttackRate = sub.getFloat("[@shieldAttackRate]");
				float shieldDefenceRate = sub.getFloat("[@shieldDefenceRate]");
				int shieldRate = sub.getInt("[@shieldRate]");
				int shieldRateWeight = sub.getInt("[@shieldRateWeight]");
				int shieldRateMax = sub.getInt("[@shieldRateMax]");
				int restraintRateMax = sub.getInt("[@restraintRateMax]");
				int critLevelRate = sub.getInt("[@critLevelRate]");
				int tenLevelRate = sub.getInt("[@tenLevelRate]");
				
				attributeNumberConfig.setId(id);
				attributeNumberConfig.setDefWeight(defWeight);
				attributeNumberConfig.setDefRate(defRate);
				attributeNumberConfig.setDefLevelRate(defLevelRate);
				attributeNumberConfig.setDefBound(defBound);
				attributeNumberConfig.setCritRate(critRate);
				attributeNumberConfig.setCritRateWeight(critRateWeight);
				attributeNumberConfig.setTenRate(tenRate);
				attributeNumberConfig.setTenRateWeight(tenRateWeight);
				attributeNumberConfig.setCritDamRate(critDamRate);
				attributeNumberConfig.setCritDamWeight(critDamWeight);
				attributeNumberConfig.setCritDamLevelRate(critDamLevelRate);
				attributeNumberConfig.setCritRateMin(critRateMin);
				attributeNumberConfig.setCritRateBound(critRateBound);
				attributeNumberConfig.setRestrainRate(restrainRate);
				attributeNumberConfig.setRestrainedRate(restrainedRate);
				attributeNumberConfig.setDefIngore(defIngore);
				attributeNumberConfig.setTenIngore(tenIngore);
				attributeNumberConfig.setShieldAttackRate(shieldAttackRate);
				attributeNumberConfig.setShieldDefenceRate(shieldDefenceRate);
				attributeNumberConfig.setShieldRate(shieldRate);
				attributeNumberConfig.setShieldRateWeight(shieldRateWeight);
				attributeNumberConfig.setShieldRateMax(shieldRateMax);
				attributeNumberConfig.setRestraintRateMax(restraintRateMax);
				attributeNumberConfig.setCritLevelRate(critLevelRate);
				attributeNumberConfig.setTenLevelRate(tenLevelRate);
				
				attributeNumberConfigMap.put(id, attributeNumberConfig);
			}
		}
	}
}
