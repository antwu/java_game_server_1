package com.game.battle.config;


public class SkillConfig {
	private int id;
	private String name;
	//类型（1武器普通2武器终结3怪物）
	private int castType;
	//武器系别（0无1刀剑2枪3锤4盾5弓6匕首）
	private int weaponType;
	//消耗行动力
	private int consumePower;
	//技能类型（1伤害类 2护盾类3治疗4瞬移5召唤6变身,7加buff)
	private int skillType;
	//与目标关系（1.友好2.敌对3.所有）
	private int relation;
	//攻击形状（0.无形状选点1.扇形2.矩形3.圆形）
	private int attackRangeType;
	//最大射程（怪物射程走怪物表，基础属性）
	private float maxRange;
	//攻击半径
	private float radius;
	//攻击角度
	private int angle;
	//攻击目标数量
	private int targetCount;
	//1：可被动态障碍阻挡（吸收伤害）2：直接摧毁动态障碍 3：直接忽略动态障碍
	private int throughObType;
	//0：不能穿越地形障碍 1：可穿越地形障碍
	private int throughTerrainType;
	//属性伤害类型
	private int damageAttType;
	//转化系数（攻击力100×转化系数）
	private float convertNum;
	//附加伤害值
	private float addDamage;
	//附加护盾值
	private float addShield;
	//附加效果
	private String addEffect;
	//附加能量
	private float addEnergy;
	//附加仇恨
	private float addAngry;
	//动作组
	private int actionID;
	/**
	 * 召唤的怪物
	 */
	private String sunmmonUnits;
	//变身成为什么的id
	private int transformId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCastType() {
		return castType;
	}
	public void setCastType(int castType) {
		this.castType = castType;
	}
	public int getWeaponType() {
		return weaponType;
	}
	public void setWeaponType(int weaponType) {
		this.weaponType = weaponType;
	}
	public int getConsumePower() {
		return consumePower;
	}
	public void setConsumePower(int consumePower) {
		this.consumePower = consumePower;
	}
	public int getSkillType() {
		return skillType;
	}
	public void setSkillType(int skillType) {
		this.skillType = skillType;
	}
	public int getRelation() {
		return relation;
	}
	public void setRelation(int relation) {
		this.relation = relation;
	}
	public int getAttackRangeType() {
		return attackRangeType;
	}
	public void setAttackRangeType(int attackRangeType) {
		this.attackRangeType = attackRangeType;
	}
	public float getMaxRange() {
		return maxRange;
	}
	public void setMaxRange(float maxRange) {
		this.maxRange = maxRange;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public int getAngle() {
		return angle;
	}
	public void setAngle(int angle) {
		this.angle = angle;
	}
	public int getTargetCount() {
		return targetCount;
	}
	public void setTargetCount(int targetCount) {
		this.targetCount = targetCount;
	}
	public int getThroughObType() {
		return throughObType;
	}
	public void setThroughObType(int throughObType) {
		this.throughObType = throughObType;
	}
	public int getThroughTerrainType() {
		return throughTerrainType;
	}
	public void setThroughTerrainType(int throughTerrainType) {
		this.throughTerrainType = throughTerrainType;
	}
	public int getDamageAttType() {
		return damageAttType;
	}
	public void setDamageAttType(int damageAttType) {
		this.damageAttType = damageAttType;
	}
	public float getConvertNum() {
		return convertNum;
	}
	public void setConvertNum(float convertNum) {
		this.convertNum = convertNum;
	}
	public float getAddDamage() {
		return addDamage;
	}
	public void setAddDamage(float addDamage) {
		this.addDamage = addDamage;
	}
	public float getAddShield() {
		return addShield;
	}
	public void setAddShield(float addShield) {
		this.addShield = addShield;
	}
	public String getAddEffect() {
		return addEffect;
	}
	public void setAddEffect(String addEffect) {
		this.addEffect = addEffect;
	}
	public float getAddEnergy() {
		return addEnergy;
	}
	public void setAddEnergy(float addEnergy) {
		this.addEnergy = addEnergy;
	}
	public float getAddAngry() {
		return addAngry;
	}
	public void setAddAngry(float addAngry) {
		this.addAngry = addAngry;
	}
	public int getActionID() {
		return actionID;
	}
	public void setActionID(int actionID) {
		this.actionID = actionID;
	}
	
	public String getSunmmonUnits() {
		return sunmmonUnits;
	}
	public void setSunmmonUnits(String sunmmonUnits) {
		this.sunmmonUnits = sunmmonUnits;
	}
	
	public int getTransformId() {
		return transformId;
	}
	public void setTransformId(int transformId) {
		this.transformId = transformId;
	}
	
	public float getMaxDist()
    {
        switch(attackRangeType)
        {
            case 1:
                return this.radius;
            case 4:
                return this.maxRange;
            case 2://EnumAttackRangeType.Rect.ordinal()
                return this.maxRange;
            default:
                break;
        }

        return 1;
    }

}
