package com.game.battle.config;




public class BuffConfig {
	private int id;
	//互斥组ID
	private int group;
	//不同来源互斥；1=互斥，0=不互斥
	private int mutex;
	//效果性质(1.正面 2.负面 3.中立)
	private int kind;
	//是否即时生效 1=即时 0=下一个结算点开始
	private int isImmediate;
	//部署是否提示 1=提示 0=不提示
	private int isHint;
	//部署是否计算 1=计算 0=不计算
	private int isCount;
	//限定武器ID（0=不限定）
	private int isWeaponLimit;
	//持续时间
	private int time;
	//发动次数（达到次数后消失）
	private int useNumber;
	//BUFF效果
	private String effects;
	//死亡删除（1=删除）
	private int dieCancle;
	//是否显示
	private int isShow;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getEffects() {
		return effects;
	}

	public void setEffects(String effects) {
		this.effects = effects;
	}
	
	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public int getMutex() {
		return mutex;
	}

	public void setMutex(int mutex) {
		this.mutex = mutex;
	}

	public int getKind() {
		return kind;
	}

	public void setKind(int kind) {
		this.kind = kind;
	}

	public int getIsImmediate() {
		return isImmediate;
	}

	public void setIsImmediate(int isImmediate) {
		this.isImmediate = isImmediate;
	}

	public int getIsHint() {
		return isHint;
	}

	public void setIsHint(int isHint) {
		this.isHint = isHint;
	}

	public int getIsCount() {
		return isCount;
	}

	public void setIsCount(int isCount) {
		this.isCount = isCount;
	}

	public int getIsWeaponLimit() {
		return isWeaponLimit;
	}

	public void setIsWeaponLimit(int isWeaponLimit) {
		this.isWeaponLimit = isWeaponLimit;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getUseNumber() {
		return useNumber;
	}

	public void setUseNumber(int useNumber) {
		this.useNumber = useNumber;
	}

	public int getDieCancle() {
		return dieCancle;
	}

	public void setDieCancle(int dieCancle) {
		this.dieCancle = dieCancle;
	}

	public int getIsShow() {
		return isShow;
	}

	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}
	
}
