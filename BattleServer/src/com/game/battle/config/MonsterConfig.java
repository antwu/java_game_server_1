package com.game.battle.config;

import java.util.ArrayList;
import java.util.List;


public class MonsterConfig {
	//怪物id
	private int id;
	
	//专精等级
	private int lv;
	//生命
	private int hp;
	//攻击
	private int att;
	//防御
	private int def;
	//暴击
	private int crit;
	//韧性
	private int ten;
	//暴击伤害
	private int critDam;
	//行动力上限
	private int cap;
	//单位距离消耗行动力
	private int moveCap;
	//先手值
	private int speed;
	//有机抗性
	private int org;
	//机械抗性
	private int mac;
	//合金抗性
	private int all;
	//纤维抗性
	private int fib;
	//穿刺伤害
	private int pun;
	//粉碎伤害
	private int sma;
	//冲击伤害
	private int imp;
	//切割伤害
	private int inc;
	
	//护盾值
	private int shield;
	
	private int armorType;
	//视野
	private int scanRange;
		
	private String ai;
	private List<Integer> aiList = new ArrayList<>();

	private String drops;

	public String getAi() {
		return ai;
	}

	public void setAi(String ai) {
		this.ai = ai;
		if(ai != null && !"".equals(ai)){
			String[] ais = ai.split(",");
			for(String s : ais){
				aiList.add(Integer.valueOf(s));
			}
		}
	}
	
	public List<Integer> getAiList() {
		return aiList;
	}

	public void setAiList(List<Integer> aiList) {
		this.aiList = aiList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getLv() {
		return lv;
	}

	public void setLv(int lv) {
		this.lv = lv;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAtt() {
		return att;
	}

	public void setAtt(int att) {
		this.att = att;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getCrit() {
		return crit;
	}

	public void setCrit(int crit) {
		this.crit = crit;
	}

	public int getTen() {
		return ten;
	}

	public void setTen(int ten) {
		this.ten = ten;
	}

	public int getCritDam() {
		return critDam;
	}

	public void setCritDam(int critDam) {
		this.critDam = critDam;
	}

	public int getCap() {
		return cap;
	}

	public void setCap(int cap) {
		this.cap = cap;
	}

	public int getMoveCap() {
		return moveCap;
	}

	public void setMoveCap(int moveCap) {
		this.moveCap = moveCap;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getOrg() {
		return org;
	}

	public void setOrg(int org) {
		this.org = org;
	}

	public int getMac() {
		return mac;
	}

	public void setMac(int mac) {
		this.mac = mac;
	}

	public int getAll() {
		return all;
	}

	public void setAll(int all) {
		this.all = all;
	}

	public int getFib() {
		return fib;
	}

	public void setFib(int fib) {
		this.fib = fib;
	}

	public int getPun() {
		return pun;
	}

	public void setPun(int pun) {
		this.pun = pun;
	}

	public int getSma() {
		return sma;
	}

	public void setSma(int sma) {
		this.sma = sma;
	}

	public int getImp() {
		return imp;
	}

	public void setImp(int imp) {
		this.imp = imp;
	}

	public int getInc() {
		return inc;
	}

	public void setInc(int inc) {
		this.inc = inc;
	}
	
	public int getShield() {
		return shield;
	}

	public void setShield(int shield) {
		this.shield = shield;
	}
	
	public int getArmorType() {
		return armorType;
	}

	public void setArmorType(int armorType) {
		this.armorType = armorType;
	}
	
	public int getScanRange() {
		return scanRange;
	}

	public void setScanRange(int scanRange) {
		this.scanRange = scanRange;
	}

	public String getDrops() {
		return drops;
	}

	public void setDrops(String drops) {
		this.drops = drops;
	}
}
