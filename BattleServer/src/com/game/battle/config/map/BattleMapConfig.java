package com.game.battle.config.map;

import java.util.HashMap;
import java.util.Map;

public class BattleMapConfig {
	public Map<Integer, BattleMapItemConfig> maps = new HashMap<>();

	
	public static final class BattleMapItemConfig{
		private int id;
		private String name;
		private String grids;
		
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getGrids() {
			return grids;
		}
		public void setGrids(String grids) {
			this.grids = grids;
		}
	}
}
