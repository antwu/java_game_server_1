package com.game.battle.config.map;

import java.util.HashMap;
import java.util.Map;

public class MapLevelConfig {
	public Map<Integer, MapLevelItemConfig> levels = new HashMap<>();
	
	
	public static final class MapLevelItemConfig{
		private int id;
		private int sceneID;
		private String step1;
		private String step2;
		private String step3;
		
		private int stap1GridIndex;
		private int stap2GridIndex;
		private int stap3GridIndex;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getSceneID() {
			return sceneID;
		}
		public void setSceneID(int sceneID) {
			this.sceneID = sceneID;
		}
		public String getStep1() {
			return step1;
		}
		public void setStep1(String step1) {
			this.step1 = step1;
		}
		public String getStep2() {
			return step2;
		}
		public void setStep2(String step2) {
			this.step2 = step2;
		}
		public String getStep3() {
			return step3;
		}
		public void setStep3(String step3) {
			this.step3 = step3;
		}
		public int getStap1GridIndex() {
			return stap1GridIndex;
		}
		public void setStap1GridIndex(int stap1GridIndex) {
			this.stap1GridIndex = stap1GridIndex;
		}
		public int getStap2GridIndex() {
			return stap2GridIndex;
		}
		public void setStap2GridIndex(int stap2GridIndex) {
			this.stap2GridIndex = stap2GridIndex;
		}
		public int getStap3GridIndex() {
			return stap3GridIndex;
		}
		public void setStap3GridIndex(int stap3GridIndex) {
			this.stap3GridIndex = stap3GridIndex;
		}
	}
}
