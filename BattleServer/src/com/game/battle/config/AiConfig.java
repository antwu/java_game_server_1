package com.game.battle.config;

import java.util.ArrayList;
import java.util.List;



public class AiConfig {
	private int id;
	private String events;
	//行为执行类型(1.依次执行100%触发 2.概率选择1个行为执行）
	private int actionType;
	private String actionIds;
	private String actionRates;
	/**
	 * 开始执行的回合数（填0为无限制）
	 */
	private int startFireRound;
	/**
	 * 间隔执行的回合数（每x回合执行，0=无限制）
	 */
	private int intervalRound;
	/**
	 * 单回合最大执行次数（填0为无限制）
	 */
	private int useCount;
	
	
	private List<String> eventList = new ArrayList<>();
	private List<Integer> actionList = new ArrayList<>();
	private List<Integer> actionRateList = new ArrayList<>();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getEvents() {
		return events;
	}

	public void setEvents(String events) {
		this.events = events;
		if(events != null && !"".equals(events)){
			String[] eventArray = events.split("\\|");
			for(String e : eventArray){
				eventList.add(e);
			}
		}
	}

	public int getActionType() {
		return actionType;
	}

	public void setActionType(int actionType) {
		this.actionType = actionType;
	}

	public String getActionIds() {
		return actionIds;
	}

	public void setActionIds(String actionIds) {
		this.actionIds = actionIds;
		if(actionIds != null && !"".equals(actionIds)){
			String[] actionArray = actionIds.split(",");
			for(String e : actionArray){
				actionList.add(Integer.valueOf(e));
			}
		}
	}

	public String getActionRates() {
		return actionRates;
	}

	public void setActionRates(String actionRates) {
		this.actionRates = actionRates;
		if(actionRates != null && !"".equals(actionRates)){
			String[] actionRateArray = actionRates.split(",");
			for(String e : actionRateArray){
				actionRateList.add(Integer.valueOf(e));
			}
		}
	}
	
	public List<String> getEventList() {
		return eventList;
	}

	public void setEventList(List<String> eventList) {
		this.eventList = eventList;
	}
	
	public List<Integer> getActionList() {
		return actionList;
	}

	public void setActionList(List<Integer> actionList) {
		this.actionList = actionList;
	}

	public List<Integer> getActionRateList() {
		return actionRateList;
	}

	public void setActionRateList(List<Integer> actionRateList) {
		this.actionRateList = actionRateList;
	}
	
	public int getStartFireRound() {
		return startFireRound;
	}

	public void setStartFireRound(int startFireRound) {
		this.startFireRound = startFireRound;
	}

	public int getIntervalRound() {
		return intervalRound;
	}

	public void setIntervalRound(int intervalRound) {
		this.intervalRound = intervalRound;
	}

	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}
}
