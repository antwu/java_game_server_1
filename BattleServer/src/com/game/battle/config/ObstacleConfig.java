package com.game.battle.config;

public class ObstacleConfig {
	//怪物id
	private int id;
	
	//生命
	private int hp;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
}
