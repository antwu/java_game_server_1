package com.game.battle.config;

public class DungeonConditionConfig {
	private int id;//关卡达成条件表id
	private int type;//类型（1.胜利，2.失败，3.评价）
	private String condition;//条件
	private String word;//描述
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
}
