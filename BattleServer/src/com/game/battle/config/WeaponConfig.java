package com.game.battle.config;

public class WeaponConfig {
	private int id;
	private int mainSkillId;
	//类别（1刀剑2枪3锤4盾5弓6匕首）
	private int weaponType;
	private String talents;
	
	public int getMainSkillId() {
		return mainSkillId;
	}

	public void setMainSkillId(int mainSkillId) {
		this.mainSkillId = mainSkillId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getWeaponType() {
		return weaponType;
	}

	public void setWeaponType(int weaponType) {
		this.weaponType = weaponType;
	}
	
	public String getTalents() {
		return talents;
	}

	public void setTalents(String talents) {
		this.talents = talents;
	}
}
