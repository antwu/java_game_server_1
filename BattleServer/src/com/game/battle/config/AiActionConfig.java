package com.game.battle.config;

import java.util.ArrayList;
import java.util.List;

public class AiActionConfig {
	private int id;
	private String target;
	private String action;
	
	private List<String> actionList = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
		if(action != null && !"".equals(action)){
			String[] actionArray = action.split("\\|");
			for(String e : actionArray){
				actionList.add(e);
			}
		}
	}
	
	public List<String> getActionList() {
		return actionList;
	}

	public void setActionList(List<String> actionList) {
		this.actionList = actionList;
	}
}
