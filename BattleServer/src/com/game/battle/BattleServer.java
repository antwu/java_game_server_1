package com.game.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.config.XMLTemplateService;
import com.game.battle.service.server.ServerService;
import com.game.battle.start.NetInitializer;
import com.game.battle.start.TaskInitializer;
import com.game.common.config.BaseLocalConfigInitializer;
import com.game.common.config.BaseServerConfig;
import com.game.core.service.ServiceContainer;

public class BattleServer
{
	static private Logger logger = LoggerFactory.getLogger(BattleServer.class);
	public static void main(String[] args)
	{
		BaseLocalConfigInitializer baseLocalConfigIntializer = new BaseLocalConfigInitializer();
		BaseServerConfig config = new BaseServerConfig();
		try {
			XMLTemplateService.initTemplateData();
			baseLocalConfigIntializer.init(config);
		} catch (Exception e) {
			logger.error("Failed to load local configuration Exception err={}", e.toString());
			return;
		}

		NetInitializer netInitializer = new NetInitializer();
		try {
			netInitializer.init();
			// task initial
			if (TaskInitializer.getInstance().init() != 0) {
				logger.error("initial task failed.");
				System.exit(-3);
			}
		} catch (Exception e) {
			logger.error("Failed to NetInitializer Exception err={}", e.toString());
			return;
		}

		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		service.setNetInitializer(netInitializer);
		service.setServerConfig(config);
		netInitializer.connectToGame();
	}
}
