package com.game.battle.service.server;

import java.util.concurrent.ConcurrentHashMap;

import com.game.battle.start.NetInitializer;
import com.game.common.Transmitter;
import com.game.common.config.BaseServerConfig;
import com.game.common.constants.GlobalConstants;
import com.game.common.constants.ServerType;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.message.proto.server.ServerProtoBuf.SSRegisterServerREQ;

/**
 * 目前是区列表管理，以后有需要的功能在加
 * @author cuichao
 */
public class ServerService extends PublicService
{
	private static final long serialVersionUID = -8272027997288462700L;

	private NetInitializer netInitializer;
	private RemoteNode gameNode;
	private ConcurrentHashMap<Integer, RemoteNode> gatewayNodes = new ConcurrentHashMap<Integer, RemoteNode>();
	private BaseServerConfig serverConfig;
	
	public BaseServerConfig getServerConfig()
	{
		return serverConfig;
	}

	public void setServerConfig(BaseServerConfig config)
	{
		serverConfig = config;
	}

	public void setNetInitializer(NetInitializer netInitializer)
    {
	    this.netInitializer = netInitializer;
    }
	
	public NetInitializer getNetInitializer()
	{
		return netInitializer;
	}
	
	public void setGameNode(RemoteNode node)
	{
		if(serverConfig == null)
			return;
		
		if(serverConfig.getManageServerAddress().equals(node.getAddress()))
		{
			gameNode = node;
			SSRegisterServerREQ.Builder sendBuilder = SSRegisterServerREQ.newBuilder();
		    sendBuilder.setServerName(serverConfig.getServerName());
		    sendBuilder.setServerID(serverConfig.getServerID());
		    sendBuilder.setServerType(ServerType.BATTLE_SERVER);
		    Transmitter.getInstance().write(node, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
		}
	}
	
	public RemoteNode getGameNode()
	{
		return gameNode;
	}
	
	public void setGatewayNode(int serverID, RemoteNode node)
	{
		gatewayNodes.put(serverID, node);
	}
	
	public RemoteNode getGatewayNode(int serverID)
	{
		return gatewayNodes.get(serverID);
	}
}