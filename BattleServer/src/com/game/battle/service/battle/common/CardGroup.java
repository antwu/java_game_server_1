package com.game.battle.service.battle.common;

/**
 * 能够构成一组(台)的牌
 */
public class CardGroup
{
	public CardGroupType groupType = CardGroupType.UNKNOWN;
	public int[] cardList = null;;

	public CardGroup()
	{
	}

	public CardGroup(CardGroupType groupType, int card0)
	{
		this.groupType = groupType;
		this.cardList = new int[]
		{ card0 };
	}

	public CardGroup(CardGroupType groupType, int card0, int card1)
	{
		this.groupType = groupType;
		this.cardList = new int[]
		{ card0, card1 };
	}

	public CardGroup(CardGroupType groupType, int card0, int card1, int card2)
	{
		this.groupType = groupType;
		this.cardList = new int[]
		{ card0, card1, card2 };
	}

	/**
	 * 获取card在CardList中的第一个索引
	 * 
	 * @param card
	 * @return
	 */
	public int getFirstIndexOfCard(int card)
	{
		for (int index = 0; index < cardList.length; index++)
			if (cardList[index] == card)
				return index;

		return -1;
	}
}
