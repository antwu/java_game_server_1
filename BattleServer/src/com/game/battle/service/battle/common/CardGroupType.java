package com.game.battle.service.battle.common;

/** 刻，相同的三张 坎，三张顺序排，例如（1万，2万，3万） */
public enum CardGroupType
{
	UNKNOWN,

	/** 将 */
	JIANG,

	/** 暗坎 */
	AN_KAN,

	/** 暗刻 */
	AN_KE,
}