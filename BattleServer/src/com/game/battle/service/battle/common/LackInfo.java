package com.game.battle.service.battle.common;

// 缺门信息
public class LackInfo
{
	private boolean isLack;  	// 是否含有缺门
	private int lackCardCount;	// 含有缺门牌数量
	private int lackCard;		// 第一个缺门牌
	
	public boolean isLack()
	{
		return isLack;
	}
	
	public int getLackCardCount()
	{
		return lackCardCount;
	}
	
	public void addLackCardCount(int card)
	{
		this.isLack = true;
		this.lackCardCount++;
		this.lackCard = card;
	}
	
	public int getLackCard()
	{
		return lackCard;
	}
}
