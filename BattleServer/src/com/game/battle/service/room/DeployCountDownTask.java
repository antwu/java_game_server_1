package com.game.battle.service.room;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kodgames.core.task.TaskService;
import com.kodgames.core.task.timeEvent.TaskTimeInfo;
import com.kodgames.core.timer.TimeUtils;
import com.kodgames.core.timer.event.TimerPair;
import com.game.battle.common.BattleServerTask;
import com.game.battle.service.battle.BattleService;
import com.game.core.service.ServiceContainer;

public class DeployCountDownTask
{
	Logger logger = LoggerFactory.getLogger(DeployCountDownTask.class);
	private static DeployCountDownTask task = new DeployCountDownTask();
	private static final long FIVE_SECONDS = 5 * 1000;
	
	private DeployCountDownTask()
	{
	}
	
	public static DeployCountDownTask getInstance()
	{
		return task;
	}
	
	public void init()
    {
		long now = System.currentTimeMillis();
		
		logger.debug("task VoteDestroyTask init.");
		List<TimerPair> list = new ArrayList<TimerPair>();

		//一分钟执行2次，30秒钟执行一次
		for (int i = 1; i <= 12; i++)
		{
			TimerPair pair = new TimerPair(now + (i * FIVE_SECONDS), i);
			list.add(pair);
		}

		TaskTimeInfo timeInfo = new TaskTimeInfo(now, 0, TimeUtils.Minute, 1, list);
		TaskService.getInstance().registerScheduleTask(new DeployCountDown(), timeInfo);
    }
	
	private class DeployCountDown extends BattleServerTask
	{
		@Override
        public void run(int dumb)
        {
			BattleService service = ServiceContainer.getInstance().getPublicService(BattleService.class);
			service.checkDeployTime();
        }
	}
	
}
