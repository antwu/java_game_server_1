package com.game.battle.service.room;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.common.Room;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.message.proto.room.RoomProtoBuf.GBRoomCreateREQ;

public class RoomService extends PublicService
{
	private static final long serialVersionUID = -95924175253120279L;
	private final Logger logger = LoggerFactory.getLogger(RoomService.class);
	private ConcurrentHashMap<Integer, Room> rooms = new ConcurrentHashMap<Integer, Room>();
	private ConcurrentHashMap<String, Integer> accountRooms = new ConcurrentHashMap<String, Integer>();
	private static Long ONE_MINS = 60 * 1000L;

	public Room getRoom(int room)
	{
		return rooms.get(room);
	}

	public void createRoom(RemoteNode remoteNode, GBRoomCreateREQ message, int callback)
	{
	}

}