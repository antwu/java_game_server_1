package com.game.battle.net.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.AbstractConnectionHandler;

public class CSConnectionHandler extends AbstractConnectionHandler<Object> 
{
	static private Logger logger = LoggerFactory.getLogger(CSConnectionHandler.class);
	@Override
    public void handleConnectionActive(RemoteNode remoteNode)
    {
	    logger.info("Connection active. Remote Client Address:{}", remoteNode.getAddress());
    }

	@Override
    public void handleConnectionInactive(RemoteNode remoteNode)
    {
		logger.info("Connection inactive. Remote Client Address:{}", remoteNode.getAddress());  
    }

	@Override
    public Object getMessageKey(RemoteNode remoteNode)
    {
	    return null;
    }
}
