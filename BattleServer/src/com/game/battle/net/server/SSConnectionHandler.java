package com.game.battle.net.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.battle.service.server.ServerService;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.AbstractConnectionHandler;
import com.game.core.service.ServiceContainer;

public class SSConnectionHandler extends AbstractConnectionHandler<Object> 
{
	static private Logger logger = LoggerFactory.getLogger(SSConnectionHandler.class);
	@Override
    public void handleConnectionActive(RemoteNode remoteNode)
    {
	    ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
	    service.setGameNode(remoteNode);
	    logger.info("Connection active. Remote Server Address:{}", remoteNode.getAddress());
    }

	@Override
    public void handleConnectionInactive(RemoteNode remoteNode)
    {
		logger.info("Connection inactive. Remote Server Address:{}", remoteNode.getAddress());  
    }

	@Override
    public Object getMessageKey(RemoteNode remoteNode)
    {
	    return null;
    }

}
