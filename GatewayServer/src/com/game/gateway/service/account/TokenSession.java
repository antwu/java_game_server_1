package com.game.gateway.service.account;

import com.game.core.session.Session;

public class TokenSession
{
	private Session session;
	private String name;
	private String head;
	private String sex;
	private String refreshToken;
	private long userAccTime; // 用户数据获取时间
	private String deviceId;

	public TokenSession(Session session)
	{
		this.session = session;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setHead(String head)
	{
		this.head = head;
	}

	public String getHead()
	{
		return head;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public String getRefreshToken()
	{
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken)
	{
		this.refreshToken = refreshToken;
	}

	public Session getSession()
	{
		return session;
	}

	public void setSession(Session session)
	{
		this.session = session;
	}

	public void setUserAccTime(long userAccTime)
	{
		this.userAccTime = userAccTime;
	}

	public long getUserAccTime()
	{
		return userAccTime;
	}

	public String getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(String deviceId)
	{
		this.deviceId = deviceId;
	}

	public boolean isUserInfoValid()
	{
		return (dataInValid(name) && dataInValid(head) && dataInValid(sex)) == false;
	}

	public boolean isRefreshtokenValid()
	{
		return userAccTime != 0l && daysValid(userAccTime, System.currentTimeMillis());
	}

	private boolean daysValid(long dateOne, long dateTwo)
	{
		return (dateTwo - dateOne) < (7 * 24 * 60 * 60 * 1000);
	}

	private boolean dataInValid(String data)
	{
		return data == null || data.isEmpty();
	}
}
