package com.game.gateway.service.encrypt;

import java.util.ArrayList;

import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.message.proto.encrypt.EncryptProtoBuf.WCSendYRES;
import com.game.message.proto.encrypt.EncryptProtoBuf.WCUseNewKeyRES;

public class EncryptService extends PublicService
{
	private static final long serialVersionUID = -6257485138087572036L;

	public WCSendYRES sendYQtoClient(RemoteNode clientNode)
	{
		WCSendYRES.Builder builder = WCSendYRES.newBuilder();
		ArrayList<String> yAndQ = clientNode.getEncryptCoder().genYandQ();

		if(yAndQ == null)
		{
			return null;
		}
		builder.setServerY(yAndQ.get(0));
		int qIndex = Integer.parseInt(yAndQ.get(1));
		builder.setQIndex(qIndex);
		
		return builder.build();
	}
	
	public WCUseNewKeyRES computeNewKey(RemoteNode clientNode, String clientY)
	{
		WCUseNewKeyRES.Builder builder = WCUseNewKeyRES.newBuilder();
		return builder.build();
	}
}
