package com.game.gateway.service.weapon;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.weapon.WeaponProtoBuf.*;

public class WeaponService extends PublicService{

	public void changeWeaponSet(RemoteNode node, CWChangeWeaponSetREQ req, int callback) {
		//////////////////////////////
		// TODO check for login///
		//////////////////////////////
		WGChangeWeaponSetREQ.Builder sendBuilder = WGChangeWeaponSetREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setNewWeapon(req.getNewWeapon());
		sendBuilder.setChangeWeaponSet(req.getChangeWeaponSet());
		sendBuilder.setChangePosition(req.getChangePosition());
		
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeWeaponSetResult(GWChangeWeaponSetRES res) {
		WCChangeWeaponSetRES.Builder sendBuilder = WCChangeWeaponSetRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeCurrentWeaponSet(RemoteNode node, CWChangeCurrentWeaponSetREQ req, int callback) {
		WGChangeCurrentWeaponSetREQ.Builder sendBuilder = WGChangeCurrentWeaponSetREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setChangeWeaponSet(req.getChangeWeaponSet());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeCurrentWeaponSetResult(GWChangeCurrentWeaponSetRES res) {
		WCChangeCurrentWeaponSetRES.Builder sendBuilder = WCChangeCurrentWeaponSetRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeWeaponSetName(RemoteNode node, CWChangeWeaponSetNameREQ req, int callback) {
		WGChangeWeaponSetNameREQ.Builder sendBuilder = WGChangeWeaponSetNameREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setChangeWeaponSet(req.getChangeWeaponSet());
		sendBuilder.setNewName(req.getNewName());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeWeaponSetNameResult(GWChangeWeaponSetNameRES res) {
		WCChangeWeaponSetNameRES.Builder sendBuilder = WCChangeWeaponSetNameRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}

	public void changeFashionSet(RemoteNode node, CWChangeFashionSetREQ req, int callback) {
		WGChangeFashionSetREQ.Builder sendBuilder = WGChangeFashionSetREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setOldFashionId(req.getOldFashionId());
		sendBuilder.setNewFashionId(req.getNewFashionId());
		sendBuilder.setChangeWeaponSet(req.getChangeWeaponSet());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeFashionSetResult(GWChangeFashionSetRES res) {
		WCChangeFashionSetRES.Builder sendBuilder = WCChangeFashionSetRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeColourSet(RemoteNode node, CWChangeColourSetREQ req, int callback) {
		WGChangeColourSetREQ.Builder sendBuilder = WGChangeColourSetREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setFashionId(req.getFashionId());
		sendBuilder.addAllNewColour(req.getNewColourList());
		//sendBuilder.setNewColour(req.getNewColour());
		//sendBuilder.setChangePosition(req.getChangePosition());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeColourSetResult(GWChangeColourSetRES res) {
		WCChangeColourSetRES.Builder sendBuilder = WCChangeColourSetRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeBuncherSet(RemoteNode node, CWChangeBuncherSetREQ req, int callback) {
		WGChangeBuncherSetREQ.Builder sendBuilder = WGChangeBuncherSetREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setOldBuncherId(req.getOldBuncherId());
		sendBuilder.setNewBuncherId(req.getNewBuncherId());
		sendBuilder.setChangeWeaponSet(req.getChangeWeaponSet());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public void changeBuncherSetResult(GWChangeBuncherSetRES res) {
		WCChangeBuncherSetRES.Builder sendBuilder = WCChangeBuncherSetRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}

	public void changeBuncherUnlock(RemoteNode remoteNode, CWChangeBuncherUnlockREQ req, int callback) {
		WGChangeBuncherUnlockREQ.Builder sendBuilder = WGChangeBuncherUnlockREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setUnlocked(req.getUnlocked());
		sendBuilder.setChangeWeaponSet(req.getChangeWeaponSet());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}

	public void changeBuncherUnlockResult(GWChangeBuncherUnlockRES res) {
		WCChangeBuncherUnlockRES.Builder sendBuilder = WCChangeBuncherUnlockRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());		
	}

	public void changeFashionUnlock(RemoteNode remoteNode, CWChangeFashionUnlockREQ req, int callback) {
		WGChangeFashionUnlockREQ.Builder sendBuilder = WGChangeFashionUnlockREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setFashionId(req.getFashionId());
		sendBuilder.setUnlocked(req.getUnlocked());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
		
	}

	public void changeFashionUnlockResult(GWChangeFashionUnlockRES res) {
		WCChangeFashionUnlockRES.Builder sendBuilder = WCChangeFashionUnlockRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());		
	}
	
	public void changeColourUnlock(RemoteNode remoteNode, CWChangeColourUnlockREQ req, int callback) {
		WGChangeColourUnlockREQ.Builder sendBuilder = WGChangeColourUnlockREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setFashionId(req.getFashionId());
		sendBuilder.addAllUnlockColour(req.getUnlockColourList());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}

	public void changeColourUnlockResult(GWChangeColourUnlockRES res) {
		WCChangeColourUnlockRES.Builder sendBuilder = WCChangeColourUnlockRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());		
	}

	public void changeMainNodeUnlock(RemoteNode remoteNode, CWChangeMainNodeUnlockREQ req, int callback) {
		WGChangeMainNodeUnlockREQ.Builder sendBuilder = WGChangeMainNodeUnlockREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setWeaponId(req.getWeaponId());
		sendBuilder.setMainId(req.getMainId());
		sendBuilder.setStatus(req.getStatus());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}


	public void changeMainNodeUnlockResult(GWChangeMainNodeUnlockRES res) {
		WCChangeMainNodeUnlockRES.Builder sendBuilder = WCChangeMainNodeUnlockRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		if(res.getWeaponInfos()!=null) {
			sendBuilder.setWeaponInfos(res.getWeaponInfos());
		}
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());		
	}

	public void changeItemNodeUnlock(RemoteNode remoteNode, CWChangeItemNodeUnlockREQ req, int callback) {
		WGChangeItemNodeUnlockREQ.Builder sendBuilder = WGChangeItemNodeUnlockREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setWeaponId(req.getWeaponId());
		sendBuilder.setMainId(req.getMainId());
		sendBuilder.setItemId(req.getItemId());
		sendBuilder.setStatus(req.getStatus());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
		
	}

	public void changeItemNodeUnlockResult(GWChangeItemNodeUnlockRES res) {
		WCChangeItemNodeUnlockRES.Builder sendBuilder = WCChangeItemNodeUnlockRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		if(res.getWeaponInfos()!=null) {
			sendBuilder.setWeaponInfos(res.getWeaponInfos());
		}
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());	
	}

	public void pushSynWeaponInfo(GWSynWeaponInfoRES res) {
		WCSynWeaponInfoRES.Builder sendBuilder = WCSynWeaponInfoRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setSynSource(res.getSynSource());
		sendBuilder.setWeaponInfo(res.getWeaponInfo());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}

	public void pushSynFashionInfo(GWSynFashionInfoRES res) {
		WCSynFashionInfoRES.Builder sendBuilder = WCSynFashionInfoRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setSynSource(res.getSynSource());
		sendBuilder.setFashionInfo(res.getFashionInfo());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());	
	}
}
