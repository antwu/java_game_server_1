package com.game.gateway.service.team;

import com.google.protobuf.GeneratedMessage;
import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.team.TeamProtoBuf.*;

public class TeamService extends PublicService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void sendReqToGameNode(GeneratedMessage message) {
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		this.sendMesToRemoteNode(service.getGameNode(), message);
	}
	
	public void sendResToClient(String account, GeneratedMessage message) {
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(account);
		this.sendMesToRemoteNode(remoteNode, message);
	}
	
	public void sendMesToRemoteNode(RemoteNode remoteNode, GeneratedMessage message) {
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, message);
	}
	
	public void getTeamList(RemoteNode remoteNode, CWGetTeamListREQ req, int callback) {
		WGGetTeamListREQ.Builder sendBuilder = WGGetTeamListREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void getTeamListResult(GWGetTeamListRES res) {
		WCGetTeamListRES.Builder sendBuilder = WCGetTeamListRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.addAllTeamInfos(res.getTeamInfosList());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void getTeamInfo(RemoteNode remoteNode, CWGetTeamInfoREQ req, int callback) {
		WGGetTeamInfoREQ.Builder sendBuilder = WGGetTeamInfoREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamId(req.getTeamId());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void getTeamInfoResult(GWGetTeamInfoRES res) {
		WCGetTeamInfoRES.Builder sendBuilder = WCGetTeamInfoRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setTeamInfo(res.getTeamInfo());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void joinTeam(RemoteNode remoteNode, CWJoinTeamREQ req, int callback) {
		WGJoinTeamREQ.Builder sendBuilder = WGJoinTeamREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamId(req.getTeamId());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void joinTeamResult(GWJoinTeamRES res) {
		WCJoinTeamRES.Builder sendBuilder = WCJoinTeamRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void quickJoinTeam(RemoteNode remoteNode, CWQuickJoinTeamREQ req, int callback) {
		WGQuickJoinTeamREQ.Builder sendBuilder = WGQuickJoinTeamREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		this.sendReqToGameNode(sendBuilder.build()); 
	}
	
	public void quickJoinTeamResult(GWQuickJoinTeamRES res) {
		WCQuickJoinTeamRES.Builder sendBuilder = WCQuickJoinTeamRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void createTeam(RemoteNode remoteNode, CWCreateTeamREQ req, int callback) {
		WGCreateTeamREQ.Builder sendBuilder = WGCreateTeamREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamName(req.getTeamName());
		sendBuilder.setPassword(req.getPassword());
		sendBuilder.setMaxPlayerNum(req.getMaxPlayerNum());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void createTeamResult(GWCreateTeamRES res) {
		WCCreateTeamRES.Builder sendBuilder = WCCreateTeamRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		sendBuilder.setTeamId(res.getTeamId());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void modifyTeam(RemoteNode remoteNode, CWModifyTeamREQ req, int callback) {
		WGModifyTeamREQ.Builder sendBuilder = WGModifyTeamREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamId(req.getTeamId());
		sendBuilder.setTeamName(req.getTeamName());
		sendBuilder.setPassword(req.getPassword());
		sendBuilder.setMaxPlayerNum(req.getMaxPlayerNum());
		sendBuilder.setRequirePower(req.getRequirePower());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void modifyTeamResult(GWModifyTeamRES res) {
		WCModifyTeamRES.Builder sendBuilder = WCModifyTeamRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void kickTeamMember(RemoteNode remoteNode, CWKickTeamMemberREQ req, int callback) {
		WGKickTeamMemberREQ.Builder sendBuilder = WGKickTeamMemberREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setTeamId(req.getTeamId());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setKickAccount(req.getKickAccount());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void kickTeamMemberResult(GWKickTeamMemberRES res) {
		WCKickTeamMemberRES.Builder sendBuilder = WCKickTeamMemberRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}

	public void changeTeamPlayerStatus(RemoteNode remoteNode, CWChangeTeamPlayerStatusREQ req, int callback) {
		WGChangeTeamPlayerStatusREQ.Builder sendBuilder = WGChangeTeamPlayerStatusREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamId(req.getTeamId());
		sendBuilder.setChangeStatus(req.getChangeStatus());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void changeTeamPlayerStatusResult(GWChangeTeamPlayerStatusRES res) {
		WCChangeTeamPlayerStatusRES.Builder sendBuilder = WCChangeTeamPlayerStatusRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void startTeamBattle(RemoteNode remoteNode, CWStartTeamBattleREQ req, int callback) {
		WGStartTeamBattleREQ.Builder sendBuilder = WGStartTeamBattleREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamId(req.getTeamId());
		this.sendReqToGameNode(sendBuilder.build());
	} 
	
	public void startTeamBattleResult(GWStartTeamBattleRES res) {
		WCStartTeamBattleRES.Builder sendBuilder = WCStartTeamBattleRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void leaveTeam(RemoteNode remoteNode, CWLeaveTeamREQ req, int callback) {
		WGLeaveTeamREQ.Builder sendBuilder = WGLeaveTeamREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		sendBuilder.setTeamId(req.getTeamId());
		this.sendReqToGameNode(sendBuilder.build());
	}
	
	public void leaveTeamResult(GWLeaveTeamRES res) {
		WCLeaveTeamRES.Builder sendBuilder = WCLeaveTeamRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void startSingleBattle(RemoteNode remoteNode, CWStartSingleBattleREQ req, int callback) {
		WGStartSingleBattleREQ.Builder sendBuilder = WGStartSingleBattleREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMissionId(req.getMissionId());
		this.sendReqToGameNode(sendBuilder.build());
	} 
	
	public void startSingleBattleResult(GWStartSingleBattleRES res) {
		WCStartSingleBattleRES.Builder sendBuilder = WCStartSingleBattleRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
	
	public void finishBattle(GWFinishBattleRES res) {
		WCFinishBattleRES.Builder sendBuilder = WCFinishBattleRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setMissionInfo(res.getMissionInfo());
		sendBuilder.addAllMonstersDropItems(res.getMonstersDropItemsList());
		sendBuilder.addAllGamecopyDropItems(res.getGamecopyDropItemsList());
		sendBuilder.addAllAchievementDropItems(res.getAchievementDropItemsList());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
}
