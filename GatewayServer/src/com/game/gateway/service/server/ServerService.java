package com.game.gateway.service.server;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.game.common.Transmitter;
import com.game.common.config.BaseServerConfig;
import com.game.common.constants.GlobalConstants;
import com.game.common.constants.ServerType;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.gateway.start.NetInitializer;
import com.game.message.proto.server.ServerProtoBuf.BattleInfoPROTO;
import com.game.message.proto.server.ServerProtoBuf.SSRegisterServerREQ;

/**
 * 目前是区列表管理，以后有需要的功能在加
 * @author cuichao
 */
public class ServerService extends PublicService
{
	private static final long serialVersionUID = -8272027997288462700L;

	private NetInitializer netInitializer;
	private List<BattleInfoPROTO> battles = new ArrayList<BattleInfoPROTO>();
	private RemoteNode gameNode;
	private ConcurrentHashMap<Integer, RemoteNode> battleNodes = new ConcurrentHashMap<Integer, RemoteNode>(); // <ServerID, BattleNode>
	private BaseServerConfig serverConfig;
	private ConcurrentHashMap<Integer, Integer> roomBattles = new ConcurrentHashMap<Integer, Integer>(); // <RoomID, ServerID>
	
	public BaseServerConfig getServerConfig()
	{
		return serverConfig;
	}
	
	public void setRoomBattles(int room, int serverID)
	{
		serverID %= 100;
		roomBattles.put(room, serverID);
	}
	
	public void removeRoomBattles(int room)
	{
		roomBattles.remove(room);
	}
	
	/*
	 * 获得room对应的BattleNode
	 */
	public RemoteNode getRoomNode(int room)
	{
		Integer serverID = roomBattles.get(room);
		if (serverID == null)
			return null;
		return battleNodes.get(serverID);
	}

	public void setServerConfig(BaseServerConfig config)
	{
		serverConfig = config;
	}

	public void setNetInitializer(NetInitializer netInitializer)
    {
	    this.netInitializer = netInitializer;
    }
	
	public NetInitializer getNetInitializer()
	{
		return netInitializer;
	}
	
	public final List<BattleInfoPROTO> getBattles()
	{
		return battles;
	}
	
	public void updateBattle(BattleInfoPROTO battle)
	{
		if(!battleNodes.containsKey(battle.getServerID() % 100))
			battles.add(battle);
	}
	
	public void updateGameNode(RemoteNode node)
	{
		gameNode = node;
	}
	
	public RemoteNode getGameNode()
	{
		return gameNode;
	}
	
	public void setRemoteNode(RemoteNode node)
	{
		Boolean isBattle = false;
		for (BattleInfoPROTO battle : battles)
		{
			if (node.getAddress().equals(new InetSocketAddress(battle.getIpForServer(), battle.getPortForServer())))
			{
				node.setServerID(battle.getServerID());
				battleNodes.put(battle.getServerID() % 100, node);
				isBattle = true;
			}
		}
		if (!isBattle)
			gameNode = node;
		SSRegisterServerREQ.Builder sendBuilder = SSRegisterServerREQ.newBuilder();
	    sendBuilder.setServerName(serverConfig.getServerName());
	    sendBuilder.setServerID(serverConfig.getServerID());
	    sendBuilder.setServerType(ServerType.GATEWAY_SERVER);
	    Transmitter.getInstance().write(node, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());
	}
	
	public RemoteNode getBattleNode(int serverID)
	{
		return battleNodes.get(serverID % 100);
	}
	
	public ConcurrentHashMap<Integer, RemoteNode> getBattleNodes()
	{
		return battleNodes;
	}
}