package com.game.gateway.service.item;

import com.google.protobuf.GeneratedMessage;
import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.item.ItemProtoBuf.GWSynItemInfoRES;
import com.game.message.proto.item.ItemProtoBuf.WCSynItemInfoRES;

public class ItemService extends PublicService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void sendResToClient(String account, GeneratedMessage message) {
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(account);
		this.sendMesToRemoteNode(remoteNode, message);
	}
	
	public void sendMesToRemoteNode(RemoteNode remoteNode, GeneratedMessage message) {
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, message);
	}
	
	public void pushSynItemInfo(GWSynItemInfoRES res) {
		WCSynItemInfoRES.Builder sendBuilder = WCSynItemInfoRES.newBuilder();
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.addAllItemInfos(res.getItemInfosList());
		sendBuilder.setSynSource(res.getSynSource());
		this.sendResToClient(res.getAccount(), sendBuilder.build());
	}
}
