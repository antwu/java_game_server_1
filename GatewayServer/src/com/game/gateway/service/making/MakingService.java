package com.game.gateway.service.making;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.making.MakingProtoBuf.*;

public class MakingService extends PublicService{

	public void makeEquipmentRequest(RemoteNode remoteNode, CWMakeEquipmentREQ req, int callback) {
		WGMakeEquipmentREQ.Builder sendBuilder = WGMakeEquipmentREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMakingId(req.getMakingId());
		sendBuilder.setOperate(req.getOperate());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());	
	}

	public void makeEquipmentResult(GWMakeEquipmentRES res) {
		WCMakeEquipmentRES.Builder sendBuilder = WCMakeEquipmentRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		sendBuilder.setOperate(res.getOperate());
		sendBuilder.setMakingId(res.getMakingId());
		if(res.getMakingInfosList()!=null) {
			sendBuilder.addAllMakingInfos(res.getMakingInfosList());
		}
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());			
	}

	public void speedEquipmentRequest(RemoteNode remoteNode, CWSpeedEquipmentREQ req, int callback) {
		WGSpeedEquipmentREQ.Builder sendBuilder = WGSpeedEquipmentREQ.newBuilder();
		sendBuilder.setAccount(req.getAccount());
		sendBuilder.setMakingId(req.getMakingId());
		sendBuilder.addAllSpeedInfos(req.getSpeedInfosList());
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(service.getGameNode(), GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());	
	}

	public void speedEquipmentResult(GWSpeedEquipmentRES res) {
		WCSpeedEquipmentRES.Builder sendBuilder = WCSpeedEquipmentRES.newBuilder();
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode remoteNode = accountService.getRemoteNode(res.getAccount());
		sendBuilder.setAccount(res.getAccount());
		sendBuilder.setResult(res.getResult());
		sendBuilder.setMakingId(res.getMakingId());
		if(res.getMakingInfosList()!=null) {
			sendBuilder.addAllMakingInfos(res.getMakingInfosList());
		}
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, sendBuilder.build());			
	}
}
