package com.game.gateway.service.update;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.message.proto.server.ServerProtoBuf.SSUpdateConfigSYN;
import com.game.message.proto.server.ServerProtoBuf.UpdateConfigPROTO;

public class UpdateService extends PublicService
{
	private static final long serialVersionUID = 7519332241403817783L;
	private ConcurrentHashMap<Integer, HashMap<String, Object>> updateConfig = new ConcurrentHashMap<Integer, HashMap<String, Object>>();
	
	public void reloadUpdateConfig(RemoteNode remoteNode, SSUpdateConfigSYN message, int callback)
	{
		for (UpdateConfigPROTO update : message.getUpdatesList())
		{
			HashMap<String, Object> config = new HashMap<String, Object>();
			config.put("channel", update.getChannelID());
			config.put("resourceVersion", update.getResourceVersion());
			config.put("resourceUrl", update.getResourceUrl());
			config.put("packageVersion", update.getPackageVersion());
			config.put("packageUrl", update.getPackageUrl());
			config.put("loginType", update.getLoginType());
			updateConfig.put(update.getChannelID(), config);
		}
	}
	
}
