package com.game.gateway.service.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.account.TokenSession;
import com.game.message.proto.room.RoomProtoBuf.CWRoomCreateREQ;
import com.game.message.proto.room.RoomProtoBuf.CWRoomEnterREQ;
import com.game.message.proto.room.RoomProtoBuf.WGRoomCreateREQ;
import com.game.message.proto.room.RoomProtoBuf.WGRoomDestoryREQ;
import com.game.message.proto.room.RoomProtoBuf.WGRoomEnterREQ;

public class RoomService extends PublicService
{
	private static final long serialVersionUID = -9203951844097303774L;
	
	private final Logger logger = LoggerFactory.getLogger(RoomService.class);
	
	public WGRoomCreateREQ createRoom(RemoteNode node, CWRoomCreateREQ req)
	{
		WGRoomCreateREQ.Builder builder = WGRoomCreateREQ.newBuilder();
		
		builder.setAccountID(node.getAccountID());
		builder.setID(node.getID());
		builder.setType(req.getType());
		builder.setCount(req.getCount());
		builder.addAllRules(req.getRulesList());
		builder.setIp(node.getAddress().getAddress().getHostAddress());
		return builder.build();
	}
	
	public WGRoomEnterREQ enterRoom(RemoteNode node, CWRoomEnterREQ req)
	{
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		TokenSession tokenSession = accountService.getTokenSession(node.getAccountID());
		 
		WGRoomEnterREQ.Builder builder = WGRoomEnterREQ.newBuilder();
		builder.setAccountID(node.getAccountID());
		builder.setID(node.getID());
		builder.setRoomID(req.getRoom());
		builder.setHead(tokenSession.getHead());
		builder.setName(tokenSession.getName());
		builder.setSex(tokenSession.getSex());
		builder.setIp(node.getAddress().getAddress().getHostAddress());
		return builder.build(); 
	}
	
	
	public WGRoomDestoryREQ destoryRoom(RemoteNode node)
	{
		WGRoomDestoryREQ.Builder builder = WGRoomDestoryREQ.newBuilder();
		builder.setAccountID(node.getAccountID());
		builder.setRoomID(node.getRoomID());
		
		return builder.build();
	}

}
