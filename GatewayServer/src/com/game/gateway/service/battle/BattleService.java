package com.game.gateway.service.battle;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.PublicService;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.battle.BattleProtoBuf.CWCancleOperateREQ;
import com.game.message.proto.battle.BattleProtoBuf.CWChangeWeaponREQ;
import com.game.message.proto.battle.BattleProtoBuf.CWEnterBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.CWReadyBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.CWSearchPathREQ;
import com.game.message.proto.battle.BattleProtoBuf.CWStartBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.CWUseShieldREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBCancleOperateREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBChangeWeaponREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBEnterBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBReadyBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBSearchPathREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBStartBattleREQ;
import com.game.message.proto.battle.BattleProtoBuf.WBUseShieldREQ;

public class BattleService extends PublicService
{
	private static final long serialVersionUID = 1874622515568988238L;
	public void searchPath(CWSearchPathREQ message ,int callback) {
		WBSearchPathREQ.Builder msg = WBSearchPathREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(message.getAccount());
		msg.setStart(message.getStart());
		msg.setTargetId(message.getTargetId());
		msg.setSearchType(message.getSearchType());
		if(message.hasModifyNode()){
			msg.setModifyNode(message.getModifyNode());
		}
		if(message.hasStep()){
			msg.setStep(message.getStep());
		}
		if(message.hasDesNode()){
			msg.setDesNode(message.getDesNode());
		}
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}
	
	public void enterBattle(CWEnterBattleREQ message, int callback) {
		WBEnterBattleREQ.Builder msg = WBEnterBattleREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(message.getAccount());
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}
	
	
	public void startBattle(CWStartBattleREQ message, int callback) {
		WBStartBattleREQ.Builder msg = WBStartBattleREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(message.getAccount());
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}

	/**
	 * 玩家部署完毕 点击准备
	 * @param account
	 */
	public void readyBattle(CWReadyBattleREQ message, int callback) {
		WBReadyBattleREQ.Builder msg = WBReadyBattleREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(message.getAccount());
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}
	
	/**
	 * 点击撤回操作
	 * @param message
	 * @param callback
	 */
	public void cancleOperate(CWCancleOperateREQ message, int callback) {
		WBCancleOperateREQ.Builder msg = WBCancleOperateREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(message.getAccount());
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}
	
	/**
	 * 战斗服换武器 （游戏服需要换的时候再起名吧）
	 * @param message
	 * @param callback
	 */
	public void changeWeapon(CWChangeWeaponREQ message, int callback) {
		WBChangeWeaponREQ.Builder msg = WBChangeWeaponREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(message.getAccount());
		msg.setWeaponCfgId(message.getWeaponCfgId());
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}
	
	/**
	 * 使用护盾技能
	 * @param message
	 * @param callback
	 */
	public void useShield(String account,CWUseShieldREQ message, int callback) {
		WBUseShieldREQ.Builder msg = WBUseShieldREQ.newBuilder();
		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		msg.setAccount(account);
		
		RemoteNode remoteNode = service.getBattleNode(2000);
		if(remoteNode != null)
			Transmitter.getInstance().write(remoteNode, callback, msg.build());
	}
}
