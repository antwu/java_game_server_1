package com.game.gateway.listener;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ListenerAnnotation
{
	Class<?> eventClass();
	Class<?> listenerClass();
	int level() default 15;
}

