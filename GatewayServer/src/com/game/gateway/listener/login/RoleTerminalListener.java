package com.game.gateway.listener.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kodgames.core.event.IEvent;
import com.kodgames.core.event.IEventListener;
import com.game.core.net.common.RemoteNode;
import com.game.core.service.ServiceContainer;
import com.game.gateway.event.login.ITerminateEvent;
import com.game.gateway.listener.ListenerAnnotation;
import com.game.gateway.service.battle.BattleService;

@ListenerAnnotation(eventClass = ITerminateEvent.class, listenerClass = RoleTerminalListener.class)
public class RoleTerminalListener implements IEventListener
{
	static private Logger logger = LoggerFactory.getLogger(RoleTerminalListener.class);
	@Override
	public void trigger(Class<? extends IEvent> event, Object[] args)
	{
		RemoteNode remoteNode = (RemoteNode)args[0];
		BattleService battleService = ServiceContainer.getInstance().getPublicService(BattleService.class);
//		battleService.playerTerminal(remoteNode);
		logger.debug("Remote Node {} has terminal", remoteNode.getAccountID());
	}

}
