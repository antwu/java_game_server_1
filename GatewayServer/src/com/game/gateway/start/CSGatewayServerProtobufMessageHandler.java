package com.game.gateway.start;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.GeneratedMessage;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.AbstractMessageInitializer;
import com.game.core.net.handler.CSProtobufMessageHandler;
import com.game.core.service.AbstractMessageService;

public abstract class CSGatewayServerProtobufMessageHandler<S extends AbstractMessageService, T extends GeneratedMessage>
		extends CSProtobufMessageHandler<S, T>
{
	private static Logger logger = LoggerFactory.getLogger(CSGatewayServerProtobufMessageHandler.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, int protocolID, int callback, T message)
	{
		if (remoteNode == null)
		{
			logger.error("Handler Message Error, remoteNode is null, protocolID is " + protocolID);
			return;
		}

		if(AbstractMessageInitializer.getProtocolPrivilege(protocolID) && remoteNode.getID() == 0)
		{
			logger.error("handleMessage: player must login for handle this message. address is " + remoteNode.getAddress() + 
					", protocol id is " + protocolID + ", message name is " + message.getClass().getSimpleName());
			return;
		}
		
		super.handleMessage(remoteNode, protocolID, callback, message);
	}

}
