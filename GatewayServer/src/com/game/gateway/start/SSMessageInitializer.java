package com.game.gateway.start;

import com.game.common.Project4MessageInitializer;
import com.game.gateway.net.server.SSConnectionHandler;

public class SSMessageInitializer extends Project4MessageInitializer
{
	public SSMessageInitializer(String actionPackageName)
    {
	    super(actionPackageName);
    }

	@Override
    protected void initMessages() throws Exception
    {
		super.initMessages();
	    SSConnectionHandler ssConnectionHandler = new SSConnectionHandler();
	    setConnectionActiveHandler(ssConnectionHandler);
	    setConnectionInactiveHandler(ssConnectionHandler);
    }
}
