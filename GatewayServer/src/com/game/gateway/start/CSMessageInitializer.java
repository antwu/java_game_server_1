package com.game.gateway.start;

import com.game.common.Project4MessageInitializer;
import com.game.gateway.net.server.CSConnectionHandler;

public class CSMessageInitializer extends Project4MessageInitializer
{

	public CSMessageInitializer(String actionPackageName)
    {
	    super(actionPackageName);
    }

	@Override
    protected void initMessages() throws Exception
    {
		super.initMessages();
		CSConnectionHandler csConnectionHandler = new CSConnectionHandler();
	    setConnectionActiveHandler(csConnectionHandler);
	    setConnectionInactiveHandler(csConnectionHandler);
    }

}
