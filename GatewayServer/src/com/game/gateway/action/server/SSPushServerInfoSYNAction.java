package com.game.gateway.action.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.config.BaseServerConfig;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.server.ServerProtoBuf.SSPushServerInfoSYN;

@ActionAnnotation(actionClass = SSPushServerInfoSYNAction.class, messageClass = SSPushServerInfoSYN.class, serviceClass = ServerService.class)
public class SSPushServerInfoSYNAction  extends SSProtobufMessageHandler<ServerService, SSPushServerInfoSYN>
{
	final static Logger logger = LoggerFactory.getLogger(SSPushServerInfoSYNAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, ServerService service, SSPushServerInfoSYN message, int callback)
	{
		BaseServerConfig config = service.getServerConfig();
		config.fromProtoBuf(message.getServerConfig());
		service.getNetInitializer().openPort4Client(config.getPort4Client());
		logger.info("Gateway server is ready!!! servername:{} ", config.getServerName());
	}
}