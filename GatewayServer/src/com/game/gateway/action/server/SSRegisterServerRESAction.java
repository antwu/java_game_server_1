package com.game.gateway.action.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.server.ServerProtoBuf.SSRegisterServerRES;

@ActionAnnotation(actionClass = SSRegisterServerRESAction.class, messageClass = SSRegisterServerRES.class, serviceClass = ServerService.class)
public class SSRegisterServerRESAction extends SSProtobufMessageHandler<ServerService, SSRegisterServerRES>
{
	private static Logger logger = LoggerFactory.getLogger(SSRegisterServerRESAction.class);
	@Override
    public void handleMessage(RemoteNode remoteNode, ServerService service, SSRegisterServerRES message, int callback)
    {
		logger.info("result : {}", message.getResult());
    }
}
