package com.game.gateway.action.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.server.ServerService;
import com.game.message.proto.server.ServerProtoBuf.BattleInfoPROTO;
import com.game.message.proto.server.ServerProtoBuf.SSPushBattleListSYN;

@ActionAnnotation(actionClass = SSPushBattleListSYNAction.class, messageClass = SSPushBattleListSYN.class, serviceClass = ServerService.class)
public class SSPushBattleListSYNAction extends SSProtobufMessageHandler<ServerService, SSPushBattleListSYN>
{
	final static Logger logger = LoggerFactory.getLogger(SSPushBattleListSYNAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, ServerService service, SSPushBattleListSYN message, int callback)
	{
		for(BattleInfoPROTO battleInfo : message.getBattlesList())
		{
			service.updateBattle(battleInfo);
			logger.info("serverid:{} serverip:{} serverport:{}", battleInfo.getServerID(), battleInfo.getIpForServer(), battleInfo.getPortForServer());
		}
		service.getNetInitializer().connectToBattles();
	}
}
