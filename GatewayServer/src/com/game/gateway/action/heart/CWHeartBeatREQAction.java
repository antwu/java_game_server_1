package com.game.gateway.action.heart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.server.ServerService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.heart.HeartProtoBuf.CWHeartBeatREQ;
import com.game.message.proto.heart.HeartProtoBuf.WCHeartBeatRES;

@ActionAnnotation(actionClass = CWHeartBeatREQAction.class, messageClass = CWHeartBeatREQ.class, serviceClass = ServerService.class)
public class CWHeartBeatREQAction extends CSGatewayServerProtobufMessageHandler<ServerService, CWHeartBeatREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWHeartBeatREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, ServerService service, CWHeartBeatREQ message, int callback)
	{
		//logger.debug("CWHeartBeatREQAction, accountID={}, ip={}", remoteNode.getAccountID(), remoteNode.getAddress());
		WCHeartBeatRES.Builder builder = WCHeartBeatRES.newBuilder();
		builder.setCurrentTime(System.currentTimeMillis());
		Transmitter.getInstance().write(remoteNode, callback, builder.build());
	}
}