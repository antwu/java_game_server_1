package com.game.gateway.action.item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.item.ItemService;
import com.game.message.proto.item.ItemProtoBuf.GWSynItemInfoRES;

@ActionAnnotation(actionClass = GWSynItemInfoRESAction.class, messageClass = GWSynItemInfoRES.class, serviceClass = ItemService.class)
public class GWSynItemInfoRESAction extends SSProtobufMessageHandler<ItemService, GWSynItemInfoRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWSynItemInfoRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, ItemService service, GWSynItemInfoRES res, int callback) {
		logger.info("account={}, itemInfo={} ", res.getAccount(), res.getItemInfosList().toString());
		service.pushSynItemInfo(res);
	}

}
