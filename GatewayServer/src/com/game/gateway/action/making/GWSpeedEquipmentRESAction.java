package com.game.gateway.action.making;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.making.MakingService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.making.MakingProtoBuf.GWSpeedEquipmentRES;

@ActionAnnotation(actionClass = GWSpeedEquipmentRESAction.class, messageClass = GWSpeedEquipmentRES.class, serviceClass = MakingService.class)
public class GWSpeedEquipmentRESAction extends CSGatewayServerProtobufMessageHandler<MakingService, GWSpeedEquipmentRES>{
	final static Logger logger = LoggerFactory.getLogger(GWSpeedEquipmentRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, MakingService service, GWSpeedEquipmentRES message,
			int callback) {
		logger.info("account={}, result={} getMakingInfosCount={}", message.getAccount(), message.getResult(), message.getMakingInfosCount());
		service.speedEquipmentResult(message);
	}

}
