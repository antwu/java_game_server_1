package com.game.gateway.action.making;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.making.MakingService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.making.MakingProtoBuf.CWMakeEquipmentREQ;

@ActionAnnotation(actionClass = CWMakeEquipmentREQAction.class, messageClass = CWMakeEquipmentREQ.class, serviceClass = MakingService.class)
public class CWMakeEquipmentREQAction extends CSGatewayServerProtobufMessageHandler<MakingService, CWMakeEquipmentREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWMakeEquipmentREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, MakingService service, CWMakeEquipmentREQ message,
			int callback) {
		logger.info("account={}, getMakingId={} ,getOperate={}", message.getAccount(),message.getMakingId(),message.getOperate());
		service.makeEquipmentRequest(remoteNode, message, callback);
	}

}
