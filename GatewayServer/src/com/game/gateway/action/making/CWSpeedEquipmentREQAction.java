package com.game.gateway.action.making;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.making.MakingService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.making.MakingProtoBuf.CWSpeedEquipmentREQ;

@ActionAnnotation(actionClass = CWSpeedEquipmentREQAction.class, messageClass = CWSpeedEquipmentREQ.class, serviceClass = MakingService.class)
public class CWSpeedEquipmentREQAction extends CSGatewayServerProtobufMessageHandler<MakingService, CWSpeedEquipmentREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWSpeedEquipmentREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, MakingService service, CWSpeedEquipmentREQ message,
			int callback) {
		logger.info("account={}, getMakingId={}", message.getAccount(),message.getMakingId());
		service.speedEquipmentRequest(remoteNode, message, callback);
	}

}
