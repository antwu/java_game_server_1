package com.game.gateway.action.encrypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.encrypt.EncryptService;
import com.game.message.proto.encrypt.EncryptProtoBuf.GWEncryptKeySYN;
import com.game.message.proto.encrypt.EncryptProtoBuf.WCSendYRES;

@ActionAnnotation(actionClass = GWEncryptKeySYNAction.class, messageClass = GWEncryptKeySYN.class, serviceClass = EncryptService.class)
public class GWEncryptKeySYNAction extends SSProtobufMessageHandler<EncryptService, GWEncryptKeySYN>
{
	final static Logger logger = LoggerFactory.getLogger(GWEncryptKeySYNAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, EncryptService service, GWEncryptKeySYN message, int callback)
	{
		logger.info("accountID={}", message.getAccountID());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		RemoteNode clientNode = accountService.getRemoteNode(message.getAccountID());
		if(clientNode == null)
		{
			logger.debug("accountID={}, remoteNode is null", message.getAccountID());
			return;
		}
			
		WCSendYRES res = service.sendYQtoClient(clientNode);
		if(res == null)
		{
			logger.debug("accountID={}, wcSendYREs is null", message.getAccountID());
			return;
		}
		
		Transmitter.getInstance().write(clientNode, callback, res);
	}
	
	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWEncryptKeySYN message)
	{
		return message.getAccountID();
	}
}