package com.game.gateway.action.encrypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.encrypt.EncryptService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.encrypt.EncryptProtoBuf.CWUseNewKeyRES;

@ActionAnnotation(actionClass = CWUseNewKeyRESAction.class, messageClass = CWUseNewKeyRES.class, serviceClass = EncryptService.class)
public class CWUseNewKeyRESAction extends CSGatewayServerProtobufMessageHandler<EncryptService, CWUseNewKeyRES>
{
	final static Logger logger = LoggerFactory.getLogger(CWUseNewKeyRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, EncryptService service, CWUseNewKeyRES message, int callback)
	{
		logger.info(" getAccountID={} ",remoteNode.getAccountID());
	}
}