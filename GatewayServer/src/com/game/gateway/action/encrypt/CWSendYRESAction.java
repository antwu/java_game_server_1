package com.game.gateway.action.encrypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.encrypt.EncryptService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.encrypt.EncryptProtoBuf.CWSendYRES;
import com.game.message.proto.encrypt.EncryptProtoBuf.WCUseNewKeyRES;

@ActionAnnotation(actionClass = CWSendYRESAction.class, messageClass = CWSendYRES.class, serviceClass = EncryptService.class)
public class CWSendYRESAction extends CSGatewayServerProtobufMessageHandler<EncryptService, CWSendYRES>
{
	final static Logger logger = LoggerFactory.getLogger(CWSendYRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, EncryptService service, CWSendYRES message, int callback)
	{
		logger.info("accountID={} clientY={}",remoteNode.getAccountID(), message.getClientY());
		WCUseNewKeyRES res = service.computeNewKey(remoteNode, message.getClientY());
		Transmitter.getInstance().write(remoteNode, callback, res);
	}
}