package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWReadyBattleRES;
import com.game.message.proto.battle.BattleProtoBuf.WCReadyBattleRES;

@ActionAnnotation(actionClass = BWReadyBattleRESAction.class, messageClass = BWReadyBattleRES.class, serviceClass = BattleService.class)
public class BWReadyBattleRESAction extends SSProtobufMessageHandler<BattleService, BWReadyBattleRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWReadyBattleRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWReadyBattleRES message, int callback) 
	{
		logger.info("getAccount={} ",message.getAccount());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCReadyBattleRES.Builder builder = WCReadyBattleRES.newBuilder();
		builder.setAccount(message.getAccount());
		builder.setOperFightUnitInfo(message.getOperFightUnitInfo());
		builder.setAccount(message.getOperAccount());
		String account = message.getAccount();
		remoteNode = accountService.getRemoteNode(account);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		for (String account : accountService.tokenSessions.keySet()) {
//			remoteNode = accountService.getRemoteNode(account);
//			
//			Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		}
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWReadyBattleRES message)
	{
		return null;
	}
}
