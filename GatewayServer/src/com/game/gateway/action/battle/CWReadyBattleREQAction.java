package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWReadyBattleREQ;

@ActionAnnotation(actionClass = CWReadyBattleREQAction.class, messageClass = CWReadyBattleREQ.class, serviceClass = BattleService.class)
public class CWReadyBattleREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWReadyBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWReadyBattleREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWReadyBattleREQ message, int callback) {
//		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
//		WCReadyBattleRES.Builder builder = WCReadyBattleRES.newBuilder();
//		builder.setAccount(message.getAccount());
//		for (String account : accountService.tokenSessions.keySet()) {
//			remoteNode = accountService.getRemoteNode(account);
//			Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		}
		logger.info("getAccount={} ", message.getAccount());
		service.readyBattle(message,callback);
	}
}
