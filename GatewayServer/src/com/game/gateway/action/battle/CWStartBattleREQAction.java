package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWStartBattleREQ;

@ActionAnnotation(actionClass = CWStartBattleREQAction.class, messageClass = CWStartBattleREQ.class, serviceClass = BattleService.class)
public class CWStartBattleREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWStartBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWStartBattleREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWStartBattleREQ message, int callback) {
		logger.info("getAccount={} ", message.getAccount());
		service.startBattle(message,callback); 
	}


}
