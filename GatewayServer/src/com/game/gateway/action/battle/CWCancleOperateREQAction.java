package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWCancleOperateREQ;

@ActionAnnotation(actionClass = CWCancleOperateREQAction.class, messageClass = CWCancleOperateREQ.class, serviceClass = BattleService.class)
public class CWCancleOperateREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWCancleOperateREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWCancleOperateREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWCancleOperateREQ message, int callback) {
		logger.info("getAccount={} ", message.getAccount());
		service.cancleOperate(message,callback);
	}
}
