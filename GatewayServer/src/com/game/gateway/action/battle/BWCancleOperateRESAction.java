package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWCancleOperateRES;
import com.game.message.proto.battle.BattleProtoBuf.WCCancleOperateRES;

@ActionAnnotation(actionClass = BWCancleOperateRESAction.class, messageClass = BWCancleOperateRES.class, serviceClass = BattleService.class)
public class BWCancleOperateRESAction extends SSProtobufMessageHandler<BattleService, BWCancleOperateRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWCancleOperateRESAction.class);
	
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWCancleOperateRES message, int callback) 
	{
		logger.info("getAccount={}, result={} ", message.getAccount(), message.getResult());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCCancleOperateRES.Builder builder = WCCancleOperateRES.newBuilder();
		builder.setOperAccount(message.getOperAccount());
		builder.setResult(message.getResult());
		if(message.hasPos()){
			builder.setPos(message.getPos());
		}
		builder.addAllFightUnits(message.getFightUnitsList());
		builder.setStep(message.getStep());
		builder.addAllLastEffectedUnits(message.getLastEffectedUnitsList());
		remoteNode = accountService.getRemoteNode(message.getAccount());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWCancleOperateRES message)
	{
		return null;
	}
}
