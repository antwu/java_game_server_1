package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWChangeWeaponREQ;

@ActionAnnotation(actionClass = CWChangeWeaponREQAction.class, messageClass = CWChangeWeaponREQ.class, serviceClass = BattleService.class)
public class CWChangeWeaponREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWChangeWeaponREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWChangeWeaponREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWChangeWeaponREQ message, int callback) {
		logger.info("getAccount={} ", message.getAccount());
		service.changeWeapon(message,callback); 
	}


}
