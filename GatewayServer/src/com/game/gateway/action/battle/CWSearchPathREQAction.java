package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWSearchPathREQ;

@ActionAnnotation(actionClass = CWSearchPathREQAction.class, messageClass = CWSearchPathREQ.class, serviceClass = BattleService.class)
public class CWSearchPathREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWSearchPathREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWSearchPathREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWSearchPathREQ message, int callback) {
		logger.info("getAccount={} ", message.getAccount());
		service.searchPath(message,callback);
	}


}
