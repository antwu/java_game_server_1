package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWStartBattleRES;
import com.game.message.proto.battle.BattleProtoBuf.WCStartBattleRES;

@ActionAnnotation(actionClass = BWStartBattleRESAction.class, messageClass = BWStartBattleRES.class, serviceClass = BattleService.class)
public class BWStartBattleRESAction extends SSProtobufMessageHandler<BattleService, BWStartBattleRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWStartBattleRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWStartBattleRES message, int callback) 
	{
		logger.info("getAccount={} ", message.getAccount());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCStartBattleRES.Builder builder = WCStartBattleRES.newBuilder();
		builder.addAllBattleSteps(message.getBattleStepsList());
		if(message.hasStageInfo()){
			builder.setStageInfo(message.getStageInfo());
		}
		if(message.hasGameOverInfo()){
			builder.setGameOverInfo(message.getGameOverInfo());
		}
		if(message.hasOperFightUnitInfo()){
			builder.setOperFightUnitInfo(message.getOperFightUnitInfo());
		}
		builder.addAllFightUnits(message.getFightUnitsList());
		builder.addAllMonsterDropItems(message.getMonsterDropItemsList());
		builder.addAllAchieveCondition(message.getAchieveConditionList());
		String account = message.getAccount();
		remoteNode = accountService.getRemoteNode(account);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		for (String account : accountService.tokenSessions.keySet()) {
//			remoteNode = accountService.getRemoteNode(account);
//			
//			Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		}
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWStartBattleRES message)
	{
		return null;
	}
}
