package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWEnterBattleRES;
import com.game.message.proto.battle.BattleProtoBuf.WCEnterBattleRES;

@ActionAnnotation(actionClass = BWEnterBattleRESAction.class, messageClass = BWEnterBattleRES.class, serviceClass = BattleService.class)
public class BWEnterBattleRESAction extends SSProtobufMessageHandler<BattleService, BWEnterBattleRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWEnterBattleRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWEnterBattleRES message, int callback) 
	{
		logger.info("getAccount={} ", message.getAccount());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCEnterBattleRES.Builder builder = WCEnterBattleRES.newBuilder();
		builder.addAllFightUnits(message.getFightUnitsList());
		builder.addAllEnemyObstacles(message.getEnemyObstaclesList());
		builder.setOperFightUnitInfo(message.getOperFightUnitInfo());
		builder.setMapIndex(message.getMapIndex());
		String account = message.getAccount();
		remoteNode = accountService.getRemoteNode(account);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		for (String account : accountService.tokenSessions.keySet()) {
//			remoteNode = accountService.getRemoteNode(account);
//			
//			Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		}
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWEnterBattleRES message)
	{
		return null;
	}
}
