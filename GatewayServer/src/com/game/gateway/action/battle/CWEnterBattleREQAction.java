package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWEnterBattleREQ;

@ActionAnnotation(actionClass = CWEnterBattleREQAction.class, messageClass = CWEnterBattleREQ.class, serviceClass = BattleService.class)
public class CWEnterBattleREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWEnterBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWEnterBattleREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWEnterBattleREQ message, int callback) {
		logger.info("getAccount={} ", message.getAccount());
		service.enterBattle(message,callback);
	}
}
