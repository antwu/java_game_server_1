package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWUseShieldRES;
import com.game.message.proto.battle.BattleProtoBuf.WCUseShieldRES;

@ActionAnnotation(actionClass = BWUseShieldRESAction.class, messageClass = BWUseShieldRES.class, serviceClass = BattleService.class)
public class BWUseShieldRESAction extends SSProtobufMessageHandler<BattleService, BWUseShieldRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWUseShieldRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWUseShieldRES message, int callback) 
	{
		logger.info("getAccount={} ", message.getAccount());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCUseShieldRES.Builder builder = WCUseShieldRES.newBuilder();
		builder.setOperAccount(message.getOperAccount());
		builder.setResult(message.getResult());
		builder.setStep(message.getStep());
		builder.addAllFightUnits(message.getFightUnitsList());
		builder.addAllEffectUnits(message.getEffectUnitsList());
		if(message.hasShieldInfo()){
			builder.setShieldInfo(message.getShieldInfo());
		}
		remoteNode = accountService.getRemoteNode(message.getAccount());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWUseShieldRES message)
	{
		return null;
	}
}
