package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWChangeWeaponRES;
import com.game.message.proto.battle.BattleProtoBuf.WCChangeWeaponRES;

@ActionAnnotation(actionClass = BWChangeWeaponRESAction.class, messageClass = BWChangeWeaponRES.class, serviceClass = BattleService.class)
public class BWChangeWeaponRESAction extends SSProtobufMessageHandler<BattleService, BWChangeWeaponRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWChangeWeaponRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWChangeWeaponRES message, int callback) 
	{
		logger.info("getAccount={}, getOperAccount={} ", message.getAccount(), message.getOperAccount());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCChangeWeaponRES.Builder builder = WCChangeWeaponRES.newBuilder();
		
		String operAccount = message.getOperAccount();
		builder.setOperAccount(operAccount);
		builder.setWeaponInfo(message.getWeaponInfo());
		remoteNode = accountService.getRemoteNode(message.getAccount());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		for (String account : accountService.tokenSessions.keySet()) {
//			remoteNode = accountService.getRemoteNode(account);
//			
//			Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		}
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWChangeWeaponRES message)
	{
		return null;
	}
}
