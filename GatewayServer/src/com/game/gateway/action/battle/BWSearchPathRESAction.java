package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.service.battle.BattleService;
import com.game.message.proto.battle.BattleProtoBuf.BWSearchPathRES;
import com.game.message.proto.battle.BattleProtoBuf.WCSearchPathRES;

@ActionAnnotation(actionClass = BWSearchPathRESAction.class, messageClass = BWSearchPathRES.class, serviceClass = BattleService.class)
public class BWSearchPathRESAction extends SSProtobufMessageHandler<BattleService, BWSearchPathRES>
{	
	final static Logger logger = LoggerFactory.getLogger(BWSearchPathRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service, BWSearchPathRES message, int callback) 
	{
		logger.info("getAccount={} ", message.getAccount());
		AccountService accountService = ServiceContainer.getInstance().getPublicService(AccountService.class);
		WCSearchPathRES.Builder builder = WCSearchPathRES.newBuilder();
		builder.setAccount(message.getAccount());
		builder.addAllPathNodes(message.getPathNodesList());
		builder.addAllLastEffectedUnits(message.getLastEffectedUnitsList());
		builder.setAttackTarget(message.getAttackTarget());
		builder.addAllEffectUnits(message.getEffectUnitsList());
		builder.setStep(message.getStep());
		builder.setAccount(message.getOperAccount());
		builder.setResult(message.getResult());
		builder.addAllFightUnits(message.getFightUnitsList());
		builder.addAllTriggeredTalents(message.getTriggeredTalentsList());
		builder.setSearchType(message.getSearchType());
		String account = message.getAccount();
		remoteNode = accountService.getRemoteNode(account);
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		for (String account : accountService.tokenSessions.keySet()) {
//			remoteNode = accountService.getRemoteNode(account);
//			
//			Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
//		}
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, BWSearchPathRES message)
	{
		return message.getAccount();
	}
}
