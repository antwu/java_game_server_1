package com.game.gateway.action.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.battle.BattleService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.battle.BattleProtoBuf.CWUseShieldREQ;

@ActionAnnotation(actionClass = CWUseShieldREQAction.class, messageClass = CWUseShieldREQ.class, serviceClass = BattleService.class)
public class CWUseShieldREQAction extends CSGatewayServerProtobufMessageHandler<BattleService, CWUseShieldREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWUseShieldREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, BattleService service,
			CWUseShieldREQ message, int callback) {
		logger.info("getAccount={} ", message.getAccount());
		service.useShield(remoteNode.getAccountID(),message,callback); 
	}


}
