package com.game.gateway.action.update;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.CSProtobufMessageHandler;
import com.game.gateway.service.update.UpdateService;
import com.game.message.proto.server.ServerProtoBuf.SSUpdateConfigSYN;

@ActionAnnotation(actionClass = SSUpdateConfigSYNAction.class, messageClass = SSUpdateConfigSYN.class, serviceClass = UpdateService.class)
public class SSUpdateConfigSYNAction extends CSProtobufMessageHandler<UpdateService, SSUpdateConfigSYN>
{
	final static Logger logger = LoggerFactory.getLogger(SSUpdateConfigSYNAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, UpdateService service, SSUpdateConfigSYN message, int callback)
	{
		logger.info("getAccountID={}",remoteNode.getAccountID());
		service.reloadUpdateConfig(remoteNode, message, callback);
	}
}
