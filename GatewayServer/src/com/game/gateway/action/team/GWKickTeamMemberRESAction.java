package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWKickTeamMemberRES;

@ActionAnnotation(actionClass = GWKickTeamMemberRESAction.class, messageClass = GWKickTeamMemberRES.class, serviceClass = TeamService.class)
public class GWKickTeamMemberRESAction extends SSProtobufMessageHandler<TeamService, GWKickTeamMemberRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWKickTeamMemberRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWKickTeamMemberRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.kickTeamMemberResult(res);;
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWKickTeamMemberRES message)
	{
		return message.getAccount();
	}
}
