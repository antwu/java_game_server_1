package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWQuickJoinTeamREQ;

@ActionAnnotation(actionClass = CWQuickJoinTeamREQAction.class, messageClass = CWQuickJoinTeamREQ.class, serviceClass = TeamService.class)
public class CWQuickJoinTeamREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWQuickJoinTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWQuickJoinTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWQuickJoinTeamREQ req,
			int callback) {
		logger.info("CWChangeTeamPlayerStatusREQAction handleMessage account={}, missionId={}", 
				req.getAccount(), req.getMissionId());
		service.quickJoinTeam(remoteNode, req, callback);
	}
}
