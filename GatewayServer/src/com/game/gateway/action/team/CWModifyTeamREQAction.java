package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWModifyTeamREQ;

@ActionAnnotation(actionClass = CWModifyTeamREQAction.class, messageClass = CWModifyTeamREQ.class, serviceClass = TeamService.class)
public class CWModifyTeamREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWModifyTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWModifyTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWModifyTeamREQ req,
			int callback) {
		logger.info("CWModifyTeamREQAction handleMessage account={},  teamId={}", 
				req.getAccount(), req.getTeamId());
		service.modifyTeam(remoteNode, req, callback);
	}
}
