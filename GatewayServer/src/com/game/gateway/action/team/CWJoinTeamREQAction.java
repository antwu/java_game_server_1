package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWJoinTeamREQ;

@ActionAnnotation(actionClass = CWJoinTeamREQAction.class, messageClass = CWJoinTeamREQ.class, serviceClass = TeamService.class)
public class CWJoinTeamREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWJoinTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWJoinTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWJoinTeamREQ req, int callback) {
		logger.info("account={}, missionId={}, teamId={} ", req.getAccount(), req.getMissionId(), req.getTeamId());
		service.joinTeam(remoteNode, req, callback);	
	}
}
