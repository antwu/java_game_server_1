package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWChangeTeamPlayerStatusREQ;

@ActionAnnotation(actionClass = CWChangeTeamPlayerStatusREQAction.class, messageClass = CWChangeTeamPlayerStatusREQ.class, serviceClass = TeamService.class)
public class CWChangeTeamPlayerStatusREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWChangeTeamPlayerStatusREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWChangeTeamPlayerStatusREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWChangeTeamPlayerStatusREQ req,
			int callback) {
		logger.info("account={}, missionId={}, teamId={}, changeStatus={}", 
				req.getAccount(), req.getMissionId(), req.getTeamId(), req.getChangeStatus());
		service.changeTeamPlayerStatus(remoteNode, req, callback);
	}
}
