package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWGetTeamInfoREQ;

@ActionAnnotation(actionClass = CWGetTeamInfoREQAction.class, messageClass = CWGetTeamInfoREQ.class, serviceClass = TeamService.class)
public class CWGetTeamInfoREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWGetTeamInfoREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWGetTeamInfoREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWGetTeamInfoREQ req, int callback) {
		logger.info("account={}, missionId={}, teamId={}", 
				req.getAccount(), req.getMissionId(), req.getTeamId());
		service.getTeamInfo(remoteNode, req, callback);
	}
}
