package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWGetTeamInfoRES;

@ActionAnnotation(actionClass = GWGetTeamInfoRESAction.class, messageClass = GWGetTeamInfoRES.class, serviceClass = TeamService.class)
public class GWGetTeamInfoRESAction extends SSProtobufMessageHandler<TeamService, GWGetTeamInfoRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWGetTeamInfoRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWGetTeamInfoRES res, int callback) {
		logger.info("account={}, teamInfosList={} ", res.getAccount(), res.getTeamInfo().toString());
		service.getTeamInfoResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWGetTeamInfoRES message)
	{
		return remoteNode.getAccountID();
	}
}
