package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWModifyTeamRES;

@ActionAnnotation(actionClass = GWModifyTeamRESAction.class, messageClass = GWModifyTeamRES.class, serviceClass = TeamService.class)
public class GWModifyTeamRESAction extends SSProtobufMessageHandler<TeamService, GWModifyTeamRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWModifyTeamRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWModifyTeamRES res,
			int callback) {
		logger.info("GWChangeTeamPlayerStatusRESAction handleMessage account={}, result={} ",
				res.getAccount(), res.getResult());
		service.modifyTeamResult(res);
	}

}
