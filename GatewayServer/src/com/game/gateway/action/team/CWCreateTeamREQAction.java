package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWCreateTeamREQ;

@ActionAnnotation(actionClass = CWCreateTeamREQAction.class, messageClass = CWCreateTeamREQ.class, serviceClass = TeamService.class)
public class CWCreateTeamREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWCreateTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWCreateTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWCreateTeamREQ req, int callback) {
		logger.info("account={}, missionid={}, password={}", 
				req.getAccount(), req.getMissionId(), req.getPassword());
		service.createTeam(remoteNode, req, callback);
	}
}
