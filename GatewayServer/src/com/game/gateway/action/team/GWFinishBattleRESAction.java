package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWFinishBattleRES;

@ActionAnnotation(actionClass = GWFinishBattleRESAction.class, messageClass = GWFinishBattleRES.class, serviceClass = TeamService.class)
public class GWFinishBattleRESAction extends SSProtobufMessageHandler<TeamService, GWFinishBattleRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWFinishBattleRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWFinishBattleRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getMissionInfo().toString());
		service.finishBattle(res);
	}
}
