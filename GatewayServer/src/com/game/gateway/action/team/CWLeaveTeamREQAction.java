package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWLeaveTeamREQ;

@ActionAnnotation(actionClass = CWLeaveTeamREQAction.class, messageClass = CWLeaveTeamREQ.class, serviceClass = TeamService.class)
public class CWLeaveTeamREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWLeaveTeamREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWLeaveTeamREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWLeaveTeamREQ req, int callback) {
		logger.info("account={}, missionId={}, teamId={}", req.getAccount(), req.getMissionId(), req.getTeamId());
		service.leaveTeam(remoteNode, req, callback);
	}
}
