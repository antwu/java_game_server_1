package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWCreateTeamRES;

@ActionAnnotation(actionClass = GWCreateTeamRESAction.class, messageClass = GWCreateTeamRES.class, serviceClass = TeamService.class)
public class GWCreateTeamRESAction extends SSProtobufMessageHandler<TeamService, GWCreateTeamRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWCreateTeamRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWCreateTeamRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.createTeamResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWCreateTeamRES message)
	{
		return message.getAccount();
	}
}
