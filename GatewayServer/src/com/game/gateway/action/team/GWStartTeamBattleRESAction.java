package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWStartTeamBattleRES;

@ActionAnnotation(actionClass = GWStartTeamBattleRESAction.class, messageClass = GWStartTeamBattleRES.class, serviceClass = TeamService.class)
public class GWStartTeamBattleRESAction extends SSProtobufMessageHandler<TeamService, GWStartTeamBattleRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWStartTeamBattleRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWStartTeamBattleRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.startTeamBattleResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWStartTeamBattleRES message)
	{
		return message.getAccount();
	}
}
