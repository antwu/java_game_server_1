package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWStartSingleBattleRES;

@ActionAnnotation(actionClass = GWStartSingleBattleRESAction.class, messageClass = GWStartSingleBattleRES.class, serviceClass = TeamService.class)
public class GWStartSingleBattleRESAction extends SSProtobufMessageHandler<TeamService, GWStartSingleBattleRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWStartSingleBattleRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWStartSingleBattleRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.startSingleBattleResult(res);
	}
}
