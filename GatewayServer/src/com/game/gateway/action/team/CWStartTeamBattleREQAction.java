package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWStartTeamBattleREQ;

@ActionAnnotation(actionClass = CWStartTeamBattleREQAction.class, messageClass = CWStartTeamBattleREQ.class, serviceClass = TeamService.class)
public class CWStartTeamBattleREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWStartTeamBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWStartTeamBattleREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWStartTeamBattleREQ req, int callback) {
		logger.info("account={}, missionId={}, teamId={}", req.getAccount(), req.getMissionId(), req.getTeamId());
		service.startTeamBattle(remoteNode, req, callback);
	}
}
