package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWJoinTeamRES;

@ActionAnnotation(actionClass = GWJoinTeamRESAction.class, messageClass = GWJoinTeamRES.class, serviceClass = TeamService.class)
public class GWJoinTeamRESAction extends SSProtobufMessageHandler<TeamService, GWJoinTeamRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWJoinTeamRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWJoinTeamRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.joinTeamResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWJoinTeamRES message)
	{
		return message.getAccount();
	}
}
