package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWQuickJoinTeamRES;

@ActionAnnotation(actionClass = GWQuickJoinTeamRESAction.class, messageClass = GWQuickJoinTeamRES.class, serviceClass = TeamService.class)
public class GWQuickJoinTeamRESAction extends SSProtobufMessageHandler<TeamService, GWQuickJoinTeamRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWQuickJoinTeamRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWQuickJoinTeamRES res,
			int callback) {
		logger.info("GWChangeTeamPlayerStatusRESAction handleMessage account={}, result={} ",
				res.getAccount(), res.getResult());
		service.quickJoinTeamResult(res);
	}

}
