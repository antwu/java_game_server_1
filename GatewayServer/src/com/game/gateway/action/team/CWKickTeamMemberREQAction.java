package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWKickTeamMemberREQ;

@ActionAnnotation(actionClass = CWKickTeamMemberREQAction.class, messageClass = CWKickTeamMemberREQ.class, serviceClass = TeamService.class)
public class CWKickTeamMemberREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWKickTeamMemberREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWKickTeamMemberREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWKickTeamMemberREQ req, int callback) {
		logger.info("account={}, missionId={}, teamId={}, kickaccount={}", 
				req.getAccount(), req.getMissionId(), req.getTeamId(), req.getKickAccount());
		service.kickTeamMember(remoteNode, req, callback);
	}
}
