package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWStartSingleBattleREQ;

@ActionAnnotation(actionClass = CWStartSingleBattleREQAction.class, messageClass = CWStartSingleBattleREQ.class, serviceClass = TeamService.class)
public class CWStartSingleBattleREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWStartSingleBattleREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWStartSingleBattleREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWStartSingleBattleREQ req, int callback) {
		logger.info("account={}, missionId={}", req.getAccount(), req.getMissionId());
		service.startSingleBattle(remoteNode, req, callback);
	}
}
