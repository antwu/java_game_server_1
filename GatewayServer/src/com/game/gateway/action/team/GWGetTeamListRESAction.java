package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWGetTeamListRES;

@ActionAnnotation(actionClass = GWGetTeamListRESAction.class, messageClass = GWGetTeamListRES.class, serviceClass = TeamService.class)
public class GWGetTeamListRESAction extends SSProtobufMessageHandler<TeamService, GWGetTeamListRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWGetTeamListRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWGetTeamListRES res, int callback) {
		logger.info("account={}, teamInfosList={} ", res.getAccount(), res.getTeamInfosList().toString());
		service.getTeamListResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWGetTeamListRES message)
	{
		return message.getAccount();
	}
}
