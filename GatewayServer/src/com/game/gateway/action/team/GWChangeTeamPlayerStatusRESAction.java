package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWChangeTeamPlayerStatusRES;

@ActionAnnotation(actionClass = GWChangeTeamPlayerStatusRESAction.class, messageClass = GWChangeTeamPlayerStatusRES.class, serviceClass = TeamService.class)
public class GWChangeTeamPlayerStatusRESAction extends SSProtobufMessageHandler<TeamService, GWChangeTeamPlayerStatusRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWChangeTeamPlayerStatusRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWChangeTeamPlayerStatusRES res,
			int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.changeTeamPlayerStatusResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWChangeTeamPlayerStatusRES message)
	{
		return message.getAccount();
	}
}
