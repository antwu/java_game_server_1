package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.team.TeamService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.team.TeamProtoBuf.CWGetTeamListREQ;

@ActionAnnotation(actionClass = CWGetTeamListREQAction.class, messageClass = CWGetTeamListREQ.class, serviceClass = TeamService.class)
public class CWGetTeamListREQAction extends CSGatewayServerProtobufMessageHandler<TeamService, CWGetTeamListREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWGetTeamListREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, CWGetTeamListREQ req, int callback) {
		logger.info("account={}, missionid={}",	req.getAccount(), req.getMissionId());
		service.getTeamList(remoteNode, req, callback);
	}
}
