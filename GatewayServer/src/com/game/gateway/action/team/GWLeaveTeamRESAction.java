package com.game.gateway.action.team;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.team.TeamService;
import com.game.message.proto.team.TeamProtoBuf.GWLeaveTeamRES;

@ActionAnnotation(actionClass = GWLeaveTeamRESAction.class, messageClass = GWLeaveTeamRES.class, serviceClass = TeamService.class)
public class GWLeaveTeamRESAction extends SSProtobufMessageHandler<TeamService, GWLeaveTeamRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWLeaveTeamRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, TeamService service, GWLeaveTeamRES res, int callback) {
		logger.info("account={}, result={} ", res.getAccount(), res.getResult());
		service.leaveTeamResult(res);
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWLeaveTeamRES message)
	{
		return message.getAccount();
	}
}
