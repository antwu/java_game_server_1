package com.game.gateway.action.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.player.PlayerProtoBuf.CWChangePlayerInfoREQ;

@ActionAnnotation(actionClass = CWChangePlayerInfoREQAction.class, messageClass = CWChangePlayerInfoREQ.class, serviceClass = AccountService.class)
public class CWChangePlayerInfoREQAction extends CSGatewayServerProtobufMessageHandler<AccountService, CWChangePlayerInfoREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWChangePlayerInfoREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, CWChangePlayerInfoREQ message,
			int callback) {
		logger.info("account={}, name={}, appearance={}, sex={}", message.getAccount(), message.getName(), message.getAppearance(), message.getSex());
		service.changePlayerInfo(remoteNode, message, callback);
	}
	
	
}
