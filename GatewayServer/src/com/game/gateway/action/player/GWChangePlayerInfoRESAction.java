package com.game.gateway.action.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.player.PlayerProtoBuf.GWChangePlayerInfoRES;

@ActionAnnotation(actionClass = GWChangePlayerInfoRESAction.class, messageClass = GWChangePlayerInfoRES.class, serviceClass = AccountService.class)
public class GWChangePlayerInfoRESAction extends SSProtobufMessageHandler<AccountService, GWChangePlayerInfoRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWChangePlayerInfoRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWChangePlayerInfoRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changePlayerInfoResult(message);
	}
}
