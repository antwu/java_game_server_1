package com.game.gateway.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.account.AccountService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.account.AccountProtoBuf.CWAccountLoginREQ;

@ActionAnnotation(actionClass = CWAccountLoginREQAction.class, messageClass = CWAccountLoginREQ.class, serviceClass = AccountService.class)
public class CWAccountLoginREQAction extends CSGatewayServerProtobufMessageHandler<AccountService, CWAccountLoginREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWAccountLoginREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, CWAccountLoginREQ message, int callback)
	{
		logger.info("account={}, token={}, platform={}", message.getAccount(), message.getToken(), message.getPlatform());
		service.login(remoteNode, message, callback);
	}
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, CWAccountLoginREQ message)
    {
	    return message.getAccount() + GlobalConstants.ACCOUNTID_SPLIT + message.getPlatform();
    }
}
