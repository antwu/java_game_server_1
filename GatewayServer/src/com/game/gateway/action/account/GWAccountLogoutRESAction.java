package com.game.gateway.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.core.service.ServiceContainer;
import com.game.core.session.SessionService;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.account.AccountProtoBuf.GWAccountLogoutRES;
import com.game.message.proto.account.AccountProtoBuf.WCAccountLogoutRES;
import com.game.message.protocol.ProtocolsConfig;

@ActionAnnotation(actionClass = GWAccountLogoutRESAction.class, messageClass = GWAccountLogoutRES.class, serviceClass = AccountService.class)
public class GWAccountLogoutRESAction extends SSProtobufMessageHandler<AccountService, GWAccountLogoutRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWAccountLogoutRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWAccountLogoutRES message, int callback)
	{
		logger.info("sessionID={}, result={} ", message.getSessionID(), message.getResult());
		WCAccountLogoutRES.Builder msg = WCAccountLogoutRES.newBuilder();
		SessionService ss = ServiceContainer.getInstance().getPublicService(SessionService.class);
		remoteNode = ss.getRemoteNodeBySessionID(message.getSessionID());
		if (remoteNode == null)
		{
			logger.info("AccountID:{} , remote Node is null ", message.getAccountID());
			return;
		}
			
		if(message.getResult() == ProtocolsConfig.GW_LOGOUT_FORBID)
			msg.setResult(ProtocolsConfig.LOGOUT_FORBID);
		else if(message.getResult() == ProtocolsConfig.GW_LOGOUT_KICKOFF)
			msg.setResult(ProtocolsConfig.LOGOUT_KICKOFF);
		
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, msg.build());
		ss.RemoveSession(remoteNode);
		// 收到game通知的踢下线消息，不再想game服务器发送掉线信息。
		remoteNode.setAccountID(null);
		remoteNode.setID(0);
		remoteNode.close();
	}
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWAccountLogoutRES message)
    {
	    return message.getAccountID();
    }
}
