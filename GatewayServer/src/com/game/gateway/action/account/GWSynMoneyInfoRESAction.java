package com.game.gateway.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.account.AccountProtoBuf.GWSynMoneyInfoRES;

@ActionAnnotation(actionClass = GWSynMoneyInfoRESAction.class, messageClass = GWSynMoneyInfoRES.class, serviceClass = AccountService.class)
public class GWSynMoneyInfoRESAction extends SSProtobufMessageHandler<AccountService, GWSynMoneyInfoRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWSynMoneyInfoRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWSynMoneyInfoRES res, int callback) {
		logger.info("account={}, getExperience={} , getGoldCoin={} , getDiamonds={} , getGiftCert={} , getFeats={} ",
				res.getAccount(), res.getExperience(), res.getGoldCoin(),res.getDiamonds(),res.getGiftCert(),res.getFeats());
		service.pushSynMoneyInfoResult(res);
	}

}
