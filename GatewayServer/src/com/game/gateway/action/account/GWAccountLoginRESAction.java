package com.game.gateway.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.account.AccountProtoBuf.GWAccountLoginRES;

@ActionAnnotation(actionClass = GWAccountLoginRESAction.class, messageClass = GWAccountLoginRES.class, serviceClass = AccountService.class)
public class GWAccountLoginRESAction extends SSProtobufMessageHandler<AccountService, GWAccountLoginRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWAccountLoginRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWAccountLoginRES message, int callback)
	{
		logger.info("account={}, result={} ",message.getAccount(), message.getResult());
		service.gameLoginResult(message);
	}
	
	@Override
    public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWAccountLoginRES message)
    {
	    return message.getAccountID();
    }
}
