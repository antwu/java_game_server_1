package com.game.gateway.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.account.AccountProtoBuf.GWAccountModifySYN;
import com.game.message.proto.account.AccountProtoBuf.WCAccountModifySYN;

@ActionAnnotation(actionClass = GWAccountModifySYNAction.class, messageClass = GWAccountModifySYN.class, serviceClass = AccountService.class)
public class GWAccountModifySYNAction extends SSProtobufMessageHandler<AccountService, GWAccountModifySYN>
{
	final static Logger logger = LoggerFactory.getLogger(GWAccountModifySYNAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWAccountModifySYN message, int callback)
	{
		logger.info("GateWay Acc Modify accountID={}, roomCard={}", message.getAccountID(), message.getRoomCard());
		WCAccountModifySYN.Builder resBuilder = WCAccountModifySYN.newBuilder();
		resBuilder.setRoomCard(message.getRoomCard());
		Transmitter.getInstance().write(service.getRemoteNode(message.getAccountID()), GlobalConstants.DEFAULT_CALLBACK, resBuilder.build());
	}
	
	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWAccountModifySYN message)
	{
		return message.getAccountID();
	}
}
