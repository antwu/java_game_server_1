package com.game.gateway.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.room.RoomService;
import com.game.gateway.service.server.ServerService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.room.RoomProtoBuf.CWRoomEnterREQ;
import com.game.message.proto.room.RoomProtoBuf.WGRoomEnterREQ;

@ActionAnnotation(actionClass = CWRoomEnterREQAction.class, messageClass = CWRoomEnterREQ.class, serviceClass = RoomService.class)
public class CWRoomEnterREQAction extends CSGatewayServerProtobufMessageHandler<RoomService, CWRoomEnterREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWRoomEnterREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, CWRoomEnterREQ message, int callback)
	{
		logger.info("room={} accountID={}", message.getRoom(), remoteNode.getAccountID());

		if (message.getRoom() > 0)
		{
			WGRoomEnterREQ req = service.enterRoom(remoteNode, message);
			ServerService server = ServiceContainer.getInstance().getPublicService(ServerService.class);
			Transmitter.getInstance().write(server.getGameNode(), callback, req);
		}
	}
}
