package com.game.gateway.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.room.RoomProtoBuf.GWContinueBattleSYN;
import com.game.message.proto.room.RoomProtoBuf.WCContinueBattleSYN;

@ActionAnnotation(actionClass = GWContinueBattleSYNAction.class, messageClass = GWContinueBattleSYN.class, serviceClass = AccountService.class)
public class GWContinueBattleSYNAction extends SSProtobufMessageHandler<AccountService, GWContinueBattleSYN>
{
	final static Logger logger = LoggerFactory.getLogger(GWContinueBattleSYNAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWContinueBattleSYN message, int callback)
	{
		logger.info("getResult={} getRoomID={}", message.getResult(), message.getRoomID());
		remoteNode = service.getRemoteNode(message.getAccountID());
		WCContinueBattleSYN.Builder builder = WCContinueBattleSYN.newBuilder();
		builder.setResult(message.getResult());
		builder.setRoomID(message.getRoomID());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());		
	}
	
}
