package com.game.gateway.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.room.RoomService;
import com.game.gateway.service.server.ServerService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.room.RoomProtoBuf.CWRoomDestoryREQ;
import com.game.message.proto.room.RoomProtoBuf.WGRoomDestoryREQ;

@ActionAnnotation(actionClass = CWRoomDestoryREQAction.class, messageClass = CWRoomDestoryREQ.class, serviceClass = RoomService.class)
public class CWRoomDestoryREQAction extends CSGatewayServerProtobufMessageHandler<RoomService, CWRoomDestoryREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWRoomDestoryREQAction.class);
	
	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, CWRoomDestoryREQ message, int callback)
	{
		logger.info("accountID={}, roomID={}", remoteNode.getAccountID(), remoteNode.getRoomID());
		WGRoomDestoryREQ req = service.destoryRoom(remoteNode);
		ServerService server = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(server.getGameNode(), callback, req);
	}
}
