package com.game.gateway.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.room.RoomService;
import com.game.gateway.service.server.ServerService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.room.RoomProtoBuf.CWRoomCreateREQ;
import com.game.message.proto.room.RoomProtoBuf.WGRoomCreateREQ;

@ActionAnnotation(actionClass = CWRoomCreateREQAction.class, messageClass = CWRoomCreateREQ.class, serviceClass = RoomService.class)
public class CWRoomCreateREQAction extends CSGatewayServerProtobufMessageHandler<RoomService, CWRoomCreateREQ>
{
	final static Logger logger = LoggerFactory.getLogger(CWRoomCreateREQAction.class);
	
	@Override
	public void handleMessage(RemoteNode remoteNode, RoomService service, CWRoomCreateREQ message, int callback)
	{
		logger.info("accountID={}, count={} type={}",remoteNode.getAccountID(), message.getCount(), message.getType());
		
		WGRoomCreateREQ req = service.createRoom(remoteNode, message);
		
		ServerService server = ServiceContainer.getInstance().getPublicService(ServerService.class);
		Transmitter.getInstance().write(server.getGameNode(), callback, req);
	}
}
