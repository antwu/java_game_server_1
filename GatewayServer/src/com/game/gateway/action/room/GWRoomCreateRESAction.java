package com.game.gateway.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.room.RoomProtoBuf.GWRoomCreateRES;
import com.game.message.proto.room.RoomProtoBuf.WCRoomCreateRES;

@ActionAnnotation(actionClass = GWRoomCreateRESAction.class, messageClass = GWRoomCreateRES.class, serviceClass = AccountService.class)
public class GWRoomCreateRESAction extends SSProtobufMessageHandler<AccountService, GWRoomCreateRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWRoomCreateRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWRoomCreateRES message, int callback)
	{
		logger.info("accountID={} roomID={}", message.getAccountID(), message.getRoomID());
		WCRoomCreateRES.Builder msg = WCRoomCreateRES.newBuilder();
		msg.setRoom(message.getRoomID());
		msg.setResult(message.getResult());
		remoteNode = service.getRemoteNode(message.getAccountID());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, msg.build());
	}

	@Override
	public Object getMessageKey(RemoteNode remoteNode, int protocoliD, GWRoomCreateRES message)
	{
		return message.getAccountID();
	}
}
