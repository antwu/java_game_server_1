package com.game.gateway.action.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.Transmitter;
import com.game.common.constants.GlobalConstants;
import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.account.AccountService;
import com.game.message.proto.room.RoomProtoBuf.GWContinueBattleRES;
import com.game.message.proto.room.RoomProtoBuf.WCContinueBattleRES;

@ActionAnnotation(actionClass = GWContinueBattleRESAction.class, messageClass = GWContinueBattleRES.class, serviceClass = AccountService.class)
public class GWContinueBattleRESAction extends SSProtobufMessageHandler<AccountService, GWContinueBattleRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWContinueBattleRESAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, AccountService service, GWContinueBattleRES message, int callback)
	{
		logger.info("getResult={} getRoomID={}", message.getResult(), message.getRoomID());
		remoteNode = service.getRemoteNode(message.getAccountID());
		WCContinueBattleRES.Builder builder = WCContinueBattleRES.newBuilder();
		builder.setResult(message.getResult());
		builder.setRoomID(message.getRoomID());
		Transmitter.getInstance().write(remoteNode, GlobalConstants.DEFAULT_CALLBACK, builder.build());
	}
}
