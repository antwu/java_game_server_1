package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeItemNodeUnlockREQ;

@ActionAnnotation(actionClass = CWChangeItemNodeUnlockREQAction.class, messageClass = CWChangeItemNodeUnlockREQ.class, serviceClass = WeaponService.class)
public class CWChangeItemNodeUnlockREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeItemNodeUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeItemNodeUnlockREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeItemNodeUnlockREQ message,
			int callback) {
		logger.info("account={}, getWeaponId={}, getMainId={}, getItemId={}, getStatus={}", message.getAccount(),message.getWeaponId(),message.getItemId(),message.getMainId(),message.getStatus());
		service.changeItemNodeUnlock(remoteNode, message, callback);
	}

}
