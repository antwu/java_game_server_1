package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeFashionSetREQ;

@ActionAnnotation(actionClass = CWChangeFashionSetREQAction.class, messageClass = CWChangeFashionSetREQ.class, serviceClass = WeaponService.class)
public class CWChangeFashionSetREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeFashionSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeFashionSetREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeFashionSetREQ message,
			int callback) {
		logger.info("account={}, getOldFashionId={}, getNewFashionId={}", message.getAccount(), message.getOldFashionId(), message.getNewFashionId());
		service.changeFashionSet(remoteNode, message, callback);
	}

}
