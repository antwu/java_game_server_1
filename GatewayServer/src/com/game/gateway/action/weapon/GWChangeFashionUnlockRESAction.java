package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionUnlockRES;

@ActionAnnotation(actionClass = GWChangeFashionUnlockRESAction.class, messageClass = GWChangeFashionUnlockRES.class, serviceClass = WeaponService.class)
public class GWChangeFashionUnlockRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeFashionUnlockRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeFashionUnlockRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeFashionUnlockRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeFashionUnlockResult(message);
	}

}
