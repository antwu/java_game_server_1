package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeColourUnlockREQ;

@ActionAnnotation(actionClass = CWChangeColourUnlockREQAction.class, messageClass = CWChangeColourUnlockREQ.class, serviceClass = WeaponService.class)
public class CWChangeColourUnlockREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeColourUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeColourUnlockREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeColourUnlockREQ message,
			int callback) {
		//logger.info("account={}, fashionId={}, unlockColourList={}", message.getAccount(), message.getFashionId(), message.getUnlockColourList().toString());
		service.changeColourUnlock(remoteNode, message, callback);
	}

}
