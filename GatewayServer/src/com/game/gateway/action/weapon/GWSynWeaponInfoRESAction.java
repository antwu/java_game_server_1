package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWSynWeaponInfoRES;

@ActionAnnotation(actionClass = GWSynWeaponInfoRESAction.class, messageClass = GWSynWeaponInfoRES.class, serviceClass = WeaponService.class)
public class GWSynWeaponInfoRESAction extends SSProtobufMessageHandler<WeaponService, GWSynWeaponInfoRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWSynWeaponInfoRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWSynWeaponInfoRES res, int callback) {
		logger.info("account={}, itemInfo={} ", res.getAccount(), res.getWeaponInfo().toString());
		service.pushSynWeaponInfo(res);
	}

}
