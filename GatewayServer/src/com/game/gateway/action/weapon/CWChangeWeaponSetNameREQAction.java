package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeWeaponSetNameREQ;

@ActionAnnotation(actionClass = CWChangeWeaponSetNameREQAction.class, messageClass = CWChangeWeaponSetNameREQ.class, serviceClass = WeaponService.class)
public class CWChangeWeaponSetNameREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeWeaponSetNameREQ> {
	final static Logger logger = LoggerFactory.getLogger(CWChangeWeaponSetNameREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeWeaponSetNameREQ message,
			int callback) {
		logger.info("account={}, changeSet={}, name={}", message.getAccount(), message.getChangeWeaponSet(), message.getNewName());
		service.changeWeaponSetName(remoteNode, message, callback);
	}

}
