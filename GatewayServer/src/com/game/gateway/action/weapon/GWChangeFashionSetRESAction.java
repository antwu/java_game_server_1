package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeFashionSetRES;

@ActionAnnotation(actionClass = GWChangeFashionSetRESAction.class, messageClass = GWChangeFashionSetRES.class, serviceClass = WeaponService.class)
public class GWChangeFashionSetRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeFashionSetRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeFashionSetRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeFashionSetRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeFashionSetResult(message);
	}

}
