package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeWeaponSetREQ;

@ActionAnnotation(actionClass = CWChangeWeaponSetREQAction.class, messageClass = CWChangeWeaponSetREQ.class, serviceClass = WeaponService.class)
public class CWChangeWeaponSetREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeWeaponSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeWeaponSetREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeWeaponSetREQ message,
			int callback) {
		logger.info("account={}, newweapon={}, set={}, position={}", message.getAccount(), message.getNewWeapon(), message.getChangeWeaponSet(), message.getChangePosition());
		service.changeWeaponSet(remoteNode, message, callback);
	}

}
