package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeBuncherUnlockRES;

@ActionAnnotation(actionClass = GWChangeBuncherUnlockRESAction.class, messageClass = GWChangeBuncherUnlockRES.class, serviceClass = WeaponService.class)
public class GWChangeBuncherUnlockRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeBuncherUnlockRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeBuncherUnlockRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeBuncherUnlockRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeBuncherUnlockResult(message);
	}

}
