package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeColourSetREQ;

@ActionAnnotation(actionClass = CWChangeColourSetREQAction.class, messageClass = CWChangeColourSetREQ.class, serviceClass = WeaponService.class)
public class CWChangeColourSetREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeColourSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeColourSetREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeColourSetREQ message,
			int callback) {
		//logger.info("account={}, fashionId={}, newcolour={}, position={}", message.getAccount(), message.getFashionId(), message.getNewColour(), message.getChangePosition());
		service.changeColourSet(remoteNode, message, callback);
	}

}
