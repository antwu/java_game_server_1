package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeCurrentWeaponSetRES;

@ActionAnnotation(actionClass = GWChangeCurrentWeaponSetRESAction.class, messageClass = GWChangeCurrentWeaponSetRES.class, serviceClass = WeaponService.class)
public class GWChangeCurrentWeaponSetRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeCurrentWeaponSetRES> {
	final static Logger logger = LoggerFactory.getLogger(GWChangeCurrentWeaponSetRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeCurrentWeaponSetRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeCurrentWeaponSetResult(message);
	}

}
