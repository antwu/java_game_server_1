package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeWeaponSetRES;

@ActionAnnotation(actionClass = GWChangeWeaponSetRESAction.class, messageClass = GWChangeWeaponSetRES.class, serviceClass = WeaponService.class)
public class GWChangeWeaponSetRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeWeaponSetRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeWeaponSetRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeWeaponSetRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeWeaponSetResult(message);
	}

}
