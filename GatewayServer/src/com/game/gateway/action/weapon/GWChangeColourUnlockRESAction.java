package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourUnlockRES;

@ActionAnnotation(actionClass = GWChangeColourUnlockRESAction.class, messageClass = GWChangeColourUnlockRES.class, serviceClass = WeaponService.class)
public class GWChangeColourUnlockRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeColourUnlockRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeColourUnlockRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeColourUnlockRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeColourUnlockResult(message);
	}

}
