package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeBuncherUnlockREQ;

@ActionAnnotation(actionClass = CWChangeBuncherUnlockREQAction.class, messageClass = CWChangeBuncherUnlockREQ.class, serviceClass = WeaponService.class)
public class CWChangeBuncherUnlockREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeBuncherUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeBuncherUnlockREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeBuncherUnlockREQ message,
			int callback) {
		logger.info("account={}, getUnlocked={}, weaponset={}", message.getAccount(), message.getUnlocked(), message.getChangeWeaponSet());
		service.changeBuncherUnlock(remoteNode, message, callback);
	}

}
