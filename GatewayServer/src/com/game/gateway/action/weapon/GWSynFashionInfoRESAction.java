package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.core.net.handler.SSProtobufMessageHandler;
import com.game.gateway.service.weapon.WeaponService;
import com.game.message.proto.weapon.WeaponProtoBuf.GWSynFashionInfoRES;

@ActionAnnotation(actionClass = GWSynFashionInfoRESAction.class, messageClass = GWSynFashionInfoRES.class, serviceClass = WeaponService.class)
public class GWSynFashionInfoRESAction extends SSProtobufMessageHandler<WeaponService, GWSynFashionInfoRES>
{
	final static Logger logger = LoggerFactory.getLogger(GWSynFashionInfoRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWSynFashionInfoRES res, int callback) {
		logger.info("account={}, itemInfo={} ", res.getAccount(), res.getFashionInfo().toString());
		service.pushSynFashionInfo(res);
	}

}
