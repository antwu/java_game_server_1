package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeColourSetRES;

@ActionAnnotation(actionClass = GWChangeColourSetRESAction.class, messageClass = GWChangeColourSetRES.class, serviceClass = WeaponService.class)
public class GWChangeColourSetRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeColourSetRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeColourSetRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeColourSetRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeColourSetResult(message);
	}

}
