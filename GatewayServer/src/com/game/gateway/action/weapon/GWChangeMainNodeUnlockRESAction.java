package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeMainNodeUnlockRES;

@ActionAnnotation(actionClass = GWChangeMainNodeUnlockRESAction.class, messageClass = GWChangeMainNodeUnlockRES.class, serviceClass = WeaponService.class)
public class GWChangeMainNodeUnlockRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeMainNodeUnlockRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeMainNodeUnlockRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeMainNodeUnlockRES message,
			int callback) {
		logger.info("account={}, result={} weaponInfos={}", message.getAccount(), message.getResult(), message.getWeaponInfos());
		service.changeMainNodeUnlockResult(message);
	}

}
