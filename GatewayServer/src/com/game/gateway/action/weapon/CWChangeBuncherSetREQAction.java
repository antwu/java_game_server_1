package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeBuncherSetREQ;

@ActionAnnotation(actionClass = CWChangeBuncherSetREQAction.class, messageClass = CWChangeBuncherSetREQ.class, serviceClass = WeaponService.class)
public class CWChangeBuncherSetREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeBuncherSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeBuncherSetREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeBuncherSetREQ message,
			int callback) {
		logger.info("account={}, oldBuncherId={}, newBuncherId={}, weaponset={}", message.getAccount(), message.getOldBuncherId(), message.getNewBuncherId(), message.getChangeWeaponSet());
		service.changeBuncherSet(remoteNode, message, callback);
	}

}
