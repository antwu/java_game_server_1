package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeBuncherSetRES;

@ActionAnnotation(actionClass = GWChangeBuncherSetRESAction.class, messageClass = GWChangeBuncherSetRES.class, serviceClass = WeaponService.class)
public class GWChangeBuncherSetRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeBuncherSetRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeBuncherSetRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeBuncherSetRES message,
			int callback) {
		logger.info("account={}, result={}", message.getAccount(), message.getResult());
		service.changeBuncherSetResult(message);
	}

}
