package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.GWChangeItemNodeUnlockRES;

@ActionAnnotation(actionClass = GWChangeItemNodeUnlockRESAction.class, messageClass = GWChangeItemNodeUnlockRES.class, serviceClass = WeaponService.class)
public class GWChangeItemNodeUnlockRESAction extends CSGatewayServerProtobufMessageHandler<WeaponService, GWChangeItemNodeUnlockRES>{
	final static Logger logger = LoggerFactory.getLogger(GWChangeItemNodeUnlockRESAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, GWChangeItemNodeUnlockRES message,
			int callback) {
		logger.info("account={}, result={} weaponInfos={}", message.getAccount(), message.getResult(), message.getWeaponInfos());
		service.changeItemNodeUnlockResult(message);
	}

}
