package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeFashionUnlockREQ;

@ActionAnnotation(actionClass = CWChangeFashionUnlockREQAction.class, messageClass = CWChangeFashionUnlockREQ.class, serviceClass = WeaponService.class)
public class CWChangeFashionUnlockREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeFashionUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeFashionUnlockREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeFashionUnlockREQ message,
			int callback) {
		logger.info("account={}, getUnlocked={}, getFashionId={}", message.getAccount(), message.getUnlocked(), message.getFashionId());
		service.changeFashionUnlock(remoteNode, message, callback);
	}

}
