package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeMainNodeUnlockREQ;

@ActionAnnotation(actionClass = CWChangeMainNodeUnlockREQAction.class, messageClass = CWChangeMainNodeUnlockREQ.class, serviceClass = WeaponService.class)
public class CWChangeMainNodeUnlockREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeMainNodeUnlockREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeMainNodeUnlockREQAction.class);
	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeMainNodeUnlockREQ message,
			int callback) {
		logger.info("account={}, getWeaponId={}, getMainId={}, getStatus={}", message.getAccount(),message.getWeaponId(),message.getMainId(),message.getStatus());
		service.changeMainNodeUnlock(remoteNode, message, callback);
	}

}
