package com.game.gateway.action.weapon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.core.net.common.RemoteNode;
import com.game.core.net.handler.ActionAnnotation;
import com.game.gateway.service.weapon.WeaponService;
import com.game.gateway.start.CSGatewayServerProtobufMessageHandler;
import com.game.message.proto.weapon.WeaponProtoBuf.CWChangeCurrentWeaponSetREQ;

@ActionAnnotation(actionClass = CWChangeCurrentWeaponSetREQAction.class, messageClass = CWChangeCurrentWeaponSetREQ.class, serviceClass = WeaponService.class)
public class CWChangeCurrentWeaponSetREQAction extends CSGatewayServerProtobufMessageHandler<WeaponService, CWChangeCurrentWeaponSetREQ>{

	final static Logger logger = LoggerFactory.getLogger(CWChangeWeaponSetREQAction.class);

	@Override
	public void handleMessage(RemoteNode remoteNode, WeaponService service, CWChangeCurrentWeaponSetREQ message,
			int callback) {
		logger.info("account={}, changeSet={}", message.getAccount(), message.getChangeWeaponSet());
		service.changeCurrentWeaponSet(remoteNode, message, callback);
	}

}
