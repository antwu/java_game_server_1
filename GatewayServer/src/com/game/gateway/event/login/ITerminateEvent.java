package com.game.gateway.event.login;
import com.kodgames.core.event.EventResult;
import com.kodgames.core.event.IEvent;
import com.game.core.net.common.RemoteNode;

public interface ITerminateEvent extends IEvent
{
	public EventResult onTerminate(RemoteNode remoteNode);
}
