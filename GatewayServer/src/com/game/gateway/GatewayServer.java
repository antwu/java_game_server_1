package com.game.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.game.common.config.BaseLocalConfigInitializer;
import com.game.common.config.BaseServerConfig;
import com.game.core.service.ServiceContainer;
import com.game.gateway.service.server.ServerService;
import com.game.gateway.start.EventInitializer;
import com.game.gateway.start.NetInitializer;

public class GatewayServer {
	static private Logger logger = LoggerFactory.getLogger(GatewayServer.class);

	public static void main(String[] args) {
		BaseLocalConfigInitializer baseLocalConfigIntializer = new BaseLocalConfigInitializer();
		BaseServerConfig config = new BaseServerConfig();
		try {
			baseLocalConfigIntializer.init(config);
		} catch (Exception e) {
			logger.error("Failed to load local configuration Exception err={}", e.toString());
			return;
		}

		NetInitializer netInitializer = new NetInitializer();
		try {
			netInitializer.init();
			// task initial
			if (EventInitializer.getInstance().init() != 0) {
				logger.error("initial task failed.");
				System.exit(-3);
			}
		} catch (Exception e) {
			logger.error("Failed to NetInitializer Exception err={}", e.toString());
			return;
		}

		ServerService service = ServiceContainer.getInstance().getPublicService(ServerService.class);
		service.setNetInitializer(netInitializer);
		service.setServerConfig(config);
		netInitializer.connectToGame();
	}
}
