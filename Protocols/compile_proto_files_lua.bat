protoc -I protobuf --descriptor_set_out pb/account.pb protobuf/account.proto
protoc -I protobuf --descriptor_set_out pb/room.pb protobuf/room.proto
protoc -I protobuf --descriptor_set_out pb/marquee.pb protobuf/marquee.proto
protoc -I protobuf --descriptor_set_out pb/battle.pb protobuf/battle.proto
protoc -I protobuf --descriptor_set_out pb/chat.pb protobuf/chat.proto
protoc -I protobuf --proto_path=./ --descriptor_set_out pb/record.pb protobuf/record.proto
protoc -I protobuf --descriptor_set_out pb/heart.pb protobuf/heart.proto
protoc -I protobuf --descriptor_set_out pb/update.pb protobuf/update.proto
protoc -I protobuf --descriptor_set_out pb/encrypt.pb protobuf/encrypt.proto
protoc -I protobuf --descriptor_set_out pb/notice.pb protobuf/notice.proto
protoc -I protobuf --descriptor_set_out pb/contact.pb protobuf/contact.proto
protoc -I protobuf --descriptor_set_out pb/playerreward.pb protobuf/playerreward.proto
protoc -I protobuf --descriptor_set_out pb/email.pb protobuf/email.proto
protoc -I protobuf --descriptor_set_out pb/activity.pb protobuf/activity.proto

pause