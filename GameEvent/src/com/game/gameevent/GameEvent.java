﻿package com.game.gameevent;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kodgames.httpserver.HttpServer;

public class GameEvent
{
	private static Logger logger = LoggerFactory.getLogger(GameEvent.class);
	
    public static void main(String[] args)
    {
		Properties props = new Properties();
		HttpServer httpServer;
		HttpRequestListener httpRequestListener;
		try
		{
			InputStream in = GameEvent.class.getResourceAsStream("/config.properties");
	        props.load(in);
	        in.close();
		}
		catch (Exception e) 
		{
			logger.error("GameEvent GameEventHttpServer load config {}", e.toString());
			return;
		}
		int port = Integer.parseInt(props.getProperty("gameevent_port"));
		httpServer = new HttpServer();
		httpRequestListener = new HttpRequestListener();
		httpServer.setListener(httpRequestListener);
		httpServer.onStart(port);
		logger.info("GameEvent Start On Port {}", port);
    }
}